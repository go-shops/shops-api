FROM node:10
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .
COPY .env.example .env

RUN npm run-script build-prod && npm ci --only=production && rm -r ./src && mkdir -p /img

EXPOSE 3000
VOLUME ["/usr/src/app/logs", "/img"]

CMD [ "node", "./dist/app.js" ]