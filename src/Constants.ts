/**
 * Retrieve whitelisted origins for CORS requests
 */
function getOriginWhitelist(): string[] {
    if (!!process.env.ORIGINS) {
        return process.env.ORIGINS.split(",");
    } else {
        return process.env.NODE_ENV === "PRODUCTION"
            ? ["https://go-shops.fr", "https://www.go-shops.fr"]
            : ["https://dev.go-shops.fr", "https://www.dev.go-shops.fr",
            "http://shops.local:8080", "http://localhost:8080"];
    }
}

export default class Constants {
    public static PORT: number = parseInt(process.env.PORT, 10) || 3000;
    public static ORIGIN: string[] = getOriginWhitelist();

    public static DB_PORT: number = parseInt(process.env.DB_PORT, 10) || 3306;
    public static DB_HOST: string = process.env.DB_HOST || "localhost";
    public static DB_USER: string = process.env.DB_USER || "shops";
    public static DB_PASS: string = process.env.DB_PASS || "eipshops*";
    public static DB_NAME: string = process.env.DB_NAME || "shops_dev";

    public static LOG_LEVEL: string = process.env.LOG_LEVEL || "info";
    public static LOG_DIR: string = process.env.LOG_DIR || "logs/dev";
    public static NODE_ENV: string = process.env.NODE_ENV || "dev";

    public static ELASTIC_HOST: string = process.env.ELASTIC_HOST || "localhost:9200";
    public static ELASTIC_PRODUCTS_INDEX: string = process.env.ELASTIC_PRODUCTS_INDEX || "products";
    public static ELASTIC_PRODUCTS_TYPE: string = "product";
    public static ELASTIC_STORES_INDEX: string = process.env.ELASTIC_STORES_INDEX || "stores";
    public static ELASTIC_STORES_TYPE: string = "store";
    public static ELASTIC_USERS_INDEX: string = process.env.ELASTIC_USERS_INDEX || "users";
    public static ELASTIC_USERS_TYPE: string = "user";
    public static ELASTIC_TAGS_INDEX: string = process.env.ELASTIC_TAGS_INDEX || "tags";
    public static ELASTIC_TAGS_TYPE: string = "tag";
    public static ELASTIC_BRANDS_INDEX: string = process.env.ELASTIC_BRANDS_INDEX || "brands";
    public static ELASTIC_BRANDS_TYPE: string = "brand";
    public static ELASTIC_CATEGORIES_INDEX: string = process.env.ELASTIC_CATEGORIES_INDEX || "categories";
    public static ELASTIC_CATEGORIES_TYPE: string = "category";
    public static ELASTIC_PERMISSIONS_INDEX: string = process.env.ELASTIC_PERMISSIONS_INDEX || "permissions";
    public static ELASTIC_PERMISSIONS_TYPE: string = "permission";

    public static MAIL: string = process.env.MAIL || "shops.eip@gmail.com";
    public static MAIL_HOST: string = process.env.MAIL_HOST || "mail.go-shops.fr";
    public static MAIL_USER: string = process.env.MAIL_USER || "shops.eip@gmail.com";
    public static MAIL_PASS: string = process.env.MAIL_PASS || "1eipshops*2";
    public static MAIL_PORT: number = parseInt(process.env.MAIL_PORT, 10) || 587;
    public static MAIL_PASSWORD_SUBJECT: string = process.env.MAIL_PASSWORD_SUBJECT || "Changement de mot de passe " +
        "de votre compte Shops";
    public static MAIL_PASSWORD_LINK: string = process.env.MAIL_PASSWORD_LINK || "https://dev.go-shops.fr" +
        "/password-reset";
    public static MAIL_VALIDATION_SUBJECT: string = process.env.MAIL_VALIDATION_SUBJECT || "Validation de votre " +
        "compte Shops";
    public static MAIL_VALIDATION_LINK: string = process.env.MAIL_VALIDATION_LINK || "https://dev.go-shops.fr" +
        "/user/activate";

    public static SESSION_NAME: string = process.env.SESSION_NAME || "qid";
    public static SESSION_SECRET: string = process.env.SESSION_SECRET || "e#/~?KHn*3&<!3;fMzcBYRkgH}jY)8BL@q}]'@`r_e!";
    public static REDIS_HOST: string = process.env.REDIS_HOST || "localhost";
    public static REDIS_PORT: number = parseInt(process.env.REDIS_PORT, 10) || 6379;

    public static IMG_FOLDER: string = process.env.IMG_FOLDER || "/img";
    public static IMG_BASE_LINK: string = process.env.IMG_BASE_LINK || "https://cdn.dev.go-shops.fr/uploads";
}
