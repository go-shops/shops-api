import dotenv = require("dotenv");
dotenv.config();
import Constants from "./Constants";

/**
 * If app is launch in test mode, mock some dependencies
 */
import mockery = require("mockery");
import nodemailerMock = require("nodemailer-mock");
import logger from "./utils/Logger";
if (process.env.NODE_ENV === "test") {
    logger.debug("[app.ts - Express]: Mock dependencies");
    mockery.enable({
        warnOnUnregistered: false,
    });
    /** Once mocked, any code that calls require('nodemailer')
     * will get our nodemailerMock
     */
    mockery.registerMock("nodemailer", nodemailerMock);
}

import * as bodyParser from "body-parser";
import RedisSession from "connect-redis";
import cors from "cors";
import express = require("express");
import session from "express-session";
import i18n from "i18n";
import morgan from "morgan";
import {createConnection} from "typeorm";
import {
    AddressEntity,
    BrandEntity,
    CategoryEntity,
    HourEntity,
    OfferEntity,
    PermissionEntity,
    PictureEntity,
    ProductCommentEntity,
    ProductEntity,
    StoreCommentEntity,
    StoreEntity,
    TagEntity,
    UserEntity,
} from "./entity";
import {
    authRoutes,
    commentsRoutes,
    healthRoutes,
    mailsRoutes,
    offersRoutes,
    picturesRoutes,
    productsRoutes,
    searchRoutes,
    storesRoutes,
    usersRoutes,
} from "./routes";
import {AuthMiddleware, Elasticsearch, JoiError} from "./utils";

// Setup Express server
logger.debug("[app.ts - Express]: Create application");
const app = express();

// Setup locales
i18n.configure({
    defaultLocale: "fr",
    cookie: "Accept-Language",
    directory: __dirname + "/../locales",

    // setting of log level WARN - default to require('debug')('i18n:warn')
    logWarnFn: (msg) => {
        logger.warn("[app.ts - i18n]: %s", msg);
    },
    // setting of log level ERROR - default to require('debug')('i18n:error')
    logErrorFn: (msg)  => {
        logger.error("[app.ts - i18n]: %s", msg);
    },
});
app.use(i18n.init);

if (Constants.NODE_ENV === "DEVELOPMENT" || Constants.NODE_ENV === "PRODUCTION") {
    logger.debug("[app.ts - Sentry]: Setup sentry");
    Sentry.init({
        dsn: "https://ec07fb740575420d8e8c1f680eba902f@sentry.io/1432915",
        environment: process.env.NODE_ENV
    });
    app.use(Sentry.Handlers.requestHandler() as express.RequestHandler);
}

// Register middlewares
logger.debug("[app.ts - Express]: Set application headers");
app.use(cors({
    origin: Constants.ORIGIN,
    credentials: true,
}));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE, OPTIONS");
    next();
});

logger.debug("[app.ts - Express]: Set parsing to JSON");
app.use(bodyParser.json());
app.use(morgan("[Morgan][:date[web]] :method :url :status :res[content-length] - :response-time ms",
    {stream: logger.stream}));

// Setup session
app.set("trust proxy", 1); // trust first proxy
const redisStore = RedisSession(session);
const sess = {
    store: new redisStore({host: Constants.REDIS_HOST, port: Constants.REDIS_PORT}),
    name: Constants.SESSION_NAME,
    secret: Constants.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        httpOnly: true,
        secure: process.env.NODE_ENV === "PRODUCTION",
        maxAge: 1000 * 60 * 60 * 24 * 7, // 7 days
    },
};
app.use(session(sess));

// Routes
logger.debug("[app.ts - Express]: Set routes");
app.use("/auth", authRoutes);
app.use("/comments", commentsRoutes);
app.use("/mails", mailsRoutes);
app.use("/offers", offersRoutes);
app.use("/pictures", picturesRoutes);
app.use("/products", productsRoutes);
app.use("/search", searchRoutes);
app.use("/stores", storesRoutes);
app.use("/users", usersRoutes);
app.use("/health", healthRoutes);

app.use((err, req, res, next) => {
    if (err && err.error && err.error.isJoi) {
        // we had a joi error, let's return a custom 400 json response
        logger.debug("[app.ts - Error handler]: %s", err.error.message);
        const message: string  = JoiError.buildError(err.error, res);
        res.status(400).send({
            type: err.type, // Could be "query", "headers", "body", or "params"
            message,
        });
        return;
    } else {
        // pass on to another error handler
        next(err);
    }
});

if (Constants.NODE_ENV === "DEVELOPMENT" || Constants.NODE_ENV === "PRODUCTION") {
    app.use(Sentry.Handlers.errorHandler() as express.ErrorRequestHandler);
}

app.use((err, req, res, next) => {
    if (err && err.message) {
        logger.error("[app.ts - Error handler]: %s", err.message);
        res.status(500).send({message: res.__("system_something_went_wrong")});
        return;
    } else if (!req.session) {
        logger.error("[app.ts - Redis]: Something goes wrong with redis or express-session");
        logger.error("[app.ts - Redis]: %s", err.message);
        res.status(500).send({message: res.__("system_something_went_wrong")});
        return;
    } else {
        // pass on to another error handler
        next(err);
    }
});

if (process.env.NODE_ENV === "DEVELOPMENT" || process.env.NODE_ENV === "PRODUCTION") {
    app.use(Sentry.Handlers.errorHandler() as express.ErrorRequestHandler);
    app.use(function onError(err, req, res, next) {
        // The error id is attached to `res.sentry` to be returned
        // and optionally displayed to the user for support.
        res.statusCode = 500;
        res.end(res.sentry + "\n");
        next(err);
    });
}

// Connect to database
logger.info("[app.ts - TypeORM]: Connect to database");
createConnection({
    database: Constants.DB_NAME,
    entities: [UserEntity, StoreEntity, StoreCommentEntity, AddressEntity, HourEntity, PermissionEntity,
        CategoryEntity, ProductEntity, PictureEntity, OfferEntity, BrandEntity, TagEntity, ProductCommentEntity],
    host: Constants.DB_HOST,
    password: Constants.DB_PASS,
    port: Constants.DB_PORT,
    charset: "UTF8MB4_GENERAL_CI",
    type: "mysql",
    username: Constants.DB_USER,
})
    .then(async () => {
        try {
            logger.info("[app.ts - TypeORM]: Connected to database");

            // Check if elasticsearch index exist, if not create it
            await Elasticsearch.ensureIndices();
            logger.info("[app.ts - Elasticsearch]: Index ok");

            logger.info("[app.ts - Permissions]: Ensure all users permissions exists");
            await AuthMiddleware.ensurePermissions();
            // Start app
            logger.debug("[app.ts - Express]: Start listening");
            app.listen(Constants.PORT, () => {
                logger.info("[app.ts - Express]: App is running at http://localhost:%d in mode %s",
                    Constants.PORT, Constants.NODE_ENV);
                logger.info("[app.ts - Express]: Press CTRL-C to stop");
                app.emit("ServerReady");
            });
        } catch (err) {
            logger.error("[app.ts]: %s", err);
        }
    }).catch((error) => logger.error("[app.ts - TypeORM]: Connection error: %s", error));

// Export app (for running test)
logger.debug("[app.ts - Express]: Export app module");
export default app;

// Catch CTRL-C
process.on("SIGINT", () => {
    logger.info("[app.ts - System]: Receive SIGINT signal");
    logger.info("[app.ts - System]: Application stopped");
    process.exit();
});
