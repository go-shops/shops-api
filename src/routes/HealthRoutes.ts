import {Router} from "express";
import {HealthController} from "../controllers";

const router = Router();

/**
 * @api {get} /health Get API status
 * @apiVersion 1.4.5
 * @apiName GetHealth
 * @apiGroup Health
 *
 * @apiDescription Get api status.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *         message: "Ok"
 *     }
 *
 * @apiUse      InternalError
 */
router.route("/")
    .get(
        HealthController.getHealth,
    );

export const healthRoutes = router;
