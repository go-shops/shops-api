import {Router} from "express";
import {CommentsController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {getComment, postProductComment, postStoreComment} from "../validations/comments";

const router = Router();

/**
 * @api {get} /comments/stores/:commentId Get store comment by ID
 * @apiVersion 1.4.14
 * @apiName GetStoreCommentId
 * @apiGroup Comments
 *
 * @apiDescription Provides all public information about requested store comment.
 *
 * @apiParam    {Number}     id       The comment-ID.
 *
 * @apiSuccess  {Number}        id                      The comment-ID.
 * @apiSuccess  {String}        comment                 The comment.
 * @apiSuccess  {Number{0-5}}   mark                    The comment mark.
 * @apiSuccess  {Store}         store                   The store link to the comment (Cf. Store model)
 * @apiSuccess  {User}          owner                   The comment owner.
 * @apiSuccess  {Number}        owner.id                The Owner-ID.
 * @apiSuccess  {String}        owner.username          The owner username.
 * @apiSuccess  {String}        owner.bio               The owner biography.
 * @apiSuccess  {URL}           owner.picture           The owner picture.
 * @apiSuccess  {URL}           owner.banner            The owner banner.
 * @apiSuccess  {String}        owner.role              The owner role.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *        "id": 1,
 *        "comment": "Super magasin",
 *        "mark": 4,
 *        "store": {
 *          "id": 8,
 *          "name": "Atmosphère Fleurs no loc2",
 *          "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *          "avgMark": 4,
 *          "siret": "01234867189123",
 *          "address": {
 *            "id": 6,
 *            "streetNbr": 95,
 *            "street": "Rue Maréchal Joffre",
 *            "zipCode": "44000",
 *            "city": "Nantes",
 *            "country": "France",
 *            "location": null
 *          },
 *          "hours": [],
 *          "categories": [
 *            {
 *              "id": 12,
 *              "name": "Fleuriste"
 *            },
 *            {
 *              "id": 13,
 *              "name": "Mariage"
 *            }
 *          ],
 *          "pictures": [],
 *          "nbProducts": 0
 *         },
 *        "owner": {
 *          "id": 1,
 *          "username": "Barrie",
 *          "banner": null,
 *          "picture": null,
 *          "bio": null,
 *          "role": "User"
 *        }
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/stores/:commentId")
    .get(
        validate.params(getComment.params),
        CommentsController.getStoreCommentById,
    );

/**
 * @api {get} /comments/products/:commentId Get product comment by ID
 * @apiVersion 1.4.14
 * @apiName GetProductCommentId
 * @apiGroup Comments
 *
 * @apiDescription Provides all public information about requested product comment.
 *
 * @apiParam    {Number}     id       The comment-ID.
 *
 * @apiSuccess  {Number}        id                      The comment-ID.
 * @apiSuccess  {String}        comment                 The comment.
 * @apiSuccess  {Number{0-5}}   mark                    The comment mark.
 * @apiSuccess  {Product}       product                 The product link to the comment (Cf. Product model)
 * @apiSuccess  {User}          owner                   The comment owner.
 * @apiSuccess  {Number}        owner.id                The Owner-ID.
 * @apiSuccess  {String}        owner.username          The owner username.
 * @apiSuccess  {String}        owner.bio               The owner biography.
 * @apiSuccess  {URL}           owner.picture           The owner picture.
 * @apiSuccess  {URL}           owner.banner            The owner banner.
 * @apiSuccess  {String}        owner.role              The owner role.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *        "id": 1,
 *        "comment": "Super magasin",
 *        "mark": 4,
 *        "product": {
 *          "id": 12,
 *          "name": "Bouquet florale",
 *          "description": "Magnifique composition florale, parfaite pour un mariage",
 *          "avgMark": 4,
 *          "brand": null,
 *          "pictures": [
 *            {
 *              "id": 10,
 *              "name": "Bouquet florale",
 *              "description": "Belle composition florale",
 *              "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *            }
 *          ],
 *          "tags": [
 *            {
 *              "id": 5,
 *              "name": "Fleurs"
 *            }
 *          ]
 *        },
 *        "owner": {
 *          "id": 1,
 *          "username": "Barrie",
 *          "banner": null,
 *          "picture": null,
 *          "bio": null,
 *          "role": "User"
 *        }
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/products/:commentId")
    .get(
        validate.params(getComment.params),
        CommentsController.getProductCommentById,
    );

/**
 * @api {post} /comments/stores/:storeId Create store comment
 * @apiVersion 1.4.14
 * @apiName PostStoreComment
 * @apiGroup Comments
 *
 * @apiDescription Post a new comment on a store.<br/>
 * User can post only one comment and store owner are not allowed to post on here own store.<br/>
 * Permission: Comment.Create
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        storeId     The store where the comment will be post.
 * @apiParam (Request Body) {String}        comment     The comment.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum
 * @apiParam (Request Body) {Number{0-5}}   mark        The comment mark.
 * @apiExample {json}   Json
 * {
 *   "comment": "Super magasin",
 *   "mark": 4
 * }
 *
 * @apiSuccess  (Created 201) {Number}        id              The comment-ID.
 * @apiSuccess  (Created 201) {String}        comment         The comment.
 * @apiSuccess  (Created 201) {Number{0-5}}   mark            The comment mark.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "comment": "Super magasin",
 *       "mark": 4
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUSe      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/stores/:storeId")
    .post(
        AuthMiddleware.authorize({perms: [permissions.Comment.Create]}),
        validate.params(postStoreComment.params),
        validate.body(postStoreComment.body),
        CommentsController.postStoreComment,
    );

/**
 * @api {post} /comments/products/:productId Create product comment
 * @apiVersion 1.4.14
 * @apiName PostProductComment
 * @apiGroup Comments
 *
 * @apiDescription Post a new comment on a product.<br/>
 * User can post only one comment.<br/>
 * Permission: Comment.Create
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        productId   The product where the comment will be post.
 * @apiParam (Request Body) {String}        comment     The comment.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum
 * @apiParam (Request Body) {Number{0-5}}   mark        The comment mark.
 * @apiExample {json}   Json
 * {
 *   "comment": "Super produit",
 *   "mark": 4
 * }
 *
 * @apiSuccess  (Created 201) {Number}        id              The comment-ID.
 * @apiSuccess  (Created 201) {String}        comment         The comment.
 * @apiSuccess  (Created 201) {Number{0-5}}   mark            The comment mark.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "comment": "Super produit",
 *       "mark": 4
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUSe      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/products/:productId")
    .post(
        AuthMiddleware.authorize({perms: [permissions.Comment.Create]}),
        validate.params(postProductComment.params),
        validate.body(postProductComment.body),
        CommentsController.postProductComment,
    );

/**
 * @api {put} /comments/stores/:commentId Edit store comment (PUT)
 * @apiVersion 1.4.14
 * @apiName PutStoreComment
 * @apiGroup Comments
 *
 * @apiDescription Edit store comment.<br/>
 * Permission: Comment.Update.All || Comment.Update.Own<br/>
 * User with Comment.Update.All permission can edit comment who don't belong to him.
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        commentId   The comment-ID of comment to edit.
 * @apiParam (Request Body) {String}        comment     The comment.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {Number{0-5}}   mark        The comment mark.
 * @apiExample {json}   Json
 * {
 *   "comment": "Super magasin",
 *   "mark": 5
 * }
 *
 * @apiSuccess  {Number}        id              The comment-ID.
 * @apiSuccess  {String}        comment         The comment.
 * @apiSuccess  {Number{0-5}}   mark            The comment mark.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     {
 *       "id": 1,
 *       "comment": "Super magasin",
 *       "mark": 5
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/stores/:commentId")
    .put(
        AuthMiddleware.authorize({perms: [permissions.Comment.Update.All, permissions.Comment.Update.Own]}),
        validate.params(getComment.params),
        validate.body(postStoreComment.body),
        CommentsController.putStoreComment,
    );

/**
 * @api {put} /comments/products/:commentId Edit product comment (PUT)
 * @apiVersion 1.4.14
 * @apiName PutProductComment
 * @apiGroup Comments
 *
 * @apiDescription Edit product comment.<br/>
 * Permission: Comment.Update.All || Comment.Update.Own<br/>
 * User with Comment.Update.All permission can edit comment who don't belong to him.
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        commentId   The comment-ID of comment to edit.
 * @apiParam (Request Body) {String}        comment     The comment.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {Number{0-5}}   mark        The comment mark.
 * @apiExample {json}   Json
 * {
 *   "comment": "Super produit",
 *   "mark": 5
 * }
 *
 * @apiSuccess  {Number}        id              The comment-ID.
 * @apiSuccess  {String}        comment         The comment.
 * @apiSuccess  {Number{0-5}}   mark            The comment mark.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     {
 *       "id": 1,
 *       "comment": "Super produit",
 *       "mark": 5
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/products/:commentId")
    .put(
        AuthMiddleware.authorize({perms: [permissions.Comment.Update.All, permissions.Comment.Update.Own]}),
        validate.params(getComment.params),
        validate.body(postProductComment.body),
        CommentsController.putProductComment,
    );

/**
 * @api {delete} /comments/stores/:commentId Delete store comment
 * @apiVersion 1.4.14
 * @apiName DeleteStoreComment
 * @apiGroup Comments
 *
 * @apiDescription Delete store comment.<br/>
 * Permission: Comment.Delete.All || Comment.Delete.Own<br/>
 * User with Comment.Delete.All permission can delete comment who don't belong to him.
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam    {Number}        commentId   The comment-ID of comment to delete.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/stores/:commentId")
    .delete(
        AuthMiddleware.authorize({perms: [permissions.Comment.Delete.All, permissions.Comment.Delete.Own]}),
        validate.params(getComment.params),
        CommentsController.deleteStoreComment,
    );

/**
 * @api {delete} /comments/products/:commentId Delete product comment
 * @apiVersion 1.4.14
 * @apiName DeleteProductComment
 * @apiGroup Comments
 *
 * @apiDescription Delete product comment.<br/>
 * Permission: Comment.Delete.All || Comment.Delete.Own<br/>
 * User with Comment.Delete.All permission can delete comment who don't belong to him.
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam    {Number}        commentId   The comment-ID of comment to delete.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/products/:commentId")
    .delete(
        AuthMiddleware.authorize({perms: [permissions.Comment.Delete.All, permissions.Comment.Delete.Own]}),
        validate.params(getComment.params),
        CommentsController.deleteProductComment,
    );

export const commentsRoutes = router;
