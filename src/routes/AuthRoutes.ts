import {Router} from "express";
import {AuthController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {editPassword, editUser, loginUser, postUser} from "../validations/users";

const router = Router();

/**
 * @api {post} /auth/register Register user
 * @apiVersion 1.5.0
 * @apiName PostAuthRegister
 * @apiGroup Auth
 *
 * @apiDescription Register a new user. On successful registration, an validation email is send to user email.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}    username        The user username.<br/>
 *                                                      Rules: 3 characters minimum and 16 maximum.
 * @apiParam (Request Body) {Email}     email           The user email.<br/>
 *                                                      Rules: Any valid email.
 * @apiParam (Request Body) {String}    password        The user password.<br/>
 *                                                      Rules: 8 characters minimum and 32 maximum,
 *                                                      1 uppercase, 1 lowercase, 1 number and 1 special.
 * @apiParam (Request Body) {Email}     [parentEmail]   The user parent email.<br/>
 *                                                      Rules: Any valid email.
 * @apiExample {json}   Json (minimal)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 *   "parentEmail": "parent.barrie@domain.fr",
 * }
 *
 * @apiSuccess (Created 201) {String}           id              The User-ID.
 * @apiSuccess (Created 201) {String}           username        The User username.
 * @apiSuccess (Created 201) {Email}            email           The User email.
 * @apiSuccess (Created 201) {String=["Admin", "Moderator", "Contributor", "User", "Custom"]}    role    The User role.
 * @apiSuccess (Created 201) {Permission[]}     permissions     The User permissions.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "username": "John",
 *       "email": "john@email.test"
 *       "role": "Custom",
 *       "permissions": [
 *         {
 *           "id": 2,
 *           "permission": "User.Read.Public"
 *         },
 *         {
 *           "id": 5,
 *           "permission": "User.Update.Own"
 *         }
 *       ]
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      InternalError
 * @apiError    (Bad Request 400)   UserExist   A user already exists with this email
 */
router.route("/register")
    .post(
        validate.body(postUser.body),
        AuthController.postRegister,
    );

/**
 * @api {post} /auth/login Login user
 * @apiVersion 1.4.9
 * @apiName PostAuthLogin
 * @apiGroup Auth
 *
 * @apiDescription Generate a cookie used for authenticate user requests.<br/>
 * This cookie is valid only for 1 week. After this time, you have to re-authenticate.<br/>
 * You can send it to any request. If authentication is not needed, it will be ignored.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {Email}     email       The user email.<br/>
 *                                                  Rules: Any valid email
 * @apiParam (Request Body) {String}    password    The user password.<br/>
 *                                                  Rules: 8 characters minimum and 32 maximum, 1 uppercase,
 *                                                  1 lowercase, 1 number and 1 special.
 * @apiExample {json}   Json
 * {
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 * }
 *
 * @apiSuccess (Success 200 - Cookie) {Session}   session          The User session cookie.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     COOKIE
 *      set-cookie →qid=s%3A7PWV2HjshSVStrWJ5Un2b-slSqrk1tcT.1ef7W4AqkCinIXrcE1tQO%2F95v%2BL8gZaN%2FJhOK6hXMhI; Path=/;
 *                  Expires=Tue, 02 Apr 2019 05:35:06 GMT; HttpOnly
 *
 * @apiUse      ValidationError
 * @apiError    (Bad Request 400)               InvalidCredential   Invalid Credentials
 * @apiUse      InternalError
 */
router.route("/login")
    .post(
        validate.body(loginUser.body),
        AuthController.postLogin,
    );

/**
 * @api {get} /auth/logout Logout user
 * @apiVersion 1.4.11
 * @apiName GetAuthLogout
 * @apiGroup Auth
 *
 * @apiDescription Logout user. Client have to remove the cookie itself.
 *
 * @apiUse AuthenticationHeader
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *         message: "Logout success"
 *     }
 *
 * @apiUse      AuthorisationError
 * @apiUse      InternalError
 */
router.route("/logout")
    .get(
        AuthMiddleware.authorize(),
        AuthController.getLogout,
    );

/**
 * @api {patch} /auth/password Change password
 * @apiVersion 1.4.15
 * @apiName changePassword
 * @apiGroup Auth
 *
 * @apiDescription Changes the user's password.<br/>
 * Note: <code>If the user is logged the old password is used. Otherwise the userHash is used.</code>
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}    password        The user new password.<br/>
 *                                                      Rules: 8 characters minimum and 32 maximum,
 *                                                      1 uppercase, 1 lowercase, 1 number and 1 special.
 * @apiParam (Request Body) {String}    [oldPassword]   The user password.<br/>
 *                                                      Rules: 8 characters minimum and 32 maximum,
 *                                                      1 uppercase, 1 lowercase, 1 number and 1 special.<br/>
 *                                                      Note: Used only if the user is logged.
 * @apiParam (Request Body) {String}    [userHash]      The user Hash.<br/>
 *                                                      Note: Used only if the user is not logged.
 *
 *
 * @apiExample {json}   Json (logged)
 * {
 *   "password": "somePassword2*"
 *   "oldPassword": "somePassword1*",
 * }
 * @apiExample {json}   Json (not logged)
 * {
 *   "password": "somePassword2*",
 *   "userHash": "BlWZCrVZY9upGIwnpzB6Cg"
 * }
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *         message: "Password updated"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      InternalError
 */
router.route("/password")
    .patch(
        AuthMiddleware.authorize({
            optionalLogin: true,
            perms: [permissions.User.Update.Own],
        }),
        validate.body(editPassword.body),
        AuthController.changePassword,
    );
export const authRoutes = router;
