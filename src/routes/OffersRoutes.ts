import {Router} from "express";
import {OffersController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {editOffer, getOffer, postOffer} from "../validations/offers";

const router = Router();

/**
 * @api {get} /offers Get offers
 * @apiVersion 1.4.6
 * @apiName GetOffers
 * @apiGroup Offers
 *
 * @apiDescription Get list of all offers
 *
 * @apiParam    {Number{1-200}}    [page=1]     Page number.
 * @apiParam    {Number{1-50}}     [maxRes=25]  Max results.
 * @apiExample  {Request} Request
 * GET /offers?page=10&maxRes=50
 *
 * @apiSuccess {Offers[]}  .   List of all offers. (Cf. offer model for more information)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *       "id": 1,
 *       "name": "Test",
 *       "description": "Test d'une offre",
 *       "discount": 70,
 *       "product": {
 *           "id": 2,
 *           "name": "Bouquet",
 *           "description": "Bouquet",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *               {
 *                   "id": 2,
 *                   "name": "Bouquet",
 *                   "description": "Beau bouquet",
 *                   "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *               }
 *           ],
 *           "tags": []
 *       },
 *       "startTime": "2019-03-09T15:16:33.000Z",
 *       "expiryTime": null
 *      }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/")
    .get(
        validate.query(getOffer.query),
        OffersController.getOffers,
    );

/**
 * @api {get} /offers/:offerId Get offer by ID
 * @apiVersion 1.4.10
 * @apiName GetOfferById
 * @apiGroup Offers
 *
 * @apiDescription Get information about requested offer.
 *
 * @apiParam {Number}   id   The offer-ID.
 *
 * @apiSuccess  {Number}        id                      The offer-ID.
 * @apiSuccess  {String}        name                    The offer name.
 * @apiSuccess  {Number}        id                      The offer-ID
 * @apiSuccess  {String}        description             The offer description.
 * @apiSuccess  {Number{1-100}} discount                The offer discount.
 * @apiSuccess  {Products[]}    products                List of products (Cf products model).
 * @apiSuccess  {String}        startTime               Start time of the offer.<br/>
 *                                                      Rules: Any ISO 8601 date
 * @apiSuccess {String}        [expiryTime]             End time of the offer.<br/>
 *                                                      Rules: Any ISO 8601 date greater than startTime
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *      "id": 1,
 *           "name": "Test",
 *           "description": "Test d'une offre",
 *           "discount": 70,
 *           "product": {
 *               "id": 2,
 *               "name": "Bouquet",
 *               "description": "Bouquet",
 *               "avgMark": 0,
 *               "brand": null,
 *               "pictures": [
 *                   {
 *                       "id": 2,
 *                       "name": "Bouquet",
 *                       "description": "Beau bouquet",
 *                       "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *                   }
 *               ],
 *               "tags": []
 *           },
 *           "startTime": "2019-03-18T13:58:30.000Z",
 *           "expiryTime": null
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:offerId")
    .get(
        validate.params(getOffer.params),
        OffersController.getOfferById,
    );

/**
 * @api {post} /offers Create offer
 * @apiVersion 1.5.0
 * @apiName PostOffer
 * @apiGroup Offers
 *
 * @apiDescription Create a new offer<br/>
 * Permissions: Offer.Create.All || Offer.Create.NoOwner || Offer.Create.OwnStore
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}            name            The offer name.<br/>
 *                                                              Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}            description     The offer description.<br/>
 *                                                              Rules: 1 characters minimum.
 * @apiParam (Request Body) {Number{1-100}}     discount        Discount %.
 * @apiParam (Request Body) {String}            startTime       Start time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date
 * @apiParam (Request Body) {String}            [expiryTime]    End time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date greater than startTime
 * @apiParam (Request Body) {Number}            productId       The product ID link to this offer.
 * @apiParam (Request Body) {Number}            storeId         The store ID link to this offer.
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "productId": 23,
 *   "storeId": 15
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "expiryTime": "2019-04-18T13:58:30.000Z",
 *   "productId": 1,
 *   "storeId": 1
 * }
 *
 * @apiSuccess  (Created 201) {Number}      id              The offer-ID.
 * @apiSuccess  (Created 201) {String}      name            The offer name.
 * @apiSuccess  (Created 201) {String}      description     The offer description.
 * @apiSuccess  (Created 201) {Number}      discount        The offer discount.
 * @apiSuccess  (Created 201) {Product}     product         The product link to this offer (Cf. product model).
 * @apiSuccess  (Created 201) {Store}       store           The store link to this offer (Cf. store endpoint).
 * @apiSuccess  (Created 201) {String}      startTime       The start time of the offer.
 * @apiSuccess  (Created 201) {String}      expiryTime      The end time of the offer.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "description": "Test d'une offre",
 *       "discount": 70,
 *       "store": {},
 *       "product": {},
 *       "startTime": "2019-03-18T13:58:30.000Z",
 *       "expiryTime": "2019-04-18T13:58:30.000Z"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/")
    .post(
        AuthMiddleware.authorize({
            perms: [permissions.Offer.Create.All, permissions.Offer.Create.NoOwner, permissions.Offer.Create.OwnStore],
        }),
        validate.body(postOffer.body),
        OffersController.postOffer,
    );

/**
 * @api {put} /offers/:offerId Edit offer (PUT)
 * @apiVersion 1.5.0
 * @apiName EditOffer
 * @apiGroup Offers
 *
 * @apiDescription Edit the requested offer<br/>
 * Permissions: Offer.Update.All || Offer.Update.NoOwner || Offer.Update.OwnStore
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam {Number}   id   The offer-ID.
 *
 * @apiParam (Request Body) {String}            name            The offer name.<br/>
 *                                                              Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}            description     The offer description.<br/>
 *                                                              Rules: 1 characters minimum.
 * @apiParam (Request Body) {Number{1-100}}     discount        Discount %.
 * @apiParam (Request Body) {String}            startTime       Start time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date
 * @apiParam (Request Body) {String}            [expiryTime]    End time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date greater than startTime
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z"
 * }
 * * @apiExample {json}   Json (full)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "expiryTime": "2019-04-18T13:58:30.000Z"
 * }
 *
 * @apiSuccess  (Created 201) {Number}      id              The offer-ID.
 * @apiSuccess  (Created 201) {String}      name            The offer name.
 * @apiSuccess  (Created 201) {String}      description     The offer description.
 * @apiSuccess  (Created 201) {Number}      discount        The offer discount.
 * @apiSuccess  (Created 201) {Product}     product         The product link to this offer (Cf. product model).
 * @apiSuccess  (Created 201) {Store}       store           The store link to this offer (Cf. store endpoint).
 * @apiSuccess  (Created 201) {String}      startTime       The start time of the offer.
 * @apiSuccess  (Created 201) {String}      expiryTime      The end time of the offer.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "description": "Test d'une offre",
 *       "discount": 70,
 *       "store": {},
 *       "product": {},
 *       "startTime": "2019-03-18T13:58:30.000Z",
 *       "expiryTime": "2019-04-18T13:58:30.000Z"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:offerId")
    .put(
        AuthMiddleware.authorize({
            perms: [permissions.Offer.Update.All, permissions.Offer.Update.NoOwner, permissions.Offer.Update.OwnStore],
        }),
        validate.params(getOffer.params),
        validate.body(editOffer.body),
        OffersController.putOffer,
    );

/**
 * @api {delete} /offer/:offerId  Delete offer
 * @apiVersion 1.4.11
 * @apiName DeleteOffer
 * @apiGroup Offers
 *
 * @apiDescription Delete offer.<br/>
 * Permissions: Offer.Delete.All || Offer.Delete.OwnStore
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam {Number}     offerId       The offer-ID.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:offerId")
    .delete(
        AuthMiddleware.authorize({perms: [permissions.Offer.Delete.All, permissions.Offer.Delete.OwnStore]}),
        validate.params(getOffer.params),
        OffersController.deleteOffer,
    );

router.route("/product/:productId")
    .get(
        validate.params(getOffer.params),
        OffersController.getProductOffer,
    );
export const offersRoutes = router;
