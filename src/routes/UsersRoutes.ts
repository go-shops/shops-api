import {Router} from "express";
import {UsersController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {editUser, editUserPermissions, getUser, validateUser} from "../validations/users";

const router = Router();

/**
 * @api {get} /users/me     Get user private
 * @apiVersion 1.4.11
 * @apiName getUserMe
 * @apiGroup Users
 *
 * @apiDescription Provides information about user.<br/>
 * Login is optional. If user is not logged only return permissions of guest user<br/>
 * If user is logged show all information about itself.<br/>
 *
 * @apiUse AuthenticationHeader
 *
 * @apiSuccess  {Number}                                                [id             The user-ID.
 * @apiSuccess  {String}                                                [username]      The user username.
 * @apiSuccess  {Email}                                                 [email]         The user email.
 * @apiSuccess  {URL}                                                   [picture]       The user picture.
 * @apiSuccess  {URL}                                                   [banner]        The user banner.
 * @apiSuccess  {String}                                                [bio]           The user biography.
 * @apiSuccess  {String=Admin, Moderator, Contributor, User, Custom}     [role]          The user role.
 * @apiSuccess  {Email}                                                 [parentEmail    The user parent email.
 * @apiSuccess  {Permission}                                            permissions     The user permissions.
 * @apiSuccessExample {json} Success (not logged)
 *     HTTP/1.1 200 OK
 *     {
 *       "permissions": [
 *           {
 *               "id": 1,
 *               permission: "User.Read.Public"
 *           }
 *       ]
 *     }
 *
 * @apiSuccessExample {json} Success (logged)
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "username": "Barrie",
 *       "email": "barrie@domain.fr",
 *       "picture": null,
 *       "banner": null,
 *       "bio": null,
 *       "role": "User",
 *       "active": true,
 *       "userHash": "3daub4q2NDVxEd3xIYo3",
 *       "parentEmail": "barrie.parent@domain.fr",
 *       "permissions": [
 *           {
 *               "id": 1,
 *               permission: "User.Read.Public"
 *           }
 *       ]
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/me")
    .get(
        AuthMiddleware.authorize({optionalLogin: true}),
        UsersController.getUserMe,
    );

/**
 * @api {get} /users/:userId Get user by ID
 * @apiVersion 1.4.11
 * @apiName getUserById
 * @apiGroup Users
 *
 * @apiDescription Provides information about requested user.<br/>
 * Permissions: User.Read.Public || User.Read.Private<br/>
 * Login is optional. If user is not logged only return public information<br/>
 * If user is logged and request itself show all information.<br/>
 * If user is logged and have User.Read.Private permission, print all information.<br/>
 * If user is logged and have User.Read.Public permission, print public information.<br/>
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam    {Number}    id        The User-ID.
 *
 * @apiSuccess  {Number}                                                id              The user-ID.
 * @apiSuccess  {String}                                                username        The user username.
 * @apiSuccess  {Email}                                                 [email]         The user email.
 * @apiSuccess  {URL}                                                   picture         The user picture.
 * @apiSuccess  {URL}                                                   banner          The user banner.
 * @apiSuccess  {String}                                                bio             The user biography.
 * @apiSuccess  {String=Admin, Moderator, Contributor, User, Custom}     role            The user role.
 * @apiSuccess  {Email}                                                 [parentEmail    The user parent email.
 * @apiSuccessExample {json} Success (not logged/no permission)
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "username": "Barrie",
 *       "picture": null,
 *       "banner": null,
 *       "bio": null,
 *       "role": "User"
 *     }
 *
 * @apiSuccessExample {json} Success (logged/have permission)
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "username": "Barrie",
 *       "email": "barrie@domain.fr",
 *       "picture": null,
 *       "banner": null,
 *       "bio": null,
 *       "role": "User",
 *       "active": true,
 *       "userHash": "3daub4q2NDVxEd3xIYo3",
 *       "parentEmail": "barrie.parent@domain.fr"
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:userId")
    .get(
        AuthMiddleware.authorize({
            perms: [permissions.User.Read.Private, permissions.User.Read.Public],
            optionalLogin: true,
        }),
        validate.params(getUser.params),
        UsersController.getUserById,
    );

/**
 * @api {get} /users/:userId/comments Get user comments
 * @apiVersion 1.4.11
 * @apiName getUserComments
 * @apiGroup Users
 *
 * @apiDescription Return all comments own by a user.
 *
 * @apiParam    {Number}    id        The User-ID.
 *
 * @apiSuccess  {Comments}      comments            List of comments.
 * @apiSuccess  {Number}        comments.id         Comment-ID.
 * @apiSuccess  {String}        comments.comment    The comment.
 * @apiSuccess  {Number=[0-5]}  comments.mark       The comment mark.
 * @apiSuccess  {Store}         comments.store      The store link to the comment.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 2,
 *         "comment": "Super magasin",
 *         "mark": 4,
 *         "store": {
 *           "id": 1,
 *           "name": "Atmosphère Fleurs no loc2",
 *           "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *           "avgMark": 4,
 *           "siret": "01234867189123",
 *           "address": {
 *             "id": 1,
 *             "streetNbr": 95,
 *             "street": "Rue Maréchal Joffre",
 *             "zipCode": "44000",
 *             "city": "Nantes",
 *             "country": "France",
 *             "location": null
 *           },
 *           "hours": [],
 *           "categories": [
 *             {
 *               "id": 1,
 *               "name": "Fleuriste"
 *             },
 *             {
 *               "id": 2,
 *               "name": "Mariage"
 *             }
 *           ],
 *           "pictures": [],
 *           "nbProducts": 0
 *         }
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:userId/comments")
    .get(
        validate.params(getUser.params),
        UsersController.getUserComments,
    );

/**
 * @api {get} /users/:userId/stores Get user stores
 * @apiVersion 1.4.11
 * @apiName getUserStores
 * @apiGroup Users
 *
 * @apiDescription Return all stores own by a user.
 *
 * @apiParam    {Number}    id        The User-ID.
 *
 * @apiSuccess {Store[]}  .   List of all stores. (Cf. store model for more information)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "L'Atmosphère Des Fleurs",
 *         "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *         "siret": "01234567890123",
 *         "avgMark": 0,
 *         "address": {
 *           "id": 2,
 *           "streetNbr": 94,
 *           "street": "Rue Maréchal Joffre",
 *           "zipCode": "44000",
 *           "city": "Nantes",
 *           "location": {
 *             "lat": 0,
 *             "lon": 0
 *           }
 *         },
 *         "hours": [],
 *         "categories": [
 *           {
 *             "id": 1,
 *             "name": "Fleuriste"
 *           },
 *           {
 *             "id": 2,
 *             "name": "Mariage"
 *           }
 *         ],
 *         "pictures": [],
 *         "nbProducts": 0
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:userId/stores")
    .get(
        validate.params(getUser.params),
        UsersController.getUserStores,
    );

/**
 * @api {get} /users/me/rgpd Get user rgpd
 * @apiVersion 1.4.12
 * @apiName getUserRGPD
 * @apiGroup Users
 *
 * @apiDescription Return all information about user in json file.
 *
 * @apiUse AuthenticationHeader
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK + JSON file
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/me/rgpd")
    .get(
        AuthMiddleware.authorize(),
        UsersController.getUserRGPD,
    );

/**
 * @api {put} /users/:userId Edit user by ID (PUT)
 * @apiVersion 1.4.11
 * @apiName EditUserById
 * @apiGroup Users
 *
 * @apiDescription Edit user.<br/>
 * Permissions: User.Update.Own || User.Update.All<br/>
 * If user have User.Update.All permission he can edit any user. Else he can only edit itself.
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}    id              The User-ID.
 *
 * @apiParam (Request Body) {String}    username        The user username.<br/>
 *                                                      Rules: 3 characters minimum and 16 maximum.
 * @apiParam (Request Body) {Email}     email           The user email.<br/>
 *                                                      Rules: Any valid email.
 * @apiParam (Request Body) {String}    [password]      The user password.<br/>
 *                                                      Rules: 8 characters minimum and 32 maximum,
 *                                                      1 uppercase, 1 lowercase, 1 number and 1 special.
 * @apiParam (Request Body) {Email}     [parentEmail]   The user parent email.<br>
 *                                                      Rules: Any valid email.
 * @apiParam (Request Body) {String}    [bio]           The user biography.<br/>
 *                                                      Rules: 1 characters minimum.
 * @apiParam (Request Body) {URL}       [picture]       The user picture.<br/>
 *                                                      Rules: Valid url, maximum 255 characters.
 * @apiParam (Request Body) {URL}       [banner]        The user banner.<br/>
 *                                                      Rules: Valid url, maximum 255 characters.
 * @apiExample {json}   Json (minimal)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 *   "parentEmail": "parent.barrie@domain.fr",
 *   "bio": "This my bio",
 *   "picture": "https://someUrl.com/picture.jpg"
 * }
 *
 * @apiSuccess  {Number}        id              The user-ID.
 * @apiSuccess  {String}        username        The user username.
 * @apiSuccess  {Email}         email           The user email.
 * @apiSuccess  {URL}           user            The user picture.
 * @apiSuccess  {String}        bio             The user biography.
 * @apiSuccess  {String}        userHash        The user hash.
 * @apiSuccess  {Boolean}       active          The user active status.
 * @apiSuccess  {Email}         parentEmail     The user parent email (RGPD).
 * @apiSuccess  {Comment[]}     comments        Array of comments owned by user (Cf. comments endpoint)
 * @apiSuccess  {Store[]}       stores          Array of stores owned by user (Cf. stores endpoint)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "username": "Barrie",
 *       "email": "barrie@domain.fr",
 *       "banner": null,
 *       "picture": null,
 *       "bio": null,
 *       "active": true,
 *       "userHash": "3daub4q2NDVxEd3xIYo3",
 *       "parentEmail": null,
 *     }
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:userId")
    .put(
        AuthMiddleware.authorize({
            perms: [permissions.User.Update.All, permissions.User.Update.Own],
        }),
        validate.params(getUser.params),
        validate.body(editUser.body),
        UsersController.putUserById,
    );

/**
 * @api {put} /users/:userId/permissions Edit user permissions (PUT)
 * @apiVersion 1.4.11
 * @apiName putUserPermissions
 * @apiGroup Users
 *
 * @apiDescription Update user permissions. Replace all existing permissions.<br/>
 * Permissions: User.Update.Permissions
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}    id              The User-ID.
 *
 * @apiParam (Request Body) {String=["Admin", "Moderator", "Contributor", "User", "Custom"]}    role     The user role.
 * @apiParam (Request Body) {Number[]}  [permissions]   Array of permissions ID. Only with Custom role.
 * @apiExample {json}   Json (minimal)
 * {
 *   "role": "Moderator"
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "role": "Custom",
 *   "permissions": [1, 2, 5, 6]
 * }
 *
 * @apiSuccess  {Number}        id              The user-ID.
 * @apiSuccess  {String}        username        The user username.
 * @apiSuccess  {Email}         email           The user email.
 * @apiSuccess  {URL}           user            The user picture.
 * @apiSuccess  {String}        bio             The user biography.
 * @apiSuccess  {String}        userHash        The user hash.
 * @apiSuccess  {Boolean}       active          The user active status.
 * @apiSuccess  {Email}         parentEmail     The user parent email (RGPD).
 * @apiSuccess  {Comment[]}     comments        Array of comments owned by user (Cf. comments endpoint)
 * @apiSuccess  {Store[]}       stores          Array of stores owned by user (Cf. stores endpoint)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 2,
 *         "permission": "User.Read.Public"
 *       },
 *       {
 *         "id": 5,
 *         "permission": "User.UpdateOwn"
 *       }
 *     ]
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:userId/permissions")
    .put(
        AuthMiddleware.authorize({perms: [permissions.User.Update.Permissions]}),
        validate.params(getUser.params),
        validate.body(editUserPermissions.body),
        UsersController.putUserPermissions,
    );

/**
 * @api {delete} /users/:userId Delete user by ID
 * @apiVersion 1.4.11
 * @apiName DeleteUser
 * @apiGroup Users
 *
 * @apiDescription Delete user.
 * Permissions: User.Delete.All || User.Delete.Own
 * If logged user have User.Delete.All permission, he can delete any user account.
 * Else he can only delete it's own account.
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}    id              The User-ID.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUSe      ForbiddenError
 * @apiUse      InternalError
 */
router.route("/:userId")
    .delete(
        AuthMiddleware.authorize({perms: [permissions.User.Delete.All, permissions.User.Delete.Own]}),
        validate.params(getUser.params),
        UsersController.deleteUserMe,
    );

/**
 * @api {get} /users/validate/:hash Validate user's account
 * @apiVersion 1.4.15
 * @apiName UserValidation
 * @apiGroup Users
 *
 * @apiDescription Validate User's account
 *
 * @apiParam    {String}        hash            The User-hash.
 *
 * @apiSuccess  {String}        message         Successful message.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Activation success, you can now login to your account."
 *     }
 *
 * @apiUSe      ForbiddenError
 * @apiUse      InternalError
 * @apiUse      DataNotFoundError
 */
router.get("/validate/:hash",
    validate.params(validateUser.params),
    UsersController.getUserValidation);

/**
 * @api {get} /users/validate/resend/:id Resend validation email
 * @apiVersion 1.4.15
 * @apiName resendValidationEmail
 * @apiGroup Users
 *
 * @apiDescription Resend the validation email to the user
 *
 *
 * @apiParam                {Number}    id              The User-ID.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *          "message": "Email sent, Please check your mailbox"
 *     }
 *
 * @apiUSe      ForbiddenError
 * @apiUse      InternalError
 * @apiUse      DataNotFoundError
 */
router.get("/validate/resend/:id", UsersController.getResendValidationMail);
export const usersRoutes = router;
