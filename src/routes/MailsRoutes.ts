import {Router} from "express";
import {MailsController} from "../controllers";
import validate from "../utils/ValidationMiddleware";
import {postMail} from "../validations/mails";

const router = Router();

/**
 * @api {post} /mails Send email
 * @apiVersion 1.3.0
 * @apiName PostMail
 * @apiGroup Mails
 *
 * @apiDescription Send email for contacting shops
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {Email}     email       The user email.
 * @apiParam (Request Body) {String}    subject     The email subject.<br/>
 *                                                  Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}    content     The email body.<br/>
 *                                                  Rules: 1 characters minimum and 2048 maximum.
 * @apiExample {json}   Json
 * {
 *   "email": "barrie@domain.fr",
 *   "subject": "Test email",
 *   "content": "Ceci est un test"
 * }
 *
 * @apiSuccess {String} message Email successfully sent
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Email successfully sent"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      InternalError
 */
router.route("/")
    .post(
        validate.body(postMail.body),
        MailsController.postMail,
    );

export const mailsRoutes = router;
