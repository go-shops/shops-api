import {Router} from "express";
import {StoresController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {bindProduct, claimStore, getStore, patchStore, postStore} from "../validations/stores";

const router = Router();

/**
 * @api {get} /stores Get stores
 * @apiVersion 1.4.4
 * @apiName GetStores
 * @apiGroup Stores
 *
 * @apiDescription Get list of all stores
 *
 * @apiParam    {Number{1-200}}    [page=1]     Page number.
 * @apiParam    {Number{1-50}}     [maxRes=25]  Max results.
 * @apiExample  {Request} Request
 * GET /stores?page=10&maxRes=50
 *
 * @apiSuccess {Store[]}  .   List of all stores. (Cf. store model for more information)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "L'Atmosphère Des Fleurs",
 *         "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *         "siret": "01234567890123",
 *         "avgMark": 0,
 *         "owner": {
 *           "id": 1,
 *           "username": "Barrie",
 *           "banner": null,
 *           "picture": null,
 *           "bio": null,
 *           "role": "User"
 *         },
 *         "address": {
 *           "id": 2,
 *           "streetNbr": 94,
 *           "street": "Rue Maréchal Joffre",
 *           "zipCode": "44000",
 *           "city": "Nantes",
 *           "location": {
 *             "lat": 0,
 *             "lon": 0
 *           }
 *         },
 *         "hours": [],
 *         "categories": [
 *           {
 *             "id": 1,
 *             "name": "Fleuriste"
 *           },
 *           {
 *             "id": 2,
 *             "name": "Mariage"
 *           }
 *         ],
 *         "pictures": [],
 *         "products": [],
 *         "offers": [],
 *         "nbProducts": 0
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/")
    .get(
        validate.query(getStore.query),
        StoresController.getStores,
    );

/**
 * @api {get} /stores/:storeId  Get store by ID
 * @apiVersion 1.4.0
 * @apiName GetStoreById
 * @apiGroup Stores
 *
 * @apiDescription Get store by it's ID
 *
 * @apiParam {Number}     storeId       The store-ID.
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {Hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "nbProducts": 0
 *      }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId")
    .get(
        validate.params(getStore.params),
        StoresController.getStoreById,
    );

/**
 * @api {post} /stores Create store
 * @apiVersion 1.5.0
 * @apiName PostStore
 * @apiGroup Stores
 *
 * @apiDescription Post a new store.<br/>
 * Permission: Store.Create
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}        name        The store name.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}        description The store description.
 * @apiParam (Request Body) {String}        siret       The store siret.<br/>
 *                                                      Rules: 14 digits.
 * @apiParam (Request Body) {Address}       address     The store address (Cf. address model).
 * @apiParam (Request Body) {Category[]}    categories  Array of categories (Cf. category model).
 * @apiParam (Request Body) {Hour[]}        [hours]     Array of hours (Cf. hour model).
 * @apiParam (Request Body) {Picture[]}     [pictures]  Array of pictures (Cf. picture model).
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *       "streetNbr": 94,
 *       "street": "Rue Maréchal Joffre",
 *       "zipCode": "44000",
 *       "city": "Nantes",
 *       "country": "France"
 *   },
 *   "categories": [
 *       {
 *           "name": "Fleuriste"
 *       },
 *       {
 *           "name": "Mariage"
 *       }
 *   ]
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *     "streetNbr": 94,
 *     "street": "Rue Maréchal Joffre",
 *     "zipCode": "44000",
 *     "city": "Nantes",
 *     "country": "France",
 *     "location": {
 *       "lat": 47.2184,
 *       "lon": 1.5536
 *     }
 *   },
 *   "hours": [
 *     {
 *       "day": "tuesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "wednesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "thursday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "friday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "saturday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "sunday",
 *       "timeOpen": "09:30:00",
 *       "timeClosed": "13:00:00"
 *     }
 *   ],
 *   "categories": [
 *     {
 *       "name": "Fleuriste"
 *     },
 *     {
 *       "name": "Mariage"
 *     }
 *   ],
 *   "pictures": [
 *     {
 *       "name": "Facade",
 *       "description": "Facade du magasin",
 *       "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 *     },
 *     {
 *       "name": "Intérieur",
 *       "description": "Intérieur du magasin",
 *       "url": "https://lh5.googleusercontent.com/p/AF1QipOvnBGPNMJk4sDQQ01kfD7rFM8eKAM3R9M8MSv8=s177-k-no"
 *     }
 *   ]
 * }
 *
 * @apiSuccess (Created 201)  {Number}        id                            The store-ID.
 * @apiSuccess (Created 201)  {String}        name                          The store name.
 * @apiSuccess (Created 201)  {String}        description                   The store description.
 * @apiSuccess (Created 201)  {String}        siret                         The store siret.
 * @apiSuccess (Created 201)  {Number{0-5}}   avgMark                       The store average mark.
 * @apiSuccess (Created 201)  {User}          owner                         The store owner.
 * @apiSuccess (Created 201)  {Number}        owner.id                      The User-ID.
 * @apiSuccess (Created 201)  {String}        owner.username                The user username.
 * @apiSuccess (Created 201)  {String}        owner.bio                     The user biography.
 * @apiSuccess (Created 201)  {URL}           owner.picture                 The user picture.
 * @apiSuccess (Created 201)  {Address}       address                       The store address (Cf address model).
 * @apiSuccess (Created 201)  {Number}        address.id                    The address-ID.
 * @apiSuccess (Created 201)  {Number}        address.streetNbr             The street number.
 * @apiSuccess (Created 201)  {String}        address.street                The street name.
 * @apiSuccess (Created 201)  {String}        address.zipCode               The address zipCode.
 * @apiSuccess (Created 201)  {String}        address.city                  The city name.
 * @apiSuccess (Created 201)  {String}        address.country               The country name.
 * @apiSuccess (Created 201)  {Location}      address.location              The address location
 * @apiSuccess (Created 201)  {Float}         address.location.lat          The address latitude.
 * @apiSuccess (Created 201)  {Float}         address.location.lon          The address longitude.
 * @apiSuccess (Created 201)  {Hours[]}       hours                         List of hours (Cf hour model).
 * @apiSuccess (Created 201)  {Number}        hours.id                      The hour-ID.
 * @apiSuccess (Created 201)  {String}        hours.day                     The week day.
 * @apiSuccess (Created 201)  {String}        hours.timeOpen                The open time.
 * @apiSuccess (Created 201)  {String}        hours.timeClosed              The close time.
 * @apiSuccess (Created 201)  {Categories[]}  categories                    List of categories (Cf categories model).
 * @apiSuccess (Created 201)  {Number}        categories.id                 The category-ID.
 * @apiSuccess (Created 201)  {String}        categories.name               The category name.
 * @apiSuccess (Created 201)  {Pictures[]}    pictures                      List of pictures (Cf pictures model).
 * @apiSuccess (Created 201)  {Number}        pictures.id                   The picture-ID.
 * @apiSuccess (Created 201)  {String}        picture.name                  The picture name.
 * @apiSuccess (Created 201)  {String}        picture.description           The picture description.
 * @apiSuccess (Created 201)  {URL}           picture.url                   The picture url.
 * @apiSuccess (Created 201)  {Products[]}    products                      List of products (Cf products model).
 * @apiSuccess (Created 201)  {Offers[]}      offers                        List of offers.
 * @apiSuccess (Created 201)  {Number}        nbProducts                    Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "nbProducts": 0
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      ForbiddenError
 * @apiUse      AuthorisationError
 * @apiUse      InternalError
 */
router.route("/")
    .post(
        AuthMiddleware.authorize({perms: [permissions.Store.Create]}),
        validate.body(postStore.body),
        StoresController.postStore,
    );

/**
 * @api {put} /stores/:storeId Edit store (PUT)
 * @apiVersion 1.5.0
 * @apiName PutStore
 * @apiGroup Stores
 *
 * @apiDescription Edit an existing store.<br/>
 * Permissions: Store.Update.All || Store.Update.NoOwner || Store.Update.Own
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        storeId     ID of store to edit.
 * @apiParam (Request Body) {String}        name        The store name.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}        description The store description.
 * @apiParam (Request Body) {String}        siret       The store siret.<br/>
 *                                                      Rules: 14 digits.
 * @apiParam (Request Body) {Address}       address     The store address (Cf. address model).
 * @apiParam (Request Body) {Category[]}    categories  Array of categories (Cf. category model).
 * @apiParam (Request Body) {Hour[]}        [hours]     Array of hours (Cf. hour model).
 * @apiParam (Request Body) {Picture[]}     [pictures]  Array of pictures (Cf. picture model).
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *       "streetNbr": 94,
 *       "street": "Rue Maréchal Joffre",
 *       "zipCode": "44000",
 *       "city": "Nantes",
 *       "country": "France"
 *   },
 *   "categories": [
 *       {
 *           "name": "Fleuriste"
 *       },
 *       {
 *           "name": "Mariage"
 *       }
 *   ]
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *     "streetNbr": 94,
 *     "street": "Rue Maréchal Joffre",
 *     "zipCode": "44000",
 *     "city": "Nantes",
 *     "country": "France",
 *     "location": {
 *       "lat": 47.2184,
 *       "lon": 1.5536
 *     }
 *   },
 *   "hours": [
 *     {
 *       "day": "tuesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "wednesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "thursday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "friday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "saturday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "sunday",
 *       "timeOpen": "09:30:00",
 *       "timeClosed": "13:00:00"
 *     }
 *   ],
 *   "categories": [
 *     {
 *       "name": "Fleuriste"
 *     },
 *     {
 *       "name": "Mariage"
 *     }
 *   ],
 *   "pictures": [
 *     {
 *       "name": "Facade",
 *       "description": "Facade du magasin",
 *       "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 *     },
 *     {
 *       "name": "Intérieur",
 *       "description": "Intérieur du magasin",
 *       "url": "https://lh5.googleusercontent.com/p/AF1QipOvnBGPNMJk4sDQQ01kfD7rFM8eKAM3R9M8MSv8=s177-k-no"
 *     }
 *   ]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": 44000,
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 47.2184,
 *           "lon": 1.5536
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "nbProducts": 0
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId")
    .put(
        AuthMiddleware.authorize({
            perms: [permissions.Store.Update.All, permissions.Store.Update.NoOwner, permissions.Store.Update.Own],
        }),
        validate.params(getStore.params),
        validate.body(postStore.body),
        StoresController.putStore,
    );

/**
 * @api {patch} /stores/:storeId Edit store (PATCH)
 * @apiVersion 1.4.18
 * @apiName PatchStore
 * @apiGroup Stores
 *
 * @apiDescription Edit an existing store.<br/>
 * Permissions: Store.Update.All || Store.Update.NoOwner || Store.Update.Own<br/>
 * Note that sub-object (like tags, brand or pictures) have to be send entire and will replace the array of exiting
 * objects.
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        storeId         ID of store to edit.
 * @apiParam (Request Body) {String}        [name]          The store name.<br/>
 *                                                          Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}        [description]   The store description.
 * @apiParam (Request Body) {String}        [siret]         The store siret.<br/>
 *                                                          Rules: 14 digits.
 * @apiParam (Request Body) {Address}       [address]       The store address (Cf. address model).
 * @apiParam (Request Body) {Category[]}    [categories]    Array of categories (Cf. category model).
 * @apiParam (Request Body) {Hour[]}        [hours]         Array of hours (Cf. hour model).
 * @apiParam (Request Body) {Picture[]}     [pictures]      Array of pictures (Cf. picture model).
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *       "streetNbr": 94,
 *       "street": "Rue Maréchal Joffre",
 *       "zipCode": "44000",
 *       "city": "Nantes",
 *       "country": "France"
 *   },
 *   "categories": [
 *       {
 *           "name": "Fleuriste"
 *       },
 *       {
 *           "name": "Mariage"
 *       }
 *   ]
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *     "streetNbr": 94,
 *     "street": "Rue Maréchal Joffre",
 *     "zipCode": "44000",
 *     "city": "Nantes",
 *     "country": "France",
 *     "location": {
 *       "lat": 47.2184,
 *       "lon": 1.5536
 *     }
 *   },
 *   "hours": [
 *     {
 *       "day": "tuesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "wednesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "thursday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "friday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "saturday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "sunday",
 *       "timeOpen": "09:30:00",
 *       "timeClosed": "13:00:00"
 *     }
 *   ],
 *   "categories": [
 *     {
 *       "name": "Fleuriste"
 *     },
 *     {
 *       "name": "Mariage"
 *     }
 *   ],
 *   "pictures": [
 *     {
 *       "name": "Facade",
 *       "description": "Facade du magasin",
 *       "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 *     },
 *     {
 *       "name": "Intérieur",
 *       "description": "Intérieur du magasin",
 *       "url": "https://lh5.googleusercontent.com/p/AF1QipOvnBGPNMJk4sDQQ01kfD7rFM8eKAM3R9M8MSv8=s177-k-no"
 *     }
 *   ]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": 44000,
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 47.2184,
 *           "lon": 1.5536
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "nbProducts": 0
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId")
    .patch(
        AuthMiddleware.authorize({
            perms: [permissions.Store.Update.All, permissions.Store.Update.NoOwner, permissions.Store.Update.Own],
        }),
        validate.params(getStore.params),
        validate.body(patchStore.body),
        StoresController.patchStore,
    );

/**
 * @api {delete} /stores/:storeId Delete store
 * @apiVersion 1.2.0
 * @apiName DeleteStore
 * @apiGroup Stores
 *
 * @apiDescription Delete store.<br/>
 * Permissions: Store.Delete.All || Store.Delete.NoOwner || Store.Delete.Own
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam    {Number}        storeId   The store-ID of store to delete.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId")
    .delete(
        AuthMiddleware.authorize({
            perms: [permissions.Store.Delete.All, permissions.Store.Delete.NoOwner, permissions.Store.Delete.Own],
        }),
        validate.params(getStore.params),
        StoresController.deleteStore,
    );

/**
 * @api {get} /stores/:storeId/products Get products
 * @apiVersion 1.4.7
 * @apiName GetProductsFromStore
 * @apiGroup Stores
 *
 * @apiDescription Get all products link to a store
 *
 * @apiParam    {Number}    storeId     The store-ID where to get list of products.
 *
 * @apiSuccess {Product[]}  .   List of all products. (Cf. product model for more information)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Fleur",
 *         "description": "Belle fleur",
 *         "avgMark": 0,
 *         "brand": {
 *           "id": 1,
 *           "name": "Marque"
 *         },
 *         "pictures": [
 *           {
 *             "id": 1,
 *             "name": "Fleur",
 *             "description": "Belle fleur",
 *             "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *           }
 *         ],
 *         "stores": [],
 *         "tags": []
 *       },
 *       {
 *         "id": 2,
 *         "name": "Bouquet",
 *         "description": "Bouquet",
 *         "avgMark": 0,
 *         "brand": {
 *           "id": 1,
 *           "name": "Marque"
 *         },
 *         "pictures": [
 *           {
 *             "id": 2,
 *             "name": "Bouquet",
 *             "description": "Beau bouquet",
 *             "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *           }
 *         ],
 *         "stores": [],
 *         "tags": []
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId/products")
    .get(
        validate.params(getStore.params),
        StoresController.getStoreProducts,
    );

/**
 * @api {post} /stores/:storeId/products Add products
 * @apiVersion 1.4.11
 * @apiName PostProductToStore
 * @apiGroup Stores
 *
 * @apiDescription Edit list of products bind to store. Add product to existing list.
 * If product already linked, do nothing.<br/>
 * Permissions: Store.Update.All || Store.Update.NoOwner || Store.Update.Own
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam    {Number}    storeId     The store-ID where to edit list of products.
 *
 * @apiParam (Request Body) {Number[]}  products    List of product-ID to bind to this store.
 * @apiExample {json}   Json
 * {
 *   products: [1, 2]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [
 *         {
 *           "id": 1,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         },
 *         {
 *           "id": 2,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         }
 *       ],
 *       "offers": [],
 *       "nbProducts": 2
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId/products")
    .post(
        AuthMiddleware.authorize({
            perms: [permissions.Store.Update.All, permissions.Store.Update.NoOwner, permissions.Store.Update.Own],
        }),
        validate.params(getStore.params),
        validate.body(bindProduct.body),
        StoresController.postProductToStore);

/**
 * @api {put} /stores/:storeId/products Edit products (PUT)
 * @apiVersion 1.4.11
 * @apiName PutProductToStore
 * @apiGroup Stores
 *
 * @apiDescription Edit list of products bind to store. Previous list will be deleted.<br/>
 * Permissions: Store.Update.All || Store.Update.NoOwner || Store.Update.Own
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam    {Number}    storeId     The store-ID where to edit list of products.
 *
 * @apiParam (Request Body) {Number[]}  products    List of product-ID to bind to this store.
 * @apiExample {json}   Json
 * {
 *   products: [1, 2]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [
 *         {
 *           "id": 1,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         },
 *         {
 *           "id": 2,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         }
 *       ],
 *       "offers": [],
 *       "nbProducts": 2
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId/products")
    .put(
        AuthMiddleware.authorize({
            perms: [permissions.Store.Update.All, permissions.Store.Update.NoOwner, permissions.Store.Update.Own],
        }),
        validate.params(getStore.params),
        validate.body(bindProduct.body),
        StoresController.putProductToStore,
    );

/**
 * @api {get} /stores/:storeId/offers Get offers
 * @apiVersion 1.4.10
 * @apiName GetStoreOffers
 * @apiGroup Stores
 *
 * @apiDescription Get all the offers of a store
 *
 * @apiParam {Number}   storeId   The store-ID.
 *
 * @apiSuccess  {Number}        id                      The offer-ID.
 * @apiSuccess  {String}        name                    The offer name.
 * @apiSuccess  {String}        description             The offer description.
 * @apiSuccess  {Number{1-100}} discount                The offer discount.
 * @apiSuccess  {Products[]}    products                List of products (Cf products model).
 * @apiSuccess  {String}        startTime               Start time of the offer.<br/>
 *                                                      Rules: Any ISO 8601 date
 * @apiSuccess {String}        [expiryTime]             End time of the offer.<br/>
 *                                                      Rules: Any ISO 8601 date greater than startTime
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Test",
 *         "description": "Test d'une offre",
 *         "discount": 70,
 *         "product": {
 *           "id": 2,
 *           "name": "Bouquet",
 *           "description": "Bouquet",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 2,
 *               "name": "Bouquet",
 *               "description": "Beau bouquet",
 *               "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *             }
 *           ],
 *           "tags": []
 *         },
 *         "startTime": "2019-03-18T13:58:30.000Z",
 *         "expiryTime": null
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId/offers")
    .get(
        validate.params(getStore.params),
        StoresController.getStoreOffer,
    );

/**
 * @api {get} /stores/:storeId/comments Get comments
 * @apiVersion 1.4.13
 * @apiName GetStoreComments
 * @apiGroup Stores
 *
 * @apiDescription Get all the comments of a store
 *
 * @apiParam    {Number}        storeId                 The store-ID.
 *
 * @apiSuccess  {Number}        id                      The comment-ID.
 * @apiSuccess  {String}        comment                 The comment.
 * @apiSuccess  {Number}        mark                    The comment mark.
 * @apiSuccess  {User}          owner                   The comment owner.
 * @apiSuccess  {Number}        owner.id                The Owner-ID.
 * @apiSuccess  {String}        owner.username          The owner username.
 * @apiSuccess  {String}        owner.bio               The owner biography.
 * @apiSuccess  {URL}           owner.picture           The owner picture.
 * @apiSuccess  {URL}           owner.banner            The owner banner.
 * @apiSuccess  {String}        owner.role              The owner role.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "comment": "Super magasin",
 *         "mark": 4,
 *         "owner": {
 *           "id": 1,
 *           "username": "Barrie",
 *           "banner": null,
 *           "picture": null,
 *           "bio": null,
 *           "role": "User"
 *         }
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:storeId/comments")
    .get(
        validate.params(getStore.params),
        StoresController.getStoreComments,
    );

/**
 * @api {put} /stores/:storeId/claim Claim Store (PUT)
 * @apiVersion 1.5.0
 * @apiName putClaimStore
 * @apiGroup Stores
 *
 * @apiDescription Claim a store
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam    {Number}    storeId     The store-ID where to edit list of products.
 *
 * @apiParam (Request Body) {String}        [siret]         The store siret.<br/>
 * @apiExample {json}   Json
 * {
 *   products: [1, 2]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [
 *         {
 *           "id": 1,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         },
 *         {
 *           "id": 2,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         }
 *       ],
 *       "offers": [],
 *       "nbProducts": 2
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

router.route("/:storeId/claim")
    .put(
        AuthMiddleware.authorize(),
        validate.params(claimStore.params),
        validate.body(claimStore.body),
        StoresController.putClaimStore,
    );
export const storesRoutes = router;
