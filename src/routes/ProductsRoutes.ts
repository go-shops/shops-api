import {Router} from "express";
import {ProductsController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {getProduct, patchProduct, postProduct} from "../validations/products";

const router = Router();

/**
 * @api {get} /products Get products
 * @apiVersion 1.4.5
 * @apiName GetProducts
 * @apiGroup Products
 *
 * @apiDescription Get list of all products
 *
 * @apiParam    {Number{1-200}}    [page=1]    Page number.
 * @apiParam    {Number{1-50}}    [maxRes=25] Max results.
 * @apiExample  {Request} Request
 * GET /products?page=10&maxRes=50
 *
 * @apiSuccess {Product[]}  .   List of all products. (Cf. product model for more information)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Fleur",
 *         "description": "Belle fleur",
 *         "avgMark": 0,
 *         "brand": {
 *           "id": 1,
 *           "name": "Marque"
 *         },
 *         "pictures": [
 *           {
 *             "id": 1,
 *             "name": "Fleur",
 *             "description": "Belle fleur",
 *             "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *           }
 *         ],
 *         "stores": [],
 *         "tags": []
 *       },
 *       {
 *         "id": 2,
 *         "name": "Bouquet",
 *         "description": "Bouquet",
 *         "avgMark": 0,
 *         "brand": {
 *           "id": 1,
 *           "name": "Marque"
 *         },
 *         "pictures": [
 *           {
 *             "id": 2,
 *             "name": "Bouquet",
 *             "description": "Beau bouquet",
 *             "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *           }
 *         ],
 *         "stores": [],
 *         "tags": []
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/")
    .get(
        validate.query(getProduct.query),
        ProductsController.getProducts,
    );

/**
 * @api {get} /products/:productId  Get product by ID
 * @apiVersion 1.4.4
 * @apiName GetProductById
 * @apiGroup Products
 *
 * @apiDescription Get product by it's ID
 *
 * @apiParam {Number}     productId       The product-ID.
 *
 * @apiSuccess  {Number}        id                      The product-ID.
 * @apiSuccess  {String}        name                    The product name.
 * @apiSuccess  {String}        description             The product description.
 * @apiSuccess  {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess  {Brand}         brand                   The product brand.
 * @apiSuccess  {Number}        brand.id                The brand-ID.
 * @apiSuccess  {String}        brand.name              The brand name.
 * @apiSuccess  {Picture}       pictures                Array of pictures with for each the following parameters.
 * @apiSuccess  {Number}        pictures.id             The picture-ID.
 * @apiSuccess  {String}        pictures.name           The picture name.
 * @apiSuccess  {String}        pictures.description    The picture description.
 * @apiSuccess  {String}        pictures.url            The picture URL.
 * @apiSuccess  {Store}         stores                  Array of stores with for each the following parameters.
 * @apiSuccess  {Number}        stores.id               The store-ID.
 * @apiSuccess  {String}        stores.name             The store name.
 * @apiSuccess  {String}        stores.description      The store description.
 * @apiSuccess  {String}        stores.siret            The store siret.
 * @apiSuccess  {Number}        stores.avgMark          The average store mark.
 * @apiSuccess  {Address}       stores.address          The store address (Cf address model).
 * @apiSuccess  {Hours[]}       stores.hours            List of hours (Cf hours model).
 * @apiSuccess  {Categories[]}  stores.categories       List of categories (Cf categories model).
 * @apiSuccess  {Pictures[]}    stores.pictures         List of pictures (Cf pictures model).
 * @apiSuccess  {Products[]}    stores.products         List of products (Cf products model).
 * @apiSuccess  {Offers[]}      stores.offers           List of offers.
 * @apiSuccess  {Number}        stores.nbProducts       Number of products available in this store.
 * @apiSuccess  {Tag}           tags                    Array of tags with for each the following parameters.
 * @apiSuccess  {Number}        tags.id                 The tag-ID.
 * @apiSuccess  {String}        tags.name               The tag name.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Fleur",
 *       "description": "Belle fleur",
 *       "avgMark": 0,
 *       "brand": {
 *         "id": 1,
 *         "name": "Marque"
 *       },
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "stores": [],
 *       "tags": []
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:productId")
    .get(
        validate.params(getProduct.params),
        ProductsController.getProductById,
    );

/**
 * @api {get} products/:productId/offers/ Get offers by productId
 * @apiVersion 1.4.17
 * @apiName GetProductOffer
 * @apiGroup Product
 *
 * @apiDescription Get information about all the offers of a product.
 *
 * @apiParam {Number}   productId   The product-ID.
 *
 * @apiSuccess  {Number}        id                      The offer-ID.
 * @apiSuccess  {String}        name                    The offer name.
 * @apiSuccess  {Number}        id                      The offer-ID
 * @apiSuccess  {String}        description             The offer description.
 * @apiSuccess  {Number{1-100}} discount                The offer discount.
 * @apiSuccess  {Products[]}    products                List of products (Cf products model).
 * @apiSuccess  {String}        startTime               Start time of the offer.<br/>
 *                                                      Rules: Any ISO 8601 date
 * @apiSuccess {String}        [expiryTime]             End time of the offer.<br/>
 *                                                      Rules: Any ISO 8601 date greater than startTime
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *          "id": 1,
 *               "name": "Test",
 *              "description": "Test d'une offre",
 *              "discount": 70,
 *              "product": {
 *                   "id": 2,
 *                  "name": "Bouquet",
 *                  "description": "Bouquet",
 *                  "avgMark": 0,
 *                  "brand": null,
 *                  "pictures": [
 *                      {
 *                          "id": 2,
 *                          "name": "Bouquet",
 *                          "description": "Beau bouquet",
 *                          "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *                      }
 *                  ],
 *                  "tags": []
 *              },
 *              "startTime": "2019-03-18T13:58:30.000Z",
 *              "expiryTime": null
 *      }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:productId/offers")
    .get(
        validate.params(getProduct.params),
        ProductsController.getProductOffers,
    );

/**
 * @api {get} /products/:productId/comments Get comments
 * @apiVersion 1.4.14
 * @apiName GetProductComments
 * @apiGroup Products
 *
 * @apiDescription Get all the comments of a product
 *
 * @apiParam    {Number}        productId               The product-ID.
 *
 * @apiSuccess  {Number}        id                      The comment-ID.
 * @apiSuccess  {String}        comment                 The comment.
 * @apiSuccess  {Number}        mark                    The comment mark.
 * @apiSuccess  {User}          owner                   The comment owner.
 * @apiSuccess  {Number}        owner.id                The Owner-ID.
 * @apiSuccess  {String}        owner.username          The owner username.
 * @apiSuccess  {String}        owner.bio               The owner biography.
 * @apiSuccess  {URL}           owner.picture           The owner picture.
 * @apiSuccess  {URL}           owner.banner            The owner banner.
 * @apiSuccess  {String}        owner.role              The owner role.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "comment": "Super magasin",
 *         "mark": 4,
 *         "owner": {
 *           "id": 1,
 *           "username": "Barrie",
 *           "banner": null,
 *           "picture": null,
 *           "bio": null,
 *           "role": "User"
 *         }
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:productId/comments")
    .get(
        validate.params(getProduct.params),
        ProductsController.getProductComments,
    );

/**
 * @api {post} /products Create product
 * @apiVersion 1.4.11
 * @apiName PostProduct
 * @apiGroup Products
 *
 * @apiDescription Create new product.<br/>
 * Permissions: Product.Create.All || Product.Create.OwnStore<br/>
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}    name            The product name.
 * @apiParam (Request Body) {String}    description     The product description.
 * @apiParam (Request Body) {Brand}     [brand]         The product brand (Cf. brand model for more information).
 * @apiParam (Request Body) {Picture[]} pictures        Array of picture (Cf. picture model for more information).
 * @apiParam (Request Body) {Tag[]}     [tags]          Array of tag (Cf. picture model for more information).
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet",
 *   "description": "Bouquet",
 *   "pictures": [
 *       {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *       }
 *   ],
 *   "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 * }
 *
 * @apiSuccess (Created 201)  {Number}        id                      The product-ID.
 * @apiSuccess (Created 201)  {String}        name                    The product name.
 * @apiSuccess (Created 201)  {String}        description             The product description.
 * @apiSuccess (Created 201)  {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess (Created 201)  {Brand}         brand=null              The product brand.
 * @apiSuccess (Created 201)  {Number}        brand.id                The brand-ID.
 * @apiSuccess (Created 201)  {String}        brand.name              The brand name.
 * @apiSuccess (Created 201)  {Picture}       pictures                Array of pictures with for each
 *                                                                    the following parameters.
 * @apiSuccess (Created 201)  {Number}        pictures.id             The picture-ID.
 * @apiSuccess (Created 201)  {String}        pictures.name           The picture name.
 * @apiSuccess (Created 201)  {String}        pictures.description    The picture description.
 * @apiSuccess (Created 201)  {String}        pictures.url            The picture URL.
 * @apiSuccess (Created 201)  {Tag}           tags                    Array of tags with for each
 *                                                                    the following parameters.
 * @apiSuccess (Created 201)  {Number}        tags.id                 The tag-ID.
 * @apiSuccess (Created 201)  {String}        tags.name               The tag name.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "avgMark": 0,
 *       "brand": {
 *         "id": 1,
 *         "name": "Marque"
 *       },
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "tags": []
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      InternalError
 */
router.route("/")
    .post(
        AuthMiddleware.authorize({perms: [permissions.Product.Create.All, permissions.Product.Create.OwnStore]}),
        validate.body(postProduct.body),
        ProductsController.postProduct,
    );

/**
 * @api {put} /products/:productId Edit product (PUT)
 * @apiVersion 1.4.11
 * @apiName PutProduct
 * @apiGroup Products
 *
 * @apiDescription Edit existing product.<br/>
 * Permissions: Product.Update.All<br/>
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam                {Number}    productId       The product-ID to edit.
 *
 * @apiParam (Request Body) {String}    name            The product name.
 * @apiParam (Request Body) {String}    description     The product description.
 * @apiParam (Request Body) {Brand}     [brand]         The product brand (Cf. brand model for more information).
 * @apiParam (Request Body) {Picture[]} pictures        Array of picture (Cf. picture model for more information).
 * @apiParam (Request Body) {Tag[]}     [tags]          Array of tag (Cf. picture model for more information).
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet",
 *   "description": "Bouquet",
 *   "pictures": [
 *       {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *       }
 *   ],
 *   "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 * }
 *
 * @apiSuccess {Number}        id                      The product-ID.
 * @apiSuccess {String}        name                    The product name.
 * @apiSuccess {String}        description             The product description.
 * @apiSuccess {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess {Brand}         brand=null              The product brand.
 * @apiSuccess {Number}        brand.id                The brand-ID.
 * @apiSuccess {String}        brand.name              The brand name.
 * @apiSuccess {Picture}       pictures                Array of pictures with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        pictures.id             The picture-ID.
 * @apiSuccess {String}        pictures.name           The picture name.
 * @apiSuccess {String}        pictures.description    The picture description.
 * @apiSuccess {String}        pictures.url            The picture URL.
 * @apiSuccess {Tag}           tags                    Array of tags with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        tags.id                 The tag-ID.
 * @apiSuccess {String}        tags.name               The tag name.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "avgMark": 0,
 *       "brand": null,
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "tags": []
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:productId")
    .put(
        AuthMiddleware.authorize({perms: [permissions.Product.Update.All]}),
        validate.params(getProduct.params),
        validate.body(postProduct.body),
        ProductsController.putProduct,
    );

/**
 * @api {patch} /products/:productId Edit product (PATCH)
 * @apiVersion 1.4.11
 * @apiName PatchProduct
 * @apiGroup Products
 *
 * @apiDescription Patch existing product.<br/>
 * Permissions: Product.Update.All<br/>
 * Note that sub-object (like tags, brand or pictures) have to be send entire and will replace the array of exiting
 * objects.
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam                {Number}    productId       The product-ID to edit.
 *
 * @apiParam (Request Body) {String}    [name]          The product name.
 * @apiParam (Request Body) {String}    [description]   The product description.
 * @apiParam (Request Body) {Brand}     [brand]         The product brand (Cf. brand model for more information).
 * @apiParam (Request Body) {Picture[]} [pictures]      Array of picture (Cf. picture model for more information).
 * @apiParam (Request Body) {Tag[]}     [tags]          Array of tag (Cf. picture model for more information).
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet de fleurs"
 * }
 *
 * @apiSuccess {Number}        id                      The product-ID.
 * @apiSuccess {String}        name                    The product name.
 * @apiSuccess {String}        description             The product description.
 * @apiSuccess {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess {Brand}         brand=null              The product brand.
 * @apiSuccess {Number}        brand.id                The brand-ID.
 * @apiSuccess {String}        brand.name              The brand name.
 * @apiSuccess {Picture}       pictures                Array of pictures with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        pictures.id             The picture-ID.
 * @apiSuccess {String}        pictures.name           The picture name.
 * @apiSuccess {String}        pictures.description    The picture description.
 * @apiSuccess {String}        pictures.url            The picture URL.
 * @apiSuccess {Tag}           tags                    Array of tags with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        tags.id                 The tag-ID.
 * @apiSuccess {String}        tags.name               The tag name.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Bouquet de fleurs",
 *       "description": "Bouquet",
 *       "avgMark": 0,
 *       "brand": null,
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "tags": []
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:productId")
    .patch(
        AuthMiddleware.authorize({perms: permissions.Product.Update.All}),
        validate.params(getProduct.params),
        validate.body(patchProduct.body),
        ProductsController.patchProduct,
    );

/**
 * @api {delete} /products/:productId  Delete product
 * @apiVersion 1.4.11
 * @apiName DeleteProduct
 * @apiGroup Products
 *
 * @apiDescription Delete product.<br/>
 * Permissions: Product.Delete.All
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam {Number}     productId       The product-ID.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */
router.route("/:productId")
    .delete(
        AuthMiddleware.authorize({perms: [permissions.Product.Delete.All]}),
        validate.params(getProduct.params),
        ProductsController.deleteProductById,
    );

export const productsRoutes = router;
