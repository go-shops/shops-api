import {Router} from "express";
import {OffersController, PicturesController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {postPicture} from "../validations/pictures";

const router = Router();

/**
 * @api {post} /pictures Post picture
 * @apiVersion 1.5.1
 * @apiName PostPictures
 * @apiGroup Pictures
 *
 * @apiDescription Upload new picture. Picture have to be send in base 64.
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}            picture         Base64 picture.<br/>
 * @apiExample {json}   Json
 * {
 *   "picture": "iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAACXBIWXMAACO1AAAjtQH9jV="
 * }
 *
 * @apiSuccess  (Created 201) {String}      url     The picture URL.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "url": "https://cdn.go-shops.fr/uploads/oPHEvDJdnS.png"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      InternalError
 */
router.route("/")
    .post(
        AuthMiddleware.authorize(),
        validate.body(postPicture.body),
        PicturesController.postPictures,
    );

export const picturesRoutes = router;
