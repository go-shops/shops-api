import {Router} from "express";
import {SearchController} from "../controllers";
import {AuthMiddleware, permissions} from "../utils";
import validate from "../utils/ValidationMiddleware";
import {
    searchBrands,
    searchCategories, searchPermissions,
    searchProduct,
    searchStore,
    searchTags,
    searchUser,
} from "../validations/search";

const router = Router();

/**
 * @api {post}  /search/stores   Search stores
 * @apiVersion 1.4.4
 * @apiName PostSearchStores
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a store.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                                        query               The query.<br/>
 *                                                                                              Rules: 1 characters
 *                                                                                              minimum and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                                 page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                                  size                The page size.
 * @apiParam (Request Body) {String="mark", "alphabetical", "score", "geo"} [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}                          [orderType=desc]    The order type.
 * @apiParam (Request Body) {Location}                                      [location]          Current location.
 *                                                                                              Rules: only on geo sort
 * @apiParam (Request Body) {Float}                                         [location.lat]      Current latitude.
 * @apiParam (Request Body) {Float}                                         [location.lon]      Current longitude.
 * @apiParam (Request Body) {String="km", "m", "miles"}                     [geoUnit]           Unit for distance.
 *                                                                                              Rules: only on geo sort
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50,
 *   "sort": geo,
 *   "orderType": "asc",
 *   "location": {
 *     "lat": 40,
 *     "lon": -70
 *   },
 *   "geoUnit": "km"
 * }
 *
 * @apiSuccess  {Store[]}       store                   Result is array of matching stores.
 * @apiSuccess  {Number}        store.id                The store-ID.
 * @apiSuccess  {String}        store.name              The store name.
 * @apiSuccess  {String}        store.description       The store description.
 * @apiSuccess  {Number{0-5}}   store.avgMark           The store average mark.
 * @apiSuccess  {Address}       store.address           The store address (Cf. address model).
 * @apiSuccess  {Hour}          store.hour              The store hours (Cf. hours model).
 * @apiSuccess  {Category[]}    store.categories        Array of store categories (Cf. category model).
 * @apiSuccess  {Picture[]}     store.pictures          Array of store pictures (Cf. picture model).
 * @apiSuccess  {Number}        stores.nbProducts       Number of products available in this store.
 * @apiSuccess  {Float}         stores.sort             Distance of store (can be null)
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Atmosphère Fleurs",
 *         "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages",
 *         "avgMark": 0,
 *         "address": {
 *           "id": 1,
 *           "streetNbr": 94,
 *           "street": "Rue Maréchal Joffre",
 *           "zipCode": "44000",
 *           "city": "Nantes",
 *           "country": "France",
 *           "location": {
 *             "lat": 90,
 *             "lon": 87
 *           }
 *         },
 *         "hours": [],
 *         "categories": [
 *           {
 *             "id": 1,
 *             "name": "Fleuriste"
 *           },
 *           {
 *             "id": 2,
 *             "name": "Mariage"
 *           }
 *         ],
 *         "pictures": [],
 *         "nbProducts": 0,
 *         "sort": 8318.623518586845
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */
router.route("/stores")
    .post(
        validate.body(searchStore.body),
        SearchController.postSearchStores,
    );

/**
 * @api {post}  /search/products   Search products
 * @apiVersion 1.4.4
 * @apiName PostSearchProducts
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a product.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                                    query               The query.<br/>
 *                                                                                          Rules: 1 characters minimum
 *                                                                                          and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                             page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                              size                The page size.
 * @apiParam (Request Body) {String="mark", "alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}                      [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "mark",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {Product[]}     product                 Result is array of matching products.
 * @apiSuccess  {Number}        product.id              The product-ID.
 * @apiSuccess  {String}        product.name            The product name.
 * @apiSuccess  {String}        product.description     The product description.
 * @apiSuccess  {Number{0-5}}   product.avgMark         The product average mark.
 * @apiSuccess  {Brand}         product.brand           The product brand (Cf. brand model).
 * @apiSuccess  {Picture[]}     product.pictures        Array of product picture (Cf picture model).
 * @apiSuccess  {Tag[]}         product.tags            Array of product tag (Cf tag model).
 * @apiSuccess  {Float}         product.sort            Always null.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 12,
 *         "name": "Composition florale",
 *         "description": "Magnifique composition florale, parfaite pour un mariage",
 *         "avgMark": 0,
 *         "brand": {
 *           "id": 5,
 *           "name": "Test"
 *         },
 *         "pictures": [
 *           {
 *             "id": 1,
 *             "name": "Composition florale",
 *             "description": "Belle composition florale",
 *             "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *           }
 *         ],
 *         "tags": [
 *           {
 *             "id": 1,
 *             "name": "Fleurs"
 *           }
 *         ]
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */
router.route("/products")
    .post(
        validate.body(searchProduct.body),
        SearchController.postSearchProducts,
    );

/**
 * @api {post}  /search/users   Search users
 * @apiVersion 1.4.4
 * @apiName PostSearchUsers
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a user.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                            query               The query.<br/>
 *                                                                                  Rules: 1 characters minimum
 *                                                                                  and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                     page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                      size                The page size.
 * @apiParam (Request Body) {String="alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}              [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "Barrie",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "Barrie",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "alphabetical",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {User[]}        user                    Result is array of matching users.
 * @apiSuccess  {Number}        user.id                 The user-ID.
 * @apiSuccess  {String}        user.username           The user username.
 * @apiSuccess  {String}        user.bio=null           The user biography.
 * @apiSuccess  {String}        user.picture=null       The user picture.
 * @apiSuccess  {Float}         user.sort               Always null.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "username": "Barrie",
 *         "bio": "Hello this is my bio",
 *         "picture": "https://some-url.com/image.jpg"
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */
router.route("/users")
    .post(
        validate.body(searchUser.body),
        SearchController.postSearchUsers,
    );

/**
 * @api {post}  /search/tags   Search tags
 * @apiVersion 1.4.8
 * @apiName PostSearchTags
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a tag.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                            query               The query.<br/>
 *                                                                                  Rules: 1 characters minimum
 *                                                                                  and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                     page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                      size                The page size.
 * @apiParam (Request Body) {String="alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}              [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "Fleurs",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "Fleurs",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "alphabetical",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {Tag[]}         tags                    Result is array of matching tags.
 * @apiSuccess  {Number}        tags.id                 The tag-ID.
 * @apiSuccess  {String}        tags.name               The tag name.
 * @apiSuccess  {Float}         tags.sort               Always null.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Fleurs"
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */
router.route("/tags")
    .post(
        validate.body(searchTags.body),
        SearchController.postSearchTags,
    );

/**
 * @api {post}  /search/brands   Search brands
 * @apiVersion 1.4.8
 * @apiName PostSearchBrands
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a brand.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                            query               The query.<br/>
 *                                                                                  Rules: 1 characters minimum
 *                                                                                  and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                     page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                      size                The page size.
 * @apiParam (Request Body) {String="alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}              [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "OnePlus",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "OnePlus",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "alphabetical",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {Brand[]}       brands                    Result is array of matching brands.
 * @apiSuccess  {Number}        brands.id                 The brand-ID.
 * @apiSuccess  {String}        brands.name               The brand name.
 * @apiSuccess  {Float}         brands.sort               Always null.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "OnePlus"
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */
router.route("/brands")
    .post(
        validate.body(searchBrands.body),
        SearchController.postSearchBrands,
    );

/**
 * @api {post}  /search/categories   Search categories
 * @apiVersion 1.4.8
 * @apiName PostSearchCategories
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a category.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                            query               The query.<br/>
 *                                                                                  Rules: 1 characters minimum
 *                                                                                  and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                     page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                      size                The page size.
 * @apiParam (Request Body) {String="alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}              [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "Fleuriste",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "Fleuriste",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "alphabetical",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {Category[]}    categories                  Result is array of matching categories.
 * @apiSuccess  {Number}        category.id                 The category-ID.
 * @apiSuccess  {String}        category.name               The category name.
 * @apiSuccess  {Float}         category.sort               Always null.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Fleuriste"
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */
router.route("/categories")
    .post(
        validate.body(searchCategories.body),
        SearchController.postSearchCategories,
    );

/**
 * @api {post}  /search/permissions   Search permissions
 * @apiVersion 1.4.11
 * @apiName PostSearchPermissions
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a permission.<br/>
 * Permission: User.Update.Permissions
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}                            query               The query.<br/>
 *                                                                                  Rules: 1 characters minimum
 *                                                                                  and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                     page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                      size                The page size.
 * @apiParam (Request Body) {String="alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}              [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "User.Read",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "User.Read",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "alphabetical",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {Category[]}    permissions             Result is array of matching permissions.
 * @apiSuccess  {Number}        permissions.id          The permission-ID.
 * @apiSuccess  {String}        permissions.name        The permission name.
 * @apiSuccess  {Float}         permissions.sort        Allways null
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "User.Read.Public"
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */
router.route("/permissions")
    .post(
        AuthMiddleware.authorize({perms: [permissions.User.Update.Permissions]}),
        validate.body(searchPermissions.body),
        SearchController.postSearchPermissions,
    );

export const searchRoutes = router;
