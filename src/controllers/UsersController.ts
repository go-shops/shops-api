import * as bcrypt from "bcrypt";
import {Request, Response} from "express";
import * as util from "util";
import Constants from "../Constants";
import {PermissionEntity, UserEntity} from "../entity";
import {AuthMiddleware, Elasticsearch, guestPermissions, permissions, role} from "../utils";
import logger from "../utils/Logger";

export class UsersController {
    /**
     * Get user information.
     * If user is logged print user information with permission
     * If user is  not logged print guest permission
     */
    public static async getUserMe(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - GET /users/me]:" +
                "Someone is trying to get public user info");
            let user: UserEntity = req.user;

            if (user) {
                user = await UserEntity.findOne(user.id, {relations: ["permissions"]});
                logger.debug("[controllers/UsersController.ts - API - GET /users/me]:" +
                    "Send user private information");
                res.status(200).send(user.toJSON());
                return;
            }

            logger.debug("[controllers/UsersController.ts - API - GET /users/me]:" +
                "User not logged, send user permissions");
            res.status(200).send({permissions: await PermissionEntity.getPermissionsFromArray(guestPermissions)});
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get user information.
     * By default only print user public information
     * If user is logged and request itself show all information
     * If user is logged and have User.Read.Private permission, print all information
     */
    public static async getUserById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId]:" +
                "Someone is trying to get public user info");
            const {userId} = req.params;

            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId]: Search user");
            const user = await UserEntity.findOne(userId);
            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - GET /users/:userId]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }

            if (req.user && (req.user.id === user.id ||
                AuthMiddleware.userHavePermission(req.user, permissions.User.Read.Private))) {
                logger.debug("[controllers/UsersController.ts - API - GET /users/:userId]:" +
                    "Send user private information");
                res.status(200).send(user.toJSON());
                return;
            }

            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId]: Send user information");
            res.status(200).send(user.toJSONMin());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get user comments
     */
    public static async getUserComments(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/comments]:" +
                "Someone is trying to get user comments");
            const {userId} = req.params;
            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/comments]: Search user");
            const user = await UserEntity.findOne(userId, {relations: ["comments", "comments.store"]});

            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/comments]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }
            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/comments]: Send user information");
            res.status(200).send(user.comments.map( (c) => c.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get user stores
     */
    public static async getUserStores(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/stores]:" +
                "Someone is trying to get user comments");
            const {userId} = req.params;
            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/stores]: Search user");
            const user = await UserEntity.findOne(userId, {relations: ["stores"]});

            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/stores]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }
            logger.debug("[controllers/UsersController.ts - API - GET /users/:userId/stores]: Send user information");
            res.status(200).send(user.stores.map( (s) => s.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     *
     */
    public static async getUserRGPD(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - GET /users/me/rgpd]:" +
                "Someone is trying to get public user info");
            let user: UserEntity = req.user;

            user = await UserEntity.findOne(user.id, {relations: ["permissions", "comments", "stores"]});
            const buf = Buffer.from(JSON.stringify(user.toJSON()));
            res.setHeader("Content-Disposition", "attachment; filename=rgpd.txt");
            res.status(200).send(buf);
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit user information
     */
    public static async putUserById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]:" +
                "Someone is trying to update user info");
            const {userId} = req.params;

            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]: Search user");
            let user = await UserEntity.findOne(userId, {relations: ["permissions"]});
            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }

            if (!((req.user.id === user.id && AuthMiddleware.userHavePermission(req.user, permissions.User.Update.Own))
                || (AuthMiddleware.userHavePermission(req.user, permissions.User.Update.All)))) {
                logger.debug("[controllers/UsersController.ts - API - PUT /users/:id]: " +
                    "User don't have the necessary permission to edit a user");
                res.status(403).send({message: res.__("users_missing_permission_edit")});
                return;
            }

            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]:" +
                "Check if user email already exist");
            const existing: UserEntity = await UserEntity.findOne({where: {email: req.body.email}});
            if (existing && existing.id !== user.id) {
                logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]: " +
                    "Register failed : A user already exists with this email");
                res.status(400).send({message: res.__("users_already_exist")});
                return;
            }

            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]: " +
                "Check if user want to update password");
            if (req.body.password) {
                logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]: Hash user password");
                req.body.password = await bcrypt.hash(req.body.password, 10);
            }

            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]:" +
                " Update user entity and save it in database");
            await UserEntity.updateInDb(UserEntity.fromJSON(req.body), user, user.role, user.permissions);
            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId]: " +
                "Update success, send information to user");
            user = await UserEntity.findOne(user.id);
            res.status(200).send(user.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit user permissions
     */
    public static async putUserPermissions(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId/permissions]:" +
                "Someone is trying to edit user permissions");
            const {userId} = req.params;

            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId/permissions]: Search user");
            let user: UserEntity = await UserEntity.findOne(userId);
            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId/permissions]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }

            let perms: PermissionEntity[] = [];
            if (req.body.permissions) {
                perms = await PermissionEntity.findByIds(req.body.permissions);
                if (perms.length !== req.body.perms.length) {
                    logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId/permissions]: " +
                        "One or more permissions don't exist");
                    res.status(404).send({message: res.__("users_permissions_missing")});
                }
            }

            user  = await UserEntity.updateInDb(user, user, req.body.role, perms);
            logger.debug("[controllers/UsersController.ts - API - PUT /users/:userId/permissions]: "
            + "Send user permissions");
            res.status(200).send(user.permissions.map((p) => p.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Delete user
     */
    public static async deleteUserMe(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - DELETE /users/:userId]: " +
                "Someone is trying to delete user info");
            const {userId} = req.params;

            logger.debug("[controllers/UsersController.ts - API - DELETE /users/:userId]: Search user");
            const user = await UserEntity.findOne(userId);
            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - DELETE /users/:userId]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }

            if (!((req.user.id === user.id && AuthMiddleware.userHavePermission(req.user, permissions.User.Delete.Own))
                || (AuthMiddleware.userHavePermission(req.user, permissions.User.Delete.All)))) {
                logger.debug("[controllers/UsersController.ts - API - DELETE /users/:userId]: " +
                    "User don't have the necessary permission to delete a user");
                res.status(403).send({message: res.__("users_missing_permission_delete")});
                return;
            }

            await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user.id);
            logger.debug("[controllers/UsersController.ts - API - DELETE /users/:userId]:" +
                "Delete user entity in database");
            await user.remove();
            logger.debug("[controllers/UsersController.ts - API - DELETE /users/:userId]: " +
                "Delete success, send information to user");
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    }

    /**
     * Validate User
     */
    public static async getUserValidation(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - POST /users/validate/:hash]:" +
                "Someone is trying to validate a mail address");
            const {hash} = req.params;

            const user = await UserEntity.findOne({where: {userHash: hash}});
            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - POST /users/validate/:hash]:" +
                    "User not found: Something went wrong with the hash");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }

            logger.debug("[controllers/UsersController.ts - API - POST /users/validate/:hash]:" +
                "Update User's information");
            user.active = true;
            await user.save();
            await Elasticsearch.saveUser(user);
            await UserEntity.updateInDb(user, user, role.User);
            logger.debug("[controllers/UsersController.ts - API - POST /users/validate/:hash]:" +
                "Activation success sending information to User");
            res.status(200).send({message: res.__("users_activation_success")});
        } catch (err) {
            next(err);
        }
    }

    /**
     * Resend email validation
     */
    public static async getResendValidationMail(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/UsersController.ts - API - POST POST /users/validate/resend/:id]:" +
                "Someone is trying to resend Validation mail");
            const {id} = req.params;

            logger.debug("[controllers/UsersController.ts - API - POST POST /users/validate/resend/:id]:" +
                "Search user");
            const user = await UserEntity.findOne(id);
            if (!user) {
                logger.debug("[controllers/UsersController.ts - API - POST POST /users/validate/resend/:id]:" +
                    "Get User failed: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }
            logger.debug("[controllers/UsersController.ts - API - POST POST /users/validate/resend/:id]:" +
                "Sending mail");
            const mailResult = user.sendValidationMail();
            logger.debug("[controllers/UsersController.ts - API - POST POST /users/validate/resend/:id]: %s",
                util.inspect(mailResult));
            res.status(201).send({message: "Email sent, Please check your mailbox"});
        } catch (err) {
            next(err);
        }
    }
}
