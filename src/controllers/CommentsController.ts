import {Request, Response} from "express";
import {ProductCommentEntity, ProductEntity, StoreCommentEntity, StoreEntity} from "../entity";
import {AuthMiddleware, permissions, Utils} from "../utils";
import logger from "../utils/Logger";

export class CommentsController {
    /**
     * Get a store comment by it's ID
     */
    public static async getStoreCommentById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API - GET /comments/stores/:commentId]: " +
                "Someone is trying to get a comment");
            const {commentId} = req.params;

            logger.debug("[controllers/commentsController.ts - API - GET /comments/stores/:commentId]: " +
                "Search store comment");
            const comment = await StoreCommentEntity.findOne(commentId, {relations: ["owner", "store"]});
            if (!comment) {
                logger.debug("[controllers/commentsController.ts - API - GET /comments/stores/:commentId]: " +
                    "Store comment not found");
                res.status(404).send({message: res.__("comment_not_found")});
                return;
            }
            logger.debug("[controllers/UsersController.ts - API - GET /comments/stores/:commentId]: " +
                "Send comment information");
            res.status(200).send(comment.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get a product comment by it's ID
     */
    public static async getProductCommentById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API - GET /comments/products/:commentId]: " +
                "Someone is trying to get a comment");
            const {commentId} = req.params;

            logger.debug("[controllers/commentsController.ts - API - GET /comments/products/:commentId]: " +
                "Search product comment");
            const comment = await ProductCommentEntity.findOne(commentId, {relations: ["owner", "product"]});
            if (!comment) {
                logger.debug("[controllers/commentsController.ts - API - GET /comments/products/:commentId]: " +
                    "product comment not found");
                res.status(404).send({message: res.__("comment_not_found")});
                return;
            }
            logger.debug("[controllers/UsersController.ts - API - GET /comments/products/:commentId]: " +
                "Send comment information");
            res.status(200).send(comment.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Post a new store comment
     */
    public static async postStoreComment(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API- POST /comments/stores/:storeId]: " +
                "Someone is trying to post new comment");
            const {storeId} = req.params;
            const user = req.user;

            const store: StoreEntity = await StoreEntity.findOne(storeId,
                {relations: ["comments", "comments.owner", "owner"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - POST /comments/stores/:storeId]: " +
                    "Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            if (store.owner.id === user.id) {
                logger.debug("[controllers/StoresController.ts - API - POST /comments/stores/:storeId]: " +
                    "Store owner are not allowed to post comment in own store");
                res.status(403).send({message: res.__("comments_store_owner_not_allowed")});
                return;
            }

            for (const com of store.comments) {
                if (com.owner && com.owner.id === user.id) {
                    logger.debug("[controllers/StoresController.ts - API - POST /comments/stores/:storeId]: " +
                        "User can only post one comment on a store");
                    res.status(403).send({message: res.__("comments_multiple_store_comments_not_allowed")});
                    return;
                }
            }

            let comment: StoreCommentEntity = await StoreCommentEntity.saveInDb({...req.body, owner: user, store});
            comment = await StoreCommentEntity.findOne(comment.id);
            logger.debug("[controllers/commentsController.ts - API- POST /comments/stores/:storeId]: " +
                "Send comment information");
            const tmp2: StoreEntity = await StoreEntity.findOne(storeId,
                {relations: ["comments", "products", , "products.comments"]});
            await Utils.updateAvgMarkStore(tmp2.comments, tmp2);
            res.status(201).send(comment.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Post a new product comment
     */
    public static async postProductComment(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API- POST /comments/products/:productId]: " +
                "Someone is trying to post new comment");
            const {productId} = req.params;
            const user = req.user;

            const product: ProductEntity = await ProductEntity.findOne(productId,
                {relations: ["comments", "comments.owner"]});
            if (!product) {
                logger.debug("[controllers/StoresController.ts - API - POST /comments/products/:productId]: " +
                    "Product not found");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }

            for (const com of product.comments) {
                if (com.owner && com.owner.id === user.id) {
                    logger.debug("[controllers/StoresController.ts - API - POST /comments/products/:productId]: " +
                        "User can only post one comment on a product");
                    res.status(403).send({message: res.__("comments_multiple_product_comments_not_allowed")});
                    return;
                }
            }

            let comment: ProductCommentEntity = await ProductCommentEntity.saveInDb(
                {...req.body, owner: user, product});
            comment = await ProductCommentEntity.findOne(comment.id);
            logger.debug("[controllers/commentsController.ts - API- POST /comments/products/:productId]: " +
                "Send comment information");
            const tmp2: ProductEntity = await ProductEntity.findOne(productId, {relations: ["comments"]});
            await Utils.updateAvgMarkProduct(tmp2.comments, tmp2);
            res.status(201).send(comment.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit an existing store comment
     */
    public static async putStoreComment(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API- PUT /comments/stores/:commentId]: " +
                "Someone is trying to updateInDb comment");
            const {commentId} = req.params;
            const user = req.user;

            let comment = await StoreCommentEntity.findOne(commentId, {relations: ["owner", "store"]});
            if (!comment) {
                logger.debug("[controllers/commentsController.ts - API- PUT /comments/stores/:commentId]: " +
                    "Comment not found");
                res.status(404).send({message: res.__("comment_not_found")});
                return;
            }

            logger.debug("[controllers/commentsController.ts - API- PUT /comments/stores/:commentId]:" +
                "Check if user have necessary permission");
            if (!((comment.owner.id === user.id
                && AuthMiddleware.userHavePermission(user, permissions.Comment.Update.Own))
                || (AuthMiddleware.userHavePermission(user, permissions.Comment.Update.All)))) {
                logger.debug("[controllers/commentsController.ts - API- PUT /comments/stores/:commentId]: " +
                    "User don't have the necessary permission to edit this comment");
                res.status(403).send({message: res.__("comments_missing_permission_edit")});
                return;
            }

            logger.debug("[controllers/commentsController.ts - API- PUT /comments/stores/:commentId]: " +
                "Update comment information");
            await StoreCommentEntity.updateInDb({...req.body, user: comment.owner, store: comment.store}, comment);
            logger.debug("[controllers/commentsController.ts - API- PUT /comments/stores/:commentId]: " +
                "Send comment information");
            comment = await StoreCommentEntity.findOne(commentId);
            const tmp: StoreEntity = await StoreEntity.findOne(comment.storeId,
                {relations: ["comments", "products", "products.comments"]});
            await Utils.updateAvgMarkStore(tmp.comments, tmp);
            res.status(200).send(comment.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit an existing product comment
     */
    public static async putProductComment(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API- PUT /comments/products/:commentId]: " +
                "Someone is trying to updateInDb comment");
            const {commentId} = req.params;
            const user = req.user;

            let comment = await ProductCommentEntity.findOne(commentId, {relations: ["owner", "product"]});
            if (!comment) {
                logger.debug("[controllers/commentsController.ts - API- PUT /comments/products/:commentId]: " +
                    "Comment not found");
                res.status(404).send({message: res.__("comment_not_found")});
                return;
            }

            logger.debug("[controllers/commentsController.ts - API- PUT /comments/products/:commentId]:" +
                "Check if user have necessary permission");
            if (!((comment.owner.id === user.id
                && AuthMiddleware.userHavePermission(user, permissions.Comment.Update.Own))
                || (AuthMiddleware.userHavePermission(user, permissions.Comment.Update.All)))) {
                logger.debug("[controllers/commentsController.ts - API- PUT /comments/products/:commentId]: " +
                    "User don't have the necessary permission to edit this comment");
                res.status(403).send({message: res.__("comments_missing_permission_edit")});
                return;
            }

            logger.debug("[controllers/commentsController.ts - API- PUT /comments/products/:commentId]: " +
                "Update comment information");
            await ProductCommentEntity.updateInDb(
                {...req.body, user: comment.owner, product: comment.product}, comment);
            logger.debug("[controllers/commentsController.ts - API- PUT /comments/products/:commentId]: " +
                "Send comment information");
            comment = await ProductCommentEntity.findOne(commentId);
            const tmp: ProductEntity = await ProductEntity.findOne(comment.productId, {relations: ["comments"]});
            await Utils.updateAvgMarkProduct(tmp.comments, tmp);
            res.status(200).send(comment.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Delete store comment
     */
    public static async deleteStoreComment(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/stores/:commentId]: " +
                "Someone is trying to delete a comment");
            const {commentId} = req.params;
            const user = req.user;

            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/stores/:commentId]: " +
                "Search comment");
            const comment: StoreCommentEntity = await StoreCommentEntity.findOne(commentId,
                {relations: ["owner", "store"]});
            if (!comment) {
                logger.debug("[controllers/commentsController.ts - API - DELETE /comments/stores/:commentId]: " +
                    "Delete failed : Comment not found");
                res.status(404).send({message: res.__("comment_not_found")});
                return;
            }

            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/stores/:commentId]: " +
                "Check if user have necessary permission");
            if (!((comment.owner.id === user.id
                && AuthMiddleware.userHavePermission(user, permissions.Comment.Delete.Own))
                || (AuthMiddleware.userHavePermission(user, permissions.Comment.Delete.All)))) {
                logger.debug("[controllers/commentsController.ts - API - DELETE /comments/stores/:commentId]: " +
                    "User don't have the necessary permission to delete this comment");
                res.status(403).send({message: res.__("comments_missing_permission_delete")});
                return;
            }

            await comment.remove();
            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/stores/:commentId]: " +
                "Delete success: send information to user");
            const tmp: StoreEntity = await StoreEntity.findOne(comment.storeId,
                {relations: ["comments", "products", , "products.comments"]});
            await Utils.updateAvgMarkStore(tmp.comments, tmp);
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    }

    /**
     * Delete product comment
     */
    public static async deleteProductComment(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/products/:commentId]: " +
                "Someone is trying to delete a comment");
            const {commentId} = req.params;
            const user = req.user;

            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/products/:commentId]: " +
                "Search comment");
            const comment: ProductCommentEntity = await ProductCommentEntity.findOne(commentId,
                {relations: ["owner", "product"]});
            if (!comment) {
                logger.debug("[controllers/commentsController.ts - API - DELETE /comments/products/:commentId]: " +
                    "Delete failed : Comment not found");
                res.status(404).send({message: res.__("comment_not_found")});
                return;
            }

            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/products/:commentId]: " +
                "Check if user have necessary permission");
            if (!((comment.owner.id === user.id
                && AuthMiddleware.userHavePermission(user, permissions.Comment.Delete.Own))
                || (AuthMiddleware.userHavePermission(user, permissions.Comment.Delete.All)))) {
                logger.debug("[controllers/commentsController.ts - API - DELETE /comments/products/:commentId]: " +
                    "User don't have the necessary permission to delete this comment");
                res.status(403).send({message: res.__("comments_missing_permission_delete")});
                return;
            }

            await comment.remove();
            logger.debug("[controllers/commentsController.ts - API - DELETE /comments/products/:commentId]: " +
                "Delete success: send information to user");
            const tmp: ProductEntity = await ProductEntity.findOne(comment.productId, {relations: ["comments"]});
            await Utils.updateAvgMarkProduct(tmp.comments, tmp);
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    }
}
