import * as bcrypt from "bcrypt";
import {Request, Response} from "express";
import * as randomstring from "randomstring";
import * as util from "util";
import {PermissionEntity, UserEntity} from "../entity";
import {role, unactiveUserPermissions} from "../utils";
import logger from "../utils/Logger";

export class AuthController {
    public static async postRegister(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/AuthController.ts - API - /auth/register]: Someone is trying to register");

            const existing: UserEntity = await UserEntity.findOne({where: {email: req.body.email}});
            if (existing) {
                logger.debug("[controllers/AuthController.ts - API - /auth/register]: Register failed :" +
                    "A user already exists with this email");
                res.status(400).send({message: res.__("users_already_exist")});
                return;
            }

            logger.debug("[controllers/AuthController.ts - API - /auth/register]: Creating user's Hash");
            const userHash = randomstring.generate(req.body.email.length);
            logger.debug("[controllers/AuthController.ts - API - /auth/register]: Hash user password");
            const password = await bcrypt.hash(req.body.password, 10);
            logger.debug("[controllers/AuthController.ts - API - /auth/register]:" +
                "Create new user entity and saveInDb it in database");
            const user: UserEntity = await UserEntity.saveInDb({...req.body, password, userHash},
                role.Custom, await PermissionEntity.getPermissionsFromArray(unactiveUserPermissions));
            logger.debug("[controllers/AuthController.ts - API - /auth/register]:" +
                "Register success: send information to user");
            logger.debug("[controllers/AuthController.ts - API - /auth/register]: Sending validation mail");
            const mailResult = user.sendValidationMail();
            logger.debug("[controllers/AuthController.ts - API - /auth/register]: %s", util.inspect(mailResult));
            res.status(201).send(user.toJSON());
        } catch (err) {
            next(err);
        }
    }

    public static async postLogin(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/AuthController.ts - API - /auth/login]: Someone is trying to login");

            const user = await UserEntity.findOne({where: {email: req.body.email}});
            if (!user) {
                logger.debug("[controllers/AuthController.ts - API - /auth/login]: Invalid email");
                res.status(400).send({message: res.__("auth_invalid_credential")});
                return;
            }

            logger.debug("[controllers/AuthController.ts - API - /auth/login]: Check user password");
            if (await bcrypt.compare(req.body.password, user.password)) {
                logger.debug("[controllers/AuthController.ts - API - /auth/login]: Login success");
                req.session.userId = user.id;
                res.status(200).send({message: res.__("auth_login_success")});
            } else {
                logger.debug("[controllers/AuthController.ts - API - /auth/login]: Wrong password");
                res.status(400).send({message: res.__("auth_invalid_credential")});
            }
        } catch (err) {
            next(err);
        }
    }

    public static async getLogout(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/AuthController.ts - API - /auth/logout]: Someone is trying to logout");
            req.session.destroy((err) => {
                if (err) {
                    logger.error("[controllers/AuthController.ts - API - /auth/logout]: %s", err.message);
                    res.status(500).send({message: res.__("system_something_went_wrong")});
                }
                logger.debug("[controllers/AuthController.ts - API - /auth/logout]: Logout success");
                res.status(200).send({message: res.__("auth_logout_success")});
            });
        } catch (err) {
            next(err);
        }
    }

    public static async changePassword(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]: " +
                "Someone is trying to change password");
            let user: UserEntity = req.user;

            if (user) {
                user = await UserEntity.findOne(user.id, {relations: ["permissions"]});
                logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]: " +
                    "User found");
                if (!(await bcrypt.compare(req.body.oldPassword, user.password))) {
                    logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]: " +
                        "Wrong password");
                    res.status(401).send({message: res.__("auth_invalid_credential")});
                }
                logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]: " +
                        "Hash new password");
                req.body.password = await bcrypt.hash(req.body.password, 10);
            } else {
                user = await UserEntity.findOne({where: {userHash: req.body.userHash}, relations: ["permissions"]});
                logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]: " +
                    "Hash new password");
                req.body.password = await bcrypt.hash(req.body.password, 10);
            }

            if (!user) {
                logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }

            logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]:" +
                " Update user entity and save it in database");
            await UserEntity.updateInDb(UserEntity.fromJSON(req.body), user, user.role, user.permissions);
            logger.debug("[controllers/AuthController.ts - API - PATCH /auth/password]: " +
                "Update success, send information to user");
            res.status(200).send({message: res.__("users_password_updated")});
        } catch (err) {
            next(err);
        }
    }
}
