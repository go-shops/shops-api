import {Request, Response} from "express";
import {getRepository} from "typeorm";
import Constants from "../Constants";
import {AddressEntity, ProductEntity, StoreEntity, UserEntity} from "../entity";
import {AuthMiddleware, Elasticsearch, permissions} from "../utils";
import logger from "../utils/Logger";

export class StoresController {
    /**
     * Get all stores
     */
    public static async getStores(req: Request, res: Response, next) {
        try {
            let page = parseInt(req.query.page, 10);
            let maxRes = parseInt(req.query.maxRes, 10);
            if (isNaN(page)) {
                page = 1;
            }
            if (isNaN(maxRes)) {
                maxRes = 25;
            }
            logger.debug("[controllers/StoresController.ts - API - GET /stores]: Someone is trying to get stores info");
            logger.debug("[controllers/StoresController.ts - API - GET /stores]: Search stores");
            const stores = await StoreEntity.find(
                {skip: (page - 1) * maxRes, take: maxRes,
                    relations: ["owner", "products", "offers"]});
            if (stores === undefined) {
                logger.debug("[controllers/StoresController.ts - API - GET /stores]: Stores not found");
                res.status(404).send({message: res.__("stores_not_found")});
                return;
            }
            logger.debug("[controllers/StoresController.ts - API - GET /stores]: Send store's information");
            res.status(200).send(stores.map((s) => s.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get store with id equal to :storeId
     */
    public static async getStoreById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId]: " +
                "Someone is trying to get stores info");
            const {storeId} = req.params;
            logger.debug("[controllers/StoresController.ts - API - GET /stores]: Search stores");
            const store: StoreEntity = await StoreEntity.findOne(storeId,
                {relations: ["owner", "products", "offers"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - GET /stores]: Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }
            logger.debug("[controllers/StoresController.ts - API - GET /stores]: Send store's information");
            res.send(store.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Create new store
     */
    public static async postStore(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - POST /stores]: " +
                "Someone is trying to make a new store");
            const user: UserEntity = req.user;

            if (req.body.siret) {
                const existing: StoreEntity = await StoreEntity.findOne({where: {siret: req.body.siret}});
                if (existing) {
                    logger.debug("[controllers/StoresController.ts - API - POST /stores]: " +
                        "Register failed, A store already exists with this siret");
                    res.status(400).send({message: res.__("stores_already_exist")});
                    return;
                }

                logger.debug("[controllers/StoresController.ts - API - POST /stores]: Save store information");
                await StoreEntity.saveInDb(user, req.body);
                logger.debug("[controllers/StoresController.ts - API - POST /stores]: Send store information");
                const store: StoreEntity = await StoreEntity.findOne({where: {siret: req.body.siret},
                    relations: ["owner", "products", "offers"]});
                res.status(201).send(store.toJSON());
            } else {
                logger.debug("[controllers/StoresController.ts - API - POST /stores]: Save store information");
                await StoreEntity.saveInDb(null, req.body);
                logger.debug("[controllers/StoresController.ts - API - POST /stores]: Send store information");
                const store: StoreEntity = await StoreEntity.findOne({where: {name: req.body.name},
                    relations: ["owner", "products", "offers"]});
                res.status(201).send(store.toJSON());
            }
        } catch (err) {
            next(err);
        }
    }

    /**
     * Patch store when id equal to :storeId
     */
    public static async patchStore(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]: " +
                "Someone is trying to updateInDb a store");
            const {storeId} = req.params;
            const user: UserEntity = req.user;

            logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]: Search store");
            let store: StoreEntity = await StoreEntity.findOne(storeId, {relations: ["owner"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]: " +
                    "Update failed : Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]:" +
                "Check if user have necessary permission");
            if (!((!store.owner && AuthMiddleware.userHavePermission(user, permissions.Store.Update.NoOwner)) ||
                (store.owner.id === user.id && AuthMiddleware.userHavePermission(user, permissions.Store.Update.Own)) ||
                (AuthMiddleware.userHavePermission(user, permissions.Store.Update.All)))) {
                logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]:" +
                    "User don't have necessary permission to edit the store");
                res.status(403).send({message: res.__("stores_missing_permission_edit")});
                return;
            }

            if (req.body.siret !== store.siret) {
                const existing: StoreEntity = await getRepository(StoreEntity)
                    .findOne({where: {siret: req.body.siret}});
                if (existing) {
                    logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]: " +
                        "Register failed: A store already exists with this siret");
                    res.status(400).send({message: res.__("stores_already_exist")});
                    return;
                }
            }

            logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]: Update store information");
            await StoreEntity.patchInDb(user, req.body, store);
            logger.debug("[controllers/StoresController.ts - API - PATCH /stores/:storeId]: Send store information");
            store = await getRepository(StoreEntity).findOne(storeId, {
                relations: ["owner", "products", "offers"],
            });
            res.status(200).send(store.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit store when id equal to :storeId
     */
    public static async putStore(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]: " +
                "Someone is trying to updateInDb a store");
            const {storeId} = req.params;
            const user: UserEntity = req.user;

            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]: Search store");
            let store: StoreEntity = await StoreEntity.findOne(storeId, {relations: ["owner"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]: " +
                    "Update failed : Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]:" +
                "Check if user have necessary permission");
            if (!((!store.owner && AuthMiddleware.userHavePermission(user, permissions.Store.Update.NoOwner)) ||
                (store.owner.id === user.id && AuthMiddleware.userHavePermission(user, permissions.Store.Update.Own)) ||
                (AuthMiddleware.userHavePermission(user, permissions.Store.Update.All)))) {
                logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]:" +
                    "User don't have necessary permission to edit the store");
                res.status(403).send({message: res.__("stores_missing_permission_edit")});
                return;
            }

            if (req.body.siret !== store.siret) {
                const existing: StoreEntity = await getRepository(StoreEntity)
                    .findOne({where: {siret: req.body.siret}});
                if (existing) {
                    logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]: Register failed : " +
                        "A store already exists with this siret");
                    res.status(400).send({message: res.__("stores_already_exist")});
                    return;
                }
            }

            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]: Update store information");
            await StoreEntity.updateInDb(user, req.body, store);
            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId]: Send store information");
            store = await getRepository(StoreEntity).findOne(storeId, {
                where: {siret: req.body.siret},
                relations: ["owner", "products", "offers"],
            });
            res.status(200).send(store.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Delete store when id equal to :storeId
     */
    public static async deleteStore(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - DELETE /stores/:storeId]: " +
                "Someone is trying to delete a store");
            const {storeId} = req.params;
            const user: UserEntity = req.user;

            logger.debug("[controllers/StoresController.ts - API - DELETE /stores/:storeId]: Search store");
            const store: StoreEntity = await StoreEntity.findOne(storeId, {relations: ["owner"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - DELETE /stores/:storeId]: " +
                    "Delete failed : Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            logger.debug("[controllers/StoresController.ts - API - DELETE /stores/:storeId]:" +
                "Check if user have necessary permission");
            if (!((!store.owner && AuthMiddleware.userHavePermission(user, permissions.Store.Delete.NoOwner)) ||
                (store.owner.id === user.id && AuthMiddleware.userHavePermission(user, permissions.Store.Delete.Own)) ||
                (AuthMiddleware.userHavePermission(user, permissions.Store.Delete.All)))) {
                logger.debug("[controllers/StoresController.ts - API - DELETE /stores/:storeId]:" +
                    "User don't have necessary permission to edit the store");
                res.status(403).send({message: res.__("stores_missing_permission_delete")});
                return;
            }

            await Elasticsearch.delete(Constants.ELASTIC_STORES_INDEX, Constants.ELASTIC_STORES_TYPE, store.id);
            await AddressEntity.remove(store.address);
            await store.remove();
            logger.debug("[controllers/StoresController.ts - API - DELETE /stores/:storeId]: " +
                "Delete success: send information to user");
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get store product list
     */
    public static async getStoreProducts(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/products]: " +
                "Someone is trying to get store product list");
            const {storeId} = req.params;
            logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/products]: Search store");
            const store: StoreEntity = await StoreEntity.findOne(storeId, {relations: ["products", "products.stores"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/products]: " +
                    "Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }
            logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/products]: " +
                "Send products information");
            res.status(200).send(store.products.map((s) => s.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get store comments list
     */
    public static async getStoreComments(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/comments]: " +
                "Someone is trying to get store comments list");
            const {storeId} = req.params;
            logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/comments]: Search store");
            const store: StoreEntity = await StoreEntity.findOne(storeId, {relations: ["comments", "comments.owner"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/comments]: " +
                    "Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }
            logger.debug("[controllers/StoresController.ts - API - GET /stores/:storeId/comments]: " +
                "Send comments information");
            res.status(200).send(store.comments.map((c) => c.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Add product to product list
     */
    public static async postProductToStore(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - POST /stores/:storeId/products]: " +
                "Someone is trying to edit store product list");
            const {storeId} = req.params;
            const user: UserEntity = req.user;

            logger.debug("[controllers/StoresController.ts - API - POST /stores/:storeId/products]: Search store");
            const store: StoreEntity = await StoreEntity.findOne(storeId,
                {relations: ["owner", "products", "offers"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - POST /stores/:storeId/products]: " +
                    "Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            logger.debug("[controllers/StoresController.ts - API - POST /stores/:storeId/products]:" +
                "Check if user have necessary permission");
            if (!((!store.owner && AuthMiddleware.userHavePermission(user, permissions.Store.Update.NoOwner)) ||
                (store.owner.id === user.id && AuthMiddleware.userHavePermission(user, permissions.Store.Update.Own)) ||
                (AuthMiddleware.userHavePermission(user, permissions.Store.Update.All)))) {
                logger.debug("[controllers/StoresController.ts - API - POST /stores/:storeId/products]:" +
                    "User don't have necessary permission to edit the store");
                res.status(403).send({message: res.__("stores_missing_permission_post_product")});
                return;
            }

            const products: ProductEntity[] = await ProductEntity.findByIds(req.body.products);
            if (products.length !== req.body.products.length) {
                logger.debug("[controllers/StoresController.ts - API - POST /stores/:storeId/products]: " +
                    "One or more product don't exist");
                res.status(404).send({message: res.__("stores_product_missing")});
                return;
            }
            logger.debug("[controllers/StoresController.ts - API - POST /stores/:storeId/products]: " +
                "Update store information");
            StoreEntity.merge(store, {products});
            store.nbProducts = store.products.length;
            await store.save();
            await Elasticsearch.saveStore(store);
            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: " +
                "Send store information");
            res.status(200).send(store.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit product list for a store
     */
    public static async putProductToStore(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: " +
                "Someone is trying to edit store product list");
            const {storeId} = req.params;
            const user: UserEntity = req.user;

            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: Search store");
            const store: StoreEntity = await StoreEntity.findOne(storeId,
                {relations: ["owner", "products", "offers"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: " +
                    "Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]:" +
                "Check if user have necessary permission");
            if (!((!store.owner && AuthMiddleware.userHavePermission(user, permissions.Store.Update.NoOwner)) ||
                (store.owner.id === user.id && AuthMiddleware.userHavePermission(user, permissions.Store.Update.Own)) ||
                (AuthMiddleware.userHavePermission(user, permissions.Store.Update.All)))) {
                logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]:" +
                    "User don't have necessary permission to edit the store");
                res.status(403).send({message: res.__("stores_missing_permission_edit_product")});
                return;
            }

            const products: ProductEntity[] = await ProductEntity.findByIds(req.body.products);
            if (products.length !== req.body.products.length) {
                logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: " +
                "One or more product don't exist");
                res.status(404).send({message: res.__("stores_product_missing")});
            }
            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: " +
                "Update store information");
            store.products = products;
            store.nbProducts = store.products.length;
            await store.save();
            await Elasticsearch.saveStore(store);
            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: " +
                "Send store information");
            res.status(200).send(store.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get all offers link to a store
     */
    public static async getStoreOffer(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - GET /offer/stores/:storeId]: " +
                "Someone is trying to get offers form store");
            const {storeId} = req.params;
            logger.debug("[controllers/StoresController.ts - API - GET /offer/stores/:storeId]: " +
                "Search stores");
            const store: StoreEntity = await StoreEntity.findOne(storeId, {relations: ["owner", "offers"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - GET /offer/stores/:storeId]: " +
                    "Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }
            logger.debug("[controllers/StoresController.ts - API - GET /offer/stores/:storeId]: " +
                "Send store information");
            res.status(200).send(store.offers.map((o) =>  o.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    public static async putClaimStore(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - PUT /store/claim/:storeId]: " +
                "Someone is trying to claim a store");
            const {storeId} = req.params;
            const user: UserEntity = req.user;

            logger.debug("[controllers/StoresController.ts - API - PUT /store/claim/:storeId]: " +
                "Search stores");
            const store: StoreEntity = await StoreEntity.findOne(storeId, {relations: ["offers"]});
            if (!store) {
                logger.debug("[controllers/StoresController.ts - API - PUT /store/claim/:storeId]: " +
                    "Store not found");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            if (store.owner) {
                logger.debug("[controllers/StoresController.ts - API - PUT /store/claim/:storeId]: " +
                    "Store already claimed");
                res.status(403).send({message: res.__("store_already_claimed")});
                return;
            }

            if (store.siret !== null && (req.body.siret !== store.siret)) {
                logger.debug("[controllers/StoresController.ts - API - PUT /store/claim/:storeId]: " +
                    "Wrong siret");
                res.status(400).send({message: res.__("store_wrong_siret")});
                return;
            }

            store.owner = user;
            store.siret = req.body.siret;
            await store.save();
            await Elasticsearch.saveStore(store);
            logger.debug("[controllers/StoresController.ts - API - PUT /stores/:storeId/products]: " +
                "Send store information");
            res.status(200).send(store.toJSON());
        } catch (err) {
            next(err);
        }
    }
}
