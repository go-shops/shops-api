import {Request, Response} from "express";
import {OfferEntity, ProductEntity, StoreEntity} from "../entity";
import {AuthMiddleware, permissions} from "../utils";
import logger from "../utils/Logger";

export class OffersController {
    /**
     * Get all offers
     */
    public static async getOffers(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/OffersController.ts - API - GET /offers]: Someone is trying to get offers info");
            let page = parseInt(req.query.page, 10);
            let maxRes = parseInt(req.query.maxRes, 10);
            if (isNaN(page)) {
                page = 1;
            }
            if (isNaN(maxRes)) {
                maxRes = 25;
            }
            logger.debug("[controllers/OffersController.ts - API - GET /offers]: Search offers");
            const offers: OfferEntity[] = await OfferEntity.find(
                {skip: (page - 1) * maxRes, take: maxRes});
            if (offers === undefined) {
                logger.debug("[controllers/OffersController.ts - API - GET /offers]: Offer not found");
                res.status(404).send({message: res.__("offers_not_found")});
                return;
            }
            logger.debug("[controllers/OffersController.ts - API - GET /offers]: Send offers info");
            res.status(200).send(offers.map((o) => o.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get offer equal to id
     */
    public static async getOfferById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/OffersController.ts - API - GET /offers/:offerId]:" +
                " Someone is trying to get offer info");
            const {offerId} = req.params;

            logger.debug("[controllers/OffersController.ts - API - GET /offers/:offerId]: Search offer");
            const offer = await OfferEntity.findOne(offerId);
            if (!offer) {
                logger.debug("[controllers/OffersController.ts - API - GET /offers/:offerId]:" +
                    " Get offer failed: Offer not found");
                res.status(404).send({message: res.__("offer_not_found")});
                return;
            }
            logger.debug("[controllers/OffersController.ts - API - GET /offers/:offerId]: Send offer's information");
            res.send(offer.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Create new offer
     */
    public static async postOffer(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/OffersController.ts - API - POST /offers]: " +
                "Someone is trying to make new offers");

            const store: StoreEntity = await StoreEntity.findOne({where: {id: req.body.storeId}, relations: ["owner"]});
            if (!store) {
                logger.debug("[controllers/OffersController.ts - API - POST /offers]:" +
                    "Creating offer failed, the store doesn't seem to exist");
                res.status(404).send({message: res.__("store_not_found")});
                return;
            }

            if (!((!store.owner && AuthMiddleware.userHavePermission(req.user, permissions.Offer.Create.NoOwner)) ||
                (req.user.id === store.owner.id &&
                    AuthMiddleware.userHavePermission(req.user, permissions.Offer.Create.OwnStore)) ||
                (AuthMiddleware.userHavePermission(req.user, permissions.Offer.Create.All)))) {
                logger.debug("[controllers/OffersController.ts - API - POST /offers]: " +
                    "User don't have the necessary permission to post an offer");
                res.status(403).send({message: res.__("offers_missing_permission_post")});
                return;
            }

            const existing: OfferEntity = await OfferEntity.findOne(
                {where: {name: req.body.name, description: req.body.description,
                        discount: req.body.discount, productId: req.body.productId, storeId: req.body.storeId}});
            if (existing) {
                logger.debug("[controllers/OffersController.ts - API - POST /offers]: " +
                    "Creating offer failed, this offer exist");
                res.status(400).send({message: res.__("offers_already_exist")});
                return;
            }

            const product: ProductEntity = await ProductEntity.findOne( {where: {id: req.body.productId}});
            if (!product) {
                logger.debug("[controllers/OffersController.ts - API - POST /offers]:" +
                    "Creating offer failed there is not product with this id");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }

            logger.debug("[controllers/OffersController.ts - API - POST /offers]: Save offer information");
            let offer: OfferEntity = await OfferEntity.saveInDb(req.body, product, store);
            logger.debug("[controllers/OffersController.ts - API - POST /offers]: Send offer information");
            offer = await OfferEntity.findOne({where: {id: offer.id}, relations: ["store"]});
            res.status(201).send(offer.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit offer with id equal to :id
     */
    public static async putOffer(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/ProductsController.ts - API - PUT /offers/:offerId]: " +
                "Someone is trying to edit an offer");
            const {offerId} = req.params;

            logger.debug("[controllers/ProductsController.ts - API - PUT /offers/:offerId]: Search Offer");
            const offer = await OfferEntity.findOne(offerId, {relations: ["store", "store.owner"]});
            if (!offer) {
                logger.debug("[controllers/ProductsController.ts - API - PUT /offers/:offerId]: " +
                    "Offer not found");
                res.status(404).send({message: res.__("offer_not_found")});
                return;
            }

            if (!((!offer.store.owner && AuthMiddleware.userHavePermission(req.user, permissions.Offer.Create.NoOwner))
                || (req.user.id === offer.store.owner.id &&
                    AuthMiddleware.userHavePermission(req.user, permissions.Offer.Create.OwnStore)) ||
                (AuthMiddleware.userHavePermission(req.user, permissions.Offer.Create.All)))) {
                logger.debug("[controllers/OffersController.ts - API - PUT /offers/:offerId]: " +
                    "User don't have the necessary permission to post an offer");
                res.status(403).send({message: res.__("offers_missing_permission_edit")});
                return;
            }

            logger.debug("[controllers/ProductsController.ts - API - PUT /offers/:offerId]: Save offers information");
            const tmp: OfferEntity = await OfferEntity.updateInDb(
                {...req.body, store: offer.store, product: offer.product}, offer);
            logger.debug("[controllers/ProductsController.ts - API - PUT /offers/:offerId]: Send offers information");
            res.status(200).send(tmp.toJSON());
        } catch (err) {
            next(err);
        }
    }

    public static async getProductOffer(req: Request, res: Response, next) {
        try {
            let page = parseInt(req.query.page, 10);
            let maxRes = parseInt(req.query.maxRes, 10);
            if (isNaN(page)) {
                page = 1;
            }
            if (isNaN(maxRes)) {
                maxRes = 25;
            }
            logger.debug("[controllers/OffersController.ts - API - GET /offers/product/:productId]: " +
                "Someone is trying to get all offers of a product");
            const {productId} = req.params;

            logger.debug("[controllers/OffersController.ts - API - GET /offers/product/:productId]: " +
                "Search offers");
            const offers = await OfferEntity.find({skip: (page - 1) * maxRes, take: maxRes,
                where: {product: productId}, relations: ["store"]});
            if (!offers) {
                logger.debug("[controllers/OffersController.ts - API - GET /offers/product/:productId]: " +
                    "Offers not found");
                res.status(404).send({message: "No such offers"});
                return;
            }
            logger.debug("[controllers/OffersController.ts - API - GET /offers/product/:productId]: " +
                "Send Offers information");
            res.status(200).send(offers.map((o) => o.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Delete Offer with id equal to :id
     */
    public static async deleteOffer(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/OffersController.ts - API - DELETE /offers/:id]:" +
                " Someone is trying to delete offers info");
            const {offerId} = req.params;

            logger.debug("[controllers/OffersController.ts - API - DELETE /offers/:id]:" +
                " Search Offer");
            const offer: OfferEntity = await OfferEntity.findOne(offerId, {relations: ["store", "store.owner"]});
            if (!offer) {
                logger.debug("[controllers/OffersController.ts - API - DELETE /offers/:id]:" +
                    " Delete failed : Offer not found");
                res.status(404).send({message: res.__("offer_not_found")});
                return;
            }

            logger.debug("[controllers/OffersController.ts - API - DELETE /offers/:id]:" +
                " Check if user is allowed to delete this offer");
            if ((req.user.id === offer.store.owner.id &&
                !AuthMiddleware.userHavePermission(req.user, permissions.Offer.Delete.OwnStore)) ||
                (offer.store.owner.id !== req.user.id &&
                    !AuthMiddleware.userHavePermission(req.user, permissions.Offer.Delete.All))) {
                logger.debug("[controllers/OffersController.ts - API - DELETE /offers/:offerId]: " +
                    "User don't have the necessary permission to post an offer");
                res.status(403).send({message: res.__("offers_missing_permission_delete")});
                return;
            }

            await offer.remove();
            logger.debug("[controllers/OffersController.ts - API - DELETE /offers/:id]:" +
                " Delete success: send information to user");
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    }
}
