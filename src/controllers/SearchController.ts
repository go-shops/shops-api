import {Request, Response} from "express";
import Constants from "../Constants";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

export class SearchController {
    /**
     *  Perform search store on elasticsearch
     */
    public static async postSearchStores(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/SearchController.ts - API - POST /search/stores]:" +
                "Someone is trying to search a store");

            let sort = {};
            if (req.body.sort === "score") {
                sort = {_score: req.body.orderType};
            } else if (req.body.sort === "mark") {
                sort = {avgMark: req.body.orderType};
            } else if (req.body.sort === "alphabetical") {
                sort = {"name.raw": req.body.orderType};
            } else if (req.body.sort === "geo") {
              sort = {_geo_distance:
                      {"address.location": req.body.location, "order": req.body.orderType, "unit": req.body.geoUnit }};
            } else {
                sort = {_score: "desc"};
            }

            const search = {
                index: Constants.ELASTIC_STORES_INDEX,
                body: {
                    from: req.body.page * req.body.size - req.body.size,
                    size: req.body.size,
                    sort,
                    query: {
                        query_string: {
                            query: req.body.query + "*",
                            fields: ["name^3", "categories.name^2", "description"],
                            fuzziness: "AUTO",
                        },
                    },
                },
            };

            const searchResult = await Elasticsearch.search(search);
            res.status(200).send(searchResult);
        } catch (err) {
            next(err);
        }
    }

    /**
     *  Perform search product on elasticsearch
     */
    public static async postSearchProducts(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/SearchController.ts - API - POST /search/products]:" +
                "Someone is trying to search a product");

            let sort = {};
            if (req.body.sort === "score") {
                sort = {_score: req.body.orderType};
            } else if (req.body.sort === "mark") {
                sort = {avgMark: req.body.orderType};
            } else if (req.body.sort === "alphabetical") {
                sort = {"name.raw": req.body.orderType};
            } else {
                sort = {_score: "desc"};
            }

            const search = {
                index: Constants.ELASTIC_PRODUCTS_INDEX,
                body: {
                    from: req.body.page * req.body.size - req.body.size,
                    size: req.body.size,
                    sort,
                    query: {
                        query_string: {
                            query: req.body.query + "*",
                            fields: ["name^4", "brand.name^3", "tags.name^2", "description"],
                            fuzziness: "AUTO",
                        },
                    },
                },
            };

            const searchResult = await Elasticsearch.search(search);
            res.status(200).send(searchResult);
        } catch (err) {
            next(err);
        }
    }

    /**
     *  Perform search user on elasticsearch
     */
    public static async postSearchUsers(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/SearchController.ts - API - POST /search/users]:" +
                "Someone is trying to search a user");

            let sort = {};
            const orderType = req.body.orderType ? req.body.orderType : "desc";
            if (req.body.sort === "score") {
                sort = {_score: orderType};
            } else if (req.body.sort === "alphabetical") {
                sort = {"username.raw": orderType};
            } else {
                sort = {_score: orderType};
            }

            const search = {
                index: Constants.ELASTIC_USERS_INDEX,
                body: {
                    from: req.body.page * req.body.size - req.body.size,
                    size: req.body.size,
                    sort,
                    query: {
                        query_string: {
                            query: req.body.query + "*",
                            fields: ["username^2", "bio"],
                            fuzziness: "AUTO",
                        },
                    },
                },
            };

            const searchResult = await Elasticsearch.search(search);
            res.status(200).send(searchResult);
        } catch (err) {
            next(err);
        }
    }

    /**
     * Perform search tags on Elasticsearch
     */
    public static async postSearchTags(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/SearchController.ts - API - POST /search/tags]:" +
                "Someone is trying to search a tag");

            let sort = {};
            if (req.body.sort === "score") {
                sort = {_score: req.body.orderType};
            } else if (req.body.sort === "alphabetical") {
                sort = {"name.raw": req.body.orderType};
            } else {
                sort = {_score: "desc"};
            }

            const search = {
                index: Constants.ELASTIC_TAGS_INDEX,
                body: {
                    from: req.body.page * req.body.size - req.body.size,
                    size: req.body.size,
                    sort,
                    query: {
                        query_string: {
                            query: req.body.query + "*",
                            fields: ["name"],
                            fuzziness: "AUTO",
                        },
                    },
                },
            };

            const searchResult = await Elasticsearch.search(search);
            res.status(200).send(searchResult);
        } catch (err) {
            next(err);
        }
    }

    /**
     * Perform search brands on Elasticsearch
     */
    public static async postSearchBrands(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/SearchController.ts - API - POST /search/brands]:" +
                "Someone is trying to search a brand");

            let sort = {};
            if (req.body.sort === "score") {
                sort = {_score: req.body.orderType};
            } else if (req.body.sort === "alphabetical") {
                sort = {"name.raw": req.body.orderType};
            } else {
                sort = {_score: "desc"};
            }

            const search = {
                index: Constants.ELASTIC_BRANDS_INDEX,
                body: {
                    from: req.body.page * req.body.size - req.body.size,
                    size: req.body.size,
                    sort,
                    query: {
                        query_string: {
                            query: req.body.query + "*",
                            fields: ["name"],
                            fuzziness: "AUTO",
                        },
                    },
                },
            };

            const searchResult = await Elasticsearch.search(search);
            res.status(200).send(searchResult);
        } catch (err) {
            next(err);
        }
    }

    /**
     * Perform search categories on Elasticsearch
     */
    public static async postSearchCategories(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/SearchController.ts - API - POST /search/categories]:" +
                "Someone is trying to search a category");

            let sort = {};
            if (req.body.sort === "score") {
                sort = {_score: req.body.orderType};
            } else if (req.body.sort === "alphabetical") {
                sort = {"name.raw": req.body.orderType};
            } else {
                sort = {_score: "desc"};
            }

            const search = {
                index: Constants.ELASTIC_BRANDS_INDEX,
                body: {
                    from: req.body.page * req.body.size - req.body.size,
                    size: req.body.size,
                    sort,
                    query: {
                        query_string: {
                            query: req.body.query + "*",
                            fields: ["name"],
                            fuzziness: "AUTO",
                        },
                    },
                },
            };

            const searchResult = await Elasticsearch.search(search);
            res.status(200).send(searchResult);
        } catch (err) {
            next(err);
        }
    }

    /**
     * Perform search permissions on Elasticsearch
     */
    public static async postSearchPermissions(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/SearchController.ts - API - POST /search/permissions]:" +
                "Someone is trying to search a permissions");

            let sort = {};
            if (req.body.sort === "score") {
                sort = {_score: req.body.orderType};
            } else if (req.body.sort === "alphabetical") {
                sort = {"permission.raw": req.body.orderType};
            } else {
                sort = {_score: "desc"};
            }

            const search = {
                index: Constants.ELASTIC_BRANDS_INDEX,
                body: {
                    from: req.body.page * req.body.size - req.body.size,
                    size: req.body.size,
                    sort,
                    query: {
                        query_string: {
                            query: req.body.query + "*",
                            fields: ["name"],
                            fuzziness: "AUTO",
                        },
                    },
                },
            };

            const searchResult = await Elasticsearch.search(search);
            res.status(200).send(searchResult);
        } catch (err) {
            next(err);
        }
    }
}
