import {Request, Response} from "express";
import logger from "../utils/Logger";

export class HealthController {
    public static async getHealth(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/HealthController.ts - API - GET /health]: " +
                "Someone is trying to get api status");
            logger.debug("[controllers/HealthController.ts - API - GET /health]: Send api status");
            res.status(200).send({message: "Ok"});

        } catch (err) {
            next(err);
        }
    }
}
