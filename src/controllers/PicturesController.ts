import {Request, Response} from "express";
import fileType from "file-type";
import fs from "fs";
import Randomstring from "randomstring";
import Constants from "../Constants";
import logger from "../utils/Logger";

export class PicturesController {
    /**
     * Post pictures
     */
    public static async postPictures(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/PicturesController.ts - API - POST /pictures]: " +
            "Someone is trying to post a picture");
            const pictureName: string = Randomstring.generate(16);
            const allowedExtension: string[] = ["jpg", "jpeg", "gif", "png", "bmp", "svg"];
            const type = fileType(Buffer.from(req.body.picture, "base64"));
            if (!allowedExtension.includes(type.ext)) {
                logger.error("[controllers/PicturesController.ts - API - POST /pictures]: " +
                "File extension not allowed");
                res.status(403).send({message: res.__("pictures_extension_not_allowed")});
                return;
            }
            fs.writeFile(Constants.IMG_FOLDER + "/" + pictureName + "." + type.ext,
                        req.body.picture, "base64", (err) => {
                if (err) {
                    logger.error("[controllers/PicturesController.ts - API - POST /pictures]: " +
                    "Can't write picture file: " + err.message);
                    res.status(500).send({message: res.__("system_something_went_wrong")});
                    return;
                }
                logger.debug("[controllers/PicturesController.ts - API - POST /pictures]: Send picture link");
                res.status(201).send({url: Constants.IMG_BASE_LINK + "/" + pictureName + "." + type.ext});
              });
        } catch (err) {
            next(err);
        }
    }
}
