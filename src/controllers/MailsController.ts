import {Request, Response} from "express";
import nodemailer from "nodemailer";
import * as util from "util";
import Constants from "../Constants";
import logger from "../utils/Logger";

export class MailsController {
    /**
     * Send email
     */
    public static async postMail(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/mailsController.ts - API - POST /mails/]: Someone is trying send a mail");

            const transporter = nodemailer.createTransport({
                host: Constants.MAIL_HOST,
                port: Constants.MAIL_PORT,
                auth: {
                    user: Constants.MAIL_USER,
                    pass: Constants.MAIL_PASS,
                },
                tls: {rejectUnauthorized: false},
            });

            logger.debug("[controllers/mailsController.ts - API - POST /mails/]: Creating mail from the request body");
            const mailOptions = {
                from: req.body.email,
                to: Constants.MAIL,
                subject: req.body.subject,
                text: req.body.content,
            };

            logger.debug("[controllers/mailsController.ts - API - POST /mails/]: Sending mail");
            const info = await transporter.sendMail(mailOptions);
            logger.debug("[controllers/mailsController.ts - API - POST /mails/]: Mail sent: %s", util.inspect(info));
            res.status(200).send({message: res.__("email_successful_sent")});
        } catch (err) {
            next(err);
        }
    }
}
