import {Request, Response} from "express";
import Constants from "../Constants";
import {OfferEntity, PictureEntity, ProductEntity, UserEntity} from "../entity";
import {AuthMiddleware, Elasticsearch, permissions} from "../utils";
import logger from "../utils/Logger";

export class ProductsController {
    /**
     * Get all products
     */
    public static async getProducts(req: Request, res: Response, next) {
        try {
            let page = parseInt(req.query.page, 10);
            let maxRes = parseInt(req.query.maxRes, 10);
            if (isNaN(page)) {
                page = 1;
            }
            if (isNaN(maxRes)) {
                maxRes = 25;
            }
            logger.debug("[controllers/ProductsController.ts - API - GET /products/]: " +
                "Someone is trying to get all products");
            logger.debug("[controllers/ProductsController.ts - API - GET /products/]: Search all products");
            const products = await ProductEntity.find(
                {skip: (page - 1) * maxRes, take: maxRes, relations: ["stores"]});
            if (products === undefined) {
                logger.debug("[controllers/ProductsController.ts - API - GET /products/]: " +
                    "Get products failed : Products not found");
                res.status(404).send({message: res.__("products_not_found")});
                return;
            }
            logger.debug("[controllers/ProductsController.ts - API - GET /products/]: Send product's information");
            res.send(products.map((p) => p.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get product comments list
     */
    public static async getProductComments(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/StoresController.ts - API - GET /products/:productId/comments]: " +
                "Someone is trying to get product comments list");
            const {productId} = req.params;
            logger.debug("[controllers/StoresController.ts - API - GET /products/:productId/comments]: Search product");
            const product: ProductEntity = await ProductEntity.findOne(productId,
                {relations: ["comments", "comments.owner"]});
            if (!product) {
                logger.debug("[controllers/StoresController.ts - API - GET /products/:productId/comments]: " +
                    "Product not found");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }
            logger.debug("[controllers/StoresController.ts - API - GET /products/:productId/comments]: " +
                "Send comments information");
            res.status(200).send(product.comments.map((c) => c.toJSON()));
        } catch (err) {
            next(err);
        }
    }

    /**
     * Get products with id equal to :id
     */
    public static async getProductById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/ProductsController.ts - API - GET /products/:id]: " +
                "Someone is trying to get product info");
            const {productId} = req.params;

            logger.debug("[controllers/ProductsController.ts - API - GET /products/:id]: Search product");
            const product = await ProductEntity.findOne(productId, {relations: ["stores"]});
            if (!product) {
                logger.debug("[controllers/ProductsController.ts - API - GET /products/:id]: " +
                    "Get product failed : Product not found");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }
            logger.debug("[controllers/ProductsController.ts - API - GET /products/:id]: Send product's information");
            res.send(product.toJSON());
        } catch (err) {
            next(err);
        }
    }

    public static async getProductOffers(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/ProductsController.ts - API - GET /product/:productId/offers]: " +
                "Someone is trying get all offers of a product");
            const {productId} = req.params;

            logger.debug("[controllers/ProductsController.ts - API - GET /product/:productId/offers]: " +
                "Search product");
            const product = await ProductEntity.findOne(productId);
            if (!product) {
                logger.debug("[controllers/ProductsController.ts - API - GET /product/:productId/offers]: " +
                    "Get offers failed: Product not found");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }

            logger.debug("[controllers/ProductsController.ts - API - GET /product/:productId/offers]: " +
                "Search offers");
            const offers = await OfferEntity.find({where: {product: productId}});
            if (!offers) {
                logger.debug("[controllers/ProductsController.ts - API - GET /product/:productId/offers]: " +
                    "Get offers failed: Offers not found");
                res.status(404).send({message: res.__("offer_not_found")});
                return;
            }
            logger.debug("[controllers/ProductsController.ts - API - GET /product/:productId/offers]: " +
                "Send offers information");
            res.status(200).send(offers.map((o) => o.toJSON()));
        } catch (err) {
            next (err);
        }
    }

    /**
     * Create new product
     */
    public static async postProduct(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/ProductsController.ts - API - POST /products]: " +
                "Someone is trying to create a new product");

            const user: UserEntity = await UserEntity.findOne(req.user.id, {relations: ["stores", "permissions"]});
            if (!user) {
                logger.debug("[controllers/ProductsController.ts - API - POST /products]: " +
                    "User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }

            if (!((user.stores.length > 0 &&
                AuthMiddleware.userHavePermission(user, permissions.Product.Create.OwnStore))
                || (AuthMiddleware.userHavePermission(user, permissions.Product.Create.All)))) {
                logger.debug("[controllers/ProductsController.ts - API - POST /products]: " +
                    "User don't have the necessary permission to post a product");
                res.status(403).send({message: res.__("products_missing_permission_post")});
                return;
            }

            logger.debug("[controllers/ProductsController.ts - API - POST /products]: Search product");
            const product = await ProductEntity.findOne({where: {name: req.body.name}, relations: ["stores"]});
            if (product) {
                logger.debug("[controllers/ProductsController.ts - API - POST /products]: " +
                    "Product already exist");
                res.status(400).send({message: res.__("products_already_exist")});
                return;
            }

            logger.debug("[controllers/ProductsController.ts - API - POST /products]: Save products information");
            const tmp: ProductEntity[] = await ProductEntity.saveAllInDb([{...req.body}]);
            logger.debug("[controllers/ProductsController.ts - API - POST /products]: Send products information");
            res.status(201).send(tmp[0].toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Patch product with id equal to :id
     */
    public static async patchProduct(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/ProductsController.ts - API - PATCH /products/:id]: " +
                "Someone is trying to edit a product");
            const {productId} = req.params;

            logger.debug("[controllers/ProductsController.ts - API - PATCH /products/:id]: Search product");
            const product = await ProductEntity.findOne(productId);
            if (!product) {
                logger.debug("[controllers/ProductsController.ts - API - PATCH /products/:id]: " +
                    "Product not found");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }

            logger.debug("[controllers/ProductsController.ts - API - PATCH /products/:id]: Save products information");
            const tmp: ProductEntity = await ProductEntity.patchInDb(product, {...req.body});
            logger.debug("[controllers/ProductsController.ts - API - PATCH /products/:id]: Send products information");
            res.status(200).send(tmp.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Edit product with id equal to :id
     */
    public static async putProduct(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/ProductsController.ts - API - PUT /products/:id]: " +
                "Someone is trying to edit a product");
            const {productId} = req.params;

            logger.debug("[controllers/ProductsController.ts - API - PUT /products/:id]: Search product");
            const product = await ProductEntity.findOne(productId);
            if (!product) {
                logger.debug("[controllers/ProductsController.ts - API - PUT /products/:id]: " +
                    "Product not found");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }

            logger.debug("[controllers/ProductsController.ts - API - PUT /products/:id]: Save products information");
            const tmp: ProductEntity = await ProductEntity.updateInDb(product, {...req.body});
            logger.debug("[controllers/ProductsController.ts - API - PUT /products/:id]: Send products information");
            res.status(200).send(tmp.toJSON());
        } catch (err) {
            next(err);
        }
    }

    /**
     * Delete products with id equal to :id
     */
    public static async deleteProductById(req: Request, res: Response, next) {
        try {
            logger.debug("[controllers/ProductsController.ts - API - DELETE /products/:id]: " +
                "Someone is trying to delete a product");
            const {productId} = req.params;

            logger.debug("[controllers/ProductsController.ts - API - DELETE /products/:id]: Search product");
            const product = await ProductEntity.findOne(productId, {relations: ["stores"]});
            if (!product) {
                logger.debug("[controllers/productsController.ts - API - DELETE /products/:id]: " +
                    "Delete failed : Product not found");
                res.status(404).send({message: res.__("product_not_found")});
                return;
            }

            await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX, Constants.ELASTIC_PRODUCTS_TYPE, product.id);
            await PictureEntity.remove(product.pictures);
            await product.remove();
            logger.debug("[controllers/productsController.ts - API - DELETE /products/:id]: " +
                "Delete success: send information to user");
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    }
}
