import {ProductCommentEntity, ProductEntity, StoreCommentEntity, StoreEntity} from "../entity";
import {Elasticsearch} from "./Elasticsearch";
import logger from "./Logger";

export class Utils {
    public static async updateAvgMarkStore(comments: StoreCommentEntity[], store: StoreEntity): Promise<StoreEntity> {
        logger.debug("[utils/Utils.ts - updateAvgMarkStore]: Update average store mark");
        let sumMark = 0;
        for (const comment of comments) {
            sumMark += comment.mark;
        }
        let tmpMark = 0;
        if (comments.length > 0) {
            tmpMark = sumMark / comments.length;
        }

        let prodMark = 0;
        let nbOfMarks = 0;
        let avgProdMark = 0;
        for (const product of store.products) {
            for (const comment of product.comments) {
                nbOfMarks += 1;
                prodMark += comment.mark;
            }
        }
        if (store.products.length > 0) {
            avgProdMark = prodMark / nbOfMarks;
        }

        const avgMark = (tmpMark + avgProdMark) / 2;
        logger.debug("[utils/Utils.ts - updateAvgMarkStore]: Average mark -> %s", avgMark);
        store.applyJSON({...store, avgMark});
        await store.save();
        await Elasticsearch.saveStore(store);
        return store;
    }

    public static async updateAvgMarkProduct(
        comments: ProductCommentEntity[],
        product: ProductEntity): Promise<ProductEntity> {
        logger.debug("[utils/Utils.ts - updateAvgMarkProduct]: Update average product mark");
        let sumMark = 0;
        for (const comment of comments) {
            sumMark += comment.mark;
        }
        let avgMark = 0;
        if (comments.length > 0) {
            avgMark = sumMark / comments.length;
        }
        logger.debug("[utils/Utils.ts - updateAvgMarkProduct]: Average mark -> %s", avgMark);
        product.applyJSON({...product, avgMark});
        await product.save();
        await Elasticsearch.saveProduct(product);
        return product;
    }

    public static transformObjectInStringArray(obj, arr: string[]): string[] {
        Object.values(obj).forEach((value) => {
            if (typeof value !== "string") {
                this.transformObjectInStringArray(value, arr);
            } else {
                arr.push(value);
            }
        });
        return arr;
    }

    public static sleep(ms) {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    }
}
