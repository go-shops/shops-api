export  class JoiError {
    public static buildError(err, res): string {
        let result = "";
        for (const detail of err.details) {
            const type = detail.type;
            let field = detail.path.join("_");
            if (result.length !== 0) {
                result += "\n";
            }
            if (type === "object.allowUnknown") {
                result += res.__("joi_%s_" + type, field);
            } else if (field.match(/\d+/) != null) {
                const match = field.match(/\d+/)[0];
                field  = field.replace(/\d+/, "%s");
                result += res.__("joi_" + field + "_" + type, match);
            } else {
                result += res.__("joi_" + field + "_" + type);
            }
        }
        return result;
    }
}

/**
 * {
 *  type: what type of error was thrown?,
 *  field: what field has the error?,
 *  errorMsg: what error message is shown?
 * }
 */
