import joiValidation from "express-joi-validation";

const validate = joiValidation({
    passError: true,
});

export default validate;
