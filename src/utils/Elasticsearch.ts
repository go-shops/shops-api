import elasticsearch from "elasticsearch";
import * as util from "util";
import Constants from "../Constants";
import {
    BrandEntity,
    CategoryEntity,
    PermissionEntity,
    ProductEntity,
    StoreEntity,
    TagEntity,
    UserEntity,
} from "../entity";
import logger from "../utils/Logger";

export class Elasticsearch {

    public static async saveProduct(product: ProductEntity) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });

            await client.index({
                index: Constants.ELASTIC_PRODUCTS_INDEX,
                type: Constants.ELASTIC_PRODUCTS_TYPE,
                id: product.id.toString(),
                body: {
                    id: product.id,
                    name: product.name,
                    description: product.description,
                    avgMark: product.avgMark,
                    brand: product.brand,
                    pictures: product.pictures,
                    tags: product.tags,
                },
            });
            logger.debug("[Elasticsearch.ts - saveProduct]: new product registered to Elasticsearch");
        } catch (e) {
            logger.error("[Elasticsearch.ts - saveProduct]: %s", e.message);
        }
    }

    public static async saveUser(user: UserEntity) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });

            await client.index({
                index: Constants.ELASTIC_USERS_INDEX,
                type: Constants.ELASTIC_USERS_TYPE,
                id: user.id.toString(),
                body: {
                    id: user.id,
                    username: user.username,
                    role: user.role,
                    bio: user.bio || null,
                    banner: user.banner || null,
                    picture: user.picture || null,
                    active: user.active,
                },
            });
            logger.debug("[Elasticsearch.ts - saveUser]: new user registered to Elasticsearch");
        } catch (e) {
            logger.error("[Elasticsearch.ts - saveUser]: %s", e.message);
        }
    }

    public static async saveStore(store: StoreEntity) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });

            let location = null;
            if (store.address.lat && store.address.lon) {
                location = {
                    lat: store.address.lat,
                    lon: store.address.lon,
                };
            }
            await client.index({
                index: Constants.ELASTIC_STORES_INDEX,
                type: Constants.ELASTIC_STORES_TYPE,
                id: store.id.toString(),
                body: {
                    id: store.id,
                    name: store.name,
                    description: store.description,
                    avgMark: store.avgMark,
                    address: {
                        streetNbr: store.address.streetNbr,
                        street: store.address.street,
                        zipCode: store.address.zipCode,
                        city: store.address.city,
                        country: store.address.country,
                        location,
                    },
                    hours: store.hours,
                    categories: store.categories,
                    pictures: store.pictures,
                    nbProducts: store.nbProducts,
                },
            });
            logger.debug("[Elasticsearch.ts - saveStore]: new store registered to Elasticsearch");
        } catch (e) {
            logger.error("[Elasticsearch.ts - saveStore]: %s", e.message);
        }
    }

    public static async saveTag(tag: TagEntity) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });
            await client.index({
                index: Constants.ELASTIC_TAGS_INDEX,
                type: Constants.ELASTIC_TAGS_TYPE,
                id: tag.id.toString(),
                body: {
                    id: tag.id,
                    name: tag.name,
                },
            });
            logger.debug("[Elasticsearch.ts - saveTag]: new tag registered to Elasticsearch");
        } catch (e) {
            logger.error("[Elasticsearch.ts - saveTag]: %s", e.message);
        }
    }

    public static async saveBrand(brand: BrandEntity) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });
            await client.index({
                index: Constants.ELASTIC_BRANDS_INDEX,
                type: Constants.ELASTIC_BRANDS_TYPE,
                id: brand.id.toString(),
                body: {
                    id: brand.id,
                    name: brand.name,
                },
            });
            logger.debug("[Elasticsearch.ts - saveBrand]: new brand registered to Elasticsearch");
        } catch (e) {
            logger.error("[Elasticsearch.ts - saveBrand]: %s", e.message);
        }
    }

    public static async saveCategory(category: CategoryEntity) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });
            await client.index({
                index: Constants.ELASTIC_CATEGORIES_INDEX,
                type: Constants.ELASTIC_CATEGORIES_TYPE,
                id: category.id.toString(),
                body: {
                    id: category.id,
                    name: category.name,
                },
            });
            logger.debug("[Elasticsearch.ts - saveCategory]: new category registered to Elasticsearch");
        } catch (e) {
            logger.error("[Elasticsearch.ts - saveCategory]: %s", e.message);
        }
    }

    public static async savePermission(permission: PermissionEntity) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });
            await client.index({
                index: Constants.ELASTIC_PERMISSIONS_INDEX,
                type: Constants.ELASTIC_PERMISSIONS_TYPE,
                id: permission.id.toString(),
                body: {
                    id: permission.id,
                    name: permission.permission,
                },
            });
            logger.debug("[Elasticsearch.ts - savePermission]: new permission registered to Elasticsearch");
        } catch (e) {
            logger.error("[Elasticsearch.ts - savePermission]: %s", e.message);
        }
    }

    public static async delete(index: string, type: string, id: number) {
        try {
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });

            await client.delete({
                index,
                type,
                id: id.toString(),
            });
            logger.debug("[Elasticsearch.ts - delete]: %s deleted from Elasticsearch", type);
        } catch (e) {
            logger.error("[Elasticsearch.ts - delete]: %s", e.message);
        }
    }

    public static async search(obj) {
        const client = new elasticsearch.Client({
            host: Constants.ELASTIC_HOST,
        });

        const result = await client.search(obj);
        logger.debug("[Elasticsearch.ts - search]: result of the query is: %s", util.inspect(result.hits.hits));
        const res = [];
        for (let i = 0; i < result.hits.hits.length; i++) {
            let sort = null;
            if (result.hits.hits[i].sort
                && result.hits.hits[i].sort.length > 0
                && typeof result.hits.hits[i].sort[0] === "number") {
                sort = result.hits.hits[i].sort[0];
            }
            res[i] = {...result.hits.hits[i]._source, sort};
        }
        return res;
    }

    public static async ensureIndices() {
        try {
            logger.debug("[Elasticsearch.ts - ensureIndices]: create new index in Elasticsearch");
            const client = new elasticsearch.Client({
                host: Constants.ELASTIC_HOST,
            });

            const stores: boolean = await client.indices.exists({index: Constants.ELASTIC_STORES_INDEX});
            if (stores) {
                logger.debug("[Elasticsearch.ts - ensureIndices]: stores index already exist");
            } else {
                await client.indices.create({
                    index: Constants.ELASTIC_STORES_INDEX,
                    body: {
                        settings: {
                            number_of_shards: 1,
                            analysis: {
                                filter: {edge_ngram_filter: {type: "edge_ngram", min_gram: 1, max_gram: 64}},
                                analyzer: {
                                    edge_ngram_analyzer: {
                                        type: "custom",
                                        tokenizer: "standard",
                                        filter: ["lowercase", "edge_ngram_filter"],
                                    },
                                    case_insensitive_sort: {
                                        tokenizer: "keyword",
                                        filter: ["lowercase"],
                                    },
                                },
                            },
                        },
                        mappings: {
                            store: {
                                properties: {
                                    name: {
                                        type: "text",
                                        analyzer: "edge_ngram_analyzer",
                                        search_analyzer: "standard",
                                        fields: {
                                            raw: {
                                                type: "text",
                                                analyzer: "case_insensitive_sort",
                                                fielddata: true,
                                            },
                                        },
                                    },
                                    description: {
                                        type: "text",
                                        analyzer: "edge_ngram_analyzer",
                                        search_analyzer: "standard",
                                    },
                                    avgMark: {type: "float", null_value: "0"},
                                    owner: {
                                      properties: {
                                          username: {type: "text"},
                                          bio: {type: "text"},
                                          picture: {type: "text"},
                                      },
                                    },
                                    address: {
                                        properties: {
                                            streetNbr: {type: "integer"},
                                            street: {type: "text"},
                                            zipCode: {type: "text"},
                                            city: {type: "text"},
                                            country: {type: "text"},
                                            location: {type: "geo_point"},
                                        },
                                    },
                                    hours: {
                                        properties: {
                                            day: {type: "text"},
                                            timeOpen: {type: "text"},
                                            timeClosed: {type: "text"},
                                        },
                                    },
                                    categories: {
                                        properties: {
                                            name: {
                                                type: "text",
                                                analyzer: "edge_ngram_analyzer",
                                                search_analyzer: "standard",
                                            },
                                        },
                                    },
                                    pictures: {
                                        properties: {
                                            name: {type: "text"},
                                            description: {type: "text"},
                                            url: {type: "text"},
                                        },
                                    },
                                    nbProducts: {type: "integer", null_value: "0"},
                                },
                            },
                        },
                    },
                });
                logger.debug("[Elasticsearch.ts - ensureIndices]: stores index created");
            }

            const products: boolean = await client.indices.exists({index: Constants.ELASTIC_PRODUCTS_INDEX});
            if (products) {
                logger.debug("[Elasticsearch.ts - ensureIndices]: products index already exist");
            } else {
                await client.indices.create({
                    index: Constants.ELASTIC_PRODUCTS_INDEX,
                    body: {
                        settings: {
                            number_of_shards: 1,
                            analysis: {
                                filter: {edge_ngram_filter: {type: "edge_ngram", min_gram: 1, max_gram: 64}},
                                analyzer: {
                                    edge_ngram_analyzer: {
                                        type: "custom",
                                        tokenizer: "standard",
                                        filter: ["lowercase", "edge_ngram_filter"],
                                    },
                                    case_insensitive_sort: {
                                        tokenizer: "keyword",
                                        filter: ["lowercase"],
                                    },
                                },
                            },
                        },
                        mappings: {
                            product: {
                                properties: {
                                    name: {
                                        type: "text",
                                        analyzer: "edge_ngram_analyzer",
                                        search_analyzer: "standard",
                                        fields: {
                                            raw: {
                                                type: "text",
                                                analyzer: "case_insensitive_sort",
                                                fielddata: true,
                                            },
                                        },
                                    },
                                    description: {
                                        type: "text",
                                        analyzer: "edge_ngram_analyzer",
                                        search_analyzer: "standard",
                                    },
                                    avgMark: {type: "float", null_value: "0"},
                                    brand: {
                                        properties: {
                                            name: {
                                                type: "text",
                                                analyzer: "edge_ngram_analyzer",
                                                search_analyzer: "standard",
                                            },
                                        },
                                    },
                                    pictures: {
                                        properties: {
                                            name: {type: "text"},
                                            description: {type: "text"},
                                            url: {type: "text"},
                                        },
                                    },
                                    tags: {
                                        properties: {
                                            name: {
                                                type: "text",
                                                analyzer: "edge_ngram_analyzer",
                                                search_analyzer: "standard",
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                });
                logger.debug("[Elasticsearch.ts - ensureIndices]: products index created");
            }

            const users: boolean = await client.indices.exists({index: Constants.ELASTIC_USERS_INDEX});
            if (users) {
                logger.debug("[Elasticsearch.ts - ensureIndices]: users index already exist");
            } else {
                await client.indices.create({
                    index: Constants.ELASTIC_USERS_INDEX,
                    body: {
                        settings: {
                            number_of_shards: 1,
                            analysis: {
                                filter: {edge_ngram_filter: {type: "edge_ngram", min_gram: 1, max_gram: 64}},
                                analyzer: {
                                    edge_ngram_analyzer: {
                                        type: "custom",
                                        tokenizer: "standard",
                                        filter: ["lowercase", "edge_ngram_filter"],
                                    },
                                    case_insensitive_sort: {
                                        tokenizer: "keyword",
                                        filter: ["lowercase"],
                                    },
                                },
                            },
                        },
                        mappings: {
                            user: {
                                properties: {
                                    username: {
                                        type: "text",
                                        analyzer: "edge_ngram_analyzer",
                                        search_analyzer: "standard",
                                        fields: {
                                            raw: {
                                                type: "text",
                                                analyzer: "case_insensitive_sort",
                                                fielddata: true,
                                            },
                                        },
                                    },
                                    bio: {
                                        type: "text",
                                        analyzer: "edge_ngram_analyzer",
                                        search_analyzer: "standard",
                                    },
                                    banner: {type: "text"},
                                    picture: {type: "text"},
                                    active: {type: "boolean"},
                                },
                            },
                        },
                    },
                });
                logger.debug("[Elasticsearch.ts - ensureIndices]: users index created");

                const tags: boolean = await client.indices.exists({index: Constants.ELASTIC_TAGS_INDEX});
                if (tags) {
                    logger.debug("[Elasticsearch.ts - ensureIndices]: tags index already exist");
                } else {
                    await client.indices.create({
                        index: Constants.ELASTIC_TAGS_INDEX,
                        body: {
                            settings: {
                                number_of_shards: 1,
                                analysis: {
                                    filter: {edge_ngram_filter: {type: "edge_ngram", min_gram: 1, max_gram: 64}},
                                    analyzer: {
                                        edge_ngram_analyzer: {
                                            type: "custom",
                                            tokenizer: "standard",
                                            filter: ["lowercase", "edge_ngram_filter"],
                                        },
                                        case_insensitive_sort: {
                                            tokenizer: "keyword",
                                            filter: ["lowercase"],
                                        },
                                    },
                                },
                            },
                            mappings: {
                                tag: {
                                    properties: {
                                        name: {
                                            type: "text",
                                            analyzer: "edge_ngram_analyzer",
                                            search_analyzer: "standard",
                                            fields: {
                                                raw: {
                                                    type: "text",
                                                    analyzer: "case_insensitive_sort",
                                                    fielddata: true,
                                                },
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    });
                    logger.debug("[Elasticsearch.ts - ensureIndices]: tags index created");
                }

                const brands: boolean = await client.indices.exists({index: Constants.ELASTIC_BRANDS_INDEX});
                if (brands) {
                    logger.debug("[Elasticsearch.ts - ensureIndices]: brands index already exist");
                } else {
                    await client.indices.create({
                        index: Constants.ELASTIC_BRANDS_INDEX,
                        body: {
                            settings: {
                                number_of_shards: 1,
                                analysis: {
                                    filter: {edge_ngram_filter: {type: "edge_ngram", min_gram: 1, max_gram: 64}},
                                    analyzer: {
                                        edge_ngram_analyzer: {
                                            type: "custom",
                                            tokenizer: "standard",
                                            filter: ["lowercase", "edge_ngram_filter"],
                                        },
                                        case_insensitive_sort: {
                                            tokenizer: "keyword",
                                            filter: ["lowercase"],
                                        },
                                    },
                                },
                            },
                            mappings: {
                                brand: {
                                    properties: {
                                        name: {
                                            type: "text",
                                            analyzer: "edge_ngram_analyzer",
                                            search_analyzer: "standard",
                                            fields: {
                                                raw: {
                                                    type: "text",
                                                    analyzer: "case_insensitive_sort",
                                                    fielddata: true,
                                                },
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    });
                    logger.debug("[Elasticsearch.ts - ensureIndices]: brands index created");
                }

                const categories: boolean = await client.indices.exists(
                    {index: Constants.ELASTIC_CATEGORIES_INDEX});
                if (categories) {
                    logger.debug("[Elasticsearch.ts - ensureIndices]: categories index already exist");
                } else {
                    await client.indices.create({
                        index: Constants.ELASTIC_CATEGORIES_INDEX,
                        body: {
                            settings: {
                                number_of_shards: 1,
                                analysis: {
                                    filter: {edge_ngram_filter: {type: "edge_ngram", min_gram: 1, max_gram: 64}},
                                    analyzer: {
                                        edge_ngram_analyzer: {
                                            type: "custom",
                                            tokenizer: "standard",
                                            filter: ["lowercase", "edge_ngram_filter"],
                                        },
                                        case_insensitive_sort: {
                                            tokenizer: "keyword",
                                            filter: ["lowercase"],
                                        },
                                    },
                                },
                            },
                            mappings: {
                                category: {
                                    properties: {
                                        name: {
                                            type: "text",
                                            analyzer: "edge_ngram_analyzer",
                                            search_analyzer: "standard",
                                            fields: {
                                                raw: {
                                                    type: "text",
                                                    analyzer: "case_insensitive_sort",
                                                    fielddata: true,
                                                },
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    });
                    logger.debug("[Elasticsearch.ts - ensureIndices]: categories index created");
                }

                const permissions: boolean = await client.indices.exists(
                    {index: Constants.ELASTIC_PERMISSIONS_INDEX});
                if (permissions) {
                    logger.debug("[Elasticsearch.ts - ensureIndices]: permissions index already exist");
                } else {
                    await client.indices.create({
                        index: Constants.ELASTIC_PERMISSIONS_INDEX,
                        body: {
                            settings: {
                                number_of_shards: 1,
                                analysis: {
                                    filter: {edge_ngram_filter: {type: "edge_ngram", min_gram: 1, max_gram: 64}},
                                    analyzer: {
                                        edge_ngram_analyzer: {
                                            type: "custom",
                                            tokenizer: "standard",
                                            filter: ["lowercase", "edge_ngram_filter"],
                                        },
                                        case_insensitive_sort: {
                                            tokenizer: "keyword",
                                            filter: ["lowercase"],
                                        },
                                    },
                                },
                            },
                            mappings: {
                                permission: {
                                    properties: {
                                        permission: {
                                            type: "text",
                                            analyzer: "edge_ngram_analyzer",
                                            search_analyzer: "standard",
                                            fields: {
                                                raw: {
                                                    type: "text",
                                                    analyzer: "case_insensitive_sort",
                                                    fielddata: true,
                                                },
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    });
                    logger.debug("[Elasticsearch.ts - ensureIndices]: permissions index created");
                }
            }
        } catch (e) {
            logger.error("[Elasticsearch.ts - ensureIndices]: %s", e.message);
        }
    }
}
