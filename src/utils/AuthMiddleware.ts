import {PermissionEntity, UserEntity} from "../entity";
import logger from "./Logger";
import {Utils} from "./Utils";

export class AuthMiddleware {
    private static async auth(req, res, next) {
        try {
            logger.debug("[utils/AuthMiddleware.ts - auth]: Check Authentication");
            logger.debug("[utils/AuthMiddleware.ts - auth]: Check if session exist");
            if (!req.session || !req.session.userId) {
                logger.debug("[utils/AuthMiddleware.ts]: Authentication failed");
                res.status(401).send({message: res.__("auth_not_logged")});
                return;
            }
            logger.debug("[utils/AuthMiddleware.ts - auth]: Search user");
            const user: UserEntity = await UserEntity.findOne(req.session.userId, {relations: ["permissions"]});
            if (!user) {
                logger.debug("[utils/AuthMiddleware.ts - auth]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }
            logger.debug("[utils/AuthMiddleware.ts - auth]: Authentication ok");
            req.user = user;
            return next();
        } catch (err) {
            next(err);
        }
    }

    private static async optionalAuth(req, res, next) {
        try {
            logger.debug("[utils/AuthMiddleware.ts - optionalAuth]: Check Authentication");
            logger.debug("[utils/AuthMiddleware.ts - optionalAuth]: Check if session exist");
            if (!req.session || !req.session.userId) {
                logger.debug("[utils/AuthMiddleware.ts - optionalAuth]: User not logged");
                req.user = null;
                return next();
            }
            logger.debug("[utils/AuthMiddleware.ts - optionalAuth]: Search user");
            const user: UserEntity = await UserEntity.findOne(req.session.userId, {relations: ["permissions"]});
            if (!user) {
                logger.debug("[utils/AuthMiddleware.ts - optionalAuth]: User not found");
                res.status(404).send({message: res.__("user_not_found")});
                return;
            }
            logger.debug("[utils/AuthMiddleware.ts - optionalAuth]: Authentication ok");
            req.user = user;
            return next();
        } catch (err) {
            next(err);
        }
    }

    public static authorize(options = null) {
        const perms = options && options.perms !== undefined ? options.perms : [];
        const optionalLogin = options && options.optionalLogin !== undefined ? options.optionalLogin : false;

        let auth = AuthMiddleware.auth;
        if (optionalLogin) {
            auth = AuthMiddleware.optionalAuth;
        }
        return [
            // Log user
            auth,
            // authorize based on user permissions
            (req, res, next) => {
                if (req.user) {
                    let allowed: boolean = false;
                    for (const perm of req.user.permissions) {
                        if (perms.includes(perm.permission)) {
                            allowed = true;
                            break;
                        }
                    }
                    if (perms.length && !allowed) {
                        logger.debug("[utils/AuthMiddleware.ts - authorize]: User is not authorized");
                        return res.status(403).send({message: res.__("auth_no_permissions")});
                    }
                    req.perms = perms;
                    req.optionalLogin = optionalLogin;
                }
                return next();
            }];
    }

    public static userHavePermission(user: UserEntity, permission: string) {
        let allowed: boolean = false;
        for (const perm of user.permissions) {
            if (permission.includes(perm.permission)) {
                allowed = true;
                break;
            }
        }
        return allowed;
    }

    public static async ensurePermissions() {
        const perms: string[] = Utils.transformObjectInStringArray(permissions, []);

        for (const perm of perms) {
            const existing: PermissionEntity = await PermissionEntity.findOne({where: {permission: perm}});
            if (!existing) {
                logger.debug("[utils/AuthMiddleware.ts - ensurePermissions]: One permission is missing, adding it");
                await PermissionEntity.saveInDb({permission: perm});
            }
        }
        logger.debug("[utils/AuthMiddleware.ts - ensurePermissions]: Permissions ok");
    }
}

export const role = {
    Admin: "Admin",
    Moderator: "Moderator",
    Contributor: "Contributor",
    User: "User",
    Custom: "Custom",
};
export const permissions = {
    User: {
        Create: "User.Create",
        Read: {
            Public: "User.Read.Public",
            Private: "User.Read.Private",
        },
        Update: {
            All: "User.Update.All",
            Own: "User.Update.Own",
            Permissions: "User.Update.Permissions",
        },
        Delete: {
            All: "User.Delete.All",
            Own: "User.Delete.Own",
        },
    },
    Store: {
        Create: "Store.Create",
        Read: "Store.Read",
        Update: {
            All: "Store.Update.All",
            Own: "Store.Update.Own",
            NoOwner: "Store.Update.NoOwner",
        },
        Delete: {
            All: "Store.Delete.All",
            Own: "Store.Delete.Own",
            NoOwner: "Store.Delete.NoOwner",
        },
    },
    Comment: {
        Create: "Comment.Create",
        Read: "Comment.Read",
        Update: {
            All: "Comment.Update.All",
            Own: "Comment.Update.Own",
        },
        Delete: {
            All: "Comment.Delete.All",
            Own: "Comment.Delete.Own",
        },
    },
    Product: {
        Create: {
            All: "Product.Create.All",
            OwnStore: "Product.Create.OwnStore",
        },
        Read: "Product.Read",
        Update: {
            All: "Product.Update.All",
        },
        Delete: {
            All: "Product.Delete.All",
        },
    },
    Offer: {
        Create: {
            All: "Offer.Create.All",
            OwnStore: "Offer.Create.OwnStore",
            NoOwner: "Offer.Create.NoOwner",
        },
        Read: "Offer.Read",
        Update: {
            All: "Offer.Update.All",
            OwnStore: "Offer.Update.OwnStore",
            NoOwner: "Offer.Update.NoOwner",
        },
        Delete: {
            All: "Offer.Delete.All",
            OwnStore: "Offer.Delete.OwnStore",
        },
    },
};

export const guestPermissions: string[] = [permissions.User.Create, permissions.User.Read.Public,
    permissions.Store.Read, permissions.Comment.Read, permissions.Product.Read, permissions.Offer.Read];

export const unactiveUserPermissions: string[] = [permissions.User.Read.Public, permissions.User.Update.Own,
    permissions.User.Delete.Own, permissions.Store.Read, permissions.Comment.Read, permissions.Product.Read,
    permissions.Offer.Read];

export const userPermissions: string[] = [permissions.User.Read.Public, permissions.User.Update.Own,
    permissions.User.Delete.Own, permissions.Store.Create, permissions.Store.Read, permissions.Store.Update.Own,
    permissions.Store.Delete.Own, permissions.Comment.Create, permissions.Comment.Read, permissions.Comment.Update.Own,
    permissions.Comment.Delete.Own, permissions.Product.Create.OwnStore, permissions.Product.Read,
    permissions.Offer.Create.OwnStore, permissions.Offer.Read, permissions.Offer.Update.OwnStore,
    permissions.Offer.Delete.OwnStore];

export const contributorPermissions: string[] = [permissions.User.Read.Public, permissions.User.Update.Own,
    permissions.User.Delete.Own, permissions.Store.Create, permissions.Store.Read, permissions.Store.Update.Own,
    permissions.Store.Update.NoOwner, permissions.Store.Delete.Own, permissions.Comment.Create,
    permissions.Comment.Read, permissions.Comment.Update.Own, permissions.Comment.Delete.Own,
    permissions.Product.Create.OwnStore, permissions.Product.Read, permissions.Product.Create.All,
    permissions.Offer.Create.OwnStore, permissions.Offer.Create.NoOwner, permissions.Offer.Read,
    permissions.Offer.Update.OwnStore, permissions.Offer.Update.NoOwner, permissions.Offer.Delete.OwnStore];

export const moderatorPermissions: string[] = [permissions.User.Read.Public, permissions.User.Update.Own,
    permissions.User.Update.All, permissions.User.Delete.Own, permissions.Store.Create, permissions.Store.Read,
    permissions.Store.Update.Own, permissions.Store.Update.NoOwner, permissions.Store.Delete.Own,
    permissions.Store.Delete.NoOwner, permissions.Comment.Create, permissions.Comment.Read,
    permissions.Comment.Update.Own, permissions.Comment.Delete.Own, permissions.Comment.Delete.All,
    permissions.Product.Create.OwnStore, permissions.Product.Create.All, permissions.Product.Read,
    permissions.Product.Update.All, permissions.Product.Delete.All, permissions.Offer.Create.OwnStore,
    permissions.Offer.Create.NoOwner, permissions.Offer.Create.All, permissions.Offer.Read,
    permissions.Offer.Update.OwnStore, permissions.Offer.Update.NoOwner, permissions.Offer.Update.All,
    permissions.Offer.Delete.OwnStore, permissions.Offer.Delete.All];

export const adminPermissions = Utils.transformObjectInStringArray(permissions, []);
