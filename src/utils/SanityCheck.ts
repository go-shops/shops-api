import Joi from "joi";
import logger from "./Logger";
import util = require("util");
import {Utils} from "./Utils";
import {role} from "./AuthMiddleware";

export class SanityCheck {
    /**
     * Validate the sanity of user json object
     */
    public static validateUser(obj: JSON): Joi {
        const reg: string = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$%&()*+,-.:;<=>?@^_{|}~])" +
            "[A-Za-z\\d#$%&()*+,-.:;<=>?@^_{|}~]{8,32}$";
        const schema = Joi.object().keys({
            username: Joi.string().alphanum().min(4).max(16).required(),
            email: Joi.string().email().required(),
            password: Joi.string().regex(new RegExp(reg)).required(),
            parentEmail: Joi.string().email().allow(null),
            bio: Joi.string().min(1).allow(null),
            picture: Joi.string().min(1).max(255).uri().allow(null)
        });
        logger.silly("[utils/SanityCheck.ts - validateUser][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/SanityCheck.ts - validateUser][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    /**
     * Validate the sanity of user json object when updateInDb
     */
    public static validateUpdateUser(obj: JSON): Joi {
        const reg: string = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$%&()*+,-.:;<=>?@^_{|}~])" +
            "[A-Za-z\\d#$%&()*+,-.:;<=>?@^_{|}~]{8,32}$";
        const schema = Joi.object().keys({
            username: Joi.string().alphanum().min(4).max(16).required(),
            email: Joi.string().email().required(),
            password: Joi.string().regex(new RegExp(reg)),
            parentEmail: Joi.string().email().allow(null),
            bio: Joi.string().min(1).allow(null),
            picture: Joi.string().min(1).max(255).uri().allow(null),
            banner: Joi.string().min(1).max(255).uri().allow(null)
        });
        logger.silly("[utils/SanityCheck.ts - validateUpdateUser][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/SanityCheck.ts - validateUpdateUser][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    /**
     * Validate the sanity of user login json object
     */
    public static validateUserLogin(obj: JSON): Joi {
        const schema = Joi.object().keys({
            email: Joi.string().email().required(),
            password: Joi.string().required()
        });
        logger.silly("[utils/SanityCheck.ts - validateUserLogin][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/SanityCheck.ts - validateUserLogin][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    /**
     * Validate store json object
     */
    public static validateStore(obj): Joi {
        const addressSchema = {
            city: Joi.string().min(1).max(255).required(),
            location: Joi.object().keys({
                lat: Joi.number().precision(8).required(),
                lon: Joi.number().precision(8).required()
            }).allow(null),
            street: Joi.string().min(1).max(255).required(),
            streetNbr: Joi.number().min(0).required(),
            zipCode: Joi.string().min(2).max(255).required(),
            country: Joi.string().min(1).max(255).required()
        };
        const hoursSchema = Joi.object().keys({
            day: Joi.string().valid(["monday", "tuesday", "wednesday",
                "thursday", "friday", "saturday", "sunday"]).required(),
            timeClosed: Joi.string().regex(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/).required(),
            timeOpen: Joi.string().regex(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/).required()
        });
        const categoriesSchema = Joi.object({
            name: Joi.string().min(1).max(255).required()
        });
        const picturesSchema = Joi.object({
            description: Joi.string().min(1).required(),
            name: Joi.string().min(1).max(255).required(),
            url: Joi.string().min(1).max(255).uri().required()
        });
        const brandSchema = {
            name: Joi.string().min(1).max(255).required()
        };
        const tagsSchema = Joi.object({
            name: Joi.string().min(1).max(255).required()
        });
        const productsSchema = Joi.object({
            description: Joi.string().min(1).required(),
            name: Joi.string().min(1).max(255).required(),
            brand: Joi.object().keys(brandSchema).allow(null),
            pictures: Joi.array().items(picturesSchema).allow(null).required(),
            tags: Joi.array().items(tagsSchema).allow(null)
        });

        const schema = Joi.object().keys({
            name: Joi.string().min(1).max(255).required(),
            description: Joi.string().min(1).max(2048).required(),
            siret: Joi.string().alphanum().min(14).max(14).required(),
            address: Joi.object().keys(addressSchema).required(),
            hours: Joi.array().items(hoursSchema).allow(null),
            categories: Joi.array().items(categoriesSchema).min(1).required(),
            pictures: Joi.array().items(picturesSchema).allow(null),
            products: Joi.array().items(productsSchema).allow(null),
            offers: Joi.allow(null).error(new Error("Store: offers are not provided"))
        });
        logger.silly("[utils/SanityCheck.ts - validateStore][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/SanityCheck.ts - validateStore][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateComment(obj: JSON): Joi {
        const schema = Joi.object().keys({
            comment: Joi.string().min(1).max(225).required(),
            mark: Joi.number().min(0).max(5).required()
        });
        logger.silly("[utils/JoiValidate.ts - validatePicture][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validatePicture][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateMail(obj: JSON): Joi {
        const schema = Joi.object().keys({
            email: Joi.string().email().required(),
            subject: Joi.string().min(1).max(255).required(),
            content: Joi.string().min(1).max(2048).required()
        });
        logger.silly("[utils/JoiValidate.ts - validateMail][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateMail][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateSearchStores(obj: JSON): Joi {
        const schema = Joi.object().keys({
            query: Joi.string().allow("").max(64).required(),
            page: Joi.number().min(1).max(200).required(),
            size: Joi.number().min(1).max(50).required(),
            sort: Joi.string().valid(["mark", "alphabetical", "score", "geo"]),
            orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
            location: Joi.when(Joi.ref("sort"), {is: "geo", then: Joi.object().keys({
                    lat: Joi.number().precision(8).required(),
                    lon: Joi.number().precision(8).required()
                }).required()}),
            geoUnit: Joi.when(Joi.ref("sort"), {is: "geo", then: Joi.string().valid(["km", "m", "miles"]).required()})
        });
        logger.silly("[utils/JoiValidate.ts - validateSearchStores][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateSearchStores][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateSearchProducts(obj: JSON): Joi {
        const schema = Joi.object().keys({
            query: Joi.string().allow("").max(64).required(),
            page: Joi.number().min(1).max(200).required(),
            size: Joi.number().min(1).max(50).required(),
            sort: Joi.string().valid(["mark", "alphabetical", "score"]),
            orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])})
        });
        logger.silly("[utils/JoiValidate.ts - validateSearchProducts][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateSearchProducts][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateSearchUsers(obj: JSON): Joi {
        const schema = Joi.object().keys({
            query: Joi.string().allow("").max(64).required(),
            page: Joi.number().min(1).max(200).required(),
            size: Joi.number().min(1).max(50).required(),
            sort: Joi.string().valid(["alphabetical", "score"]),
            orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])})
        });
        logger.silly("[utils/JoiValidate.ts - validateSearchUsers][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateSearchUsers][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateSearchTags(obj: JSON): Joi {
        const schema = Joi.object().keys({
            query: Joi.string().allow("").max(64).required(),
            page: Joi.number().min(1).max(200).required(),
            size: Joi.number().min(1).max(50).required(),
            sort: Joi.string().valid(["alphabetical", "score"]),
            orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])})
        });
        logger.silly("[utils/JoiValidate.ts - validateSearchTags][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateSearchTags][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateSearchBrands(obj: JSON): Joi {
        const schema = Joi.object().keys({
            query: Joi.string().allow("").max(64).required(),
            page: Joi.number().min(1).max(200).required(),
            size: Joi.number().min(1).max(50).required(),
            sort: Joi.string().valid(["alphabetical", "score"]),
            orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"]).required()})
        });
        logger.silly("[utils/JoiValidate.ts - validateSearchBrands][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateSearchBrands][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateSearchCategories(obj: JSON): Joi {
        const schema = Joi.object().keys({
            query: Joi.string().allow("").max(64).required(),
            page: Joi.number().min(1).max(200).required(),
            size: Joi.number().min(1).max(50).required(),
            sort: Joi.string().valid(["alphabetical", "score"]),
            orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"]).required()})
        });
        logger.silly("[utils/JoiValidate.ts - validateSearchCategories][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateSearchCategories][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateSearchPermissions(obj: JSON): Joi {
        const schema = Joi.object().keys({
            query: Joi.string().allow("").max(64).required(),
            page: Joi.number().min(1).max(200).required(),
            size: Joi.number().min(1).max(50).required(),
            sort: Joi.string().valid(["alphabetical", "score"]),
            orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"]).required()})
        }).and("sort", "orderType");
        logger.silly("[utils/JoiValidate.ts - validateSearchPermissions][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateSearchPermissions][Joi - validate]: %s", util.inspect(result));
        return result;
    }

    public static validateOffer(obj: JSON): Joi {
        const schema = Joi.object().keys({
            description: Joi.string().min(1).max(2048).required(),
            name: Joi.string().min(1).max(255).required(),
            discount: Joi.number().min(1).max(100).required(),
            startTime: Joi.date().iso().required(),
            expiryTime: Joi.date().iso().min(Joi.ref("startTime")).allow(null),
            productId: Joi.number().min(0).required(),
            storeId: Joi.number().min(0).required()

        });
        logger.silly("[utils/JoiValidate.ts - validateOffer][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateOffer][Joi - schema]: %s", util.inspect(result));
        return result;
    }

    public static validateUpdateOffer(obj: JSON): Joi {
        const schema = Joi.object().keys({
            description: Joi.string().min(1).max(2048).required(),
            name: Joi.string().min(1).max(255).required(),
            discount: Joi.number().min(1).max(100).required(),
            startTime: Joi.date().iso().required(),
            expiryTime: Joi.date().iso().min(Joi.ref("startTime")).allow(null)

        });
        logger.silly("[utils/JoiValidate.ts - validateOffer][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateOffer][Joi - schema]: %s", util.inspect(result));
        return result;
    }

    public static validateProduct(obj: JSON): Joi {
        const picturesSchema = Joi.object({
            description: Joi.string().min(1).required(),
            name: Joi.string().min(1).max(255).required(),
            url: Joi.string().min(1).max(255).uri().required()
        });
        const brandSchema = {
            name: Joi.string().min(1).max(255).required()
        };
        const tagsSchema = Joi.object({
            name: Joi.string().min(1).max(255).required()
        });
        const schema = Joi.object().keys({
            description: Joi.string().min(1).required(),
            name: Joi.string().min(1).max(255).required(),
            brand: Joi.object().keys(brandSchema).allow(null),
            pictures: Joi.array().items(picturesSchema).allow(null).required(),
            tags: Joi.array().items(tagsSchema).allow(null)
        });
        logger.silly("[utils/JoiValidate.ts - validateProduct][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateProduct][Joi - schema]: %s", util.inspect(result));
        return result;
    }

    public static validateProductPatch(obj: JSON): Joi {
        const picturesSchema = Joi.object({
            description: Joi.string().min(1).required(),
            name: Joi.string().min(1).max(255).required(),
            url: Joi.string().min(1).max(255).uri().required()
        });
        const brandSchema = {
            name: Joi.string().min(1).max(255).required()
        };
        const tagsSchema = Joi.object({
            name: Joi.string().min(1).max(255).required()
        });
        const schema = Joi.object().keys({
            description: Joi.string().min(1),
            name: Joi.string().min(1).max(255),
            brand: Joi.object().keys(brandSchema).allow(null),
            pictures: Joi.array().items(picturesSchema).allow(null),
            tags: Joi.array().items(tagsSchema).allow(null)
        });
        logger.silly("[utils/JoiValidate.ts - validateProductPatch][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateProductPatch][Joi - schema]: %s", util.inspect(result));
        return result;
    }

    public static validateBindProduct(obj: JSON): Joi {
        const schema = Joi.object({
            products: Joi.array().items(Joi.number().min(1)).required()
        });
        logger.silly("[utils/JoiValidate.ts - validateBindProduct][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateBindProduct][Joi - schema]: %s", util.inspect(result));
        return result;
    }

    public static validateUserPermissions(obj: JSON): Joi {
        const schema = Joi.object().keys({
            role: Joi.string().valid(Utils.transformObjectInStringArray(role, [])).required(),
            permissions: Joi.when("role", {
                is: role.Custom,
                then: Joi.array().items(Joi.number().min(1)).required(),
                otherwise: Joi.forbidden()})
        });
        logger.silly("[utils/JoiValidate.ts - validateUserPermissions][Joi - schema]: %s", util.inspect(schema));
        const result = Joi.validate(obj, schema, {abortEarly: false});
        logger.debug("[utils/JoiValidate.ts - validateUserPermissions][Joi - schema]: %s", util.inspect(result));
        return result;
    }
}
