import {createLogger, format, transports} from "winston";
import DailyRotateFile from "winston-daily-rotate-file";
import Constants from "../Constants";

const myFormat = format.printf((info) => {
    return `[${info.timestamp}][${info.level}]${info.message}`;
});

let logger;

if (Constants.NODE_ENV === "PRODUCTION") {
    logger = createLogger({
        format: format.combine(
            format.colorize(),
            format.splat(),
            format.simple(),
            format.timestamp({format: "YYYY-MM-DD HH:mm:ss.SSS"}),
            myFormat),
        level: Constants.LOG_LEVEL,
        transports: [
            new (DailyRotateFile)({
                datePattern: "YYYY-MM-DD",
                dirname: Constants.LOG_DIR,
                filename: "shops-api-%DATE%.log",
                maxFiles: "7d",
                zippedArchive: true,
            }),
            new transports.Console()],
    });
} else {
    logger = createLogger({
        format: format.combine(
            format.colorize(),
            format.splat(),
            format.simple(),
            format.timestamp({format: "YYYY-MM-DD HH:mm:ss.SSS"}),
            myFormat),
        level: Constants.LOG_LEVEL,
        transports: [
            new (DailyRotateFile)({
                datePattern: "YYYY-MM-DD",
                dirname: Constants.LOG_DIR,
                filename: "shops-api-%DATE%.log",
                maxFiles: "7d",
                zippedArchive: true,
            })],
    });
    if (Constants.NODE_ENV !== "test") {
        logger.add(new transports.Console());
    }
}

// noinspection JSUnusedGlobalSymbols
logger.stream = {
    write: (message) => {
        logger.info(message);
    },
};

export default logger;
