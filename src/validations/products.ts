import Joi from "joi";

const picturesSchema = Joi.object({
    description: Joi.string().trim().min(1).required(),
    name: Joi.string().trim().min(1).max(255).required(),
    url: Joi.string().trim().min(1).max(255).uri().required(),
});
const brandSchema = {
    name: Joi.string().trim().min(1).max(255).required(),
};
const tagsSchema = Joi.object({
    name: Joi.string().trim().min(1).max(255).required(),
});

export const getProduct = {
    query: Joi.object().keys({
        page: Joi.number().min(1),
        maxRes: Joi.number().min(1),
    }),
    params: Joi.object().keys({
        productId: Joi.number().min(1).required(),
    }),
};

export const postProduct = {
    body: Joi.object().keys({
        description: Joi.string().trim().min(1).required(),
        name: Joi.string().trim().min(1).max(255).required(),
        brand: Joi.object().keys(brandSchema).allow(null),
        pictures: Joi.array().items(picturesSchema).allow(null),
        tags: Joi.array().items(tagsSchema).allow(null),
    }),
};

export const patchProduct = {
    body: Joi.object().keys({
        description: Joi.string().trim().min(1),
        name: Joi.string().trim().min(1).max(255),
        brand: Joi.object().keys(brandSchema).allow(null),
        pictures: Joi.array().items(picturesSchema).allow(null),
        tags: Joi.array().items(tagsSchema).allow(null),
    }),
};
