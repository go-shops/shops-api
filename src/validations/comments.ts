import Joi from "joi";

export const getComment = {
    params: Joi.object().keys({
        commentId: Joi.number().min(1).required(),
    }),
};

export const postStoreComment = {
    params: Joi.object().keys({
        storeId: Joi.number().min(1).required(),
    }),
    body: Joi.object().keys({
        comment: Joi.string().trim().min(1).max(225).required(),
        mark: Joi.number().min(0).max(5).required(),
    }),
};

export const postProductComment = {
    params: Joi.object().keys({
        productId: Joi.number().min(1).required(),
    }),
    body: Joi.object().keys({
        comment: Joi.string().trim().min(1).max(225).required(),
        mark: Joi.number().min(0).max(5).required(),
    }),
};
