import Joi from "joi";
import {Utils} from "../utils";
import {role} from "../utils";

export const getUser = {
    params: Joi.object().keys({
        userId: Joi.number().min(1).required(),
    }),
};

export const postUser = {
    body: Joi.object().keys({
        username: Joi.string().trim().alphanum().min(3).max(16).required(),
        email: Joi.string().trim().email().required(),
        password: Joi.string().min(6).max(32).required(),
        parentEmail: Joi.string().trim().email().allow(null),
        bio: Joi.string().trim().min(1).allow(null),
        picture: Joi.string().trim().min(1).max(255).uri().allow(null),
        banner: Joi.string().trim().min(1).max(255).uri().allow(null),
    }),
};

export const editUser = {
    body: Joi.object().keys({
        username: Joi.string().trim().alphanum().min(3).max(16).required(),
        email: Joi.string().trim().email().required(),
        password: Joi.string().min(6).max(32),
        parentEmail: Joi.string().trim().email().allow(null),
        bio: Joi.string().trim().min(1).allow(null),
        picture: Joi.string().trim().min(1).max(255).uri().allow(null),
        banner: Joi.string().trim().min(1).max(255).uri().allow(null),
    }),
};

export const editPassword = {
  body: Joi.object().keys({
      password: Joi.string().min(6).max(32).required(),
      oldPassword: Joi.string().min(6).max(32).allow(null),
      userHash: Joi.string().trim().min(3).allow(null),
  }),
};

export const editUserPermissions = {
    body: Joi.object().keys({
        role: Joi.string().trim().valid(Utils.transformObjectInStringArray(role, [])).required(),
        permissions: Joi.when("role", {
            is: role.Custom,
            then: Joi.array().items(Joi.number().min(1)).required(),
            otherwise: Joi.forbidden()}),
    }),
};

export const loginUser = {
    body: Joi.object().keys({
        email: Joi.string().trim().email().required(),
        password: Joi.string().trim().required(),
    }),
};

export const validateUser = {
  params: Joi.object().keys({
      hash: Joi.string().trim().min(3).required(),
  }),
};

export const forgotPassword = {
    body: Joi.object().keys({
        email: Joi.string().trim().email().required(),
    }),
};
