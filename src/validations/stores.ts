import Joi from "joi";

const addressSchema = {
    city: Joi.string().trim().min(1).max(255).required(),
    location: Joi.object().keys({
        lat: Joi.number().precision(8).required(),
        lon: Joi.number().precision(8).required(),
    }).allow(null),
    street: Joi.string().trim().min(1).max(255).required(),
    streetNbr: Joi.number().min(0).required(),
    zipCode: Joi.string().trim().min(2).max(255).required(),
    country: Joi.string().trim().min(1).max(255).required(),
};
const hoursSchema = Joi.object().keys({
    day: Joi.string().trim().valid(["monday", "tuesday", "wednesday",
        "thursday", "friday", "saturday", "sunday"]).required(),
    timeClosed: Joi.string().trim().regex(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/).required(),
    timeOpen: Joi.string().trim().regex(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/).required(),
});
const categoriesSchema = Joi.object({
    name: Joi.string().trim().min(1).max(255).required(),
});
const picturesSchema = Joi.object({
    description: Joi.string().trim().min(1).required(),
    name: Joi.string().trim().min(1).max(255).required(),
    url: Joi.string().trim().min(1).max(255).uri().required(),
});
const brandSchema = {
    name: Joi.string().trim().min(1).max(255).required(),
};
const tagsSchema = Joi.object({
    name: Joi.string().trim().min(1).max(255).required(),
});

export const getStore = {
    query: Joi.object().keys({
        page: Joi.number().min(1),
        maxRes: Joi.number().min(1),
    }),
    params: Joi.object().keys({
        storeId: Joi.number().min(1).required(),
    }),
};

export const postStore = {
    body: Joi.object().keys({
        name: Joi.string().min(1).max(255).required(),
        description: Joi.string().min(1).max(2048).required(),
        siret: Joi.string().alphanum().min(14).max(14).allow(null),
        address: Joi.object().keys(addressSchema).required(),
        hours: Joi.array().items(hoursSchema).allow(null),
        categories: Joi.array().items(categoriesSchema).min(1).required(),
        pictures: Joi.array().items(picturesSchema).allow(null),
        offers: Joi.allow(null),
    }),
};

export const patchStore = {
    body: Joi.object().keys({
        name: Joi.string().trim().min(1).max(255),
        description: Joi.string().trim().min(1).max(2048),
        siret: Joi.string().trim().alphanum().min(14).max(14),
        address: Joi.object().keys(addressSchema),
        hours: Joi.array().items(hoursSchema).allow(null),
        categories: Joi.array().items(categoriesSchema).min(1),
        pictures: Joi.array().items(picturesSchema).allow(null),
        offers: Joi.allow(null),
    }),
};

export const bindProduct = {
    body: Joi.object({
        products: Joi.array().items(Joi.number().min(1)).required(),
    }),
};

export const claimStore = {
    params: Joi.object().keys({
        storeId: Joi.number().min(1).required(),
    }),
    body: Joi.object({
        siret: Joi.string().alphanum().min(14).max(14),
    }),
};
