import Joi from "joi";

export const postPicture = {
    body: Joi.object().keys({
        picture: Joi.string().trim().min(1).required(),
    }),
};
