import Joi from "joi";

export const postMail = {
    body: Joi.object().keys({
        email: Joi.string().trim().email().required(),
        subject: Joi.string().trim().min(1).max(255).required(),
        content: Joi.string().trim().min(1).max(2048).required(),
    }),
};
