import Joi from "joi";

export const getOffer = {
    query: Joi.object().keys({
        page: Joi.number().min(1),
        maxRes: Joi.number().min(1),
    }),
    params: Joi.object().keys({
       offerId: Joi.number().min(1).required(),
    }),
};

export const postOffer = {
    body: Joi.object().keys({
        description: Joi.string().trim().min(1).max(2048).required(),
        name: Joi.string().trim().min(1).max(255).required(),
        discount: Joi.number().min(1).max(100).required(),
        startTime: Joi.date().iso().required(),
        expiryTime: Joi.date().iso().min(Joi.ref("startTime")).allow(null),
        productId: Joi.number().min(0).required(),
        storeId: Joi.number().min(0).required(),
    }),
};

export const editOffer = {
    body: Joi.object().keys({
        description: Joi.string().trim().min(1).max(2048).required(),
        name: Joi.string().trim().min(1).max(255).required(),
        discount: Joi.number().min(1).max(100).required(),
        startTime: Joi.date().iso().required(),
        expiryTime: Joi.date().iso().min(Joi.ref("startTime")).allow(null),
    }),
};
