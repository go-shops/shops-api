import Joi from "joi";

export const searchStore = {
    body: Joi.object().keys({
        query: Joi.string().trim().allow("").max(64).required(),
        page: Joi.number().min(1).max(200).required(),
        size: Joi.number().min(1).max(50).required(),
        sort: Joi.string().trim().valid(["mark", "alphabetical", "score", "geo"]),
        orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
        location: Joi.when(Joi.ref("sort"), {is: "geo", then: Joi.object().keys({
                lat: Joi.number().precision(8).required(),
                lon: Joi.number().precision(8).required(),
            }).required()}),
        geoUnit: Joi.when(Joi.ref("sort"), {
            is: "geo",
            then: Joi.string().trim().valid(["km", "m", "miles"]).required(),
        }),
    }),
};

export const searchProduct = {
    body: Joi.object().keys({
        query: Joi.string().trim().allow("").max(64).required(),
        page: Joi.number().min(1).max(200).required(),
        size: Joi.number().min(1).max(50).required(),
        sort: Joi.string().trim().valid(["mark", "alphabetical", "score"]),
        orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
    }),
};

export const searchUser = {
    body: Joi.object().keys({
        query: Joi.string().trim().allow("").max(64).required(),
        page: Joi.number().min(1).max(200).required(),
        size: Joi.number().min(1).max(50).required(),
        sort: Joi.string().trim().valid(["alphabetical", "score"]),
        orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
    }),
};

export const searchTags = {
    body: Joi.object().keys({
        query: Joi.string().trim().allow("").max(64).required(),
        page: Joi.number().min(1).max(200).required(),
        size: Joi.number().min(1).max(50).required(),
        sort: Joi.string().trim().valid(["alphabetical", "score"]),
        orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
    }),
};

export const searchBrands = {
    body: Joi.object().keys({
        query: Joi.string().trim().allow("").max(64).required(),
        page: Joi.number().min(1).max(200).required(),
        size: Joi.number().min(1).max(50).required(),
        sort: Joi.string().trim().valid(["alphabetical", "score"]),
        orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
    }),
};

export const searchCategories = {
    body: Joi.object().keys({
        query: Joi.string().trim().allow("").max(64).required(),
        page: Joi.number().min(1).max(200).required(),
        size: Joi.number().min(1).max(50).required(),
        sort: Joi.string().trim().valid(["alphabetical", "score"]),
        orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
    }),
};

export const searchPermissions = {
    body: Joi.object().keys({
        query: Joi.string().trim().allow("").max(64).required(),
        page: Joi.number().min(1).max(200).required(),
        size: Joi.number().min(1).max(50).required(),
        sort: Joi.string().trim().valid(["alphabetical", "score"]),
        orderType: Joi.when("sort", {is: Joi.exist(), then: Joi.valid(["asc", "desc"])}),
    }),
};
