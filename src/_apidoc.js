// ------------------------------------------------------------------------------------------
// General apiDoc documentation blocks and old history blocks.
// ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------
// Models.
// ------------------------------------------------------------------------------------------
/**
 * @api ~ Address
 * @apiVersion 1.2.0
 * @apiName     AddressModel
 * @apiGroup    _Models
 *
 * @apiDescription  Description of address model.
 *
 * @apiParam (Request Body) {Number}    streetNbr               The street number.
 * @apiParam (Request Body) {String}    street                  The street name.
 * @apiParam (Request Body) {String}    zipCode                 The address zipCode.
 * @apiParam (Request Body) {String}    city                    The city name.
 * @apiParam (Request Body) {String}    country                 The country name.
 * @apiParam (Request Body) {Location}  [location]              The address location
 * @apiParam (Request Body) {Float}     [location.lat]          The address latitude.
 * @apiParam (Request Body) {Float}     [location.lon]          The address longitude.
 * @apiExample {json}   Json (minimal)
 * {
 *   "streetNbr": 94,
 *   "street": "Rue Maréchal Joffre",
 *   "zipCode": 44000,
 *   "city": "Nantes",
 *   "country": "France"
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "streetNbr": 94,
 *   "street": "Rue Maréchal Joffre",
 *   "zipCode": 44000,
 *   "city": "Nantes",
 *   "country": "France",
 *   "location": {
 *     "lat": 47.2184,
 *     "lon": 1.5536
 *   }
 * }
 */

/**
 * @api ~ Brand
 * @apiVersion 1.3.0
 * @apiName     BrandModel
 * @apiGroup    _Models
 *
 * @apiDescription  Description of brand model.
 *
 * @apiParam (Request Body) {String}    name       The brand name.
 * @apiExample {json}   Json
 * {
 *   "name": "OnePlus"
 * }
 */

/**
 * @api ~ Category
 * @apiVersion 1.3.0
 * @apiName     CategoryModel
 * @apiGroup    _Models
 *
 * @apiDescription  Description of category model.
 *
 * @apiParam (Request Body) {String}    name       The category name.
 * @apiExample {json}   Json
 * {
 *   "name": "Fleuriste"
 * }
 */

/**
 * @api ~ Picture
 * @apiVersion 1.3.0
 * @apiName     PictureModel
 * @apiGroup    _Models
 *
 * @apiDescription  Description of picture model.
 *
 * @apiParam (Request Body) {String}    name            The picture name.
 * @apiParam (Request Body) {String}    description     The picture description.
 * @apiParam (Request Body) {URL}       url             The picture url.
 * @apiExample {json}   Json
 * {
 *   "name": "Facade",
 *   "description": "Facade du magasin",
 *   "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 * }
 */

/**
 * @api ~ Product
 * @apiVersion 1.4.1
 * @apiName     ProductModel
 * @apiGroup    _Models
 *
 * @apiDescription  Description of product model.
 *
 * @apiParam (Request Body) {String}    name            The product name.
 * @apiParam (Request Body) {String}    description     The product description.
 * @apiParam (Request Body) {Brand}     [brand]         The product brand (Cf. brand model for more information).
 * @apiParam (Request Body) {Picture[]} pictures        Array of picture (Cf. picture model for more information).
 * @apiParam (Request Body) {Tag[]}     [tags]          Array of tag (Cf. picture model for more information).
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet",
 *   "description": "Bouquet",
 *   "pictures": [
 *       {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *       }
 *   ],
 *   "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 * }
 */

/**
 * @api ~ Hour
 * @apiVersion 1.4.0
 * @apiName     HourModel
 * @apiGroup    _Models
 *
 * @apiDescription  Description of hour model.
 *
 * @apiParam (Request Body) {String="monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"}   day     The week day.
 * @apiParam (Request Body) {String}    timeOpen    The open time.
 * @apiParam (Request Body) {String}    timeClosed  The close time.
 * @apiExample {json}   Json
 * {
 *   "day": "tuesday",
 *   "timeOpen": "09:00:00",
 *   "timeClosed": "20:00:00"
 * }
 */

/**
 * @api ~ Tag
 * @apiVersion 1.2.0
 * @apiName     TagModel
 * @apiGroup    _Models
 *
 * @apiDescription  Description of tag model.
 *
 * @apiParam (Request Body) {String}    name    The tag name.
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet"
 * }
 */


// ------------------------------------------------------------------------------------------
// Headers.
// ------------------------------------------------------------------------------------------
/**
 * @apiDefine ContentTypeHeader
 *
 * @apiHeader {String=application/json}    Content-Type
 */

/**
 * @apiDefine AuthenticationHeader
 *
 * @apiHeader {Cookie}    Cookie     The user session cookie to authenticate request.
 */

// ------------------------------------------------------------------------------------------
// Current Success.
// ------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------
// Current Errors.
// ------------------------------------------------------------------------------------------
/**
 * @apiDefine ValidationError
 *
 * @apiError (Bad Request 400)  ValidationError     Some parameters may contain invalid values.
 */

/**
 * @apiDefine InternalError
 *
 * @apiError    (Internal Server Error 500)     ServerError         Something went wrong.
 */

/**
 * @apiDefine AuthorisationError
 *
 * @apiError    (Unauthorized 401)  Unauthorized    Only authenticated users can access to this endpoint.
 */

/**
 * @apiDefine ForbiddenError
 *
 * @apiError    (Forbidden 403)     Forbidden   You don't have enough permission to do this.
 */

/**
 * @apiDefine DataNotFoundError
 *
 * @apiError    (Not Found 404)  NotFound    The requested data was not found.
 */


// ------------------------------------------------------------------------------------------
// Current Permissions.
// ------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------
// History.
// ------------------------------------------------------------------------------------------
/**
 * @api {get} /stores/:storesId  Get store by ID
 * @apiVersion 1.3.0
 * @apiName GetStoreById
 * @apiGroup Stores
 *
 * @apiDescription Get store by it's ID
 *
 * @apiParam {Number}     storeId       The store-ID.
 *
 * @apiSuccess  {Number}        id                      The store-ID.
 * @apiSuccess  {String}        name                    The store name.
 * @apiSuccess  {String}        description             The store description.
 * @apiSuccess  {String}        siret                   The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                 The store average mark.
 * @apiSuccess  {User}          owner                   The store owner.
 * @apiSuccess  {Number}        owner.id                The User-ID.
 * @apiSuccess  {String}        owner.username          The user username.
 * @apiSuccess  {String}        owner.bio               The user biography.
 * @apiSuccess  {URL}           owner.picture           The user picture.
 * @apiSuccess  {Address}       address                 The store address (Cf address model).
 * @apiSuccess  {Number}        address.id              The address-ID.
 * @apiSuccess  {Number}        address.streetNbr       The street number.
 * @apiSuccess  {String}        address.street          The street name.
 * @apiSuccess  {String}        address.zipCode         The address zipCode.
 * @apiSuccess  {String}        address.city            The city name.
 * @apiSuccess  {String}        address.country         The country name.
 * @apiSuccess  {Float}         address.lat             The address latitude.
 * @apiSuccess  {Float}         address.lon             The address longitude.
 * @apiSuccess  {Schedule[]}    schedules               List of Schedule (Cf Schedule model).
 * @apiSuccess  {Number}        schedules.id            The Schedule-ID.
 * @apiSuccess  {String}        schedules.day           The week day.
 * @apiSuccess  {String}        schedules.timeOpen      The open time.
 * @apiSuccess  {String}        schedules.timeClosed    The close time.
 * @apiSuccess  {Categories[]}  categories              List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id           The category-ID.
 * @apiSuccess  {String}        categories.name         The category name.
 * @apiSuccess  {Pictures[]}    pictures                List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id             The picture-ID.
 * @apiSuccess  {String}        picture.name            The picture name.
 * @apiSuccess  {String}        picture.description     The picture description.
 * @apiSuccess  {URL}           picture.url             The picture url.
 * @apiSuccess  {Products[]}    products                List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                  List of offers.
 * @apiSuccess  {Comments}      comments                List of comments (Cf comments endpoint).
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "bio": null,
 *         "picture": null
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": 44000,
 *         "city": "Nantes",
 *         "country": "France",
 *         "lat": 0,
 *         "lon": 0
 *       },
 *       "schedules": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "comments": []
 *      }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {post}  /search/users   Search user
 * @apiVersion 1.4.0
 * @apiName PostSearchUsers
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a user.<br/>
 * On dev environment, the endpoint is <code>/search/users_dev</code>.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                            query               The query.<br/>
 *                                                                                  Rules: 3 characters minimum
 *                                                                                  and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                     page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                      size                The page size.
 * @apiParam (Request Body) {String="alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}              [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "Barrie",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "Barrie",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "alphabetical",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {User[]}        user                    Result is array of matching users.
 * @apiSuccess  {Number}        user.id                 The user-ID.
 * @apiSuccess  {String}        user.username           The user username.
 * @apiSuccess  {String}        user.bio                The user biography.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "username": "Barrie",
 *         "bio": "Hello this is my bio"
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */

/**
 * @api {get} /products Get all products
 * @apiVersion 1.2.0
 * @apiName GetProducts
 * @apiGroup Products
 *
 * @apiDescription Get list of all products
 *
 * @apiSuccess {[]}  .   List of all products. (Cf. product model for more information)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Fleur",
 *         "description": "coucou",
 *         "avgMark": 0,
 *         "brand": {
 *           "id": 1,
 *           "name": "Marque"
 *         },
 *         "pictures": [
 *           {
 *             "id": 1,
 *             "name": "Fleur",
 *             "description": "Belle fleur",
 *             "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *           }
 *         ],
 *         "stores": [],
 *         "tags": []
 *       },
 *       {
 *         "id": 2,
 *         "name": "Bouquet",
 *         "description": "Bouquet",
 *         "avgMark": 0,
 *         "brand": {
 *           "id": 1,
 *           "name": "Marque"
 *         },
 *         "pictures": [
 *           {
 *             "id": 2,
 *             "name": "Bouquet",
 *             "description": "Beau bouquet",
 *             "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *           }
 *         ],
 *         "stores": [],
 *         "tags": []
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {get} /stores Get all stores
 * @apiVersion 1.4.0
 * @apiName GetStores
 * @apiGroup Stores
 *
 * @apiDescription Get list of all stores
 *
 * @apiSuccess {[]}  .   List of all stores. (Cf. store model for more information)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "L'Atmosphère Des Fleurs",
 *         "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *         "siret": "01234567890123",
 *         "avgMark": 0,
 *         "owner": {
 *           "id": 1,
 *           "username": "Barrie",
 *           "bio": null,
 *           "picture": null
 *         },
 *         "address": {
 *           "id": 2,
 *           "streetNbr": 94,
 *           "street": "Rue Maréchal Joffre",
 *           "zipCode": 44000,
 *           "city": "Nantes",
 *           "lat": 0,
 *           "lon": 0
 *         },
 *         "hours": [],
 *         "categories": [
 *           {
 *             "id": 1,
 *             "name": "Fleuriste"
 *           },
 *           {
 *             "id": 2,
 *             "name": "Mariage"
 *           }
 *         ],
 *         "pictures": [],
 *         "products": [],
 *         "offers": [],
 *         "comments": []
 *       }
 *     ]
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {get} /products/:productId  Get product by ID
 * @apiVersion 1.4.0
 * @apiName GetProductById
 * @apiGroup Products
 *
 * @apiDescription Get product by it's ID
 *
 * @apiParam {Number}     productId       The product-ID.
 *
 * @apiSuccess  {Number}        id                      The product-ID.
 * @apiSuccess  {String}        name                    The product name.
 * @apiSuccess  {String}        description             The product description.
 * @apiSuccess  {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess  {Brand}         brand                   The product brand.
 * @apiSuccess  {Number}        brand.id                The brand-ID.
 * @apiSuccess  {String}        brand.name              The brand name.
 * @apiSuccess  {Picture}       pictures                Array of pictures with for each the following parameters.
 * @apiSuccess  {Number}        pictures.id             The picture-ID.
 * @apiSuccess  {String}        pictures.name           The picture name.
 * @apiSuccess  {String}        pictures.description    The picture description.
 * @apiSuccess  {String}        pictures.url            The picture URL.
 * @apiSuccess  {Store}         stores                  Array of stores with for each the following parameters.
 * @apiSuccess  {Number}        stores.id               The store-ID.
 * @apiSuccess  {String}        stores.name             The store name.
 * @apiSuccess  {String}        stores.description      The store description.
 * @apiSuccess  {String}        stores.siret            The store siret.
 * @apiSuccess  {Number}        stores.avgMark          The average store mark.
 * @apiSuccess  {Address}       stores.address          The store address (Cf address model).
 * @apiSuccess  {Hours[]}       stores.hours            List of hours (Cf hours model).
 * @apiSuccess  {Categories[]}  stores.categories       List of categories (Cf categories model).
 * @apiSuccess  {Pictures[]}    stores.pictures         List of pictures (Cf pictures model).
 * @apiSuccess  {Products[]}    stores.products         List of products (Cf products model).
 * @apiSuccess  {Offers[]}      stores.offers           List of offers.
 * @apiSuccess  {Tag}           tags                    Array of tags with for each the following parameters.
 * @apiSuccess  {Number}        tags.id                 The tag-ID.
 * @apiSuccess  {String}        tags.name               The tag name.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Fleur",
 *       "description": "coucou",
 *       "avgMark": 0,
 *       "brand": {
 *         "id": 1,
 *         "name": "Marque"
 *       },
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "stores": [],
 *       "tags": []
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {post}  /search/stores   Search store
 * @apiVersion 1.4.1
 * @apiName PostSearchStores
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a store.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                                    query               The query.<br/>
 *                                                                                          Rules: 3 characters minimum
 *                                                                                          and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                             page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                              size                The page size.
 * @apiParam (Request Body) {String="mark", "alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}                      [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "mark",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {Store[]}       store                   Result is array of matching stores.
 * @apiSuccess  {Number}        store.id                The store-ID.
 * @apiSuccess  {Number}        store.name              The store name.
 * @apiSuccess  {Number}        store.description       The store description.
 * @apiSuccess  {Category[]}    store.categories        Array of store categories (Cf. category model).
 * @apiSuccess  {Picture[]}     store.pictures          Array of store pictures (Cf. picture model).
 * @apiSuccess  {Number{0-5}}   store.avgMark           The store average mark.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 1,
 *         "name": "Atmosphère Fleurs",
 *         "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages",
 *         "categories": [
 *           {
 *             "id": 1,
 *             "name": "Fleuriste"
 *           },
 *           {
 *             "id": 2,
 *             "name": "Mariage"
 *           }
 *         ],
 *         "pictures": [],
 *         "avgMark": 0
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */

/**
 * @api {post}  /search/products   Search product
 * @apiVersion 1.4.1
 * @apiName PostSearchProducts
 * @apiGroup Search
 *
 * @apiDescription This endpoint perform search for a product.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}                                    query               The query.<br/>
 *                                                                                          Rules: 3 characters minimum
 *                                                                                          and 64 maximum.
 * @apiParam (Request Body) {Number{1-200}}                             page                The page number.
 * @apiParam (Request Body) {Number{1-50}}                              size                The page size.
 * @apiParam (Request Body) {String="mark", "alphabetical", "score"}    [sort=score]        The sort type.<br/>
 * @apiParam (Request Body) {String="asc", "desc"}                      [orderType=desc]    The order type.
 * @apiExample {json}   Json (minimal)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "query": "fleur",
 *   "page": 1,
 *   "size": 50,
 *   "sort": "mark",
 *   "orderType": "asc"
 * }
 *
 * @apiSuccess  {Product[]}     product                 Result is array of matching products.
 * @apiSuccess  {Number}        product.id              The product-ID.
 * @apiSuccess  {String}        product.name            The product name.
 * @apiSuccess  {String}        product.description     The product description.
 * @apiSuccess  {Brand}         product.brand           The product brand (Cf. brand model).
 * @apiSuccess  {Picture[]}     product.pictures        Array of product picture (Cf picture model).
 * @apiSuccess  {Number{0-5}}   product.avgMark         The product average mark.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 20O OK
 *     [
 *       {
 *         "id": 12,
 *         "name": "Composition florale",
 *         "description": "Magnifique composition florale, parfaite pour un mariage",
 *         "brand": {
 *           "id": 5,
 *           "name": "Test"
 *         },
 *         "pictures": [
 *           {
 *             "id": 1,
 *             "name": "Composition florale",
 *             "description": "Belle composition florale",
 *             "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *           }
 *         ],
 *         "avgMark": 0
 *       }
 *     ]
 *
 * @apiUse  ValidationError
 * @apiUse  InternalError
 */

/**
 * @api {post} /auth/login Log User
 * @apiVersion 1.3.0
 * @apiName PostAuthLogin
 * @apiGroup Auth
 *
 * @apiDescription Generate a token used for authenticate user requests.
 * Token are valid only for 1 week. After this time, you have to re-authenticate..
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {Email}     email       The user email.<br/>
 *                                                  Rules: Any valid email
 * @apiParam (Request Body) {String}    password    The user password.<br/>
 *                                                  Rules: 8 characters minimum and 32 maximum, 1 uppercase,
 *                                                  1 lowercase, 1 number and 1 special.
 * @apiExample {json}   Json
 * {
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 * }
 *
 * @apiSuccess {Token}   token          The User authentication token.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXCI9.eyJpZCI6MSwiZW1haWwiOiJiYXJyaWAG9tY"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      InternalError
 * @apiError    (Bad Request 400)               InvalidCredential   Invalid Credentials
 */

/**
 * @api {get} /users/:userId Get user public information
 * @apiVersion 1.2.0
 * @apiName getUserById
 * @apiGroup Users
 *
 * @apiDescription Provides all public information about requested user.
 *
 * @apiSuccess  {Number}        id              The user-ID.
 * @apiSuccess  {String}        username        The user username.
 * @apiSuccess  {URL}           user            The user picture.
 * @apiSuccess  {String}        bio             The user biography.
 * @apiSuccess  {Comment[]}     comments        Array of comments owned by user (Cf. comments endpoint)
 * @apiSuccess  {Store[]}       stores          Array of stores owned by user (Cf. stores endpoint)
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "username": "Barrie",
 *       "picture": null,
 *       "bio": null,
 *       "comments": [],
 *       "stores": []
 *     }
 *
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {post} /offers Create new offer
 * @apiVersion 1.4.2
 * @apiName PostOffer
 * @apiGroup Offers
 *
 * @apiDescription Create a new offer
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}            name            The offer name.<br/>
 *                                                              Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}            description     The offer description.<br/>
 *                                                              Rules: 1 characters minimum.
 * @apiParam (Request Body) {Number{1-100}}     discount        Discount %.
 * @apiParam (Request Body) {String}            startTime       Start time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date
 * @apiParam (Request Body) {String}            [expiryTime]    End time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date greater than startTime
 * @apiParam (Request Body) {Number}            productId       The product ID link to this offer.
 * @apiParam (Request Body) {Number}            storeId         The store ID link to this offer.
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "productId": 23,
 *   "storeId": 15
 * }
 * * @apiExample {json}   Json (full)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "expiryTime": "2019-04-18T13:58:30.000Z",
 *   "productId": 1,
 *   "storeId": 1
 * }
 *
 * @apiSuccess  (Created 201) {Number}        id              The offer-ID.
 * @apiSuccess  (Created 201) {String}        name         The offer name.
 * @apiSuccess  (Created 201) {String}   description            The offer description.
 * @apiSuccess  (Created 201) {Number}   discount            The offer discount.
 * @apiSuccess  (Created 201) {Product}     product         The product link to this offer (Cf. product model).
 * @apiSuccess  (Created 201) {Store}     store         The store link to this offer (Cf. store endpoint).
 * @apiSuccess  (Created 201) {String}      startTime   The start time of the offer.
 * @apiSuccess  (Created 201) {String}      expiryTime   The end time of the offer.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "description": "Test d'une offre",
 *       "discount": 70,
 *       "store": {},
 *       "product": {},
 *       "startTime": "20190309171633",
 *       "expiryTime": "20190418180000"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {post} /products Create new product
 * @apiVersion 1.4.1
 * @apiName PostProduct
 * @apiGroup Products
 *
 * @apiDescription Create new product.<br>Note: <code>In future, ability to create product with POST/PUT stores will
 * be deleted. Please use this endpoint instead.</code>
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}    name            The product name.
 * @apiParam (Request Body) {String}    description     The product description.
 * @apiParam (Request Body) {Brand}     [brand]         The product brand (Cf. brand model for more information).
 * @apiParam (Request Body) {Picture[]} pictures        Array of picture (Cf. picture model for more information).
 * @apiParam (Request Body) {Tag[]}     [tags]          Array of tag (Cf. picture model for more information).
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet",
 *   "description": "Bouquet",
 *   "pictures": [
 *       {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *       }
 *   ],
 *   "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 * }
 *
 * @apiSuccess (Created 201)  {Number}        id                      The product-ID.
 * @apiSuccess (Created 201)  {String}        name                    The product name.
 * @apiSuccess (Created 201)  {String}        description             The product description.
 * @apiSuccess (Created 201)  {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess (Created 201)  {Brand}         brand=null              The product brand.
 * @apiSuccess (Created 201)  {Number}        brand.id                The brand-ID.
 * @apiSuccess (Created 201)  {String}        brand.name              The brand name.
 * @apiSuccess (Created 201)  {Picture}       pictures                Array of pictures with for each
 *                                                                    the following parameters.
 * @apiSuccess (Created 201)  {Number}        pictures.id             The picture-ID.
 * @apiSuccess (Created 201)  {String}        pictures.name           The picture name.
 * @apiSuccess (Created 201)  {String}        pictures.description    The picture description.
 * @apiSuccess (Created 201)  {String}        pictures.url            The picture URL.
 * @apiSuccess (Created 201)  {Tag}           tags                    Array of tags with for each
 *                                                                    the following parameters.
 * @apiSuccess (Created 201)  {Number}        tags.id                 The tag-ID.
 * @apiSuccess (Created 201)  {String}        tags.name               The tag name.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "avgMark": 0,
 *       "brand": {
 *         "id": 1,
 *         "name": "Marque"
 *       },
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "tags": []
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      InternalError
 */

/**
 * @api {delete} /products/:productId  Delete product
 * @apiVersion 1.2.0
 * @apiName DeleteProduct
 * @apiGroup Products
 *
 * @apiDescription Delete product.
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam {Number}     productId       The product-ID.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {patch} /products/:productId Patch product
 * @apiVersion 1.4.1
 * @apiName PatchProduct
 * @apiGroup Products
 *
 * @apiDescription Patch existing product.<br/>
 * Note that sub-object (like tags, brand or pictures) have to be send entire and will be add to the array of exiting
 * objects.
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam                {Number}    productId       The product-ID to edit.
 *
 * @apiParam (Request Body) {String}    [name]          The product name.
 * @apiParam (Request Body) {String}    [description]   The product description.
 * @apiParam (Request Body) {Brand}     [brand]         The product brand (Cf. brand model for more information).
 * @apiParam (Request Body) {Picture[]} [pictures]      Array of picture (Cf. picture model for more information).
 * @apiParam (Request Body) {Tag[]}     [tags]          Array of tag (Cf. picture model for more information).
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet de fleurs"
 * }
 *
 * @apiSuccess {Number}        id                      The product-ID.
 * @apiSuccess {String}        name                    The product name.
 * @apiSuccess {String}        description             The product description.
 * @apiSuccess {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess {Brand}         brand=null              The product brand.
 * @apiSuccess {Number}        brand.id                The brand-ID.
 * @apiSuccess {String}        brand.name              The brand name.
 * @apiSuccess {Picture}       pictures                Array of pictures with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        pictures.id             The picture-ID.
 * @apiSuccess {String}        pictures.name           The picture name.
 * @apiSuccess {String}        pictures.description    The picture description.
 * @apiSuccess {String}        pictures.url            The picture URL.
 * @apiSuccess {Tag}           tags                    Array of tags with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        tags.id                 The tag-ID.
 * @apiSuccess {String}        tags.name               The tag name.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Bouquet de fleurs",
 *       "description": "Bouquet",
 *       "avgMark": 0,
 *       "brand": null,
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "tags": []
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {put} /products/:productId Edit product
 * @apiVersion 1.4.1
 * @apiName PutProduct
 * @apiGroup Products
 *
 * @apiDescription Edit existing product.<br/>
 * Note: <code>In future, ability to create product with POST/PUT stores will
 * be deleted. Please use this endpoint instead.</code>
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam                {Number}    productId       The product-ID to edit.
 *
 * @apiParam (Request Body) {String}    name            The product name.
 * @apiParam (Request Body) {String}    description     The product description.
 * @apiParam (Request Body) {Brand}     [brand]         The product brand (Cf. brand model for more information).
 * @apiParam (Request Body) {Picture[]} pictures        Array of picture (Cf. picture model for more information).
 * @apiParam (Request Body) {Tag[]}     [tags]          Array of tag (Cf. picture model for more information).
 * @apiExample {json}   Json
 * {
 *   "name": "Bouquet",
 *   "description": "Bouquet",
 *   "pictures": [
 *       {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *       }
 *   ],
 *   "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 * }
 *
 * @apiSuccess {Number}        id                      The product-ID.
 * @apiSuccess {String}        name                    The product name.
 * @apiSuccess {String}        description             The product description.
 * @apiSuccess {Number{0-5}}   avgMark                 The average product mark.
 * @apiSuccess {Brand}         brand=null              The product brand.
 * @apiSuccess {Number}        brand.id                The brand-ID.
 * @apiSuccess {String}        brand.name              The brand name.
 * @apiSuccess {Picture}       pictures                Array of pictures with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        pictures.id             The picture-ID.
 * @apiSuccess {String}        pictures.name           The picture name.
 * @apiSuccess {String}        pictures.description    The picture description.
 * @apiSuccess {String}        pictures.url            The picture URL.
 * @apiSuccess {Tag}           tags                    Array of tags with for each
 *                                                     the following parameters.
 * @apiSuccess {Number}        tags.id                 The tag-ID.
 * @apiSuccess {String}        tags.name               The tag name.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "avgMark": 0,
 *       "brand": null,
 *       "pictures": [{
 *           "id": 1,
 *           "name": "Fleur",
 *           "description": "Belle fleur",
 *           "url": "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg"
 *       }],
 *       "tags": []
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {delete} /users/me Delete user
 * @apiVersion 1.2.0
 * @apiName DeleteUser
 * @apiGroup Users
 *
 * @apiDescription Delete user.
 *
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}    id              The User-ID.
 *
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 204 No Content
 *
 * @apiUse      AuthorisationError
 * @apiUse      InternalError
 */

/**
 * @api {post} /stores Create new store
 * @apiVersion 1.4.4
 * @apiName PostStore
 * @apiGroup Stores
 *
 * @apiDescription Post a new store.<br/>
 * </u>Minimum role:</u> Contributor
 * Note: <code>In future, ability to create product with this endpoint will be deleted.
 * Please use POST /products endpoint instead.</code>
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}        name        The store name.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}        description The store description.
 * @apiParam (Request Body) {String}        siret       The store siret.<br/>
 *                                                      Rules: 14 digits.
 * @apiParam (Request Body) {Address}       address     The store address (Cf. address model).
 * @apiParam (Request Body) {Category[]}    categories  Array of categories (Cf. category model).
 * @apiParam (Request Body) {Hour[]}        [hours]     Array of hours (Cf. hour model).
 * @apiParam (Request Body) {Product[]}     [products]  Array of products (Cf. product model).
 * @apiParam (Request Body) {Picture[]}     [pictures]  Array of pictures (Cf. picture model).
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *       "streetNbr": 94,
 *       "street": "Rue Maréchal Joffre",
 *       "zipCode": "44000",
 *       "city": "Nantes",
 *       "country": "France"
 *   },
 *   "categories": [
 *       {
 *           "name": "Fleuriste"
 *       },
 *       {
 *           "name": "Mariage"
 *       }
 *   ]
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *     "streetNbr": 94,
 *     "street": "Rue Maréchal Joffre",
 *     "zipCode": "44000",
 *     "city": "Nantes",
 *     "country": "France",
 *     "location": {
 *       "lat": 47.2184,
 *       "lon": 1.5536
 *     }
 *   },
 *   "hours": [
 *     {
 *       "day": "tuesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "wednesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "thursday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "friday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "saturday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "sunday",
 *       "timeOpen": "09:30:00",
 *       "timeClosed": "13:00:00"
 *     }
 *   ],
 *   "categories": [
 *     {
 *       "name": "Fleuriste"
 *     },
 *     {
 *       "name": "Mariage"
 *     }
 *   ],
 *   "products": [
 *     {
 *       "name": "Composition florale",
 *       "description": "Magnifique composition florale, parfaite pour un mariage",
 *       "pictures": [
 *         {
 *           "name": "Composition florale",
 *           "description": "Belle composition florale",
 *           "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *         }
 *       ],
 *       "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 *     },
 *     {
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "pictures": [
 *         {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *         }
 *       ]
 *     }
 *   ],
 *   "pictures": [
 *     {
 *       "name": "Facade",
 *       "description": "Facade du magasin",
 *       "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 *     },
 *     {
 *       "name": "Intérieur",
 *       "description": "Intérieur du magasin",
 *       "url": "https://lh5.googleusercontent.com/p/AF1QipOvnBGPNMJk4sDQQ01kfD7rFM8eKAM3R9M8MSv8=s177-k-no"
 *     }
 *   ]
 * }
 *
 * @apiSuccess (Created 201)  {Number}        id                            The store-ID.
 * @apiSuccess (Created 201)  {String}        name                          The store name.
 * @apiSuccess (Created 201)  {String}        description                   The store description.
 * @apiSuccess (Created 201)  {String}        siret                         The store siret.
 * @apiSuccess (Created 201)  {Number{0-5}}   avgMark                       The store average mark.
 * @apiSuccess (Created 201)  {User}          owner                         The store owner.
 * @apiSuccess (Created 201)  {Number}        owner.id                      The User-ID.
 * @apiSuccess (Created 201)  {String}        owner.username                The user username.
 * @apiSuccess (Created 201)  {String}        owner.bio                     The user biography.
 * @apiSuccess (Created 201)  {URL}           owner.picture                 The user picture.
 * @apiSuccess (Created 201)  {Address}       address                       The store address (Cf address model).
 * @apiSuccess (Created 201)  {Number}        address.id                    The address-ID.
 * @apiSuccess (Created 201)  {Number}        address.streetNbr             The street number.
 * @apiSuccess (Created 201)  {String}        address.street                The street name.
 * @apiSuccess (Created 201)  {String}        address.zipCode               The address zipCode.
 * @apiSuccess (Created 201)  {String}        address.city                  The city name.
 * @apiSuccess (Created 201)  {String}        address.country               The country name.
 * @apiSuccess (Created 201)  {Location}      address.location              The address location
 * @apiSuccess (Created 201)  {Float}         address.location.lat          The address latitude.
 * @apiSuccess (Created 201)  {Float}         address.location.lon          The address longitude.
 * @apiSuccess (Created 201)  {Hours[]}       hours                         List of hours (Cf hour model).
 * @apiSuccess (Created 201)  {Number}        hours.id                      The hour-ID.
 * @apiSuccess (Created 201)  {String}        hours.day                     The week day.
 * @apiSuccess (Created 201)  {String}        hours.timeOpen                The open time.
 * @apiSuccess (Created 201)  {String}        hours.timeClosed              The close time.
 * @apiSuccess (Created 201)  {Categories[]}  categories                    List of categories (Cf categories model).
 * @apiSuccess (Created 201)  {Number}        categories.id                 The category-ID.
 * @apiSuccess (Created 201)  {String}        categories.name               The category name.
 * @apiSuccess (Created 201)  {Pictures[]}    pictures                      List of pictures (Cf pictures model).
 * @apiSuccess (Created 201)  {Number}        pictures.id                   The picture-ID.
 * @apiSuccess (Created 201)  {String}        picture.name                  The picture name.
 * @apiSuccess (Created 201)  {String}        picture.description           The picture description.
 * @apiSuccess (Created 201)  {URL}           picture.url                   The picture url.
 * @apiSuccess (Created 201)  {Products[]}    products                      List of products (Cf products model).
 * @apiSuccess (Created 201)  {Offers[]}      offers                        List of offers.
 * @apiSuccess (Created 201)  {Comments}      comments                      List of comments (Cf comments endpoint).
 * @apiSuccess (Created 201)  {Number}        nbProducts                    Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "bio": null,
 *         "picture": null
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "comments": [],
 *       "nbProducts": 0
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      InternalError
 */

/**
 * @api {put} /stores/storeId Edit store
 * @apiVersion 1.4.4
 * @apiName PutStore
 * @apiGroup Stores
 *
 * @apiDescription Edit an existing store.<br>Note: <code>In future, ability to create product with this endpoint will
 * be deleted. Please use POST /products endpoint instead.</code>
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        storeId     ID of store to edit.
 * @apiParam (Request Body) {String}        name        The store name.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}        description The store description.
 * @apiParam (Request Body) {String}        siret       The store siret.<br/>
 *                                                      Rules: 14 digits.
 * @apiParam (Request Body) {Address}       address     The store address (Cf. address model).
 * @apiParam (Request Body) {Category[]}    categories  Array of categories (Cf. category model).
 * @apiParam (Request Body) {Hour[]}        [hours]     Array of hours (Cf. hour model).
 * @apiParam (Request Body) {Product[]}     [products]  Array of products (Cf. product model).
 * @apiParam (Request Body) {Picture[]}     [pictures]  Array of pictures (Cf. picture model).
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *       "streetNbr": 94,
 *       "street": "Rue Maréchal Joffre",
 *       "zipCode": "44000",
 *       "city": "Nantes",
 *       "country": "France"
 *   },
 *   "categories": [
 *       {
 *           "name": "Fleuriste"
 *       },
 *       {
 *           "name": "Mariage"
 *       }
 *   ]
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *     "streetNbr": 94,
 *     "street": "Rue Maréchal Joffre",
 *     "zipCode": "44000",
 *     "city": "Nantes",
 *     "country": "France",
 *     "location": {
 *       "lat": 47.2184,
 *       "lon": 1.5536
 *     }
 *   },
 *   "hours": [
 *     {
 *       "day": "tuesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "wednesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "thursday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "friday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "saturday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "sunday",
 *       "timeOpen": "09:30:00",
 *       "timeClosed": "13:00:00"
 *     }
 *   ],
 *   "categories": [
 *     {
 *       "name": "Fleuriste"
 *     },
 *     {
 *       "name": "Mariage"
 *     }
 *   ],
 *   "products": [
 *     {
 *       "name": "Composition florale",
 *       "description": "Magnifique composition florale, parfaite pour un mariage",
 *       "pictures": [
 *         {
 *           "name": "Composition florale",
 *           "description": "Belle composition florale",
 *           "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *         }
 *       ],
 *       "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 *     },
 *     {
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "pictures": [
 *         {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *         }
 *       ]
 *     }
 *   ],
 *   "pictures": [
 *     {
 *       "name": "Facade",
 *       "description": "Facade du magasin",
 *       "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 *     },
 *     {
 *       "name": "Intérieur",
 *       "description": "Intérieur du magasin",
 *       "url": "https://lh5.googleusercontent.com/p/AF1QipOvnBGPNMJk4sDQQ01kfD7rFM8eKAM3R9M8MSv8=s177-k-no"
 *     }
 *   ]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Comments}      comments                    List of comments (Cf comments endpoint).
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "bio": null,
 *         "picture": null
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": 44000,
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 47.2184,
 *           "lon": 1.5536
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "comments": [],
 *       "nbProducts": 0
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {post} /stores/:storeId/products Add product list
 * @apiVersion 1.4.4
 * @apiName PostProductToStore
 * @apiGroup Stores
 *
 * @apiDescription Edit list of products bind to store. Add product to existing list. If product already, do nothing.
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam    {Number}    storeId     The store-ID where to edit list of products.
 *
 * @apiParam (Request Body) {Number[]}  products    List of product-ID to bind to this store.
 * @apiExample {json}   Json
 * {
 *   products: [1, 2]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Comments}      comments                    List of comments (Cf comments endpoint).
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "bio": null,
 *         "picture": null
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [
 *         {
 *           "id": 1,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         },
 *         {
 *           "id": 2,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         }
 *       ],
 *       "offers": [],
 *       "comments": [],
 *       "nbProducts": 2
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {put} /stores/:storeId/products Edit product list
 * @apiVersion 1.4.4
 * @apiName PutProductToStore
 * @apiGroup Stores
 *
 * @apiDescription Edit list of products bind to store. Previous list will be deleted.
 *
 * @apiUse AuthenticationHeader
 * @apiUse ContentTypeHeader
 *
 * @apiParam    {Number}    storeId     The store-ID where to edit list of products.
 *
 * @apiParam (Request Body) {Number[]}  products    List of product-ID to bind to this store.
 * @apiExample {json}   Json
 * {
 *   products: [1, 2]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Comments}      comments                    List of comments (Cf comments endpoint).
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "bio": null,
 *         "picture": null
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [
 *         {
 *           "id": 1,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         },
 *         {
 *           "id": 2,
 *           "name": "Bouquet florale",
 *           "description": "Magnifique composition florale, parfaite pour un mariage",
 *           "avgMark": 0,
 *           "brand": null,
 *           "pictures": [
 *             {
 *               "id": 1,
 *               "name": "Bouquet florale",
 *               "description": "Belle composition florale",
 *               "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *             }
 *           ],
 *           "tags": [
 *             {
 *               "id": 1,
 *               "name": "Fleurs"
 *             }
 *           ]
 *         }
 *       ],
 *       "offers": [],
 *       "comments": [],
 *       "nbProducts": 2
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

/**
 * @api {post} /auth/register Register user
 * @apiVersion 1.2.0
 * @apiName PostAuthRegister
 * @apiGroup Auth
 *
 * @apiDescription Register a new user.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}    username        The user username.<br/>
 *                                                      Rules: 4 characters minimum and 16 maximum.
 * @apiParam (Request Body) {Email}     email           The user email.<br/>
 *                                                      Rules: Any valid email.
 * @apiParam (Request Body) {String}    password        The user password.<br/>
 *                                                      Rules: 8 characters minimum and 32 maximum,
 *                                                      1 uppercase, 1 lowercase, 1 number and 1 special.
 * @apiParam (Request Body) {Email}     [parentEmail]   The user parent email.<br/>
 *                                                      Rules: Any valid email.
 * @apiExample {json}   Json (minimal)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 *   "parentEmail": "parent.barrie@domain.fr",
 * }
 *
 * @apiSuccess (Created 201) {String}           id              The User-ID.
 * @apiSuccess (Created 201) {String}           username        The User username.
 * @apiSuccess (Created 201) {Email}            email           The User email.
 * @apiSuccess (Created 201) {String=["Admin", "Moderator", "Contributor", "User", "Custom"]}    role    The User role.
 * @apiSuccess (Created 201) {Permission[]}     permissions     The User permissions.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "username": "John",
 *       "email": "john@email.test"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      InternalError
 * @apiError    (Bad Request 400)   UserExist   A user already exists with this email
 */

 /**
 * @api {post} /auth/register Register user
 * @apiVersion 1.4.15
 * @apiName PostAuthRegister
 * @apiGroup Auth
 *
 * @apiDescription Register a new user. On successful registration, an validation email is send to user email.
 *
 * @apiUse ContentTypeHeader
 *
 * @apiParam (Request Body) {String}    username        The user username.<br/>
 *                                                      Rules: 3 characters minimum and 16 maximum.
 * @apiParam (Request Body) {Email}     email           The user email.<br/>
 *                                                      Rules: Any valid email.
 * @apiParam (Request Body) {String}    password        The user password.<br/>
 *                                                      Rules: 8 characters minimum and 32 maximum,
 *                                                      1 uppercase, 1 lowercase, 1 number and 1 special.
 * @apiParam (Request Body) {Email}     [parentEmail]   The user parent email.<br/>
 *                                                      Rules: Any valid email.
 * @apiExample {json}   Json (minimal)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "username": "Barrie",
 *   "email": "barrie@domain.fr",
 *   "password": "somePassword1*"
 *   "parentEmail": "parent.barrie@domain.fr",
 * }
 *
 * @apiSuccess (Created 201) {String}           id              The User-ID.
 * @apiSuccess (Created 201) {String}           username        The User username.
 * @apiSuccess (Created 201) {Email}            email           The User email.
 * @apiSuccess (Created 201) {String=["Admin", "Moderator", "Contributor", "User", "Custom"]}    role    The User role.
 * @apiSuccess (Created 201) {Permission[]}     permissions     The User permissions.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "username": "John",
 *       "email": "john@email.test"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      InternalError
 * @apiError    (Bad Request 400)   UserExist   A user already exists with this email
 */

 /**
 * @api {post} /offers Create offer
 * @apiVersion 1.4.11
 * @apiName PostOffer
 * @apiGroup Offers
 *
 * @apiDescription Create a new offer<br/>
 * Permissions: Offer.Create.All || Offer.Create.NoOwner || Offer.Create.OwnStore
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}            name            The offer name.<br/>
 *                                                              Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}            description     The offer description.<br/>
 *                                                              Rules: 1 characters minimum.
 * @apiParam (Request Body) {Number{1-100}}     discount        Discount %.
 * @apiParam (Request Body) {String}            startTime       Start time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date
 * @apiParam (Request Body) {String}            [expiryTime]    End time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date greater than startTime
 * @apiParam (Request Body) {Number}            productId       The product ID link to this offer.
 * @apiParam (Request Body) {Number}            storeId         The store ID link to this offer.
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "productId": 23,
 *   "storeId": 15
 * }
 * * @apiExample {json}   Json (full)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "expiryTime": "2019-04-18T13:58:30.000Z",
 *   "productId": 1,
 *   "storeId": 1
 * }
 *
 * @apiSuccess  (Created 201) {Number}      id              The offer-ID.
 * @apiSuccess  (Created 201) {String}      name            The offer name.
 * @apiSuccess  (Created 201) {String}      description     The offer description.
 * @apiSuccess  (Created 201) {Number}      discount        The offer discount.
 * @apiSuccess  (Created 201) {Product}     product         The product link to this offer (Cf. product model).
 * @apiSuccess  (Created 201) {Store}       store           The store link to this offer (Cf. store endpoint).
 * @apiSuccess  (Created 201) {String}      startTime       The start time of the offer.
 * @apiSuccess  (Created 201) {String}      expiryTime      The end time of the offer.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "description": "Test d'une offre",
 *       "discount": 70,
 *       "store": {},
 *       "product": {},
 *       "startTime": "20190309171633",
 *       "expiryTime": "20190418180000"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

 /**
 * @api {put} /offers/:offerId Edit offer (PUT)
 * @apiVersion 1.4.11
 * @apiName EditOffer
 * @apiGroup Offers
 *
 * @apiDescription Edit the requested offer<br/>
 * Permissions: Offer.Update.All || Offer.Update.NoOwner || Offer.Update.OwnStore
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * * @apiParam {Number}   id   The offer-ID.
 *
 * @apiParam (Request Body) {String}            name            The offer name.<br/>
 *                                                              Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}            description     The offer description.<br/>
 *                                                              Rules: 1 characters minimum.
 * @apiParam (Request Body) {Number{1-100}}     discount        Discount %.
 * @apiParam (Request Body) {String}            startTime       Start time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date
 * @apiParam (Request Body) {String}            [expiryTime]    End time of the offer.<br/>
 *                                                              Rules: Any ISO 8601 date greater than startTime
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z"
 * }
 * * @apiExample {json}   Json (full)
 * {
 *   "name": "Test",
 *   "description": "Test d'une offre",
 *   "discount": 70,
 *   "startTime": "2019-03-18T13:58:30.000Z",
 *   "expiryTime": "2019-04-18T13:58:30.000Z"
 * }
 *
 * @apiSuccess  (Created 201) {Number}      id              The offer-ID.
 * @apiSuccess  (Created 201) {String}      name            The offer name.
 * @apiSuccess  (Created 201) {String}      description     The offer description.
 * @apiSuccess  (Created 201) {Number}      discount        The offer discount.
 * @apiSuccess  (Created 201) {Product}     product         The product link to this offer (Cf. product model).
 * @apiSuccess  (Created 201) {Store}       store           The store link to this offer (Cf. store endpoint).
 * @apiSuccess  (Created 201) {String}      startTime       The start time of the offer.
 * @apiSuccess  (Created 201) {String}      expiryTime      The end time of the offer.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "description": "Test d'une offre",
 *       "discount": 70,
 *       "store": {},
 *       "product": {},
 *       "startTime": "20190309171633",
 *       "expiryTime": "20190418180000"
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */

 /**
 * @api {post} /stores Create store
 * @apiVersion 1.4.11
 * @apiName PostStore
 * @apiGroup Stores
 *
 * @apiDescription Post a new store.<br/>
 * Permission: Store.Create
 * Note: <code>In future, ability to create product with this endpoint will be deleted.
 * Please use POST /products endpoint instead.</code>
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam (Request Body) {String}        name        The store name.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}        description The store description.
 * @apiParam (Request Body) {String}        siret       The store siret.<br/>
 *                                                      Rules: 14 digits.
 * @apiParam (Request Body) {Address}       address     The store address (Cf. address model).
 * @apiParam (Request Body) {Category[]}    categories  Array of categories (Cf. category model).
 * @apiParam (Request Body) {Hour[]}        [hours]     Array of hours (Cf. hour model).
 * @apiParam (Request Body) {Product[]}     [products]  Array of products (Cf. product model).
 * @apiParam (Request Body) {Picture[]}     [pictures]  Array of pictures (Cf. picture model).
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *       "streetNbr": 94,
 *       "street": "Rue Maréchal Joffre",
 *       "zipCode": "44000",
 *       "city": "Nantes",
 *       "country": "France"
 *   },
 *   "categories": [
 *       {
 *           "name": "Fleuriste"
 *       },
 *       {
 *           "name": "Mariage"
 *       }
 *   ]
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *     "streetNbr": 94,
 *     "street": "Rue Maréchal Joffre",
 *     "zipCode": "44000",
 *     "city": "Nantes",
 *     "country": "France",
 *     "location": {
 *       "lat": 47.2184,
 *       "lon": 1.5536
 *     }
 *   },
 *   "hours": [
 *     {
 *       "day": "tuesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "wednesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "thursday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "friday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "saturday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "sunday",
 *       "timeOpen": "09:30:00",
 *       "timeClosed": "13:00:00"
 *     }
 *   ],
 *   "categories": [
 *     {
 *       "name": "Fleuriste"
 *     },
 *     {
 *       "name": "Mariage"
 *     }
 *   ],
 *   "products": [
 *     {
 *       "name": "Composition florale",
 *       "description": "Magnifique composition florale, parfaite pour un mariage",
 *       "pictures": [
 *         {
 *           "name": "Composition florale",
 *           "description": "Belle composition florale",
 *           "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *         }
 *       ],
 *       "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 *     },
 *     {
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "pictures": [
 *         {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *         }
 *       ]
 *     }
 *   ],
 *   "pictures": [
 *     {
 *       "name": "Facade",
 *       "description": "Facade du magasin",
 *       "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 *     },
 *     {
 *       "name": "Intérieur",
 *       "description": "Intérieur du magasin",
 *       "url": "https://lh5.googleusercontent.com/p/AF1QipOvnBGPNMJk4sDQQ01kfD7rFM8eKAM3R9M8MSv8=s177-k-no"
 *     }
 *   ]
 * }
 *
 * @apiSuccess (Created 201)  {Number}        id                            The store-ID.
 * @apiSuccess (Created 201)  {String}        name                          The store name.
 * @apiSuccess (Created 201)  {String}        description                   The store description.
 * @apiSuccess (Created 201)  {String}        siret                         The store siret.
 * @apiSuccess (Created 201)  {Number{0-5}}   avgMark                       The store average mark.
 * @apiSuccess (Created 201)  {User}          owner                         The store owner.
 * @apiSuccess (Created 201)  {Number}        owner.id                      The User-ID.
 * @apiSuccess (Created 201)  {String}        owner.username                The user username.
 * @apiSuccess (Created 201)  {String}        owner.bio                     The user biography.
 * @apiSuccess (Created 201)  {URL}           owner.picture                 The user picture.
 * @apiSuccess (Created 201)  {Address}       address                       The store address (Cf address model).
 * @apiSuccess (Created 201)  {Number}        address.id                    The address-ID.
 * @apiSuccess (Created 201)  {Number}        address.streetNbr             The street number.
 * @apiSuccess (Created 201)  {String}        address.street                The street name.
 * @apiSuccess (Created 201)  {String}        address.zipCode               The address zipCode.
 * @apiSuccess (Created 201)  {String}        address.city                  The city name.
 * @apiSuccess (Created 201)  {String}        address.country               The country name.
 * @apiSuccess (Created 201)  {Location}      address.location              The address location
 * @apiSuccess (Created 201)  {Float}         address.location.lat          The address latitude.
 * @apiSuccess (Created 201)  {Float}         address.location.lon          The address longitude.
 * @apiSuccess (Created 201)  {Hours[]}       hours                         List of hours (Cf hour model).
 * @apiSuccess (Created 201)  {Number}        hours.id                      The hour-ID.
 * @apiSuccess (Created 201)  {String}        hours.day                     The week day.
 * @apiSuccess (Created 201)  {String}        hours.timeOpen                The open time.
 * @apiSuccess (Created 201)  {String}        hours.timeClosed              The close time.
 * @apiSuccess (Created 201)  {Categories[]}  categories                    List of categories (Cf categories model).
 * @apiSuccess (Created 201)  {Number}        categories.id                 The category-ID.
 * @apiSuccess (Created 201)  {String}        categories.name               The category name.
 * @apiSuccess (Created 201)  {Pictures[]}    pictures                      List of pictures (Cf pictures model).
 * @apiSuccess (Created 201)  {Number}        pictures.id                   The picture-ID.
 * @apiSuccess (Created 201)  {String}        picture.name                  The picture name.
 * @apiSuccess (Created 201)  {String}        picture.description           The picture description.
 * @apiSuccess (Created 201)  {URL}           picture.url                   The picture url.
 * @apiSuccess (Created 201)  {Products[]}    products                      List of products (Cf products model).
 * @apiSuccess (Created 201)  {Offers[]}      offers                        List of offers.
 * @apiSuccess (Created 201)  {Number}        nbProducts                    Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": "44000",
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 0,
 *           "lon": 0
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "nbProducts": 0
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      ForbiddenError
 * @apiUse      AuthorisationError
 * @apiUse      InternalError
 */

 /**
 * @api {put} /stores/:storeId Edit store (PUT)
 * @apiVersion 1.4.11
 * @apiName PutStore
 * @apiGroup Stores
 *
 * @apiDescription Edit an existing store.<br/>
 * Permissions: Store.Update.All || Store.Update.NoOwner || Store.Update.Own
 * Note: <code>In future, ability to create product with this endpoint will
 * be deleted. Please use POST /products endpoint instead.</code>
 *
 * @apiUse ContentTypeHeader
 * @apiUse AuthenticationHeader
 *
 * @apiParam                {Number}        storeId     ID of store to edit.
 * @apiParam (Request Body) {String}        name        The store name.<br/>
 *                                                      Rules: 1 characters minimum and 255 maximum.
 * @apiParam (Request Body) {String}        description The store description.
 * @apiParam (Request Body) {String}        siret       The store siret.<br/>
 *                                                      Rules: 14 digits.
 * @apiParam (Request Body) {Address}       address     The store address (Cf. address model).
 * @apiParam (Request Body) {Category[]}    categories  Array of categories (Cf. category model).
 * @apiParam (Request Body) {Hour[]}        [hours]     Array of hours (Cf. hour model).
 * @apiParam (Request Body) {Product[]}     [products]  Array of products (Cf. product model).
 * @apiParam (Request Body) {Picture[]}     [pictures]  Array of pictures (Cf. picture model).
 * @apiExample {json}   Json (minimal)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *       "streetNbr": 94,
 *       "street": "Rue Maréchal Joffre",
 *       "zipCode": "44000",
 *       "city": "Nantes",
 *       "country": "France"
 *   },
 *   "categories": [
 *       {
 *           "name": "Fleuriste"
 *       },
 *       {
 *           "name": "Mariage"
 *       }
 *   ]
 * }
 * @apiExample {json}   Json (full)
 * {
 *   "name": "Atmosphère Fleurs",
 *   "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *   "siret": "01234567890123",
 *   "address": {
 *     "streetNbr": 94,
 *     "street": "Rue Maréchal Joffre",
 *     "zipCode": "44000",
 *     "city": "Nantes",
 *     "country": "France",
 *     "location": {
 *       "lat": 47.2184,
 *       "lon": 1.5536
 *     }
 *   },
 *   "hours": [
 *     {
 *       "day": "tuesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "wednesday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "thursday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "friday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "saturday",
 *       "timeOpen": "09:00:00",
 *       "timeClosed": "20:00:00"
 *     },
 *     {
 *       "day": "sunday",
 *       "timeOpen": "09:30:00",
 *       "timeClosed": "13:00:00"
 *     }
 *   ],
 *   "categories": [
 *     {
 *       "name": "Fleuriste"
 *     },
 *     {
 *       "name": "Mariage"
 *     }
 *   ],
 *   "products": [
 *     {
 *       "name": "Composition florale",
 *       "description": "Magnifique composition florale, parfaite pour un mariage",
 *       "pictures": [
 *         {
 *           "name": "Composition florale",
 *           "description": "Belle composition florale",
 *           "url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Composition_florale_-_Lys.jpg"
 *         }
 *       ],
 *       "tags": [{"name": "Bouquet"}, {"name": "Fleurs"}]
 *     },
 *     {
 *       "name": "Bouquet",
 *       "description": "Bouquet",
 *       "pictures": [
 *         {
 *           "name": "Bouquet",
 *           "description": "Beau bouquet",
 *           "url": "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg"
 *         }
 *       ]
 *     }
 *   ],
 *   "pictures": [
 *     {
 *       "name": "Facade",
 *       "description": "Facade du magasin",
 *       "url": "http://www.atmospheredujardin.com/img/facade.jpg"
 *     },
 *     {
 *       "name": "Intérieur",
 *       "description": "Intérieur du magasin",
 *       "url": "https://lh5.googleusercontent.com/p/AF1QipOvnBGPNMJk4sDQQ01kfD7rFM8eKAM3R9M8MSv8=s177-k-no"
 *     }
 *   ]
 * }
 *
 * @apiSuccess  {Number}        id                          The store-ID.
 * @apiSuccess  {String}        name                        The store name.
 * @apiSuccess  {String}        description                 The store description.
 * @apiSuccess  {String}        siret                       The store siret.
 * @apiSuccess  {Number{0-5}}   avgMark                     The store average mark.
 * @apiSuccess  {User}          owner                       The store owner.
 * @apiSuccess  {Number}        owner.id                    The User-ID.
 * @apiSuccess  {String}        owner.username              The user username.
 * @apiSuccess  {String}        owner.bio                   The user biography.
 * @apiSuccess  {URL}           owner.picture               The user picture.
 * @apiSuccess  {Address}       address                     The store address (Cf address model).
 * @apiSuccess  {Number}        address.id                  The address-ID.
 * @apiSuccess  {Number}        address.streetNbr           The street number.
 * @apiSuccess  {String}        address.street              The street name.
 * @apiSuccess  {String}        address.zipCode             The address zipCode.
 * @apiSuccess  {String}        address.city                The city name.
 * @apiSuccess  {String}        address.country             The country name.
 * @apiSuccess  {Location}      address.location            The address location.
 * @apiSuccess  {Float}         address.location.lat        The address latitude.
 * @apiSuccess  {Float}         address.location.lon        The address longitude.
 * @apiSuccess  {hours[]}       hours                       List of hours (Cf hour model).
 * @apiSuccess  {Number}        hours.id                    The hour-ID.
 * @apiSuccess  {String}        hours.day                   The week day.
 * @apiSuccess  {String}        hours.timeOpen              The open time.
 * @apiSuccess  {String}        hours.timeClosed            The close time.
 * @apiSuccess  {Categories[]}  categories                  List of categories (Cf categories model).
 * @apiSuccess  {Number}        categories.id               The category-ID.
 * @apiSuccess  {String}        categories.name             The category name.
 * @apiSuccess  {Pictures[]}    pictures                    List of pictures (Cf pictures model).
 * @apiSuccess  {Number}        pictures.id                 The picture-ID.
 * @apiSuccess  {String}        picture.name                The picture name.
 * @apiSuccess  {String}        picture.description         The picture description.
 * @apiSuccess  {URL}           picture.url                 The picture url.
 * @apiSuccess  {Products[]}    products                    List of products (Cf products model).
 * @apiSuccess  {Offers[]}      offers                      List of offers.
 * @apiSuccess  {Number}        nbProducts                  Number of products available in this store.
 * @apiSuccessExample {json} Success
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Atmosphère Fleurs",
 *       "description": "Cette boutique propose des fleurs, plantes et compositions florales pour les mariages.",
 *       "siret": "01234567890123",
 *       "avgMark": 0,
 *       "owner": {
 *         "id": 1,
 *         "username": "Barrie",
 *         "banner": null,
 *         "picture": null,
 *         "bio": null,
 *         "role": "User"
 *       },
 *       "address": {
 *         "id": 1,
 *         "streetNbr": 94,
 *         "street": "Rue Maréchal Joffre",
 *         "zipCode": 44000,
 *         "city": "Nantes",
 *         "country": "France",
 *         "location": {
 *           "lat": 47.2184,
 *           "lon": 1.5536
 *         }
 *       },
 *       "hours": [],
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Fleuriste"
 *         },
 *         {
 *           "id": 2,
 *           "name": "Mariage"
 *         }
 *       ],
 *       "pictures": [],
 *       "products": [],
 *       "offers": [],
 *       "nbProducts": 0
 *     }
 *
 * @apiUse      ValidationError
 * @apiUse      AuthorisationError
 * @apiUse      ForbiddenError
 * @apiUse      DataNotFoundError
 * @apiUse      InternalError
 */