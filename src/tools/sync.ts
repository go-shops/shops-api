import dotenv from "dotenv";
dotenv.config();
import elasticsearch from "elasticsearch";
import {createConnection} from "typeorm";
import Constants from "../Constants";
import {
    AddressEntity,
    BrandEntity,
    CategoryEntity,
    HourEntity,
    OfferEntity,
    PermissionEntity,
    PictureEntity,
    ProductCommentEntity,
    ProductEntity,
    StoreCommentEntity,
    StoreEntity,
    TagEntity,
    UserEntity,
} from "../entity";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

// Connect to database
logger.info("[sync.ts - TypeORM]: Connect to database");
createConnection({
    database: Constants.DB_NAME,
    entities: [UserEntity, StoreEntity, StoreCommentEntity, AddressEntity, HourEntity, PermissionEntity,
        CategoryEntity, ProductEntity, PictureEntity, OfferEntity, BrandEntity, TagEntity, ProductCommentEntity],
    host: Constants.DB_HOST,
    password: Constants.DB_PASS,
    port: Constants.DB_PORT,
    type: "mysql",
    username: Constants.DB_USER,
}).then(async () => {
    try {
        // Sync stores
        const client = new elasticsearch.Client({
            host: Constants.ELASTIC_HOST,
        });
        await client.indices.delete({index: Constants.ELASTIC_STORES_INDEX});
        await client.indices.delete({index: Constants.ELASTIC_PRODUCTS_INDEX});
        await client.indices.delete({index: Constants.ELASTIC_USERS_INDEX});
        await client.indices.delete({index: Constants.ELASTIC_TAGS_INDEX});
        await client.indices.delete({index: Constants.ELASTIC_BRANDS_INDEX});
        await client.indices.delete({index: Constants.ELASTIC_CATEGORIES_INDEX});
        await client.indices.delete({index: Constants.ELASTIC_PERMISSIONS_INDEX});
        await Elasticsearch.ensureIndices();
        const nbStores: number = await StoreEntity.count();
        logger.info("[sync.ts - Stores]: We have found %s stores", nbStores);
        for (let i = 0; i < nbStores; i += 25) {
            const stores = await StoreEntity.find(
                {skip: i, take: 25,
                    relations: ["owner", "comments", "comments.owner", "products", "offers"]});
            for (const store of stores) {
                logger.info("[sync.ts - Stores]: Sync store with ID %s", store.id);
                await Elasticsearch.saveStore(store);
            }
        }
        // Sync products
        const nbProducts: number = await ProductEntity.count();
        logger.info("[sync.ts - Products]: We have found %s products", nbProducts);
        for (let i = 0; i < nbProducts; i += 25) {
            const products = await ProductEntity.find(
                {skip: i, take: 25, relations: ["stores"]});
            for (const product of products) {
                logger.info("[sync.ts - Products]: Sync product with ID %s", product.id);
                await Elasticsearch.saveProduct(product);
            }
        }
        // Sync users
        const nbUsers: number = await UserEntity.count();
        logger.info("[sync.ts - Users]: We have found %s users", nbUsers);
        for (let i = 0; i < nbUsers; i += 25) {
            const users = await UserEntity.find({skip: i, take: 25});
            for (const user of users) {
                logger.info("[sync.ts - Users]: Sync user with ID %s", user.id);
                await Elasticsearch.saveUser(user);
            }
        }

        // Sync tags
        const nbTags: number = await TagEntity.count();
        logger.info("[sync.ts - Tags]: We have found %s tags", nbTags);
        for (let i = 0; i < nbTags; i += 25) {
            const tags = await TagEntity.find({skip: i, take: 25});
            for (const tag of tags) {
                logger.info("[sync.ts - Tags]: Sync tag with ID %s", tag.id);
                await Elasticsearch.saveTag(tag);
            }
        }

        // Sync brands
        const nbBrands: number = await BrandEntity.count();
        logger.info("[sync.ts - Brands]: We have found %s brands", nbBrands);
        for (let i = 0; i < nbBrands; i += 25) {
            const brands = await BrandEntity.find({skip: i, take: 25});
            for (const brand of brands) {
                logger.info("[sync.ts - Brands]: Sync brand with ID %s", brand.id);
                await Elasticsearch.saveBrand(brand);
            }
        }

        // Sync categories
        const nbCategories: number = await CategoryEntity.count();
        logger.info("[sync.ts - Categories]: We have found %s categories", nbCategories);
        for (let i = 0; i < nbCategories; i += 25) {
            const categories = await CategoryEntity.find({skip: i, take: 25});
            for (const category of categories) {
                logger.info("[sync.ts - Categories]: Sync category with ID %s", category.id);
                await Elasticsearch.saveCategory(category);
            }
        }

        // Sync permissions
        const nbPermissions: number = await PermissionEntity.count();
        logger.info("[sync.ts - Categories]: We have found %s categories", nbPermissions);
        for (let i = 0; i < nbPermissions; i += 25) {
            const permissions = await PermissionEntity.find({skip: i, take: 25});
            for (const perm of permissions) {
                logger.info("[sync.ts - Categories]: Sync category with ID %s", perm.id);
                await Elasticsearch.savePermission(perm);
            }
        }
        process.exit();
    } catch (err) {
        logger.error("[sync.ts]: %s", err.message);
        process.exit(1);
    }
}).catch((error) => logger.error("[sync.ts - TypeORM]: Connection error: %s", error));

// Catch CTRL-C
process.on("SIGINT", () => {
    logger.info("[sync.ts - System]: Receive SIGINT signal");
    logger.info("[sync.ts - System]: Application stopped");
    process.exit();
});
