import dotenv from "dotenv";
dotenv.config();
import {createConnection} from "typeorm";
import Constants from "../Constants";
import {
    AddressEntity,
    BrandEntity,
    CategoryEntity,
    HourEntity,
    OfferEntity,
    PermissionEntity,
    PictureEntity,
    ProductCommentEntity,
    ProductEntity,
    StoreCommentEntity,
    StoreEntity,
    TagEntity,
    UserEntity,
} from "../entity";
import {Utils} from "../utils";
import {permissions} from "../utils";
import logger from "../utils/Logger";

// Connect to database
logger.info("[createPermissions.ts - TypeORM]: Connect to database");
createConnection({
    database: Constants.DB_NAME,
    entities: [UserEntity, StoreEntity, StoreCommentEntity, AddressEntity, HourEntity, PermissionEntity,
        CategoryEntity, ProductEntity, PictureEntity, OfferEntity, BrandEntity, TagEntity, ProductCommentEntity],
    host: Constants.DB_HOST,
    password: Constants.DB_PASS,
    port: Constants.DB_PORT,
    type: "mysql",
    username: Constants.DB_USER,
}).then(async () => {
    try {
        const perms: string[] = Utils.transformObjectInStringArray(permissions, []);

        for (const perm of perms) {
            const existing: PermissionEntity = await PermissionEntity.findOne({where: {permission: perm}});
            if (!existing) {
                logger.debug("[createPermissions.ts - ensurePermissions]: One permission is missing, adding it");
                await PermissionEntity.saveInDb({permission: perm});
            }
        }
        logger.debug("[createPermissions.ts - ensurePermissions]: Permissions ok");
        process.exit();
    } catch (err) {
        logger.error("[createPermissions.ts]: %s", err.message);
        process.exit(1);
    }
}).catch((error) => logger.error("[createPermissions.ts - TypeORM]: Connection error: %s", error));

// Catch CTRL-C
process.on("SIGINT", () => {
    logger.info("[createPermissions.ts - System]: Receive SIGINT signal");
    logger.info("[createPermissions.ts - System]: Application stopped");
    process.exit();
});
