import {UserEntity} from "../../entity";

declare global {
    namespace Express {
        // tslint:disable-next-line:interface-name
        interface Request {
            user: UserEntity;
            roles: string[];
            mandatory: boolean;
        }
    }
}
