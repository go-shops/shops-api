import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import {
    AddressEntity,
    CategoryEntity,
    HourEntity,
    OfferEntity,
    PictureEntity,
    ProductEntity,
    StoreCommentEntity,
    UserEntity,
} from "../entity";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

@Entity("stores")
export class StoreEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public description: string;

    @Column()
    public avgMark: number;

    @Column()
    public siret: string;

    @Column()
    public nbProducts: number;

    @ManyToOne(() => UserEntity, (user) => user.stores)
    @JoinColumn({name: "ownerId"})
    public owner: UserEntity;

    @OneToOne(() => AddressEntity, {eager: true})
    @JoinColumn({name: "addressId"})
    public address: AddressEntity;

    @ManyToMany(() => HourEntity, {eager: true})
    @JoinTable({name: "storesHours", joinColumn: {name: "storeId"}, inverseJoinColumn: {name: "hourId"}})
    public hours: HourEntity[];

    @ManyToMany(() => CategoryEntity, {eager: true})
    @JoinTable({name: "storesCategories", joinColumn: {name: "storeId"}, inverseJoinColumn: {name: "categoryId"}})
    public categories: CategoryEntity[];

    @ManyToMany(() => PictureEntity, {eager: true})
    @JoinTable({name: "storesPictures", joinColumn: {name: "storeId"}, inverseJoinColumn: {name: "pictureId"}})
    public pictures: PictureEntity[];

    @ManyToMany(() => ProductEntity)
    @JoinTable({name: "storesProducts", joinColumn: {name: "storeId"}, inverseJoinColumn: {name: "productId"}})
    public products: ProductEntity[];

    @OneToMany(() => OfferEntity, (offer) => offer.store)
    public offers: OfferEntity[];

    @OneToMany(() => StoreCommentEntity, (comment) => comment.store)
    public comments: StoreCommentEntity[];

    public toJSON(): object {
        logger.debug("[entity/StoreEntity.ts - StoreEntity - toJSON]: Transform store entity into JSON object");
        if (this.owner === undefined || this.owner === null) {
            return {
                id: this.id,
                name: this.name,
                description: this.description,
                avgMark: this.avgMark,
                siret: this.siret,
                owner: this.owner,
                address: this.address,
                hours: this.hours,
                categories: this.categories,
                pictures: this.pictures,
                products: this.products,
                offers: this.offers,
                comments: this.comments,
                nbProducts: this.nbProducts,
            };
        }
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            avgMark: this.avgMark,
            siret: this.siret,
            owner: this.owner.toJSONMin(),
            address: this.address,
            hours: this.hours,
            categories: this.categories,
            pictures: this.pictures,
            products: this.products,
            offers: this.offers,
            comments: this.comments,
            nbProducts: this.nbProducts,
        };
    }

    public applyJSON(json): StoreEntity {
        logger.debug("[entity/StoreEntity.ts - StoreEntity - applyJSON]: Edit store entity from JSON object");
        this.name = json.name;
        this.description = json.description;
        this.avgMark = json.avgMark;
        this.siret = json.siret;
        this.owner = json.owner;
        this.address = json.address;
        this.hours = json.hours;
        this.categories = json.categories;
        this.pictures = json.pictures;
        this.products = json.products;
        this.offers = json.offers;
        this.comments = json.comments;
        this.nbProducts = json.nbProducts;
        return this;
    }

    public static fromJSON(json): StoreEntity {
        logger.debug("[entity/StoreEntity.ts - StoreEntity - fromJSON]: Create new store entity from JSON object");
        const store = new StoreEntity();
        store.name = json.name;
        store.description = json.description;
        store.avgMark = json.avgMark;
        store.siret = json.siret;
        store.owner = json.owner;
        store.address = json.address;
        store.hours = json.hours;
        store.categories = json.categories;
        store.pictures = json.pictures;
        store.products = json.products;
        store.offers = json.offers;
        store.comments = json.comments;
        store.nbProducts = json.nbProducts;
        return store;
    }

    public static async saveInDb(user: UserEntity, store): Promise<StoreEntity> {
        const address: AddressEntity = await AddressEntity.saveInDb(store.address);
        const hours: HourEntity[] = await HourEntity.saveAllInDb(store.hours);
        const categories: CategoryEntity[] = await CategoryEntity.saveAllInDb(store.categories);
        const products: ProductEntity[] = await ProductEntity.saveAllInDb(store.products);
        const pictures: PictureEntity[] = await PictureEntity.saveAllInDb(store.pictures);

        const tmp: StoreEntity = await StoreEntity.fromJSON({
            address,
            categories,
            description: store.description,
            name: store.name,
            avgMark: 0,
            owner: user,
            pictures,
            products,
            hours,
            siret: store.siret,
            nbProducts: products.length,
        });
        await tmp.save();
        await Elasticsearch.saveStore(tmp);
        return tmp;
    }

    public static async updateInDb(user: UserEntity, newStore, oldStore: StoreEntity): Promise<StoreEntity> {
        const address: AddressEntity = await AddressEntity.saveInDb(newStore.address);
        const hours: HourEntity[] = await HourEntity.saveAllInDb(newStore.hours);
        const categories: CategoryEntity[] = await CategoryEntity.saveAllInDb(newStore.categories);
        const products: ProductEntity[] = await ProductEntity.saveAllInDb(newStore.products);
        const pictures: PictureEntity[] = await PictureEntity.saveAllInDb(newStore.pictures);
        let nbProducts = 0;
        if (newStore.products) {
            nbProducts = newStore.products.length;
        }

        oldStore.applyJSON({
            address,
            categories,
            description: newStore.description,
            name: newStore.name,
            avgMark: oldStore.avgMark,
            owner: user,
            pictures,
            products,
            hours,
            siret: newStore.siret,
            nbProducts,
        });
        await oldStore.save();
        await Elasticsearch.saveStore(oldStore);
        return oldStore;
    }

    public static async patchInDb(user: UserEntity, patchStore, oldStore: StoreEntity): Promise<StoreEntity> {
        if (patchStore.address) {
            oldStore.address = await AddressEntity.saveInDb(patchStore.address);
        }
        if (patchStore.hours) {
            oldStore.hours = await HourEntity.saveAllInDb(patchStore.hours);
        }
        if (patchStore.categories) {
            oldStore.categories = await CategoryEntity.saveAllInDb(patchStore.categories);
        }
        if (patchStore.pictures) {
            oldStore.pictures = await PictureEntity.saveAllInDb(patchStore.pictures);
        }

        StoreEntity.merge(oldStore, {...patchStore});
        await oldStore.save();
        await Elasticsearch.saveStore(oldStore);
        return oldStore;
    }
}
