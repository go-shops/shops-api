import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany, OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import {BrandEntity, PictureEntity, ProductCommentEntity, StoreEntity, TagEntity} from "../entity";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

@Entity("products")
export class ProductEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public description: string;

    @OneToOne(() => BrandEntity, {eager: true})
    @JoinColumn({name: "brandId"})
    public brand: BrandEntity;

    @Column()
    public avgMark: number;

    @Column("time")
    public createTime: string;

    @Column("time")
    public updateTime: string;

    @ManyToMany(() => PictureEntity, {eager: true})
    @JoinTable({name: "productsPictures", joinColumn: {name: "productId"}, inverseJoinColumn: {name: "pictureId"}})
    public pictures: PictureEntity[];

    @ManyToMany(() => StoreEntity)
    @JoinTable({name: "storesProducts", joinColumn: {name: "productId"}, inverseJoinColumn: {name: "storeId"}})
    public stores: StoreEntity[];

    @ManyToMany(() => TagEntity, {eager: true})
    @JoinTable({name: "productsTags", joinColumn: {name: "productId"}, inverseJoinColumn: {name: "tagId"}})
    public tags: TagEntity[];

    @OneToMany(() => ProductCommentEntity, (comment) => comment.product)
    public comments: ProductCommentEntity[];

    public toJSON(): object {
        logger.debug("[entity/ProductEntity.ts - ProductEntity - toJSON]: Transform product entity into JSON object");
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            avgMark: this.avgMark,
            brand: this.brand,
            pictures: this.pictures,
            stores: this.stores,
            tags: this.tags,
        };
    }

    public applyJSON(json): ProductEntity {
        logger.debug("[entity/ProductEntity.ts - ProductEntity - applyJSON]: Edit product entity from JSON object");
        this.name = json.name;
        this.description = json.description;
        this.avgMark = json.avgMark;
        this.brand = json.brand;
        this.pictures = json.pictures;
        this.stores = json.stores;
        this.tags = json.tags;
        return this;
    }

    public static fromJSON(json): ProductEntity {
        logger.debug("[entity/ProductEntity.ts - ProductEntity - fromJSON]: " +
            "Create new product entity from JSON object");
        const product = new ProductEntity();
        product.name = json.name;
        product.description = json.description;
        product.brand = json.brand;
        product.avgMark = json.avgMark;
        product.pictures = json.pictures;
        product.stores = json.stores;
        product.tags = json.tags;
        return product;
    }

    public static async saveInDb(product): Promise<ProductEntity> {
        const existing: ProductEntity = await this.findOne({where: {name: product.name}});
        if (existing) {
            return existing;
        }
        let pictures = null;
        let brand = null;
        let tags = null;
        if (product.pictures) {
            pictures = await PictureEntity.saveAllInDb(product.pictures);
        }
        if (product.tags) {
            tags = await TagEntity.saveAllInDb(product.tags);
        }
        if (product.brand) {
            brand = await BrandEntity.saveInDb(product.brand);
        }
        const tmp: ProductEntity = await this.fromJSON({...product, brand, pictures, tags, avgMark: 0});
        await tmp.save();
        await Elasticsearch.saveProduct(tmp);
        return tmp;
    }

    public static async saveAllInDb(arr): Promise<ProductEntity[]> {
        const products: ProductEntity[] = [];
        if (arr === undefined) {
            return products;
        }
        for (let i = 0; i < arr.length; i++) {
            const existing: ProductEntity = await this.findOne(
                {where: {name: arr[i].name}});
            if (existing) {
                products[i] = existing;
            } else {
                products[i] = await ProductEntity.saveInDb(arr[i]);
            }
        }
        return products;
    }

    public static async updateInDb(oldProduct: ProductEntity, newProduct): Promise<ProductEntity> {
        let pictures: PictureEntity[] = [];
        let tags: TagEntity[] = [];
        let brand: BrandEntity = null;
        if (newProduct.pictures) {
            pictures = await PictureEntity.saveAllInDb(newProduct.pictures);
        }
        if (newProduct.tags) {
            tags = await TagEntity.saveAllInDb(newProduct.tags);
        }
        if (newProduct.brand) {
            brand = await BrandEntity.saveInDb(newProduct.brand);
        }
        oldProduct.applyJSON({...newProduct, brand, pictures, tags});
        await oldProduct.save();
        await Elasticsearch.saveProduct(oldProduct);
        return oldProduct;
    }

    public static async patchInDb(oldProduct: ProductEntity, patchProduct): Promise<ProductEntity> {
        if (patchProduct.pictures) {
            oldProduct.pictures = await PictureEntity.saveAllInDb(patchProduct.pictures);
        }
        if (patchProduct.tags) {
            oldProduct.tags = await TagEntity.saveAllInDb(patchProduct.tags);
        }
        if (patchProduct.brand) {
            oldProduct.brand = await BrandEntity.saveInDb(patchProduct.brand);
        }

        ProductEntity.merge(oldProduct, {...patchProduct});
        await oldProduct.save();
        await Elasticsearch.saveProduct(oldProduct);
        return oldProduct;
    }
}
