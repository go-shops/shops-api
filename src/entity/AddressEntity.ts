import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import logger from "../utils/Logger";

@Entity("addresses")
export class AddressEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public street: string;

    @Column()
    public streetNbr: number;

    @Column()
    public city: string;

    @Column()
    public zipCode: string;

    @Column("float")
    public lon: number;

    @Column("float")
    public lat: number;

    @Column()
    public country: string;

    public toJSON(): object {
        logger.debug("[entity/AddressEntity.ts - AddressEntity - toJSON]: Transform address entity into JSON object");
        let location = null;
        if (this.lon !== null && this.lat !== null) {
            location = {
                lat: this.lat,
                lon: this.lon,
            };
        }
        return {
            id: this.id,
            streetNbr: this.streetNbr,
            street: this.street,
            zipCode: this.zipCode,
            city: this.city,
            country: this.country,
            location,
        };
    }

    public applyJSON(json): AddressEntity {
        logger.debug("[entity/AddressEntity.ts - AddressEntity - applyJSON]: Edit address entity from JSON object");
        this.streetNbr = json.streetNbr;
        this.street = json.street;
        this.zipCode = json.zipCode;
        this.city = json.city;
        if (json.location) {
            this.lat = json.location.lat;
            this.lon = json.location.lon;
        } else {
            this.lat = null;
            this.lon = null;
        }
        this.country = json.country;
        return this;
    }

    public static fromJSON(json): AddressEntity {
        logger.debug("[entity/AddressEntity.ts - AddressEntity - fromJSON]: " +
            "Create new address entity from JSON object");
        const address = new AddressEntity();
        address.streetNbr = json.streetNbr;
        address.street = json.street;
        address.zipCode = json.zipCode;
        address.city = json.city;
        if (json.location) {
            address.lat = json.location.lat;
            address.lon = json.location.lon;
        } else {
            address.lat = null;
            address.lon = null;
        }
        address.country = json.country;
        return address;
    }

    public static async saveInDb(add): Promise<AddressEntity> {
        let existing: AddressEntity = null;
        if (add.location) {
            existing = await this.findOne(
                {where: {streetNbr: add.streetNbr, street: add.street, city: add.city, zipCode: add.zipCode,
                        country: add.country, lon: add.location.lon, lat: add.location.lat}});
        } else {
            existing = await this.findOne(
                {where: {streetNbr: add.streetNbr, street: add.street, city: add.city, zipCode: add.zipCode,
                        country: add.country}});
        }
        if (existing) {
            return existing;
        }
        return await this.save(AddressEntity.fromJSON(add));
    }
}
