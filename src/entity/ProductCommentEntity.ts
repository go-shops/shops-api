import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {ProductEntity, UserEntity} from "../entity";
import logger from "../utils/Logger";

@Entity("productsComments")
export class ProductCommentEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public comment: string;

    @Column()
    public mark: number;

    @Column()
    public createTime: string;

    @Column()
    public updateTime: string;

    @ManyToOne(() => ProductEntity, (product) => product.comments)
    @JoinColumn({name: "productId"})
    public product: ProductEntity;

    @ManyToOne(() => UserEntity, (user) => user.comments)
    @JoinColumn({name: "authorId"})
    public owner: UserEntity;

    @Column()
    public authorId: number;

    @Column()
    public productId: number;

    public toJSON(): object {
        logger.debug("[entity/ProductCommentEntity.ts - ProductCommentEntity - toJSON]: " +
            "Transform comment entity into JSON object");
        if (this.owner === undefined || this.owner === null) {
            return {
                id: this.id,
                comment: this.comment,
                mark: this.mark,
                product: this.product,
                user: this.owner,
            };
        }
        return {
            id: this.id,
            comment: this.comment,
            mark: this.mark,
            product: this.product,
            owner: this.owner.toJSONMin(),
        };
    }

    public applyJSON(json): ProductCommentEntity {
        logger.debug("[entity/ProductCommentEntity.ts - ProductCommentEntity - applyJSON]: " +
            "Edit user entity from JSON object");
        this.comment = json.comment;
        this.mark = json.mark;
        this.product = json.product;
        this.owner = json.owner;
        return this;
    }

    public static fromJSON(json): ProductCommentEntity {
        logger.debug("[entity/ProductCommentEntity.ts - ProductCommentEntity - fromJSON]: " +
            "Create new user entity from JSON object");
        const comment = new ProductCommentEntity();
        comment.comment = json.comment;
        comment.mark = json.mark;
        comment.product = json.product;
        comment.owner = json.owner;
        return comment;
    }

    public static async saveInDb(comment: object): Promise<ProductCommentEntity> {
        return await this.save(ProductCommentEntity.fromJSON(comment));
    }

    public static async updateInDb(comment: JSON, obj: ProductCommentEntity): Promise<ProductCommentEntity> {
        return await this.save(obj.applyJSON(comment));
    }
}
