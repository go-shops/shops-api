import nodemailer from "nodemailer";
import pug from "pug";
import {BaseEntity, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import Constants from "../Constants";
import {PermissionEntity, StoreCommentEntity, StoreEntity} from "../entity";
import {
    adminPermissions,
    contributorPermissions,
    Elasticsearch,
    moderatorPermissions,
    role,
    userPermissions,
} from "../utils";
import logger from "../utils/Logger";

@Entity("users")
export class UserEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public username: string;

    @Column()
    public email: string;

    @Column()
    public password: string;

    @Column()
    public bio: string;

    @Column()
    public parentEmail: string;

    @Column()
    public picture: string;

    @Column()
    public banner: string;

    @Column()
    public active: boolean;

    @Column()
    public userHash: string;

    @Column("enum")
    public role: string;

    @ManyToMany(() => PermissionEntity)
    @JoinTable({name: "usersPermissions", joinColumn: {name: "userId"}, inverseJoinColumn: {name: "permissionId"}})
    public permissions: PermissionEntity[];

    @OneToMany(() => StoreCommentEntity, (comment) => comment.owner)
    public comments: StoreCommentEntity[];

    @OneToMany(() => StoreEntity, (store) => store.owner)
    public stores: StoreEntity[];

    public toJSON(): object {
        logger.debug("[entity/UserEntity.ts - UserEntity - toJSON]: Transform user entity into JSON object");
        return {
            id: this.id,
            username: this.username,
            email: this.email,
            banner: this.banner,
            picture: this.picture,
            bio: this.bio,
            active: this.active,
            role: this.role,
            permissions: this.permissions,
            parentEmail: this.parentEmail,
            comments: this.comments,
            stores: this.stores,
        };
    }

    public toJSONMin(): object {
        logger.debug("[entity/UserEntity.ts - UserEntity - toJSONMin]: Transform user entity into minimal JSON object");
        return {
            id: this.id,
            username: this.username,
            banner: this.banner,
            picture: this.picture,
            bio: this.bio,
            role: this.role,
            comments: this.comments,
            stores: this.stores,
        };
    }

    public applyJSON(json): UserEntity {
        logger.debug("[entity/UserEntity.ts - UserEntity - applyJSON]: Edit user entity from JSON object");
        this.username = json.username;
        this.email = json.email;
        this.password = json.password;
        this.banner = json.banner;
        this.picture = json.picture;
        this.bio = json.bio;
        this.parentEmail = json.parentEmail;
        this.active = json.active;
        this.userHash = json.userHash;
        this.comments = json.comments;
        this.stores = json.stores;
        this.role = json.role;
        this.permissions = json.permissions;
        return this;
    }

    public static fromJSON(json): UserEntity {
        logger.debug("[entity/UserEntity.ts - UserEntity - fromJSON]: Create new user entity from JSON object");
        const user = new UserEntity();
        user.id = json.id;
        user.username = json.username;
        user.email = json.email;
        user.password = json.password;
        user.bio = json.bio;
        user.banner = json.banner;
        user.picture = json.picture;
        user.parentEmail = json.parentEmail;
        user.active = json.active;
        user.userHash = json.userHash;
        user.comments = json.comments;
        user.stores = json.stores;
        user.role = json.role;
        user.permissions = json.permissions;
        return user;
    }

    public static async saveInDb(user: object,
                                 rl: string = role.User,
                                 perms: PermissionEntity[] = []): Promise<UserEntity> {
        const tmp: UserEntity = UserEntity.fromJSON(user);
        switch (rl) {
            case role.User.toString():
                tmp.permissions = await PermissionEntity.getPermissionsFromArray(userPermissions);
                break;
            case role.Contributor.toString():
                tmp.permissions = await PermissionEntity.getPermissionsFromArray(contributorPermissions);
                break;
            case role.Moderator.toString():
                tmp.permissions = await PermissionEntity.getPermissionsFromArray(moderatorPermissions);
                break;
            case role.Admin.toString():
                tmp.permissions = await PermissionEntity.getPermissionsFromArray(adminPermissions);
                break;
            case role.Custom.toString():
                tmp.permissions = perms;
                break;
            default:
                tmp.permissions = [];
        }
        tmp.role = rl;
        await tmp.save();
        await Elasticsearch.saveUser(tmp);
        return tmp;
    }

    public static async updateInDb(newUser: UserEntity, oldUser: UserEntity, rl: string = role.User,
                                   perms: PermissionEntity[] = []): Promise<UserEntity> {
        newUser.id = oldUser.id;
        switch (rl) {
            case role.User.toString():
                newUser.permissions = await PermissionEntity.getPermissionsFromArray(userPermissions);
                break;
            case role.Contributor.toString():
                newUser.permissions = await PermissionEntity.getPermissionsFromArray(contributorPermissions);
                break;
            case role.Moderator.toString():
                newUser.permissions = await PermissionEntity.getPermissionsFromArray(moderatorPermissions);
                break;
            case role.Admin.toString():
                newUser.permissions = await PermissionEntity.getPermissionsFromArray(adminPermissions);
                break;
            case role.Custom.toString():
                newUser.permissions = perms;
                break;
            default:
                newUser.permissions = [];
        }
        newUser.role = rl;
        await newUser.save();
        await Elasticsearch.saveUser(newUser);
        return newUser;
    }

    public sendValidationMail() {
        let mailTo = null;
        if (!this.parentEmail) {
            mailTo = this.email;
        } else {
            mailTo = this.parentEmail;
        }
        const transporter = nodemailer.createTransport({
            host: Constants.MAIL_HOST,
            port: Constants.MAIL_PORT,
            auth: {
                user: Constants.MAIL_USER,
                pass: Constants.MAIL_PASS,
            },
            tls: {rejectUnauthorized: false},
        });

        const mailOptions = {
            from: Constants.MAIL_USER,
            to: mailTo,
            subject: Constants.MAIL_VALIDATION_SUBJECT,
            text: "Pour activer votre compte Shops veuillez cliquer sur le lien suivant : " +
            Constants.MAIL_VALIDATION_LINK  + "/" + this.userHash,
            html: pug.renderFile(__dirname + "/../../templates/welcome-mail.pug", {
                env: Constants.NODE_ENV,
                username: this.username,
                email: this.email,
                activationUrl: Constants.MAIL_VALIDATION_LINK  + "/" + this.userHash,
            }),
        };

        const info =  transporter.sendMail(mailOptions);
        return (info);
    }

    public sendForgotMail() {
        const transporter = nodemailer.createTransport({
            host: Constants.MAIL_HOST,
            port: Constants.MAIL_PORT,
            auth: {
                user: Constants.MAIL_USER,
                pass: Constants.MAIL_PASS,
            },
            tls: {rejectUnauthorized: false},
        });

        const mailOptions = {
            from: Constants.MAIL_USER,
            to: this.email,
            subject: Constants.MAIL_PASSWORD_SUBJECT,
            text: "Pour changer le mot de passe de votre compte Shops veuillez cliquer sur le lien suivant : " +
            Constants.MAIL_PASSWORD_LINK  + "/" + this.userHash,
            html: pug.renderFile(__dirname + "/../../templates/reset-mail.pug", {
                env: Constants.NODE_ENV,
                username: this.username,
                resetUrl: Constants.MAIL_PASSWORD_LINK  + "/" + this.userHash,
            }),
        };

        const info = transporter.sendMail(mailOptions);
        return(info);
    }
}
