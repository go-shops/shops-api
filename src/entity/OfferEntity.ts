import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import logger from "../utils/Logger";
import {ProductEntity} from "./ProductEntity";
import {StoreEntity} from "./StoreEntity";

@Entity("offers")
export class OfferEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public description: string;

    @Column()
    public discount: number;

    @Column("time")
    public createTime: number;

    @Column("time")
    public updateTime: number;

    @Column("datetime")
    public startTime: number;

    @Column("datetime")
    public expiryTime: number;

    @ManyToOne(() => StoreEntity, (store) => store.offers)
    @JoinColumn({name: "storeId"})
    public store: StoreEntity;

    @OneToOne(() => ProductEntity, {eager: true})
    @JoinColumn({name: "productId"})
    public product: ProductEntity;

    public toJSON(): object {
        logger.debug("[entity/OfferEntity.ts - OfferEntity - toJSON]: Transform offer entity into JSON object");
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            discount: this.discount,
            product: this.product,
            startTime: this.startTime || null,
            store: this.store,
            expiryTime: this.expiryTime || null,
        };
    }

    public applyJSON(json): OfferEntity {
        logger.debug("[entity/OfferEntity.ts - OfferEntity - applyJSON]: Edit offer entity from JSON object");
        this.name = json.name;
        this.description = json.description;
        this.discount = json.discount;
        this.product = json.product;
        this.startTime = json.startTime;
        this.store = json.store;
        this.expiryTime = json.expiryTime;
        return this;
    }

    public static fromJSON(json): OfferEntity {
        logger.debug("[entity/OfferEntity.ts - OfferEntity - fromJSON]: Create new offer entity from JSON object");
        const offer = new OfferEntity();
        offer.name = json.name;
        offer.description = json.description;
        offer.discount = json.discount;
        offer.product = json.product;
        offer.startTime = json.startTime;
        offer.store = json.store;
        offer.expiryTime = json.expiryTime;
        return offer;
    }

    public static async saveInDb(offer: JSON, product: ProductEntity, store: StoreEntity): Promise<OfferEntity> {
        return await this.save(OfferEntity.fromJSON({...offer, product, store}));
    }

    public static async updateInDb(offer: JSON, obj: OfferEntity): Promise<OfferEntity> {
        return await this.save(obj.applyJSON(offer));
    }
}
