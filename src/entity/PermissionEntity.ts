import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

@Entity("permissions")
export class PermissionEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column("enum")
    public permission: string;

    public toJSON(): object {
        logger.debug("[entity/PermissionEntity.ts - PermissionEntity - toJSON]:" +
            "Transform permission entity into JSON object");
        return {
            id: this.id,
            permission: this.permission,
        };
    }

    public applyJSON(json): PermissionEntity {
        logger.debug("[entity/PermissionEntity.ts - PermissionEntity - applyJSON]: " +
            "Edit permission entity from JSON object");
        this.permission = json.permission;
        return this;
    }

    public static fromJSON(json): PermissionEntity {
        logger.debug("[entity/PermissionEntity.ts - PermissionEntity - fromJSON]:" +
            "Create new permission entity from JSON object");
        const permission = new PermissionEntity();
        permission.permission = json.permission;
        return permission;
    }

    public static async getPermissionsFromArray(arr: string[]): Promise<PermissionEntity[]> {
        const res: PermissionEntity[] = [];
        for (const perm of arr) {
            const existing: PermissionEntity = await PermissionEntity.findOne({where: {permission: perm}});
            if (existing) {
                res.push(existing);
            }
        }
        return res;
    }

    public static async saveInDb(permission): Promise<PermissionEntity> {
        const tmp: PermissionEntity = PermissionEntity.fromJSON(permission);
        await tmp.save();
        await Elasticsearch.savePermission(tmp);
        return tmp;
    }
}
