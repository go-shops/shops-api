import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {StoreEntity, UserEntity} from "../entity";
import logger from "../utils/Logger";

@Entity("storesComments")
export class StoreCommentEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public comment: string;

    @Column()
    public mark: number;

    @Column()
    public createTime: string;

    @Column()
    public updateTime: string;

    @ManyToOne(() => StoreEntity, (store) => store.comments)
    @JoinColumn({name: "storeId"})
    public store: StoreEntity;

    @ManyToOne(() => UserEntity, (user) => user.comments)
    @JoinColumn({name: "authorId"})
    public owner: UserEntity;

    @Column()
    public authorId: number;

    @Column()
    public storeId: number;

    public toJSON(): object {
        logger.debug("[entity/StoreCommentEntity.ts - StoreCommentEntity - toJSON]: " +
            "Transform comment entity into JSON object");
        if (this.owner === undefined || this.owner === null) {
            return {
                id: this.id,
                comment: this.comment,
                mark: this.mark,
                store: this.store,
                user: this.owner,
            };
        }
        return {
            id: this.id,
            comment: this.comment,
            mark: this.mark,
            store: this.store,
            owner: this.owner.toJSONMin(),
        };
    }

    public applyJSON(json): StoreCommentEntity {
        logger.debug("[entity/StoreCommentEntity.ts - StoreCommentEntity - applyJSON]: " +
            "Edit user entity from JSON object");
        this.comment = json.comment;
        this.mark = json.mark;
        this.store = json.store;
        this.owner = json.owner;
        return this;
    }

    public static fromJSON(json): StoreCommentEntity {
        logger.debug("[entity/StoreCommentEntity.ts - StoreCommentEntity - fromJSON]: " +
            "Create new user entity from JSON object");
        const comment = new StoreCommentEntity();
        comment.comment = json.comment;
        comment.mark = json.mark;
        comment.store = json.store;
        comment.owner = json.owner;
        return comment;
    }

    public static async saveInDb(comment: object): Promise<StoreCommentEntity> {
        return await this.save(StoreCommentEntity.fromJSON(comment));
    }

    public static async updateInDb(comment: JSON, obj: StoreCommentEntity): Promise<StoreCommentEntity> {
        return await this.save(obj.applyJSON(comment));
    }
}
