import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

@Entity("brands")
export class BrandEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    public toJSON(): object {
        logger.debug("[entity/BrandEntity.ts - BrandEntity - toJSON]: Transform brand entity into JSON object");
        return {
            id: this.id,
            name: this.name,
        };
    }

    public applyJSON(json): BrandEntity {
        logger.debug("[entity/BrandEntity.ts - BrandEntity - applyJSON]: Edit brand entity from JSON object");
        this.name = json.name;
        return this;
    }

    public static fromJSON(json): BrandEntity {
        logger.debug("[entity/BrandEntity.ts - BrandEntity - fromJSON]: Create new brand entity from JSON object");
        const brand = new BrandEntity();
        brand.name = json.name;
        return brand;
    }

    public static async saveInDb(brand): Promise<BrandEntity> {
        const existing: BrandEntity = await this.findOne({where: {name: brand.name}});
        if (existing) {
            return existing;
        }
        const tmp: BrandEntity = this.fromJSON(brand);
        await tmp.save();
        await Elasticsearch.saveBrand(tmp);
        return tmp;
    }
}
