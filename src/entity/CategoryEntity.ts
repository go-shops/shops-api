import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

@Entity("categories")
export class CategoryEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    public toJSON(): object {
        logger.debug("[entity/CategoryEntity.ts - CategoryEntity - toJSON]: " +
            "Transform category entity into JSON object");
        return {
            id: this.id,
            name: this.name,
        };
    }

    public applyJSON(json): CategoryEntity {
        logger.debug("[entity/CategoryEntity.ts - CategoryEntity - applyJSON]: Edit category entity from JSON object");
        this.name = json.name;
        return this;
    }

    public static fromJSON(json): CategoryEntity {
        logger.debug("[entity/CategoryEntity.ts - CategoryEntity - fromJSON]: " +
            "Create new category entity from JSON object");
        const category = new CategoryEntity();
        category.name = json.name;
        return category;
    }

    public static async saveInDb(category): Promise<CategoryEntity> {
        const existing: CategoryEntity = await this.findOne({where: {name: category.name}});
        if (existing) {
            return existing;
        }
        const tmp: CategoryEntity = this.fromJSON(category);
        await tmp.save();
        await Elasticsearch.saveCategory(tmp);
        return tmp;
    }

    public static async saveAllInDb(arr): Promise<CategoryEntity[]> {
        const categories: CategoryEntity[] = [];
        if (arr === undefined) {
            return categories;
        }
        for (let i = 0; i < arr.length; i++) {
            categories[i] = await this.saveInDb(arr[i]);
        }
        return categories;
    }
}
