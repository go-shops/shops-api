import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import logger from "../utils/Logger";

@Entity("pictures")
export class PictureEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public description: string;

    @Column()
    public url: string;

    public toJSON(): object {
        logger.debug("[entity/PictureEntity.ts - PictureEntity - toJSON]: Transform picture entity into JSON object");
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            url: this.url,
        };
    }

    public applyJSON(json): PictureEntity {
        logger.debug("[entity/PictureEntity.ts - PictureEntity - applyJSON]: Edit picture entity from JSON object");
        this.name = json.name;
        this.description = json.description;
        this.url = json.url;
        return this;
    }

    public static fromJSON(json): PictureEntity {
        logger.debug("[entity/PictureEntity.ts - PictureEntity - fromJSON]: " +
            "Create new picture entity from JSON object");
        const picture = new PictureEntity();
        picture.name = json.name;
        picture.description = json.description;
        picture.url = json.url;
        return picture;
    }

    public static async saveInDb(picture): Promise<PictureEntity> {
        const existing: PictureEntity = await this.findOne(
            {where: {name: picture.name, description: picture.description, url: picture.url}});
        if (existing) {
            return existing;
        }
        return await this.save(PictureEntity.fromJSON(picture));
    }

    public static async saveAllInDb(arr): Promise<PictureEntity[]> {
        const pictures: PictureEntity[] = [];
        if (arr === undefined) {
            return pictures;
        }
        for (let i = 0; i < arr.length; i++) {
            pictures[i] = await this.saveInDb(arr[i]);
        }
        return pictures;
    }
}
