import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Elasticsearch} from "../utils";
import logger from "../utils/Logger";

@Entity("tags")
export class TagEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    public toJSON(): object {
        logger.debug("[entity/TagEntity.ts - TagEntity - toJSON]: Transform tag entity into JSON object");
        return {
            id: this.id,
            name: this.name,
        };
    }

    public applyJSON(json): TagEntity {
        logger.debug("[entity/TagEntity.ts - TagEntity - applyJSON]: Edit tag entity from JSON object");
        this.name = json.name;
        return this;
    }

    public static fromJSON(json): TagEntity {
        logger.debug("[entity/BrandEntity.ts - BrandEntity - fromJSON]: Create new tag entity from JSON object");
        const tag = new TagEntity();
        tag.name = json.name;
        return tag;
    }

    public static async saveInDb(tag): Promise<TagEntity> {
        const existing: TagEntity = await TagEntity.findOne({where: {name: tag.name}});
        if (existing) {
            return existing;
        }
        const tmp: TagEntity = this.fromJSON(tag);
        await tmp.save();
        await Elasticsearch.saveTag(tmp);
        return tmp;
    }

    public static async saveAllInDb(arr): Promise<TagEntity[]> {
        const tags: TagEntity[] = [];
        if (arr === undefined) {
            return tags;
        }
        for (let i = 0; i < arr.length; i++) {
            tags[i] = await TagEntity.saveInDb(arr[i]);
        }
        return tags;
    }
}
