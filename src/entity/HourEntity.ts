import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import logger from "../utils/Logger";

@Entity("hours")
export class HourEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column("enum")
    public day: string;

    @Column("time")
    public timeOpen: number;

    @Column("time")
    public timeClosed: number;

    public toJSON(): object {
        logger.debug("[entity/HourEntity.ts - HourEntity - toJSON]:" +
            "Transform hour entity into JSON object");
        return {
            id: this.id,
            day: this.day,
            timeOpen: this.timeOpen,
            timeClosed: this.timeClosed,
        };
    }

    public applyJSON(json): HourEntity {
        logger.debug("[entity/HourEntity.ts - HourEntity - applyJSON]: Edit hour entity from JSON object");
        this.day = json.day;
        this.timeOpen = json.timeOpen;
        this.timeClosed = json.timeClosed;
        return this;
    }

    public static fromJSON(json): HourEntity {
        logger.debug("[entity/HourEntity.ts - HourEntity - fromJSON]:" +
            "Create new hour entity from JSON object");
        const hour = new HourEntity();
        hour.day = json.day;
        hour.timeOpen = json.timeOpen;
        hour.timeClosed = json.timeClosed;
        return hour;
    }

    public static async saveInDb(hour: JSON): Promise<HourEntity> {
        return await this.save(HourEntity.fromJSON(hour));
    }

    public static async saveAllInDb(arr): Promise<HourEntity[]> {
        const hours: HourEntity[] = [];
        if (arr === undefined) {
            return hours;
        }
        for (let i = 0; i < arr.length; i++) {
            const existing: HourEntity = await this.findOne(
                {where: {day: arr[i].day, timeOpen: arr[i].timeOpen, timeClosed: arr[i].timeClosed}});
            if (existing) {
                hours[i] = existing;
            } else {
                hours[i] = await this.saveInDb(arr[i]);
            }
        }
        return hours;
    }
}
