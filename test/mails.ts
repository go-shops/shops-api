import chai from "chai";
import {expect} from "chai";
import chaiHttp from "chai-http";
import nodemailerMock from "nodemailer-mock";
import randomstring from "randomstring";
import app from "../src/app";

chai.should();
chai.use(chaiHttp);

/**
 * Test mails routes
 */
describe("Mails", () => {
    /**
     * Initial setup
     */
    before((done) => {
        done();
    });

    /**
     * Test POST /mails
     */
    describe("POST /mails", () => {
        const fields1 = {email: "dummy@dummy.com", subject: "TestEmail", content: "This is email body"};
        it("it should return 200 with email sent", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(1);
                    sentMail[0].should.have.property("from", fields1.email);
                    sentMail[0].should.have.property("to");
                    sentMail[0].should.have.property("subject", fields1.subject);
                    sentMail[0].should.have.property("text", fields1.content);
                    err ? done(err) : done();
                });
        });

        const fields2 = {email: "dummydummy.com", subject: "TestEmail", content: "This is email body"};
        it("it should return 400 (wrong email)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields3 = {email: "", subject: "TestEmail", content: "This is email body"};
        it("it should return 400 (empty email)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields4 = {subject: "TestEmail", content: "This is email body"};
        it("it should return 400 (no email)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields5 = {email: null, subject: "TestEmail", content: "This is email body"};
        it("it should return 400 (null email)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields6 = {email: "dummy@dummy.com", subject: randomstring.generate(300), content: "This is email body"};
        it("it should return 400 (wrong subject)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields7 = {email: "dummy@dummy.com", subject: "", content: "This is email body"};
        it("it should return 400 (empty subject)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields8 = {email: "dummy@dummy.com", content: "This is email body"};
        it("it should return 400 (no subject)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields9 = {email: "dummy@dummy.com", subject: "TestEmail", content: ""};
        it("it should return 400 (empty content)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields10 = {email: "dummy@dummy.com", subject: "TestEmail"};
        it("it should return 400 (no content)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields10)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields11 = {email: "dummy@dummy.com", subject: "TestEmail", content: null};
        it("it should return 400 (null content)", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields11)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });

        const fields12 = {email: "dummy@dummy.com", subject: "TestEmail", content: randomstring.generate(2000)};
        it("it should return 200 (big content (<2048))", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields12)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(1);
                    sentMail[0].should.have.property("from", fields12.email);
                    sentMail[0].should.have.property("to");
                    sentMail[0].should.have.property("subject", fields12.subject);
                    sentMail[0].should.have.property("text", fields12.content);
                    err ? done(err) : done();
                });
        });

        const fields13 = {email: "dummy@dummy.com", subject: "TestEmail", content: randomstring.generate(3000)};
        it("it should return 400 (big content (>2048))", (done) => {
            chai.request(app)
                .post("/mails")
                .send(fields13)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    const sentMail = nodemailerMock.mock.sentMail();
                    sentMail.length.should.be.equal(0);
                    err ? done(err) : done();
                });
        });
    });

    afterEach((done) => {
        // Reset the mock back to the defaults after each test
        nodemailerMock.mock.reset();
        done();
    });
});
