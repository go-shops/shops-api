import bcrypt from "bcrypt";
import chai from "chai";
import {expect} from "chai";
import chaiHttp from "chai-http";
import randomstring from "randomstring";
import {getRepository} from "typeorm";
import app from "../src/app";
import Constants from "../src/Constants";
import {ProductEntity, StoreEntity, UserEntity} from "../src/entity";
import {Elasticsearch, role, Utils} from "../src/utils";
import logger from "../src/utils/Logger";

chai.should();
chai.use(chaiHttp);

const agent1 = chai.request.agent(app); // Owner - User
const agent2 = chai.request.agent(app); // User

let user1: UserEntity = null;
let user2: UserEntity = null;
let store: StoreEntity = null;
let products: ProductEntity[] = null;

// Todo: Test bind product
// Todo: Add tests for get comments

/**
 * Test Stores route
 */
describe("Stores", () => {
    /**
     * Initial setup
     */
    before(async () => {
        await Utils.sleep(2000);
        const password = await bcrypt.hash("dummyA1*", 10);
        const fields1 = {email: "dummy@dummy.com", password, username: "username", active: true};
        user1 = await UserEntity.saveInDb(fields1, role.User);
        if (!user1) {
            throw new Error("Setup: Can't create new user");
        }
        agent1
            .post("/auth/login")
            .send({email: fields1.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields2 = {email: "dummy2@dummy.com", password, username: "username", active: true};
        user2 = await UserEntity.saveInDb(fields2, role.User);
        if (!user2) {
            throw new Error("Setup: Can't create new user");
        }
        agent2
            .post("/auth/login")
            .send({email: fields2.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields3 = [
            {
                name: "Fleur",
                description: "zzaezaze",
                brand: {name: "Marque"},
                tags: [{name: "Fleurs"}],
                pictures: [
                    {
                        name: "Fleur",
                        description: "Belle fleur",
                        url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                    }],
            },
            {
                name: "Bouquet",
                description: "Bouquet",
                brand: {name: "Marque"},
                pictures: [
                    {
                        name: "Bouquet",
                        description: "Beau bouquet",
                        url: "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg",
                    }],
            }];
        products = await ProductEntity.saveAllInDb(fields3);
        if (products.length !== 2) {
            throw new Error("Setup: Can't create new products");
        }
        await Utils.sleep(1000);
    });

    /**
     * Test POST /stores
     */
    describe("POST /stores", () => {
        const fields1 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 201 created with store info (minimalist store)", (done) => {
            agent1
                .post("/stores")
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(201);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.should.have.property("name", fields1.name);
                        res.body.should.have.property("description", fields1.description);
                        res.body.should.have.property("siret", fields1.siret);
                        res.body.should.have.property("address");
                        res.body.should.have.property("hours");
                        res.body.should.have.property("categories");
                        res.body.should.have.property("products");
                        res.body.should.have.property("pictures");

                        // Test DB record
                        store = await getRepository(StoreEntity).findOne(res.body.id,
                            {relations: ["owner", "comments", "comments.owner", "products", "offers"]});
                        if (!store) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (store.name !== fields1.name) {
                            throw new Error("DB: Expected store name to be " + fields1.name);
                        }
                        if (store.description !== fields1.description) {
                            throw new Error("DB: Expected store description to be " + fields1.description);
                        }
                        if (store.siret !== fields1.siret) {
                            throw new Error("DB: Expected store siret to be " + fields1.siret);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /stores] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .post("/stores")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .post("/stores")
                .set("Cookie", Constants.SESSION_NAME + "=WrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 400 (store with same siret already exist)", (done) => {
            agent1
                .post("/stores")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields2 = {
            name: randomstring.generate(300),
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong name)", (done) => {
            agent1
                .post("/stores")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields3 = {
            name: "",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty name)", (done) => {
            agent1
                .post("/stores")
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields4 = {
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (no name)", (done) => {
            agent1
                .post("/stores")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields5 = {
            name: null,
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (null name)", (done) => {
            agent1
                .post("/stores")
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields6 = {
            name: "Magasin de folie",
            description: randomstring.generate(3000),
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong description)", (done) => {
            agent1
                .post("/stores")
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields7 = {
            name: "Magasin de folie",
            description: "",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty description)", (done) => {
            agent1
                .post("/stores")
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields8 = {
            name: "Magasin de folie",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (no description)", (done) => {
            agent1
                .post("/stores")
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields9 = {
            name: "Magasin de folie",
            description: null,
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (null description)", (done) => {
            agent1
                .post("/stores")
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields10 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "2956789876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong siret)", (done) => {
            agent1
                .post("/stores")
                .send(fields10)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields11 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty siret)", (done) => {
            agent1
                .post("/stores")
                .send(fields11)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields12 = {
            name: "Magasin sans siret",
            description: "Magnifique magasin",
            address: {
                street: "rue de la guerre",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 201 (created but no siret)", (done) => {
            agent1
                .post("/stores")
                .send(fields12)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(201);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("name");
                    res.body.should.have.property("description");
                    res.body.should.have.property("siret");
                    res.body.should.have.property("address");
                    res.body.should.have.property("hours");
                    res.body.should.have.property("categories");
                    res.body.should.have.property("products");
                    res.body.should.have.property("pictures");
                    res.body.should.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields13 = {
            name: "Magasin sombre",
            description: "Magnifique magasin",
            siret: null,
            address: {
                street: "rue de la vie",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 201 (created with null siret)", (done) => {
            agent1
                .post("/stores")
                .send(fields13)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(201);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("name");
                    res.body.should.have.property("description");
                    res.body.should.have.property("siret");
                    res.body.should.have.property("address");
                    res.body.should.have.property("hours");
                    res.body.should.have.property("categories");
                    res.body.should.have.property("products");
                    res.body.should.have.property("pictures");
                    res.body.should.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields14 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong address - no country)", (done) => {
            agent1
                .post("/stores")
                .send(fields14)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields15 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {},
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty address)", (done) => {
            agent1
                .post("/stores")
                .send(fields15)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields16 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (no address)", (done) => {
            agent1
                .post("/stores")
                .send(fields16)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields17 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: null,
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (null address)", (done) => {
            agent1
                .post("/stores")
                .send(fields17)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields18 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: randomstring.generate(300)}],
        };
        it("it should return 400 (wrong categories)", (done) => {
            agent1
                .post("/stores")
                .send(fields18)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields19 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [],
        };
        it("it should return 400 (empty categories)", (done) => {
            agent1
                .post("/stores")
                .send(fields19)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields20 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
        };
        it("it should return 400 (no categories)", (done) => {
            agent1
                .post("/stores")
                .send(fields20)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields21 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: null,
        };
        it("it should return 400 (null categories)", (done) => {
            agent1
                .post("/stores")
                .send(fields21)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields22 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
            wrongField: "Test",
        };
        it("it should return 400 (invalid parameter)", (done) => {
            agent1
                .post("/stores")
                .send(fields22)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test PUT /stores
     */
    describe("PUT /stores/:id", () => {
        const fields1 = {
            name: "Magasin de la folie",
            description: "Magnifique magasin",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 200 updateInDb with new store info (Minimalist store)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        expect(res).to.have.status(200);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.id.should.be.equal(store.id);
                        res.body.should.have.property("name", fields1.name);
                        res.body.should.have.property("description", fields1.description);
                        res.body.should.have.property("siret", fields1.siret);
                        res.body.should.have.property("address");
                        res.body.should.have.property("hours");
                        res.body.should.have.property("categories");
                        res.body.should.have.property("products");
                        res.body.should.have.property("pictures");
                        res.body.should.have.property("owner");
                        res.body.owner.should.have.property("id", user1.id);

                        // Test DB record
                        store = await getRepository(StoreEntity).findOne(res.body.id,
                            {relations: ["owner", "comments", "comments.owner", "products", "offers"]});
                        if (!store) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (store.name !== fields1.name) {
                            throw new Error("DB: Expected store name to be " + fields1.name);
                        }
                        if (store.description !== fields1.description) {
                            throw new Error("DB: Expected store description to be " + fields1.description);
                        }
                        if (store.siret !== fields1.siret) {
                            throw new Error("DB: Expected store siret to be " + fields1.siret);
                        }

                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][PUT /stores] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .put("/stores/" + store.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .put("/stores/" + store.id)
                .set("Cookie", Constants.SESSION_NAME + "=WrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid store id)", (done) => {
            agent1
                .put("/stores/404")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 403 (wrong user)", (done) => {
            agent2
                .put("/stores/" + store.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields2 = {
            name: randomstring.generate(300),
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong name)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields3 = {
            name: "",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty name)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields4 = {
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (no name)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields5 = {
            name: null,
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (null name)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields6 = {
            name: "Magasin de folie",
            description: randomstring.generate(3000),
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong description)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields7 = {
            name: "Magasin de folie",
            description: "",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty description)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields8 = {
            name: "Magasin de folie",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (no description)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields9 = {
            name: "Magasin de folie",
            description: null,
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (null description)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields10 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "2956789876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong siret)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields10)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields11 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty siret)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields11)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields14 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (wrong address - no country)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields14)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields15 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {},
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (empty address)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields15)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields16 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (no address)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields16)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields17 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: null,
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 400 (null address)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields17)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields18 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: randomstring.generate(300)}],
        };
        it("it should return 400 (wrong categories)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields18)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields19 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [],
        };
        it("it should return 400 (empty categories)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields19)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields20 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
        };
        it("it should return 400 (no categories)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields20)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields21 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: null,
        };
        it("it should return 400 (null categories)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields21)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields22 = {
            name: "Magasin de folie",
            description: "Magnifique magasin",
            siret: "29876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
            wrongField: "Test",
        };
        it("it should return 400 (invalid parameter)", (done) => {
            agent1
                .put("/stores/" + store.id)
                .send(fields22)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /stores
     */
    describe("GET /stores", () => {
        it("it should return 200", (done) => {
            chai.request(app)
                .get("/stores")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /stores/:id
     */
    describe("GET /stores/:id", () => {
        const fields5 = {
            name: "Magasin de la folie",
            description: "Magnifique magasin",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
        };
        it("it should return 200", (done) => {
            chai.request(app)
                .get("/stores/" + store.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.id.should.be.equal(store.id);
                    res.body.should.have.property("name", fields5.name);
                    res.body.should.have.property("description", fields5.description);
                    res.body.should.have.property("siret", fields5.siret);
                    res.body.should.have.property("address");
                    res.body.should.have.property("hours");
                    res.body.should.have.property("categories");
                    res.body.should.have.property("products");
                    res.body.should.have.property("pictures");
                    res.body.should.have.property("owner");
                    res.body.owner.should.have.property("id", user1.id);
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid store id)", (done) => {
            chai.request(app)
                .get("/stores/404")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 400 (invalid store id - string)", (done) => {
            chai.request(app)
                .get("/stores/test")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test POST /stores/:id/products
     */
    describe("POST /stores/:id/products", () => {
        const fields1 = {products: [1, 2]};
        it("it should return 200", (done) => {
            fields1.products = [products[0].id, products[1].id];
            agent1
               .post("/stores/" + store.id + "/products")
               .send(fields1)
               .end((err, res) => {
                   // Test Api response
                   expect(res).to.have.status(200);
                   expect(res).to.be.json;
                   res.body.should.have.property("id");
                   res.body.should.have.property("name");
                   res.body.should.have.property("description");
                   res.body.should.have.property("siret");
                   res.body.should.have.property("address");
                   res.body.should.have.property("hours");
                   res.body.should.have.property("categories");
                   res.body.should.have.property("products");
                   res.body.products.should.have.length(2);
                   res.body.should.have.property("pictures");
                   err ? done(err) : done();
               });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .post("/stores/" + store.id + "/products")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .post("/stores/" + store.id + "/products")
                .set("Cookie", Constants.SESSION_NAME + "=WrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 403 (Invalid user)", (done) => {
            agent2
                .post("/stores/" + store.id + "/products")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (missing store)", (done) => {
            agent1
                .post("/stores/404/products")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        const fields2 = {products: [404]};
        it("it should return 404 (missing product)", (done) => {
            agent1
                .post("/stores/" + store.id + "/products")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /stores/:id/products
     */
    describe("GET /products", () => {
        it("it should return 200 with products info in array", (done) => {
            chai.request(app)
                .get("/stores/" + store.id + "/products")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    expect(res.body.length).to.eql(2);
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (Invalid store ID)", (done) => {
            chai.request(app)
                .get("/stores/404/products")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });
        // TODO: Improve tests
    });

    /**
     * Test GET /stores/:storeId/offers
     */
    describe("GET /stores/:storeId/offers", () => {
        it("it should return 200", (done) => {
            chai.request(app)
                .get("/stores/" + store.id + "/offers")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid store id)", (done) => {
            chai.request(app)
                .get("/stores/404/offers")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 400 (invalid store id - string)", (done) => {
            chai.request(app)
                .get("/stores/test/offers")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

    });

    /**
     * Test DELETE /stores/:id
     */
    describe("DELETE /stores/:id", () => {
        it("it should return 403 (wrong user)", (done) => {
            agent2
                .delete("/stores/" + store.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .delete("/stores/" + store.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .delete("/stores/" + store.id)
                .set("Cookie", Constants.SESSION_NAME + "=WrongCookie;")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (store not found)", (done) => {
            agent1
                .delete("/stores/404")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    // noinspection BadExpressionStatementJS
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("siret");
                    res.body.should.not.have.property("address");
                    res.body.should.not.have.property("hours");
                    res.body.should.not.have.property("categories");
                    res.body.should.not.have.property("products");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });

        it("it should return 204 with nothing in body", (done) => {
            agent1
                .delete("/stores/" + store.id)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const storeTmp: StoreEntity = await getRepository(StoreEntity).findOne(store.id);
                        if (storeTmp) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][DELETE /Store/:id] %s", e.message);
                        done(e);
                    }
                });
        });
    });

    /**
     * Delete created records
     */
    after(async () => {
        for (const product of products) {
            for (const picture of product.pictures) {
                await picture.remove();
            }
            if (product.tags) {
                for (const tag of product.tags) {
                    await Elasticsearch.delete(Constants.ELASTIC_TAGS_INDEX, Constants.ELASTIC_TAGS_TYPE, tag.id);
                    await tag.remove();
                }
            }
            if (product.brand) {
                await Elasticsearch.delete(Constants.ELASTIC_BRANDS_INDEX,
                    Constants.ELASTIC_BRANDS_TYPE, product.brand.id);
                await product.brand.remove();
            }
            await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX, Constants.ELASTIC_PRODUCTS_TYPE, product.id);
            await product.remove();
        }
        await store.address.remove();
        for (const category of store.categories) {
            await Elasticsearch.delete(Constants.ELASTIC_CATEGORIES_INDEX,
                Constants.ELASTIC_CATEGORIES_TYPE, category.id);
            await category.remove();
        }
        for (const hour of store.hours) {
            await hour.remove();
        }
        for (const picture of store.pictures) {
            await picture.remove();
        }
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user1.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user2.id);
        await Elasticsearch.delete(Constants.ELASTIC_STORES_INDEX, Constants.ELASTIC_STORES_TYPE, store.id);
        await user1.remove();
        await user2.remove();
        await store.remove();
        agent1.close();
        agent2.close();
    });
});
