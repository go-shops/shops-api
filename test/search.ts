import bcrypt from "bcrypt";
import chai from "chai";
import {expect} from "chai";
import chaiHttp from "chai-http";
import app from "../src/app";
import Constants from "../src/Constants";
import {StoreEntity, UserEntity} from "../src/entity";
import {Elasticsearch, Utils} from "../src/utils";

chai.should();
chai.use(chaiHttp);

let user1: UserEntity = null;
const stores: StoreEntity[] = [];

/**
 * Test Search route
 *
 */
describe("Search", () => {
    /**
     * Initial setup
     */
    before(async () => {
        await Utils.sleep(100);
        const password = await bcrypt.hash("dummyA1*", 10);
        const fields1 = {email: "dummy@dummy.com", password, username: "username", active: true};
        user1 = await UserEntity.saveInDb(fields1);
        if (!user1) {
            throw new Error("Setup: Can't create new user");
        }

        const fields2 = {
            name: "Magasin 1 ",
            description: "Magnifique magasin 1",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            hours: [
                {day: "monday", timeOpen: "08:00:00", timeClosed: "12:00:00"},
                {day: "monday", timeOpen: "13:00:00", timeClosed: "15:00:00"}],
            categories: [
                {name: "Boutique"},
                {name: "Fleuriste"}],
            products: [
                {
                    name: "Fleur",
                    description: "zzaezaze",
                    brand: {name: "Marque"},
                    tags: [{name: "Fleurs"}],
                    pictures: [
                        {
                            name: "Fleur",
                            description: "Belle fleur",
                            url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                        }],
                },
                {
                    name: "Bouquet",
                    description: "Bouquet",
                    brand: {name: "Marque"},
                    pictures: [
                        {
                            name: "Bouquet",
                            description: "Beau bouquet",
                            url: "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg",
                        }],
                }],
            pictures: [
                {
                    name: "Facade",
                    description: "Test",
                    url: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/TheSourceHillcrest_Mall.jpg/" +
                        "220px-TheSourceHillcrest_Mall.jpg",
                },
                {
                    name: "Interieur",
                    description: "coucou",
                    url: "https://www.numerama.com/content/uploads/2017/06/epicerie-magasin-fruit-legume-bio.jpg",
                }],
        };
        const store1: StoreEntity = await StoreEntity.saveInDb(user1, fields2);
        if (!store1) {
            throw new Error("Setup: Can't create new store");
        }
        stores.push(store1);

        const fields3 = {
            name: "Magasin 2",
            description: "Magnifique magasin 2",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            hours: [
                {day: "monday", timeOpen: "08:00:00", timeClosed: "12:00:00"},
                {day: "monday", timeOpen: "13:00:00", timeClosed: "15:00:00"}],
            categories: [
                {name: "Fleurs"},
                {name: "Fleuriste"}],
            products: [
                {
                    name: "Fleur de lilas",
                    description: "Fleurs de lila",
                    brand: {name: "Marque"},
                    tags: [{name: "Fleurs"}],
                    pictures: [
                        {
                            name: "Fleur",
                            description: "Belle fleur",
                            url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                        }],
                },
                {
                    name: "Bouquet de fleurs",
                    description: "Bouquet",
                    brand: {name: "Marque"},
                    pictures: [
                        {
                            name: "Bouquet",
                            description: "Beau bouquet",
                            url: "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg",
                        }],
                }],
            pictures: [
                {
                    name: "Facade",
                    description: "Test",
                    url: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/TheSourceHillcrest_Mall.jpg/" +
                        "220px-TheSourceHillcrest_Mall.jpg",
                },
                {
                    name: "Interieur",
                    description: "coucou",
                    url: "https://www.numerama.com/content/uploads/2017/06/epicerie-magasin-fruit-legume-bio.jpg",
                }],
        };
        const store2: StoreEntity = await StoreEntity.saveInDb(user1, fields3);
        if (!store2) {
            throw new Error("Setup: Can't create new store");
        }
        stores.push(store2);

        const fields4 = {
            name: "Magasin 3",
            description: "Magnifique magasin 3",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: 93000,
                country: "Afrique du Sud",
            },
            hours: [
                {day: "monday", timeOpen: "08:00:00", timeClosed: "12:00:00"},
                {day: "monday", timeOpen: "13:00:00", timeClosed: "15:00:00"}],
            categories: [
                {name: "Boutique"},
                {name: "Fleuriste"}],
            products: [
                {
                    name: "Compositions de Fleurs",
                    description: "zzaezaze",
                    brand: {name: "Marque"},
                    tags: [{name: "Fleurs"}],
                    pictures: [
                        {
                            name: "Fleur",
                            description: "Belle fleur",
                            url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                        }],
                },
                {
                    name: "Bouquet",
                    description: "Bouquet",
                    brand: {name: "Marque"},
                    pictures: [
                        {
                            name: "Bouquet",
                            description: "Beau bouquet",
                            url: "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg",
                        }],
                }],
            pictures: [
                {
                    name: "Facade",
                    description: "Test",
                    url: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/TheSourceHillcrest_Mall.jpg/" +
                        "220px-TheSourceHillcrest_Mall.jpg",
                },
                {
                    name: "Interieur",
                    description: "coucou",
                    url: "https://www.numerama.com/content/uploads/2017/06/epicerie-magasin-fruit-legume-bio.jpg",
                }],
        };
        const store3: StoreEntity = await StoreEntity.saveInDb(user1, fields4);
        if (!store3) {
            throw new Error("Setup: Can't create new store");
        }
        stores.push(store3);
        await Utils.sleep(1000);
    });

    /**
     * Test GET /search/stores
     */
    describe("GET /search/stores", () => {
        const fields4 = {
            query: "Magasin",
            page: 1,
            size: 50,
        };
        it("it should return 200 (search on stores)", (done) => {
            chai.request(app)
                .post("/search/stores")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    res.body.should.have.length(3);

                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /search/products
     */
    describe("GET /search/products", () => {
        const fields5 = {
            query: "fleur",
            page: 1,
            size: 50,
        };
        it("it should return 200 (search on product)", (done) => {
            chai.request(app)
                .post("/search/products")
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    res.body.should.have.length(4);

                    err ? done(err) : done();
                });
        });

    });

    /**
     * Test GET /search/users
     */
    describe("GET /search/users", () => {
        const fields6 = {
            query: "username",
            page: 1,
            size: 50,
        };
        it("it should return 200 (search on stores)", (done) => {
            chai.request(app)
                .post("/search/users")
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    res.body.should.have.length(1);

                    err ? done(err) : done();
                });
        });
    });

    // TODO: Improve tests

    /**
     * Delete created records
     */
    after(async () => {
        for (const store of stores) {
            await store.address.remove();
            for (const product of store.products) {
                for (const picture of product.pictures) {
                    await picture.remove();
                }
                if (product.tags) {
                    for (const tag of product.tags) {
                        await Elasticsearch.delete(Constants.ELASTIC_TAGS_INDEX, Constants.ELASTIC_TAGS_TYPE, tag.id);
                        await tag.remove();
                    }
                }
                if (product.brand) {
                    await Elasticsearch.delete(Constants.ELASTIC_BRANDS_INDEX,
                        Constants.ELASTIC_BRANDS_TYPE, product.brand.id);
                    await product.brand.remove();
                }
                await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX,
                    Constants.ELASTIC_PRODUCTS_TYPE, product.id);
                await product.remove();
            }
            for (const category of store.categories) {
                await Elasticsearch.delete(Constants.ELASTIC_CATEGORIES_INDEX,
                    Constants.ELASTIC_CATEGORIES_TYPE, category.id);
                await category.remove();
            }
            for (const hour of store.hours) {
                await hour.remove();
            }
            for (const picture of store.pictures) {
                await picture.remove();
            }
            await Elasticsearch.delete(Constants.ELASTIC_STORES_INDEX, Constants.ELASTIC_STORES_TYPE, store.id);
            await store.remove();
        }
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user1.id);
        await user1.remove();
    });
});
