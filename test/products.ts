import bcrypt from "bcrypt";
import chai from "chai";
import {expect} from "chai";
import chaiHttp from "chai-http";
import {getRepository} from "typeorm";
import app from "../src/app";
import Constants from "../src/Constants";
import {ProductEntity, UserEntity} from "../src/entity";
import {Elasticsearch, role, Utils} from "../src/utils";
import logger from "../src/utils/Logger";

chai.should();

chai.use(chaiHttp);

const agent1 = chai.request.agent(app); // User
const agent2 = chai.request.agent(app); // Admin

let user1: UserEntity = null;
let user2: UserEntity = null;
let products: ProductEntity[] = [];

/**
 * Test Products route
 */
describe("Products", () => {
    /**
     * Initial setup
     */
    before(async () => {
        await Utils.sleep(2000);
        const password = await bcrypt.hash("dummyA1*", 10);
        const fields1 = {email: "dummy@dummy.com", password, username: "username", active: true};
        user1 = await UserEntity.saveInDb(fields1, role.User);
        if (!user1) {
            throw new Error("Setup: Can't create new user");
        }
        agent1
            .post("/auth/login")
            .send({email: fields1.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields2 = {email: "dummy2@dummy.com", password, username: "username", active: true};
        user2 = await UserEntity.saveInDb(fields2, role.Admin);
        if (!user2) {
            throw new Error("Setup: Can't create new user");
        }
        agent2
            .post("/auth/login")
            .send({email: fields2.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields3 = [
            {
                name: "Fleur",
                description: "zzaezaze",
                brand: {name: "Marque"},
                tags: [{name: "Fleurs"}],
                pictures: [
                    {
                        name: "Fleur",
                        description: "Belle fleur",
                        url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                    }],
            }];
        products = await ProductEntity.saveAllInDb(fields3);
        if (products.length !== 1) {
            throw new Error("Setup: Error when create new products");
        }
        await Utils.sleep(1000);
    });

    /**
     * Test POST /products
     */

    describe("POST /products", () => {
        const fields1 = {
                name: "Fleur de feu",
                description: "zzaezaze",
                brand: {name: "Marque"},
                tags: [{name: "Fleurs"}],
                pictures: [
                    {
                        name: "Fleur",
                        description: "Belle fleur",
                        url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                    }],
            };

        it("it should return 403 (User)", (done) => {
            agent1
                .post("/products")
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(403);
                        expect(res).to.be.json;
                        res.body.message.should.be.a("string");
                        res.body.should.not.have.property("id");
                        res.body.should.not.have.property("name");
                        res.body.should.not.have.property("description");
                        res.body.should.not.have.property("brand");
                        res.body.should.not.have.property("tags");
                        res.body.should.not.have.property("pictures");
                        // Todo: Test DB
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test[POST /products]: %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 201 (Admin)", (done) => {
            agent2
                .post("/products")
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(201);
                        products.push(await ProductEntity.findOne(res.body.id));
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.should.have.property("name", fields1.name);
                        res.body.should.have.property("description", fields1.description);
                        res.body.should.have.property("brand");
                        res.body.should.have.property("tags");
                        res.body.should.have.property("pictures");
                        // Todo: Test DB
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test[POST /products]: %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .post("/products")
                .send(fields1)
                .end((err, res) => {
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("stores");
                    res.body.should.not.have.property("tags");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .post("/products")
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("stores");
                    res.body.should.not.have.property("tags");
                    err ? done(err) : done();
                });
        });
    });

    // Todo: Test edit product
    // Todo: Test patch product

    /**
     * Test GET /products
     */
    describe("GET /products", () => {
        it("it should return 200 with products info in array", (done) => {
            chai.request(app)
                .get("/products")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    err ? done(err) : done();
                });
        });
        // TODO: Improve tests
    });

    /**
     * Test GET /products/:productId
     */
    describe("GET /products/:productId", () => {
        it("it should return 200", (done) => {
            chai.request(app)
                .get("/products/" + products[0].id)
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.id.should.be.equal(products[0].id);
                    res.body.should.have.property("name", products[0].name);
                    res.body.should.have.property("description", products[0].description);
                    res.body.should.have.property("brand");
                    res.body.should.have.property("tags");
                    res.body.should.have.property("pictures");
                    res.body.should.have.property("stores");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid product id)", (done) => {
            chai.request(app)
                .get("/products/404")
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("tags");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });

        it("it should return 400 (invalid product id - string)", (done) => {
            chai.request(app)
                .get("/products/test")
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("tags");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test DELETE /products/:id
     */
    describe("DELETE/products/:id", () => {
        it("it should return 403 (User)", (done) => {
            agent1
                .delete("/products/" + products[0].id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("tags");
                    res.body.should.not.have.property("pictures");
                    err ? done(err) : done();
                });
        });

        it("it should return 204 (Admin)", (done) => {
            agent2
                .delete("/products/" + products[0].id)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const tmp: ProductEntity = await getRepository(ProductEntity).findOne(products[0].id);
                        if (tmp) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test[DELETE /products/:id]: %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .delete("/products/" + products[0].id)
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("stores");
                    res.body.should.not.have.property("tags");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .delete("/products/" + products[0].id)
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("stores");
                    res.body.should.not.have.property("tags");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 product not found", (done) => {
            agent2
                .delete("/products/" + products[0].id)
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("brand");
                    res.body.should.not.have.property("createTime");
                    res.body.should.not.have.property("updateTime");
                    res.body.should.not.have.property("pictures");
                    res.body.should.not.have.property("stores");
                    res.body.should.not.have.property("tags");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Delete created records
     */
    after(async () => {
        for (const product of products) {
            for (const picture of product.pictures) {
                await picture.remove();
            }
            if (product.tags) {
                for (const tag of product.tags) {
                    await Elasticsearch.delete(Constants.ELASTIC_TAGS_INDEX, Constants.ELASTIC_TAGS_TYPE, tag.id);
                    await tag.remove();
                }
            }
            if (product.brand) {
                await Elasticsearch.delete(Constants.ELASTIC_BRANDS_INDEX,
                    Constants.ELASTIC_BRANDS_TYPE, product.brand.id);
                await product.brand.remove();
            }
            await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX, Constants.ELASTIC_PRODUCTS_TYPE, product.id);
            await product.remove();
        }
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user1.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user2.id);
        await user1.remove();
        await user2.remove();
        agent1.close();
        agent2.close();
    });
});
