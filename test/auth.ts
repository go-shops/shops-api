import * as bcrypt from "bcrypt";
import chai from "chai";
import {expect} from "chai";
import chaiHttp from "chai-http";
import nodemailerMock from "nodemailer-mock";
import app from "../src/app";
import Constants from "../src/Constants";
import {UserEntity} from "../src/entity";
import {Elasticsearch} from "../src/utils";
import logger from "../src/utils/Logger";

chai.should();
chai.use(chaiHttp);

const agent1 = chai.request.agent(app);

let user: UserEntity = null;
let userActive: UserEntity = null;

/**
 * Test Auth routes
 */
describe("Auth", () => {
    /**
     * Initial setup
     */
    before(async () => {
        const password = await bcrypt.hash("dummyA1*", 10);
        const fields1 = {email: "dummy1@dummy.com", password, username: "username1", active: true,
            userHash: "ceciEstUnHash"};
        userActive = await UserEntity.saveInDb(fields1);
        if (!userActive) {
            throw new Error("Setup: Can't create new user");
        }
    });

    /**
     * Test POST /auth/register
     */
    describe("POST /auth/register", () => {
        const fields1 = {email: "dummy@dummy.com", password: "dummyA1*", username: "username"};
        it("it should return 201 created with user info", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(201);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.should.have.property("username");
                        res.body.username.should.be.a("string", fields1.username);
                        res.body.should.have.property("email");
                        res.body.email.should.be.a("string", fields1.email);

                        // Test DB record
                        user = await UserEntity.findOne(res.body.id);
                        if (!user) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (user.username !== fields1.username) {
                            throw new Error("DB: Expected username to be " + fields1.username);
                        }
                        if (user.email !== fields1.email) {
                            throw new Error("DB: Expected user email to be " + fields1.email);
                        }

                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /auth/register] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 400 error (duplicate email)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields2 = {email: "dummydummy.com", password: "dummyA1*", username: "username"};
        it("it should return 400 error (wrong email)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields3 = {email: "", password: "dummyA1*", username: "username"};
        it("it should return 400 error (empty email)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields4 = {password: "dummyA1*", username: "username"};
        it("it should return 400 error (no email)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields5 = {email: null, password: "dummyA1*", username: "username"};
        it("it should return 400 error (null email)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields6 = {email: "dummy@dummy.com", password: "dum", username: "username"};
        it("it should return 400 error (wrong password)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields7 = {email: "dummy@dummy.com", password: "", username: "username"};
        it("it should return 400 error (empty password)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields8 = {email: "dummy@dummy.com", username: "username"};
        it("it should return 400 error (no password)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields9 = {email: "dummy@dummy.com", password: null, username: "username"};
        it("it should return 400 error (null password)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields10 = {email: "dummy@dummy.com", password: "dummyA1", username: "abc"};
        it("it should return 400 error (invalid username)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields10)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields11 = {email: "dummy@dummy.com", password: "dummyA1", username: ""};
        it("it should return 400 error (empty username)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields11)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields12 = {email: "dummy@dummy.com", password: "dummyA1"};
        it("it should return 400 error (no username)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields12)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields13 = {email: "dummy@dummy.com", password: "dummyA1", username: null};
        it("it should return 400 error (null username)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields13)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields14 = {email: "dummy@dummy.com", password: "dummyA1", username: "username", wrongField: "Test"};
        it("it should return 400 error (invalid parameter)", (done) => {
            chai.request(app)
                .post("/auth/register")
                .send(fields14)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test POST /auth/login
     */
    describe("POST /auth/login", () => {
        const fields1 = {email: "dummy1@dummy.com", password: "dummyA1*"};
        it("it should return 200 ok", (done) => {
            agent1
                .post("/auth/login")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(res).to.have.cookie(Constants.SESSION_NAME);
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields2 = {email: "dummy@dummy.com", password: "dummy1*"};
        it("it should return 400 error (wrong password)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields3 = {email: "dummy@dummy.com", password: ""};
        it("it should return 400 error (empty password)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields4 = {email: "dummy@dummy.com"};
        it("it should return 400 error (no password)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields5 = {email: "dummy@dummy.com", password: null};
        it("it should return 400 error (null password)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields6 = {email: "dummy2@dummy.com", password: "dummyA1*"};
        it("it should return 400 error (wrong email)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields7 = {email: "", password: "dummyA1*"};
        it("it should return 400 error (empty email)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields8 = {password: "dummyA1*"};
        it("it should return 400 error (no email)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields9 = {email: null, password: "dummyA1*"};
        it("it should return 400 error (null email)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields10 = {email: "dummy@dummy.com", password: "dummyA1*", wrongField: "test"};
        it("it should return 400 error (invalid parameter)", (done) => {
            chai.request(app)
                .post("/auth/login")
                .send(fields10)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /auth/logout
     */
    describe("GET /auth/logout", () => {
            it ("it should return 401 (user not logged)", (done) => {
                chai.request(app)
                    .get("/auth/logout")
                    .send()
                    .end((err, res) => {
                        // Test Api response
                        expect(res).to.have.status(401);
                        expect(res).to.be.json;
                        res.body.should.have.property("message");
                        res.body.message.should.be.a("string");
                        err ? done(err) : done();
                    });
            });

            it ("it should return 401 (invalid cookie)", (done) => {
                chai.request(app)
                    .get("/auth/logout")
                    .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                    .send()
                    .end((err, res) => {
                        // Test Api response
                        expect(res).to.have.status(401);
                        expect(res).to.be.json;
                        res.body.should.have.property("message");
                        res.body.message.should.be.a("string");
                        err ? done(err) : done();
                    });
            });

            it ("it should return 200", (done) => {
                chai.request(app)
                    .get("/auth/logout")
                    .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                    .send()
                    .end((err, res) => {
                        // Test Api response
                        expect(res).to.have.status(401);
                        expect(res).to.be.json;
                        res.body.should.have.property("message");
                        res.body.message.should.be.a("string");
                        err ? done(err) : done();
                    });
            });
    });

    /**
     * Test GET auth/password/forgot
     */
    /*describe("POST /auth/password/forgot", () => {
        const fields1 = {email: "wrong@reallywrong.com"};
        it ("it should return 404 (unknown email)", (done) => {
            chai.request(app)
                .post("auth/password/forgot")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields2 = {email: "dummy1@dummy"};
        it ("it should return 400 (invalid email)", (done) => {
            chai.request(app)
                .post("auth/password/forgot")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields3 = {email: ""};
        it ("it should return 400 (empty email)", (done) => {
            chai.request(app)
                .post("auth/password/forgot")
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields4 = {email: "dummy1@dummy.com"};
        it ("it should return 200", (done) => {
            chai.request(app)
                .post("auth/password/forgot")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(201);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });
    });*/

    /**
     * Test patch /auth/password
     */
    /* TODO: Fix les units tests (Uncaught TypeError: Cannot use 'in' operator to search for 'status' in undefined)*/
    /*describe("patch /auth/password", () => {
        const fields1 = {userHash: "cecinestpasunhash", password: "dummyA1*"};
        it("it should return 404 (wrong hash)", (done) => {
           chai.request(app)
               .patch("auth/password")
               .send(fields1)
               .end((err, res) => {
               // Test Api response
                   expect(res).to.have.status(404);
                   expect(res).to.be.json;
                   res.body.should.have.property("message");
                   res.body.message.should.be.a("string");
                   err ? done(err) : done();
               });
        });

        const fields2 = {password: "dummyA1*"};
        it("it should return 404 (no Hash)", (done) => {
            chai.request(app)
                .patch("auth/password")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields3 = {userHash: "", password: "dummyA1*"};
        it("it should return 400 (empty hash)", (done) => {
            chai.request(app)
                .patch("auth/password")
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields4 = {userHash: "ceciEstUnHash"};
        it("it should return 400 (no password)", (done) => {
            chai.request(app)
                .patch("auth/password")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields5 = {userHash: "ceciEstUnHash", password: ""};
        it("it should return 400 (Empty password)", (done) => {
            chai.request(app)
                .patch("auth/password")
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields6 = {userHash: "ceciEstUnHash", password: "1234mdp"};
        it("it should return 400 (wrong password)", (done) => {
            chai.request(app)
                .patch("auth/password")
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        const fields7 = {userHash: "ceciEstUnHash", password: "dummyA1*"};
        it("it should return 200 (password updated)", (done) => {
            chai.request(app)
                .patch("auth/password")
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });
    });*/

    /**
     * Delete created records
     */
    after(async () => {
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, userActive.id);
        await user.remove();
        await userActive.remove();
        nodemailerMock.mock.reset();
    });
});
