import bcrypt from "bcrypt";
import {expect} from "chai";
import chai from "chai";
import chaiHttp from "chai-http";
import randomstring from "randomstring";
import app from "../src/app";
import Constants from "../src/Constants";
import {ProductCommentEntity, ProductEntity, StoreCommentEntity, StoreEntity, UserEntity} from "../src/entity";
import {Elasticsearch, role, Utils} from "../src/utils";
import logger from "../src/utils/Logger";

chai.should();
chai.use(chaiHttp);

const agent1 = chai.request.agent(app); // User - store Owner
const agent2 = chai.request.agent(app); // User
const agent3 = chai.request.agent(app); // Admin

let user1: UserEntity = null;
let user2: UserEntity = null;
let user3: UserEntity = null;
let store: StoreEntity = null;
let product: ProductEntity = null;
let storeCommentId = null;
let productCommentId = null;

/**
 * Test Comments route
 */
describe("Comments", () => {
    /**
     * Initial setup
     */
    before(async () => {
        await Utils.sleep(2000);
        const password = await bcrypt.hash("dummyA1*", 10);
        const fields1 = {email: "dummy@dummy.com", password, username: "username", active: true};
        user1 = await UserEntity.saveInDb(fields1, role.User);
        if (!user1) {
            throw new Error("Setup: Can't create new user");
        }
        agent1
            .post("/auth/login")
            .send({email: fields1.email, password: "dummyA1*"}).end((err, res) => {
                expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields2 = {email: "dummy2@dummy.com", password, username: "username", active: true};
        user2 = await UserEntity.saveInDb(fields2, role.User);
        if (!user2) {
            throw new Error("Setup: Can't create new user");
        }
        agent2
            .post("/auth/login")
            .send({email: fields2.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields3 = {email: "dummy3@dummy.com", password, username: "username", active: true};
        user3 = await UserEntity.saveInDb(fields3, role.Admin);
        if (!user2) {
            throw new Error("Setup: Can't create new user");
        }
        agent3
            .post("/auth/login")
            .send({email: fields3.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields4 = {
            name: "Toto",
            description: "Magnifique magasin",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            hours: [
                {day: "monday", timeOpen: "08:00:00", timeClosed: "12:00:00"},
                {day: "monday", timeOpen: "13:00:00", timeClosed: "15:00:00"}],
            categories: [
                {name: "Boutique"},
                {name: "Fleuriste"}],
        };
        store = await StoreEntity.saveInDb(user1, fields4);
        if (!store) {
            throw new Error("Setup: Can't create new store");
        }

        const fields5 = {
            name: "Fleur",
            description: "zzaezaze",
            brand: {name: "Marque"},
            pictures: [
                {
                    name: "Fleur",
                    description: "Belle fleur",
                    url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                }],
        };
        product = await ProductEntity.saveInDb(fields5);
        if (!product) {
            throw new Error("Setup: Error when create new product");
        }
        await Utils.sleep(1000);
    });

    /**
     * Test POST /comments/stores/:storeId
     */
    describe("POST /comments/stores/:storeId", () => {
        const fields1 = {comment: "Magnifique magasin", mark: 4};
        it("it should return 201 (User)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(201);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        storeCommentId = res.body.id;
                        res.body.should.have.property("comment", fields1.comment);
                        res.body.should.have.property("mark", fields1.mark);

                        // Test DB record
                        const comment: StoreCommentEntity = await StoreCommentEntity.findOne(res.body.id);
                        if (!comment) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (comment.comment !== fields1.comment) {
                            throw new Error("DB: Expected comment to be " + fields1.comment);
                        }
                        if (comment.mark !== fields1.mark) {
                            throw new Error("DB: Expected comment mark name to be " + fields1.mark);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /comments/stores/:storeId] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 403 (Store owner)", (done) => {
            agent1
                .post("/comments/stores/" + store.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid store)", (done) => {
            agent2
                .post("/comments/stores/404")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (no cookie)", (done) => {
            chai.request(app)
                .post("/comments/stores/" + store.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .post("/comments/stores/" + store.id)
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields2 = {comment: "Magnifique magasin", mark: 99};
        it("it should return 400 (wrong mark)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields3 = {comment: "Magnifique magasin"};
        it("it should return 400 (no mark)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields4 = {comment: "Magnifique magasin", mark: null};
        it("it should return 400 (null mark)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields5 = {comment: randomstring.generate(300), mark: 4};
        it("it should return 400 (wrong comment)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields6 = {comment: "", mark: 4};
        it("it should return 400 (empty comment)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields7 = {mark: 4};
        it("it should return 400 (no comment)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields8 = {comment: null, mark: 4};
        it("it should return 400 (null comment)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields9 = {comment: "Magnifique magasin", mark: 4, wrongField: "Test"};
        it("it should return 400 (invalid parameter)", (done) => {
            agent2
                .post("/comments/stores/" + store.id)
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test POST /comments/products/:productId
     */
    describe("POST /comments/products/:productId", () => {
        const fields1 = {comment: "Magnifique produit", mark: 4};
        it("it should return 201 (User)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(201);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        productCommentId = res.body.id;
                        res.body.should.have.property("comment", fields1.comment);
                        res.body.should.have.property("mark", fields1.mark);

                        // Test DB record
                        const comment: ProductCommentEntity = await ProductCommentEntity.findOne(res.body.id);
                        if (!comment) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (comment.comment !== fields1.comment) {
                            throw new Error("DB: Expected comment to be " + fields1.comment);
                        }
                        if (comment.mark !== fields1.mark) {
                            throw new Error("DB: Expected comment mark name to be " + fields1.mark);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /comments/products/:productId] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 404 (invalid store)", (done) => {
            agent2
                .post("/comments/products/404")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (no cookie)", (done) => {
            chai.request(app)
                .post("/comments/products/" + product.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .post("/comments/products/" + product.id)
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields2 = {comment: "Magnifique produit", mark: 99};
        it("it should return 400 (wrong mark)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields3 = {comment: "Magnifique produit"};
        it("it should return 400 (no mark)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields4 = {comment: "Magnifique produit", mark: null};
        it("it should return 400 (null mark)", (done) => {
            agent2
                .post("/comments/stores/" + product.id)
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields5 = {comment: randomstring.generate(300), mark: 4};
        it("it should return 400 (wrong comment)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields6 = {comment: "", mark: 4};
        it("it should return 400 (empty comment)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields7 = {mark: 4};
        it("it should return 400 (no comment)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields8 = {comment: null, mark: 4};
        it("it should return 400 (null comment)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields9 = {comment: "Magnifique produit", mark: 4, wrongField: "Test"};
        it("it should return 400 (invalid parameter)", (done) => {
            agent2
                .post("/comments/products/" + product.id)
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /comments/stores/:commentId
     */
    describe("GET /comments/stores/:commentId", () => {
        const fields1 = {comment: "Magnifique magasin", mark: 4};
        it("it should return 200 (Logged)", (done) => {
            agent1
                .get("/comments/stores/" + storeCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id", storeCommentId);
                    res.body.should.have.property("comment", fields1.comment);
                    res.body.should.have.property("mark", fields1.mark);
                    res.body.should.have.property("store");
                    res.body.should.have.property("owner");
                    res.body.owner.should.have.property("id", user2.id);
                    res.body.owner.should.have.property("username");
                    res.body.owner.should.have.property("bio");
                    res.body.owner.should.have.property("picture");
                    res.body.owner.should.have.property("banner");
                    res.body.owner.should.have.property("role");
                    res.body.owner.should.not.have.property("email");
                    res.body.owner.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (Not logged)", (done) => {
            agent1
                .get("/comments/stores/" + storeCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id", storeCommentId);
                    res.body.should.have.property("comment", fields1.comment);
                    res.body.should.have.property("mark", fields1.mark);
                    res.body.should.have.property("store");
                    res.body.should.have.property("owner");
                    res.body.owner.should.have.property("id", user2.id);
                    res.body.owner.should.have.property("username");
                    res.body.owner.should.have.property("bio");
                    res.body.owner.should.have.property("picture");
                    res.body.owner.should.have.property("banner");
                    res.body.owner.should.have.property("role");
                    res.body.owner.should.not.have.property("email");
                    res.body.owner.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid comment id)", (done) => {
            agent1
                .get("/comments/stores/" + 404)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    res.body.should.not.have.property("owner");
                    res.body.should.not.have.property("store");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /comments/products/:commentId
     */
    describe("GET /comments/products/:commentId", () => {
        const fields1 = {comment: "Magnifique produit", mark: 4};
        it("it should return 200 (Logged)", (done) => {
            agent1
                .get("/comments/products/" + productCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id", productCommentId);
                    res.body.should.have.property("comment", fields1.comment);
                    res.body.should.have.property("mark", fields1.mark);
                    res.body.should.have.property("product");
                    res.body.should.have.property("owner");
                    res.body.owner.should.have.property("id", user2.id);
                    res.body.owner.should.have.property("username");
                    res.body.owner.should.have.property("bio");
                    res.body.owner.should.have.property("picture");
                    res.body.owner.should.have.property("banner");
                    res.body.owner.should.have.property("role");
                    res.body.owner.should.not.have.property("email");
                    res.body.owner.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (Not logged)", (done) => {
            agent1
                .get("/comments/products/" + productCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id", productCommentId);
                    res.body.should.have.property("comment", fields1.comment);
                    res.body.should.have.property("mark", fields1.mark);
                    res.body.should.have.property("product");
                    res.body.should.have.property("owner");
                    res.body.owner.should.have.property("id", user2.id);
                    res.body.owner.should.have.property("username");
                    res.body.owner.should.have.property("bio");
                    res.body.owner.should.have.property("picture");
                    res.body.owner.should.have.property("banner");
                    res.body.owner.should.have.property("role");
                    res.body.owner.should.not.have.property("email");
                    res.body.owner.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid comment id)", (done) => {
            agent1
                .get("/comments/products/" + 404)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("owner");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test PUT /comments/stores/:commentId
     */
    describe("PUT /comments/stores/:commentId", () => {
        const fields1 = {comment: "Magnifique magasin", mark: 5};
        it("it should return 200 (User)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(200);
                        expect(res).to.be.json;
                        res.body.should.have.property("id", storeCommentId);
                        res.body.should.have.property("comment", fields1.comment);
                        res.body.should.have.property("mark", fields1.mark);

                        // Test DB record
                        const comment: StoreCommentEntity = await StoreCommentEntity.findOne(res.body.id);
                        if (!comment) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (comment.comment !== fields1.comment) {
                            throw new Error("DB: Expected comment to be " + fields1.comment);
                        }
                        if (comment.mark !== fields1.mark) {
                            throw new Error("DB: Expected comment mark name to be " + fields1.mark);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][PUT /comments/stores/:commentId] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 200 (Admin)", (done) => {
            agent3
                .put("/comments/stores/" + storeCommentId)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(200);
                        expect(res).to.be.json;
                        res.body.should.have.property("id", storeCommentId);
                        res.body.should.have.property("comment", fields1.comment);
                        res.body.should.have.property("mark", fields1.mark);

                        // Test DB record
                        const comment: StoreCommentEntity = await StoreCommentEntity.findOne(res.body.id);
                        if (!comment) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (comment.comment !== fields1.comment) {
                            throw new Error("DB: Expected comment to be " + fields1.comment);
                        }
                        if (comment.mark !== fields1.mark) {
                            throw new Error("DB: Expected comment mark name to be " + fields1.mark);
                        }

                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][PUT /comments/stores/:commentId] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 403 (Store owner)", (done) => {
            agent1
                .put("/comments/stores/" + storeCommentId)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (comment not found)", (done) => {
            agent2
                .put("/comments/stores/99")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .put("/comments/stores/" + storeCommentId)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .put("/comments/stores/" + storeCommentId)
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields2 = {comment: "Magnifique magasin", mark: 99};
        it("it should return 400 (wrong mark)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields3 = {comment: "Magnifique magasin"};
        it("it should return 400 (no mark)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields4 = {comment: "Magnifique magasin", mark: null};
        it("it should return 400 (null mark)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields5 = {comment: randomstring.generate(300), mark: 4};
        it("it should return 400 (wrong comment)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields6 = {comment: "", mark: 4};
        it("it should return 400 (empty comment)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields7 = {mark: 4};
        it("it should return 400 (no comment)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields8 = {comment: null, mark: 4};
        it("it should return 400 (null comment)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields9 = {comment: "Magnifique magasin", mark: 4, wrongField: "Test"};
        it("it should return 400 (invalid parameter)", (done) => {
            agent2
                .put("/comments/stores/" + storeCommentId)
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test PUT /comments/products/:commentId
     */
    describe("PUT /comments/products/:commentId", () => {
        const fields1 = {comment: "Magnifique produit", mark: 5};
        it("it should return 200 (User)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(200);
                        expect(res).to.be.json;
                        res.body.should.have.property("id", productCommentId);
                        res.body.should.have.property("comment", fields1.comment);
                        res.body.should.have.property("mark", fields1.mark);

                        // Test DB record
                        const comment: ProductCommentEntity = await ProductCommentEntity.findOne(res.body.id);
                        if (!comment) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (comment.comment !== fields1.comment) {
                            throw new Error("DB: Expected comment to be " + fields1.comment);
                        }
                        if (comment.mark !== fields1.mark) {
                            throw new Error("DB: Expected comment mark name to be " + fields1.mark);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][PUT /comments/products/:commentId] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 200 (Admin)", (done) => {
            agent3
                .put("/comments/products/" + productCommentId)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(200);
                        expect(res).to.be.json;
                        res.body.should.have.property("id", productCommentId);
                        res.body.should.have.property("comment", fields1.comment);
                        res.body.should.have.property("mark", fields1.mark);

                        // Test DB record
                        const comment: ProductCommentEntity = await ProductCommentEntity.findOne(res.body.id);
                        if (!comment) {
                            throw new Error("DB: Expected record to exist in DB");
                        }
                        if (comment.comment !== fields1.comment) {
                            throw new Error("DB: Expected comment to be " + fields1.comment);
                        }
                        if (comment.mark !== fields1.mark) {
                            throw new Error("DB: Expected comment mark name to be " + fields1.mark);
                        }

                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][PUT /comments/products/:commentId] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 403 (Store owner)", (done) => {
            agent1
                .put("/comments/products/" + productCommentId)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (comment not found)", (done) => {
            agent2
                .put("/comments/products/99")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .put("/comments/products/" + productCommentId)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .put("/comments/products/" + productCommentId)
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields2 = {comment: "Magnifique magasin", mark: 99};
        it("it should return 400 (wrong mark)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields3 = {comment: "Magnifique magasin"};
        it("it should return 400 (no mark)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields4 = {comment: "Magnifique magasin", mark: null};
        it("it should return 400 (null mark)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields5 = {comment: randomstring.generate(300), mark: 4};
        it("it should return 400 (wrong comment)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields6 = {comment: "", mark: 4};
        it("it should return 400 (empty comment)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields7 = {mark: 4};
        it("it should return 400 (no comment)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields8 = {comment: null, mark: 4};
        it("it should return 400 (null comment)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });

        const fields9 = {comment: "Magnifique magasin", mark: 4, wrongField: "Test"};
        it("it should return 400 (invalid parameter)", (done) => {
            agent2
                .put("/comments/products/" + productCommentId)
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("comment");
                    res.body.should.not.have.property("mark");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test DELETE /comments/stores/:commentId
     */
    describe("DELETE /comments/stores/:commentId", () => {
        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .delete("/comments/stores/" + storeCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .delete("/comments/stores/" + storeCommentId)
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (comment not found)", (done) => {
            agent2
                .delete("/comments/stores/404")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 403 (wrong user)", (done) => {
            agent1
                .delete("/comments/stores/" + storeCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 204 with nothing in body", (done) => {
            agent2
                .delete("/comments/stores/" + storeCommentId)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const comment: StoreCommentEntity = await StoreCommentEntity.findOne(storeCommentId);
                        if (comment) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][DELETE /comments/:id] %s", e.message);
                        done(e);
                    }
                });
        });
    });

    /**
     * Test DELETE /comments/stores/:commentId
     */
    describe("DELETE /comments/stores/:commentId", () => {
        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .delete("/comments/products/" + productCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .delete("/comments/products/" + productCommentId)
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (comment not found)", (done) => {
            agent2
                .delete("/comments/products/404")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 403 (wrong user)", (done) => {
            agent1
                .delete("/comments/products/" + productCommentId)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 204 with nothing in body", (done) => {
            agent2
                .delete("/comments/products/" + productCommentId)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const comment: ProductCommentEntity = await ProductCommentEntity.findOne(productCommentId);
                        if (comment) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][DELETE /comments/:id] %s", e.message);
                        done(e);
                    }
                });
        });
    });

    /**
     * Delete created records
     */
    after(async () => {
        await store.address.remove();
        for (const prd of store.products) {
            for (const picture of prd.pictures) {
                await picture.remove();
            }
            if (prd.brand) {
                await Elasticsearch.delete(Constants.ELASTIC_BRANDS_INDEX,
                    Constants.ELASTIC_BRANDS_TYPE, prd.brand.id);
                await prd.brand.remove();
            }
            await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX, Constants.ELASTIC_PRODUCTS_TYPE, prd.id);
            await prd.remove();
        }
        for (const category of store.categories) {
            await Elasticsearch.delete(Constants.ELASTIC_CATEGORIES_INDEX,
                Constants.ELASTIC_CATEGORIES_TYPE, category.id);
            await category.remove();
        }
        for (const hour of store.hours) {
            await hour.remove();
        }
        for (const picture of store.pictures) {
            await picture.remove();
        }
        for (const picture of product.pictures) {
            await picture.remove();
        }
        if (product.brand) {
            await Elasticsearch.delete(Constants.ELASTIC_BRANDS_INDEX,
                Constants.ELASTIC_BRANDS_TYPE, product.brand.id);
            await product.brand.remove();
        }
        await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX, Constants.ELASTIC_PRODUCTS_TYPE, product.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user1.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user2.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user3.id);
        await Elasticsearch.delete(Constants.ELASTIC_STORES_INDEX, Constants.ELASTIC_STORES_TYPE, store.id);
        await user1.remove();
        await user2.remove();
        await user3.remove();
        await store.remove();
        await product.remove();
        agent1.close();
        agent2.close();
        agent3.close();
    });
});
