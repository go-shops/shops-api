import bcrypt from "bcrypt";
import chai from "chai";
import {expect} from "chai";
import chaiHttp from "chai-http";
import { getRepository } from "typeorm";
import app from "../src/app";
import Constants from "../src/Constants";
import { StoreCommentEntity, StoreEntity, UserEntity } from "../src/entity";
import {Elasticsearch, role} from "../src/utils";
import logger from "../src/utils/Logger";

chai.should();
chai.use(chaiHttp);

const agent1 = chai.request.agent(app); // User
const agent2 = chai.request.agent(app); // Admin

let user1: UserEntity = null;
let user2: UserEntity = null;
let store: StoreEntity = null;
let comment: StoreCommentEntity = null;

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

/**
 * Test Users route
 */
describe("Users", () => {
    /**
     * Initial setup
     */
    before(async () => {
        await sleep(2000);
        const password = await bcrypt.hash("dummyA1*", 10);
        const fields1 = {email: "dummy@dummy.com", password, username: "username", active: true};
        user1 = await UserEntity.saveInDb(fields1, role.User);
        if (!user1) {
            throw new Error("Setup: Can't create new user");
        }
        agent1
            .post("/auth/login")
            .send({email: fields1.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields2 = {email: "dummy2@dummy.com", password, username: "username", active: true};
        user2 = await UserEntity.saveInDb(fields2, role.Admin);
        if (!user2) {
            throw new Error("Setup: Can't create new user");
        }
        agent2
            .post("/auth/login")
            .send({email: fields2.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields3 = {
            name: "Toto",
            description: "Magnifique magasin",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: "93000",
                country: "Afrique du Sud",
            },
            hours: [
                {day: "monday", timeOpen: "08:00:00", timeClosed: "12:00:00"},
                {day: "monday", timeOpen: "13:00:00", timeClosed: "15:00:00"}],
            categories: [
                {name: "Boutique"},
                {name: "Fleuriste"}],
        };
        store = await StoreEntity.saveInDb(user1, fields3);
        if (!store) {
            throw new Error("Setup: Can't create new store");
        }

        const fields4 = {comment: "Super magasin", mark: 4};
        comment = await StoreCommentEntity.saveInDb({...fields4, owner: user1, store});
        if (!comment) {
            throw  new Error("Setup: Can't create new comment");
        }
        await sleep(1000);
    });

    /**
     * Test GET /users/me
     */
    describe("GET /users/me", () => {
        const fields1 = {username: "username", email: "dummy@dummy.com", bio: null, banner: null, picture: null};
        it("it should return 200 (Logged)", (done) => {
            agent1
                .get("/users/me")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("username", fields1.username);
                    res.body.should.have.property("email", fields1.email);
                    res.body.should.have.property("banner", fields1.banner);
                    res.body.should.have.property("picture", fields1.picture);
                    res.body.should.have.property("bio", fields1.bio);
                    res.body.should.have.property("role", role.User);
                    res.body.should.have.property("permissions");
                    res.body.permissions.should.be.a("Array");
                    for (const perm of res.body.permissions) {
                        perm.should.have.property("id");
                        perm.should.have.property("permission");
                    }
                    res.body.should.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (Not logged)", (done) => {
            chai.request(app)
                .get("/users/me")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("banner");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("role");
                    res.body.should.have.property("permissions");
                    res.body.permissions.should.be.a("Array");
                    for (const perm of res.body.permissions) {
                        perm.should.have.property("id");
                        perm.should.have.property("permission");
                    }
                    res.body.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /users/me/rgpd
     */
    describe("GET /users/me/rgpd", () => {
        it("should return 200", (done) => {
            agent1
                .get("/users/me/rgpd")
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res).to.have.header("Content-Disposition", "attachment; filename=rgpd.txt");
                    err ? done(err) : done();
                });
        });

        it("should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .get("/users/me/rgpd")
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(401);
                    expect(res).to.not.have.header("Content-Disposition", "attachment; filename=rgpd.txt");
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .get("/users/me/rgpd")
                .set("Cookie", Constants.SESSION_NAME + "=wrongCookie;")
                .send()
                .end((err, res) => {
                    expect(res).to.have.status(401);
                    expect(res).to.not.have.header("Content-Disposition", "attachment; filename=rgpd.txt");
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /users/:userId
     */
    describe("GET /users/:userId", () => {
        const fields1 = {username: "username", email: "dummy@dummy.com", bio: null, banner: null, picture: null};
        it("it should return 200 (private info - User)", (done) => {
            agent1
                .get("/users/" + user1.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("username", fields1.username);
                    res.body.should.have.property("email", fields1.email);
                    res.body.should.have.property("banner", fields1.banner);
                    res.body.should.have.property("picture", fields1.picture);
                    res.body.should.have.property("bio", fields1.bio);
                    res.body.should.have.property("role", role.User);
                    res.body.should.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        const fields2 = {username: "username", email: "dummy2@dummy.com", bio: null, banner: null, picture: null};
        it("it should return 200 (public info - User)", (done) => {
            agent1
                .get("/users/" + user2.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("username", fields2.username);
                    res.body.should.not.have.property("email");
                    res.body.should.have.property("banner", fields2.banner);
                    res.body.should.have.property("picture", fields2.picture);
                    res.body.should.have.property("bio", fields2.bio);
                    res.body.should.have.property("role", role.Admin);
                    res.body.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (private info - Admin)", (done) => {
            agent2
                .get("/users/" + user1.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("username", fields1.username);
                    res.body.should.have.property("email", fields1.email);
                    res.body.should.have.property("banner", fields1.banner);
                    res.body.should.have.property("picture", fields1.picture);
                    res.body.should.have.property("bio", fields1.bio);
                    res.body.should.have.property("role", role.User);
                    res.body.should.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (User not found - id -> 404)", (done) => {
            agent1
                .get("/users/404")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("parentEmail");
                    res.body.should.not.have.property("comments");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });

        it("it should return 400 (Not found - id -> a)", (done) => {
            agent1
                .get("/users/a")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("parentEmail");
                    res.body.should.not.have.property("comments");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /users/:userId/comments
     */
    describe("GET /users/:userId/comments", () => {
        it("it should return 200 (Logged)", (done) => {
            agent1
                .get("/users/" + user1.id + "/comments")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    for (const elem of res.body) {
                        elem.should.have.property("id");
                        elem.should.have.property("mark");
                        elem.should.have.property("comment");
                        elem.should.have.property("store");
                        elem.store.should.have.property("id");
                        elem.store.should.have.property("name");
                        elem.store.should.have.property("description");
                        elem.store.should.have.property("avgMark");
                        elem.store.should.have.property("siret");
                        elem.store.should.have.property("address");
                        elem.store.should.have.property("hours");
                        elem.store.should.have.property("categories");
                        elem.store.should.have.property("pictures");
                        elem.store.should.have.property("nbProducts");
                    }
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (Not logged)", (done) => {
            chai.request(app)
                .get("/users/" + user1.id + "/comments")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    for (const elem of res.body) {
                        elem.should.have.property("id");
                        elem.should.have.property("mark");
                        elem.should.have.property("comment");
                        elem.should.have.property("store");
                        elem.store.should.have.property("id");
                        elem.store.should.have.property("name");
                        elem.store.should.have.property("description");
                        elem.store.should.have.property("avgMark");
                        elem.store.should.have.property("siret");
                        elem.store.should.have.property("address");
                        elem.store.should.have.property("hours");
                        elem.store.should.have.property("categories");
                        elem.store.should.have.property("pictures");
                        elem.store.should.have.property("nbProducts");
                    }
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /users/:userId/stores
     */
    describe("GET /users/:userId/stores", () => {
        it("it should return 200 (Logged)", (done) => {
            agent1
                .get("/users/" + user1.id + "/stores")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    for (const elem of res.body) {
                        elem.should.have.property("id");
                        elem.should.have.property("name");
                        elem.should.have.property("description");
                        elem.should.have.property("avgMark");
                        elem.should.have.property("siret");
                        elem.should.have.property("address");
                        elem.should.have.property("hours");
                        elem.should.have.property("categories");
                        elem.should.have.property("pictures");
                        elem.should.have.property("nbProducts");
                    }
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (Not logged)", (done) => {
            chai.request(app)
                .get("/users/" + user1.id + "/stores")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    for (const elem of res.body) {
                        elem.should.have.property("id");
                        elem.should.have.property("name");
                        elem.should.have.property("description");
                        elem.should.have.property("avgMark");
                        elem.should.have.property("siret");
                        elem.should.have.property("address");
                        elem.should.have.property("hours");
                        elem.should.have.property("categories");
                        elem.should.have.property("pictures");
                        elem.should.have.property("nbProducts");
                    }
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test PUT /users/:userId
     */
    describe("PUT /users/:userId", () => {
        const fields1 = {username: "username", email: "dummy@dummy.com", bio: "Ceci est une bio"};
        it("it should return 200 (User)", (done) => {
            agent1
                .put("/users/" + user1.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("username", fields1.username);
                    res.body.should.have.property("email", fields1.email);
                    res.body.should.have.property("bio", fields1.bio);
                    res.body.should.have.property("picture");
                    res.body.should.have.property("banner");
                    res.body.should.have.property("role");
                    res.body.should.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 403 (User)", (done) => {
            agent1
                .put("/users/" + user2.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("banner");
                    res.body.should.not.have.property("role");
                    res.body.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (Admin)", (done) => {
            agent2
                .put("/users/" + user1.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("username", fields1.username);
                    res.body.should.have.property("email", fields1.email);
                    res.body.should.have.property("bio", fields1.bio);
                    res.body.should.have.property("picture");
                    res.body.should.have.property("banner");
                    res.body.should.have.property("role");
                    res.body.should.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .put("/users/" + user1.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("banner");
                    res.body.should.not.have.property("role");
                    res.body.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .put("/users/" + user1.id)
                .set("Cookie", Constants.SESSION_NAME + "=wrongToken;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("banner");
                    res.body.should.not.have.property("role");
                    res.body.should.not.have.property("parentEmail");
                    err ? done(err) : done();
                });
        });

        const fields2 = {username: "username", email: "dummy2@dummy.com", bio: "Ceci est une bio"};
        it("it should return 400 (email already used)", (done) => {
            agent1
                .put("/users/" + user1.id)
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("parentEmail");
                    res.body.should.not.have.property("comments");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test PUT /users/:userId/permissions
     */
    describe("PUT /users/:userId/permissions", () => {
        const fields1 = {role: "Moderator"};
        it("it should return 403 (User)", (done) => {
            agent1
                .put("/users/" + user1.id + "/permissions")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 200 (Admin)", (done) => {
            agent2
                .put("/users/" + user1.id + "/permissions")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    for (const perm of res.body) {
                        perm.should.have.property("id");
                        perm.should.have.property("permission");
                    }
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .put("/users/" + user1.id + "/permissions")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .put("/users/" + user1.id + "/permissions")
                .set("Cookie", Constants.SESSION_NAME + "=wrongToken;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test DELETE /users/me
     */
    describe("DELETE /users/me", () => {
        it("it should return 403 (User)", (done) => {
            agent1
                .delete("/users/" + user2.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    err ? done(err) : done();
                });
        });

        it("it should return 204 (Admin)", (done) => {
            agent2
                .delete("/users/" + user1.id)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const user: UserEntity = await getRepository(UserEntity).findOne(user1.id);
                        if (user) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        comment = await getRepository(StoreCommentEntity).findOne(comment.id);
                        if (!comment || comment.owner === null) {
                            throw new Error("DB: Expected comment to exist and have user set to null");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][DELETE /users/:id] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 204 (Admin - itself)", (done) => {
            agent2
                .delete("/users/" + user2.id)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const user: UserEntity = await getRepository(UserEntity).findOne(user2.id);
                        if (user) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][DELETE /users/:id] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 404 user not found (already deleted)", (done) => {
            agent1
                .delete("/users/" + user1.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("parentEmail");
                    res.body.should.not.have.property("comments");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .delete("/users/" + user1.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("parentEmail");
                    res.body.should.not.have.property("comments");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .delete("/users/" + user1.id)
                .set("Cookie", Constants.SESSION_NAME + "=wrongToken;")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("username");
                    res.body.should.not.have.property("email");
                    res.body.should.not.have.property("bio");
                    res.body.should.not.have.property("picture");
                    res.body.should.not.have.property("parentEmail");
                    res.body.should.not.have.property("comments");
                    res.body.should.not.have.property("stores");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Delete created records
     */
    after(async () => {
        if (store) {
            await store.address.remove();
            for (const product of store.products) {
                for (const picture of product.pictures) {
                    await picture.remove();
                }
                if (product.brand) {
                    await Elasticsearch.delete(Constants.ELASTIC_BRANDS_INDEX,
                        Constants.ELASTIC_BRANDS_TYPE, product.brand.id);
                    await product.brand.remove();
                }
                await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX,
                    Constants.ELASTIC_PRODUCTS_TYPE, product.id);
                await product.remove();
            }
            for (const category of store.categories) {
                await Elasticsearch.delete(Constants.ELASTIC_CATEGORIES_INDEX,
                    Constants.ELASTIC_CATEGORIES_TYPE, category.id);
                await category.remove();
            }
            for (const hour of store.hours) {
                await hour.remove();
            }
            for (const picture of store.pictures) {
                await picture.remove();
            }
            await Elasticsearch.delete(Constants.ELASTIC_STORES_INDEX, Constants.ELASTIC_STORES_TYPE, store.id);
            await store.remove();
        }
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user1.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user2.id);
        await user1.remove();
        await user2.remove();
        agent1.close();
        agent2.close();
    });
});
