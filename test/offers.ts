import bcrypt from "bcrypt";
import {expect} from "chai";
import chai from "chai";
import chaiHttp from "chai-http";
import randomstring from "randomstring";
import {getRepository} from "typeorm";
import app from "../src/app";
import Constants from "../src/Constants";
import {
    OfferEntity,
    StoreEntity,
    UserEntity,
} from "../src/entity";
import {Elasticsearch, role, Utils} from "../src/utils";
import logger from "../src/utils/Logger";

chai.should();
chai.use(chaiHttp);

const agent1 = chai.request.agent(app); // Store Owner - User
const agent2 = chai.request.agent(app); // User
const agent3 = chai.request.agent(app); // Admin

let user1: UserEntity = null;
let user2: UserEntity = null;
let user3: UserEntity = null;
let offer1: OfferEntity = null;
let offer2: OfferEntity = null;
let store: StoreEntity = null;

/**
 * Test Offers route
 */
describe("Offers", () => {
    /**
     * Initial setup
     */
    before(async () => {
        await Utils.sleep(2000);
        const password = await bcrypt.hash("dummyA1*", 10);
        const fields1 = {email: "dummy@dummy.com", password, username: "username", active: true};
        user1 = await UserEntity.saveInDb(fields1, role.User);
        if (!user1) {
            throw new Error("Setup: Can't create new user");
        }
        agent1
            .post("/auth/login")
            .send({email: fields1.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields2 = {email: "dummy2@dummy.com", password, username: "username2", active: true};
        user2 = await UserEntity.saveInDb(fields2, role.User);
        if (!user2) {
            throw new Error("Setup: Can't create new user");
        }
        agent2
            .post("/auth/login")
            .send({email: fields2.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields3 = {email: "dummy3@dummy.com", password, username: "username2", active: true};
        user3 = await UserEntity.saveInDb(fields3, role.Admin);
        if (!user2) {
            throw new Error("Setup: Can't create new user");
        }
        agent3
            .post("/auth/login")
            .send({email: fields3.email, password: "dummyA1*"}).end((err, res) => {
            expect(res).to.have.cookie(Constants.SESSION_NAME);
        });

        const fields4 = {
            name: "Toto",
            description: "Magnifique magasin",
            siret: "09876543210987",
            address: {
                street: "rue de la paix",
                streetNbr: 78,
                city: "Paris",
                zipCode: 93000,
                country: "Afrique du Sud",
            },
            schedules: [
                {day: "monday", timeOpen: "08:00:00", timeClosed: "12:00:00"},
                {day: "monday", timeOpen: "13:00:00", timeClosed: "15:00:00"}],
            categories: [{name: "Boutique"}, {name: "Fleuriste"}],
            products: [
                {
                    name: "Fleur",
                    description: "zzaezaze",
                    brand: {name: "Marque"},
                    tags: [{name: "Fleurs"}],
                    pictures: [
                        {
                            name: "Fleur",
                            description: "Belle fleur",
                            url: "https://www.aquaportail.com/pictures1106/anemone-clown_1307889811-fleur.jpg",
                        }],
                },
                {
                    name: "Bouquet",
                    description: "Bouquet",
                    brand: {name: "Marque"},
                    pictures: [
                        {
                            name: "Bouquet",
                            description: "Beau bouquet",
                            url: "https://fyf.tac-cdn.net/images/products/large/TEV55-6.jpg",
                        }],
                }],
        };
        store = await StoreEntity.saveInDb(user1, fields4);
        if (!store) {
            throw new Error("Setup: Can't create new store");
        }
        await Utils.sleep(1000);
    });

    /**
     * Test POST /offers
     */
    describe("POST /offers", () => {
        const fields1 = {
            name: "Test1",
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 1,
            storeId: 1,
        };
        it("it should return 201 (Store Owner)", (done) => {
            fields1.productId = store.products[0].id;
            fields1.storeId = store.id;
            agent1
                .post("/offers")
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(201);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.should.have.property("name", fields1.name);
                        res.body.should.have.property("description", fields1.description);
                        res.body.should.have.property("discount", fields1.discount);
                        res.body.should.have.property("product");
                        res.body.should.have.property("startTime");
                        res.body.should.have.property("expiryTime");

                        // Test DB record
                        offer1 = await OfferEntity.findOne(res.body.id);
                        if (!offer1) {
                            throw new Error("DB: Expected record to exist in DB ");
                        }
                        if (offer1.name !== fields1.name) {
                            throw new Error("DB: Expected offer name to be" + fields1.name);
                        }
                        if (offer1.description !== fields1.description) {
                            throw new Error("DB: Expected offer description to be" + fields1.description);
                        }
                        if (offer1.discount !== fields1.discount) {
                            throw new Error("DB: Expected offer discount to be" + fields1.discount);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /offers] %s", e.message);
                        done(e);
                    }
                });
        });

        const fields2 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 60,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 1,
            storeId: 1,
        };
        it("it should return 201 (Admin)", (done) => {
            fields2.productId = store.products[0].id;
            fields2.storeId = store.id;
            agent3
                .post("/offers")
                .send(fields2)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(201);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.should.have.property("name", fields2.name);
                        res.body.should.have.property("description", fields2.description);
                        res.body.should.have.property("discount", fields2.discount);
                        res.body.should.have.property("product");
                        res.body.should.have.property("startTime");
                        res.body.should.have.property("expiryTime");

                        // Test DB record
                        offer2 = await OfferEntity.findOne(res.body.id);
                        if (!offer2) {
                            throw new Error("DB: Expected record to exist in DB ");
                        }
                        if (offer2.name !== fields2.name) {
                            throw new Error("DB: Expected offer name to be" + fields2.name);
                        }
                        if (offer2.description !== fields2.description) {
                            throw new Error("DB: Expected offer description to be" + fields2.description);
                        }
                        if (offer2.discount !== fields2.discount) {
                            throw new Error("DB: Expected offer discount to be" + fields2.discount);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /offers] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 403 (User)", (done) => {
            fields1.name = "Test 403";
            agent2
                .post("/offers")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .post("/offers")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .post("/offers")
                .set("Cookie", Constants.SESSION_NAME + "=WrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 400 (offer with same name already exist)", (done) => {
            agent1
                .post("/offers")
                .send(fields2)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields3 = {
            name: randomstring.generate(300),
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong name)", (done) => {
            agent1
                .post("/offers")
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields4 = {
            name: "",
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty name)", (done) => {
            agent1
                .post("/offers")
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
        const fields5 = {description: "Test d'une offre", discount: 70,
            startTime: "2019-03-18T13:58:30.000Z", productId: 2, storeId: 1};
        it("it should return 400 (no name)", (done) => {
            agent1
                .post("/offers")
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
        const fields6 = {
            name: null,
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null name)", (done) => {
            agent1
                .post("/offers")
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields7 = {
            name: "Test2",
            description: randomstring.generate(3000),
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong description)", (done) => {
            agent1
                .post("/offers")
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields8 = {
            name: "Test2",
            description: "",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty description)", (done) => {
            agent1
                .post("/offers")
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields9 = {
            name: "Test2",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (no description)", (done) => {
            agent1
                .post("/offers")
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
        const fields10 = {
            name: "Test2",
            description: null,
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null description)", (done) => {
            agent1
                .post("/offers")
                .send(fields10)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields11 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 420,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong discount)", (done) => {
            agent1
                .post("/offers")
                .send(fields11)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields12 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: "",
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty discount)", (done) => {
            agent1
                .post("/offers")
                .send(fields12)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields13 = {
            name: "Test2",
            description: "Test d'une offre",
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (no discount)", (done) => {
            agent1
                .post("/offers")
                .send(fields13)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields14 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 0,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null discount)", (done) => {
            agent1
                .post("/offers")
                .send(fields14)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields15 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: randomstring.generate(200),
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong start time)", (done) => {
            agent1
                .post("/offers")
                .send(fields15)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields16 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: "",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty start time)", (done) => {
            agent1
                .post("/offers")
                .send(fields16)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields17 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: null,
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null start time)", (done) => {
            agent1
                .post("/offers")
                .send(fields17)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields18 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (no start time)", (done) => {
            agent1
                .post("/offers")
                .send(fields18)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields19 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 404,
            storeId: 1,
        };
        it("it should return 404 (wrong product id)", (done) => {
            agent1
                .post("/offers")
                .send(fields19)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields20 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: "2019-03-18T13:58:30.000Z",
            storeId: 1,
        };
        it("it should return 400 (no product id)", (done) => {
            agent1
                .post("/offers")
                .send(fields20)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields21 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 404,
        };
        it("it should return 400 (wrong  store id)", (done) => {
            agent1
                .post("/offers")
                .send(fields21)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields22 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
        };
        it("it should return 400 (no store id)", (done) => {
            agent1
                .post("/offers")
                .send(fields22)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /offers
     */
    describe("GET /offers", () => {
        it("it should return 200 with offers info in array", (done) => {
            chai.request(app)
                .get("/offers")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.be.a("Array");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test GET /offers/:offerId
     */
    describe("GET /offers/:offerId", () => {
        const fields1 = {
            name: "Test1",
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 1,
            storeId: 1,
        };
        it("it should return 200", (done) => {
            chai.request(app)
                .get("/offers/" + offer1.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.body.should.have.property("id");
                    res.body.should.have.property("name", fields1.name);
                    res.body.should.have.property("description", fields1.description);
                    res.body.should.have.property("discount", fields1.discount);
                    res.body.should.have.property("product");
                    res.body.should.have.property("startTime");
                    res.body.should.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (invalid offer id)", (done) => {
            chai.request(app)
                .get("/offers/404")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 400 (invalid offer id - string)", (done) => {
            chai.request(app)
                .get("/offers/UnitTest")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test PUT /offers/:offerId
     */
    describe("PUT /offers/:offerId", () => {
        const fields1 = {
            name: "Yolo",
            description: "Test d'une offre edit",
            discount: 75,
            startTime: "2019-03-18T13:58:30.000Z",
        };
        it("it should return 200 (Store Owner)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields1)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(200);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.should.have.property("name", fields1.name);
                        res.body.should.have.property("description", fields1.description);
                        res.body.should.have.property("discount", fields1.discount);
                        res.body.should.have.property("product");
                        res.body.should.have.property("startTime");
                        res.body.should.have.property("expiryTime");

                        // Test DB record
                        offer1 = await OfferEntity.findOne(res.body.id);
                        if (!offer1) {
                            throw new Error("DB: Expected record to exist in DB ");
                        }
                        if (offer1.name !== fields1.name) {
                            throw new Error("DB: Expected offer name to be" + fields1.name);
                        }
                        if (offer1.description !== fields1.description) {
                            throw new Error("DB: Expected offer description to be" + fields1.description);
                        }
                        if (offer1.discount !== fields1.discount) {
                            throw new Error("DB: Expected offer discount to be" + fields1.discount);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /offers] %s", e.message);
                        done(e);
                    }
                });
        });

        const fields2 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 60,
            startTime: "2019-03-18T13:58:30.000Z",
        };
        it("it should return 201 (Admin)", (done) => {
            agent3
                .put("/offers/" + offer2.id)
                .send(fields2)
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(200);
                        expect(res).to.be.json;
                        res.body.should.have.property("id");
                        res.body.should.have.property("name", fields2.name);
                        res.body.should.have.property("description", fields2.description);
                        res.body.should.have.property("discount", fields2.discount);
                        res.body.should.have.property("product");
                        res.body.should.have.property("startTime");
                        res.body.should.have.property("expiryTime");

                        // Test DB record
                        offer2 = await OfferEntity.findOne(res.body.id);
                        if (!offer2) {
                            throw new Error("DB: Expected record to exist in DB ");
                        }
                        if (offer2.name !== fields2.name) {
                            throw new Error("DB: Expected offer name to be" + fields2.name);
                        }
                        if (offer2.description !== fields2.description) {
                            throw new Error("DB: Expected offer description to be" + fields2.description);
                        }
                        if (offer2.discount !== fields2.discount) {
                            throw new Error("DB: Expected offer discount to be" + fields2.discount);
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][POST /offers] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 403 (User)", (done) => {
            agent2
                .put("/offers/" + offer1.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .put("/offers/" + offer1.id)
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .put("/offers/" + offer1.id)
                .set("Cookie", Constants.SESSION_NAME + "=WrongCookie;")
                .send(fields1)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields3 = {
            name: randomstring.generate(300),
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong name)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields3)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields4 = {
            name: "",
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty name)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields4)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
        const fields5 = {
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (no name)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields5)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
        const fields6 = {
            name: null,
            description: "Test d'une offre",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null name)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields6)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields7 = {
            name: "Test2",
            description: randomstring.generate(3000),
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong description)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields7)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields8 = {
            name: "Test2",
            description: "",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty description)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields8)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields9 = {
            name: "Test2",
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (no description)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields9)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields10 = {
            name: "Test2",
            description: null,
            discount: 70,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null description)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields10)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields11 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 420,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong discount)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields11)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields12 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: "",
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty discount)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields12)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields13 = {
            name: "Test2",
            description: "Test d'une offre",
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (no discount)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields13)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields14 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 0,
            startTime: "2019-03-18T13:58:30.000Z",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null discount)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields14)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields15 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: randomstring.generate(200),
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (wrong start time)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields15)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields16 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: "",
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (empty start time)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields16)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields17 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            startTime: null,
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (null start time)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields17)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        const fields18 = {
            name: "Test2",
            description: "Test d'une offre",
            discount: 75,
            productId: 2,
            storeId: 1,
        };
        it("it should return 400 (no start time)", (done) => {
            agent1
                .put("/offers/" + offer1.id)
                .send(fields18)
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(400);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });
    });

    /**
     * Test DELETE /stores/:offerId
     */
    describe("DELETE /stores/:offerId", () => {
        it("it should return 401 (No authentication cookie)", (done) => {
            chai.request(app)
                .delete("/offers/" + offer1.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 401 (Wrong authentication cookie)", (done) => {
            chai.request(app)
                .delete("/offers/" + offer1.id)
                .set("Cookie", Constants.SESSION_NAME + "=WrongCookie;")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(401);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 404 (offer not found)", (done) => {
            agent1
                .delete("/offers/404")
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(404);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 403 (User)", (done) => {
            agent2
                .delete("/offers/" + offer1.id)
                .send()
                .end((err, res) => {
                    // Test Api response
                    expect(res).to.have.status(403);
                    expect(res).to.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.a("string");
                    res.body.should.not.have.property("id");
                    res.body.should.not.have.property("name");
                    res.body.should.not.have.property("description");
                    res.body.should.not.have.property("discount");
                    res.body.should.not.have.property("product");
                    res.body.should.not.have.property("startTime");
                    res.body.should.not.have.property("expiryTime");
                    err ? done(err) : done();
                });
        });

        it("it should return 204 (Store Owner)", (done) => {
            agent1
                .delete("/offers/" + offer2.id)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const offerTmp: OfferEntity = await getRepository(OfferEntity).findOne(offer2.id);
                        if (offerTmp) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][DELETE /offer/:id] %s", e.message);
                        done(e);
                    }
                });
        });

        it("it should return 204 (Admin)", (done) => {
            agent1
                .delete("/offers/" + offer1.id)
                .send()
                .end(async (err, res) => {
                    try {
                        // Test Api response
                        expect(res).to.have.status(204);
                        expect(res.body).to.be.empty;

                        // Test DB records
                        const offerTmp: OfferEntity = await getRepository(OfferEntity).findOne(offer1.id);
                        if (offerTmp) {
                            throw new Error("DB: Expected record to not exist in DB");
                        }
                        err ? done(err) : done();
                    } catch (e) {
                        logger.debug("[Test][DELETE /offer/:id] %s", e.message);
                        done(e);
                    }
                });
        });
    });

    after(async () => {
        await store.address.remove();
        for (const product of store.products) {
            for (const picture of product.pictures) {
                await picture.remove();
            }
            if (product.tags) {
                for (const tag of product.tags) {
                    await Elasticsearch.delete(Constants.ELASTIC_TAGS_INDEX, Constants.ELASTIC_TAGS_TYPE, tag.id);
                    await tag.remove();
                }
            }
            if (product.brand) {
                await Elasticsearch.delete(Constants.ELASTIC_BRANDS_INDEX,
                    Constants.ELASTIC_BRANDS_TYPE, product.brand.id);
                await product.brand.remove();
            }
            await Elasticsearch.delete(Constants.ELASTIC_PRODUCTS_INDEX, Constants.ELASTIC_PRODUCTS_TYPE, product.id);
            await product.remove();
        }
        for (const category of store.categories) {
            await Elasticsearch.delete(Constants.ELASTIC_CATEGORIES_INDEX,
                Constants.ELASTIC_CATEGORIES_TYPE, category.id);
            await category.remove();
        }
        for (const hour of store.hours) {
            await hour.remove();
        }
        for (const picture of store.pictures) {
            await picture.remove();
        }
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user1.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user2.id);
        await Elasticsearch.delete(Constants.ELASTIC_USERS_INDEX, Constants.ELASTIC_USERS_TYPE, user3.id);
        await Elasticsearch.delete(Constants.ELASTIC_STORES_INDEX, Constants.ELASTIC_STORES_TYPE, store.id);
        await user1.remove();
        await user2.remove();
        await user3.remove();
        await store.remove();
        agent1.close();
        agent2.close();
        agent3.close();
    });
});
