-- MySQL Script generated by MySQL Workbench
-- mer. 24 avril 2019 14:21:32 CST
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema shops_prod
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `shops_prod` ;

-- -----------------------------------------------------
-- Schema shops_prod
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `shops_prod` ;
USE `shops_prod` ;

-- -----------------------------------------------------
-- Table `shops_prod`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`users` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(16) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `bio` TEXT NULL DEFAULT NULL,
  `parentEmail` VARCHAR(255) NULL DEFAULT NULL,
  `banner` VARCHAR(255) NULL DEFAULT NULL,
  `picture` VARCHAR(255) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` ENUM('Custom', 'User', 'Maintainer', 'Moderator', 'Admin') NOT NULL DEFAULT 'User',
  `active` TINYINT NOT NULL DEFAULT 0,
  `userHash` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`addresses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`addresses` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`addresses` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `street` VARCHAR(255) NOT NULL,
  `streetNbr` INT(4) UNSIGNED NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `zipCode` VARCHAR(255) NOT NULL,
  `country` VARCHAR(255) NOT NULL,
  `lon` FLOAT NULL DEFAULT NULL,
  `lat` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`stores`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`stores` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`stores` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `siret` VARCHAR(14) DEFAULT NULL,
  `avgMark` FLOAT NOT NULL DEFAULT 0,
  `ownerId` INT UNSIGNED NULL,
  `addressId` INT UNSIGNED NOT NULL,
  `nbProducts` INT UNSIGNED NULL DEFAULT 0,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `user_idx` (`ownerId` ASC),
  INDEX `fk_Store_addresses1_idx` (`addressId` ASC),
  CONSTRAINT `user`
    FOREIGN KEY (`ownerId`)
    REFERENCES `shops_prod`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Store_addresses1`
    FOREIGN KEY (`addressId`)
    REFERENCES `shops_prod`.`addresses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`storesComments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`storesComments` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`storesComments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `comment` VARCHAR(255) NOT NULL,
  `mark` INT(1) UNSIGNED NOT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `authorId` INT UNSIGNED NULL,
  `storeId` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_comments_1_idx` (`authorId` ASC),
  INDEX `fk_storesComments_stores1_idx` (`storeId` ASC),
  CONSTRAINT `fk_comments_author`
    FOREIGN KEY (`authorId`)
    REFERENCES `shops_prod`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_storesComments_stores1`
    FOREIGN KEY (`storeId`)
    REFERENCES `shops_prod`.`stores` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`categories` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`categories` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`storesCategories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`storesCategories` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`storesCategories` (
  `categoryId` INT UNSIGNED NOT NULL,
  `storeId` INT UNSIGNED NOT NULL,
  INDEX `fk_stores_categories_store1_idx` (`storeId` ASC),
  INDEX `fk_stores_categories_categories1_idx` (`categoryId` ASC),
  CONSTRAINT `fk_stores_categories_store1`
    FOREIGN KEY (`storeId`)
    REFERENCES `shops_prod`.`stores` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_stores_categories_categories1`
    FOREIGN KEY (`categoryId`)
    REFERENCES `shops_prod`.`categories` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`brands`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`brands` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`brands` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`products` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`products` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `avgMark` FLOAT NOT NULL DEFAULT 0,
  `brandId` INT UNSIGNED NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_products_brands1_idx` (`brandId` ASC),
  CONSTRAINT `fk_products_brands1`
    FOREIGN KEY (`brandId`)
    REFERENCES `shops_prod`.`brands` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`storesProducts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`storesProducts` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`storesProducts` (
  `storeId` INT UNSIGNED NOT NULL,
  `productId` INT UNSIGNED NOT NULL,
  INDEX `fk_stores_products_products1_idx` (`storeId` ASC),
  INDEX `fk_stores_products_store1_idx` (`productId` ASC),
  CONSTRAINT `fk_stores_products_products1`
    FOREIGN KEY (`storeId`)
    REFERENCES `shops_prod`.`stores` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_stores_products_store1`
    FOREIGN KEY (`productId`)
    REFERENCES `shops_prod`.`products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`pictures`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`pictures` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`pictures` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `url` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`hours`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`hours` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`hours` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `day` ENUM('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday') NOT NULL,
  `timeOpen` TIME NOT NULL,
  `timeClosed` TIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`storesHours`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`storesHours` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`storesHours` (
  `hourId` INT UNSIGNED NOT NULL,
  `storeId` INT UNSIGNED NOT NULL,
  INDEX `fk_stores_schedules_schedules1_idx` (`hourId` ASC),
  INDEX `fk_stores_schedules_store1_idx` (`storeId` ASC),
  CONSTRAINT `fk_stores_schedules_schedules1`
    FOREIGN KEY (`hourId`)
    REFERENCES `shops_prod`.`hours` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_stores_schedules_store1`
    FOREIGN KEY (`storeId`)
    REFERENCES `shops_prod`.`stores` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`offers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`offers` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`offers` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `discount` INT(3) NOT NULL,
  `description` TEXT NOT NULL,
  `productId` INT UNSIGNED NULL,
  `storeId` INT UNSIGNED NOT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `startTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiryTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_offers_products1_idx` (`productId` ASC),
  INDEX `fk_offers_store1_idx` (`storeId` ASC),
  CONSTRAINT `fk_offers_products1`
    FOREIGN KEY (`productId`)
    REFERENCES `shops_prod`.`products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_offers_store1`
    FOREIGN KEY (`storeId`)
    REFERENCES `shops_prod`.`stores` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`tags` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`tags` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`productsTags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`productsTags` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`productsTags` (
  `productId` INT UNSIGNED NOT NULL,
  `tagId` INT UNSIGNED NOT NULL,
  INDEX `fk_productsTags_products1_idx` (`productId` ASC),
  INDEX `fk_productsTags_tags1_idx` (`tagId` ASC),
  CONSTRAINT `fk_productsTags_products1`
    FOREIGN KEY (`productId`)
    REFERENCES `shops_prod`.`products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_productsTags_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `shops_prod`.`tags` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`productsPictures`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`productsPictures` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`productsPictures` (
  `productId` INT UNSIGNED NOT NULL,
  `pictureId` INT UNSIGNED NOT NULL,
  INDEX `fk_productsPictures_pictures1_idx` (`pictureId` ASC),
  INDEX `fk_productsPictures_products1_idx` (`productId` ASC),
  CONSTRAINT `fk_productsPictures_pictures1`
    FOREIGN KEY (`pictureId`)
    REFERENCES `shops_prod`.`pictures` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_productsPictures_products1`
    FOREIGN KEY (`productId`)
    REFERENCES `shops_prod`.`products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`storesPictures`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`storesPictures` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`storesPictures` (
  `storeId` INT UNSIGNED NOT NULL,
  `pictureId` INT UNSIGNED NOT NULL,
  INDEX `fk_storesPictures_pictures1_idx` (`pictureId` ASC),
  INDEX `fk_storesPictures_stores1_idx` (`storeId` ASC),
  CONSTRAINT `fk_storesPictures_pictures1`
    FOREIGN KEY (`pictureId`)
    REFERENCES `shops_prod`.`pictures` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_storesPictures_stores1`
    FOREIGN KEY (`storeId`)
    REFERENCES `shops_prod`.`stores` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`permissions` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`permissions` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `permission` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`usersPermissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`usersPermissions` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`usersPermissions` (
  `userId` INT UNSIGNED NOT NULL,
  `permissionId` INT UNSIGNED NOT NULL,
  INDEX `fk_usersPermissions_users1_idx` (`userId` ASC),
  INDEX `fk_usersPermissions_permissions1_idx` (`permissionId` ASC),
  CONSTRAINT `fk_usersPermissions_users1`
    FOREIGN KEY (`userId`)
    REFERENCES `shops_prod`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_usersPermissions_permissions1`
    FOREIGN KEY (`permissionId`)
    REFERENCES `shops_prod`.`permissions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shops_prod`.`productsComments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shops_prod`.`productsComments` ;

CREATE TABLE IF NOT EXISTS `shops_prod`.`productsComments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `comment` VARCHAR(255) NOT NULL,
  `mark` INT(1) UNSIGNED NOT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `authorId` INT UNSIGNED NULL,
  `productId` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_comments_1_idx` (`authorId` ASC),
  INDEX `fk_productsComments_products1_idx` (`productId` ASC),
  CONSTRAINT `fk_comments_author0`
    FOREIGN KEY (`authorId`)
    REFERENCES `shops_prod`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_productsComments_products1`
    FOREIGN KEY (`productId`)
    REFERENCES `shops_prod`.`products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

GRANT ALL ON `shops_prod`.* TO 'shops_admin';
GRANT SELECT ON TABLE `shops_prod`.* TO 'shops';
GRANT SELECT, INSERT, TRIGGER ON TABLE `shops_prod`.* TO 'shops';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE `shops_prod`.* TO 'shops';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
